// Initiate a global map object
var map = {};


function initialize(mapdata) {

    // Assign the map canvas element to a variable
    var mapCanvas = document.getElementById('google_map');

    // Set the basic map data
    $(mapCanvas).gmap({
        center: new google.maps.LatLng(mapdata.lat, mapdata.lng),
        zoom: mapdata.zoom,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: true, mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.TOP_LEFT
            },
        navigationControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
        },
        callback: function (map) {

            // Create parameters
            var params = {
                language: Global.language
            };

            // Get the markers data
            $.getJSON('/umbraco/Surface/MapsSurface/GetAreaMarkers', params, function (result) {

                // Initiate a counter
                //var counter = 0;

                $.each(result.markers, function (i, marker) {

                    // Comma-separated list of node id:s representing the area's activities
                    // marker.AreaActivities;

                    // Drop markers consecutively by using window.setTimeout
                    //window.setTimeout(function () {

                        $(mapCanvas).gmap('addMarker', {
                            'position': new google.maps.LatLng(marker.Latitude, marker.Longitude),
                            'icon': new google.maps.MarkerImage(marker.Icon),
                            'title': marker.Title,
                            'tag': marker.Tag,
                            //'animation': google.maps.Animation.DROP
                        })
                        //.click(function () {
                        //});

                        // Add kml layer to display area borders
                        var borderLayer = new google.maps.KmlLayer({
                            url: 'http://www.skanskalandskap.se/mapdata/borders/' + marker.UrlName + '.kml',
                            preserveViewport: true,
                            suppressInfoWindows: true,
                            clickable: false
                        });
                        borderLayer.setMap(map);
                    //}, 200 * counter);

                    // Enumerate counter
                    //counter += 1;
                });
            });
        }
    });
}



(function ($) {


    // Make sure there is an element for the standard map
    if ($('#google_map').length > 0) {

        /* Create the default map and its markers due to Content type. */
        $.getJSON('/umbraco/Surface/MapsSurface/GetBasicData', 'nodeId=' + Global.currentNodeId, function (data) {
            // Assign the fetched map and page data to variables
            var coordLat = data.map.Latitude;
            var coordLng = data.map.Longitude;
            var zoom = data.map.Zoom;
            var urlName = data.map.UrlName;

            // Create map after page was loaded
            google.maps.event.addDomListener(window, 'load', initialize({ 'lat': coordLat, 'lng': coordLng, 'zoom': zoom }))
        });
    }



    // Attach click event to the Display All button
    $(document).on('click', '#display-all-button', function (e) {

        e.stopPropagation();

    });


})(jQuery);
