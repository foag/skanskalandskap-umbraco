﻿using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Core.Models;
using Umbraco.Web;


namespace SkanskaLandskap15.Classes.Mappers
{

    internal static class NavigationItemMappers
    {
        /// <summary>
        /// Maps IPublishedContent to a simple navigation item.
        /// </summary>
        /// <typeparam name="TModel">Type of model, to map to. Must be or inherit from NavigationItemModel</typeparam>
        /// <param name="model">Model to map</param>
        /// <param name="content">Content to map from.</param>
        /// <param name="currentPage">The current page, used to set active flag</param>
        /// <param name="areaPart">Flag indicating an area page (landing or detailed). Optional.</param>
        /// <returns>The mapped model</returns>
        /// <remarks>
        /// Note that this mapper does not map children, nor does it take a list of items. It only maps a single item. 
        /// Children must be selected in the controller as the query code can vary.
        /// </remarks>
        internal static TModel Map<TModel>(TModel model, IPublishedContent content, IPublishedContent currentPage, bool areaPart = false)
            where TModel : NavigationItemModel
        {
            model.Name = content.Name;
            if (string.IsNullOrWhiteSpace(model.Name))
            {
                model.Name = content.GetPropertyValue<string>(MyConstants.TitlePropertyAlias);
            }

            model.Url = content.Url;

            // Add node id for all landing pages except areas (to be used when generating the sub menu)
            //if(content.DocumentTypeAlias != MyConstants.AreasLandingPageDocTypeAlias)
            //{ 
            //    model.NodeId = content.Id;
            //}
            model.NodeId = content.Id;

            model.Active = currentPage.IsDescendantOrSelf(content);

            // Set a sub page flag to be able to list the sub elements differently from the top elements 
            model.SubPage = false;
            if (content.Level >= 3)
            {
                model.SubPage = true;
            }

            //// Get area's status value
            //if(areaPart == true)
            //{
            //    model.Status = content.GetPropertyValue<string>(MyConstants.AreaStatusPropertyAlias);
            //}

            return model;
        }
    }
}