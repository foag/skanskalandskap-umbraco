﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using System.Web.UI;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Web;
using Umbraco.Web.Mvc;




namespace SkanskaLandskap15.Controllers.SurfaceControllers
{

    public class MapInteractivitySurfaceController : SurfaceController
    {

        // Classes to support dynamically created properties (Duration) for the OutputCache Attribute.
        public static class CacheConfig
        {
            public static int Duration = MasterController.GetOutputCacheDuration("services");
        }

        public class MyOutputCacheAttribute : OutputCacheAttribute
        {
            public MyOutputCacheAttribute()
            {
                this.Duration = CacheConfig.Duration;
            }
        }

          
        ///<summary>
        /// Fetches a list of all service types for an area page, including the total number of each.
        /// Typically for the area page's interactive map.
        /// </summary>      
        /// <param name="language">Indicates current language.</param> 
        /// <param name="nodeId">Current area's node id.</param>
        /// <returns>A MapInteractivity object.</returns>  
        [HttpGet]
        [MyOutputCache(VaryByParam = "language;nodeId", Location = OutputCacheLocation.Server)]
        //[OutputCache(Duration = 86400, VaryByParam = "nodeId;language", Location = OutputCacheLocation.Server)]
        public JsonResult GetServiceTypesJson(string language, int nodeId)
        {
            // Initiate return model
            MapInteractivityModel model = new MapInteractivityModel();

            // Set content type
            string contentType = MyConstants.AreaPageDocTypeAlias;

            // The culture of an Ajax request thread is neutral and therefore we need to set it 
            // explicitly to be able to get the correct Umbraco context.
            if (Request.IsAjaxRequest())
            {
                UmbracoHelperExtensions.EnsureCulture(Thread.CurrentThread, language);
            }

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get id of the service types parent node
            int parentNodeId = helper.TypedContentSingleAtXPath("//" + MyConstants.ServiceTypesDocTypeAlias).Id;

            // Get content based on node id
            var content = helper.TypedContent(parentNodeId);

            // Initiate list
            List<ServiceTypeModel> serviceTypesList = null;

            // Get the (published) service types due to language
            switch (language)
            {
                // Swedish - use node's name as the services title
                case "SV":

                    serviceTypesList = content
                       .Children
                       .Where(c => c.DocumentTypeAlias == MyConstants.ServiceTypeDocTypeAlias)
                       .Select(obj => new ServiceTypeModel()
                       {
                           NodeId = obj.Id,
                           UrlName = obj.UrlName,
                           NodeName = obj.Name,
                           Name = obj.Name,
                           Title = obj.Name,
                           IconPath = helper.Media(obj.GetPropertyValue(MyConstants.ServiceTypeIconPropertyAlias)).Url
                       }).ToList();
                    break;

                // English - use the specific English term for title
                case "EN":

                    serviceTypesList = content
                     .Children
                     .Where(c => c.DocumentTypeAlias == MyConstants.ServiceTypeDocTypeAlias)
                     .Select(obj => new ServiceTypeModel()
                     {
                         NodeId = obj.Id,
                         UrlName = obj.UrlName,
                         NodeName = obj.Name,
                         Name = obj.GetPropertyValue<string>(MyConstants.TermEnglishPropertyAlias),
                         Title = obj.GetPropertyValue<string>(MyConstants.TermEnglishPropertyAlias),
                         IconPath = helper.Media(obj.GetPropertyValue(MyConstants.ServiceTypeIconPropertyAlias)).Url
                     }).ToList();
                    break;
            }


            /* GET SERVICE TYPE COUNTS FOR CURRENT AREA */

            // Make sure content type is an area page
            if (contentType == MyConstants.AreaPageDocTypeAlias)
            {
                
                // Get the area page's content
                var contentArea = helper.TypedContent(nodeId);

                // Iterate over the service types list counting current area's services
                serviceTypesList.ForEach(itemService =>
                {

                    // Set activity counts to zero
                    itemService.Counts = 0;

                    // Get current page's service nodes of a specific type 
                    IEnumerable<ServiceTypeModel> listServices = contentArea
                        .Children
                        .Where(c => c.DocumentTypeAlias == MyConstants.ServiceDocTypeAlias && c.GetPropertyValue<string>(MyConstants.ServiceTypeDocTypeAlias) == itemService.NodeName)
                        .Select(obj => new ServiceTypeModel()
                        {
                            NodeId = obj.Id,
                        }).ToList();

                    // Assign the number of nodes
                    itemService.Counts = listServices.Count();
                });
            }

            // Add list to return model
            model.ServiceTypesCollection = serviceTypesList;

            // Set the part of the site where the interactive map should be displayed
            model.ContentType = contentType;

            // Set the map element id 
            model.MapType = "osm_map";

            // Add busstop terms
            model.SearchTerm = Umbraco.GetDictionaryValue("Find");
            model.GetHereFromTerm = Umbraco.GetDictionaryValue("GetHereFrom");

            // Return model
            return Json(new { servicetypes = model }, JsonRequestBehavior.AllowGet);
        }


      
   
        ///<summary>
        /// Fetches a list of all service types for an area page, including the total number of each.
        /// Typically for the pages's information section.
        /// </summary>      
        /// <param name="language">Indicates current language.</param> 
        /// <param name="nodeId">Current area's node id.</param>
        /// <returns>A populated MapInteractivity object.</returns>      
        public MapInteractivityModel GetServiceTypes(string language, int nodeId)
        {

            // Initiate return model
            MapInteractivityModel model = new MapInteractivityModel();

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get the page's content based on node id
            var contentArea = helper.TypedContent(nodeId);

            // Make sure node id is for an area page
            if (contentArea.DocumentTypeAlias == MyConstants.AreaPageDocTypeAlias)
            {

                // Get id of the service types parent node
                int parentNodeId = helper.TypedContentSingleAtXPath("//" + MyConstants.ServiceTypesDocTypeAlias).Id;

                // Get content based on node id
                var content = helper.TypedContent(parentNodeId);

                // Initiate list
                List<ServiceTypeModel> serviceTypesList = null;

                // Get the (published) service types due to language
                switch (language)
                {
                    // Swedish - use node's name as the service's title
                    case "SV":

                        serviceTypesList = content
                           .Children
                           .Where(c => c.DocumentTypeAlias == MyConstants.ServiceTypeDocTypeAlias)
                           .Select(obj => new ServiceTypeModel()
                           {
                               NodeId = obj.Id,
                               UrlName = obj.UrlName,
                               Name = obj.Name,
                               Title = obj.Name,
                               IconPath = helper.Media(obj.GetPropertyValue(MyConstants.ServiceTypeIconPropertyAlias)).Url
                           }).ToList();
                        break;

                    // English - use the specific English term for title
                    case "EN":

                        serviceTypesList = content
                         .Children
                         .Where(c => c.DocumentTypeAlias == MyConstants.ServiceTypeDocTypeAlias)
                         .Select(obj => new ServiceTypeModel()
                         {
                             NodeId = obj.Id,
                             UrlName = obj.UrlName,
                             Name = obj.Name,
                             Title = obj.GetPropertyValue<string>(MyConstants.TermEnglishPropertyAlias),
                             IconPath = helper.Media(obj.GetPropertyValue(MyConstants.ServiceTypeIconPropertyAlias)).Url
                         }).ToList();
                        break;
                }

                // Iterate over the service types list counting current area's services
                serviceTypesList.ForEach(itemService =>
                {

                    // Set service counts to zero
                    itemService.Counts = 0;

                    // Get current page's service nodes of a specific type 
                    IEnumerable<ServiceTypeModel> listServices = contentArea
                        .Children
                        .Where(c => c.DocumentTypeAlias == MyConstants.ServiceDocTypeAlias && c.GetPropertyValue<string>(MyConstants.ServiceTypeDocTypeAlias) == itemService.Name)
                        .Select(obj => new ServiceTypeModel()
                        {
                            NodeId = obj.Id,
                        }).ToList();

                    // Assign the number of nodes
                    itemService.Counts = listServices.Count();
                });


                // Add list to return model
                model.ServiceTypesCollection = serviceTypesList;
            }

            return model;
        }


        ///<summary>
        /// Fetches a simple list of activity types including Titles and Node id:s.
        /// </summary>       
        /// <param name="language">Current language.</param>
        /// <returns>A list of activity types in Json format.</returns>      
        [HttpGet]
        public JsonResult GetActivityTypesList(string language)
        {

            // Initiate return list
            List<ActivityTypeModel> activityTypesList = null;

            // The culture of an Ajax request thread is neutral and therefore we need to set it 
            // explicitly to be able to get the correct Umbraco context.
            if (Request.IsAjaxRequest())
            {
                UmbracoHelperExtensions.EnsureCulture(Thread.CurrentThread, language);
            }

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get id of the Activity types folder node
            int nodeId = helper.TypedContentSingleAtXPath("//" + MyConstants.ActivityTypesDocTypeAlias).Id;

            // Get all content based on the node id
            var content = helper.TypedContent(nodeId);

            // Get the (published) activity types due to language
            switch (language)
            {
                // Swedish - use node's name as the activity's name
                case "SV":

                    activityTypesList = content
                       .Children
                       .Where(c => c.DocumentTypeAlias == MyConstants.ActivityTypeDocTypeAlias)
                       .OrderBy(c => c.Name)
                       .Select(obj => new ActivityTypeModel()
                       {
                           NodeId = obj.Id,
                           Name = obj.Name,
                           IconPath = helper.Media(obj.GetPropertyValue(MyConstants.ActivityTypeIconPropertyAlias)).Url
                       }).ToList();
                    break;

                // English - get the specific English term
                case "EN":

                    activityTypesList = content
                     .Children
                     .Where(c => c.DocumentTypeAlias == MyConstants.ActivityTypeDocTypeAlias)
                     .OrderBy(c => c.GetPropertyValue<string>(MyConstants.TermEnglishPropertyAlias))
                     .Select(obj => new ActivityTypeModel()
                     {
                         NodeId = obj.Id,
                         Name = obj.GetPropertyValue<string>(MyConstants.TermEnglishPropertyAlias),
                         IconPath = helper.Media(obj.GetPropertyValue(MyConstants.ActivityTypeIconPropertyAlias)).Url
                     }).ToList();
                    break;
            }

            return Json(new { activitytypes = activityTypesList }, JsonRequestBehavior.AllowGet);
        }


    }
}