﻿using System.Collections.Generic;

namespace SkanskaLandskap15.Models.Shared
{
    public class MediaFilesModel 
    {   
        public List<MediaFileModel> MediaFilesCollection { get; set; }
    }
}