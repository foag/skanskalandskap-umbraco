﻿using System;
using System.Collections.Generic;


namespace SkanskaLandskap15.Models.Shared
{
    public class SearchResultItemModel 
    {
        public int NodeId { get; set; }
        public string NodeName { get; set; }       
        public DateTime UpdateDate { get; set; }
        public string WriterName { get; set; }
        public string DocTypeAlias { get; set; }

        public string Url { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Intro { get; set; }
        public string PreviewText { get; set; }   
  
        public List<SearchResultItemModel> TestList { get; set; }
    }
}