﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Examine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using umbraco.NodeFactory;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Publishing;
using Umbraco.Core.Services;



namespace SkanskaLandskap15.Classes.Utilities
{

    ///<summary>
    /// In the ApplicationStarted method below, Umbraco property fields - contained in Nested Content datatypes - are made searchable. 
    /// All you manually needs to do is to add the Nested Content property alias to ExamineIndex.config and to the searchable properties 
    /// list in SearchSurfaceController.
    ///</summary>
    public class RegisterEvents : ApplicationEventHandler
    {
   
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            base.ApplicationStarted(umbracoApplication, applicationContext);

            // Listen for when content is published          
            ContentService.Published += ContentService_Published;

            // Listen for when content iss unpublished
            ContentService.UnPublished += ContentService_UnPublished;

            // Listen for when content is saved
            ContentService.Saved += ContentService_Saved;

            // Listen for when content is deleted
            ContentService.Deleted += ContentService_Deleted;
                   

            // Make Swedish content, included in Nested Content data types, searchable
            ExamineManager.Instance.IndexProviderCollection["SiteContentIndexer"]
                .GatheringNodeData += (sender, e) =>
                {
                    // Extract JSON properties
                    var fieldKeys = e.Fields.Keys.ToArray();
                    foreach (var key in fieldKeys)
                    {
                        var value = e.Fields[key];
                        if (value.DetectIsJson())
                        {
                            IndexNestedObject(e.Fields, JsonConvert.DeserializeObject(value), key, "SV", e);
                        }
                    }
                };

            // Make English content, included in Nested Content data types, searchable
            ExamineManager.Instance.IndexProviderCollection["SiteContentEnglishIndexer"]
                .GatheringNodeData += (sender, e) =>
                {
                    // Extract JSON properties
                    var fieldKeys = e.Fields.Keys.ToArray();
                    foreach (var key in fieldKeys)
                    {
                        var value = e.Fields[key];
                        if (value.DetectIsJson())
                        {
                            IndexNestedObject(e.Fields, JsonConvert.DeserializeObject(value), key, "EN", e);
                        }
                    }
                };
        }


        private void ContentService_Published(IPublishingStrategy sender, PublishEventArgs<IContent> e)
        {
            // Initiate service flag
            bool serviceCleared = false;

            // Iterate over the published items
            foreach (var content in e.PublishedEntities)
            {
                // Look for a service hit
                serviceCleared = CheckService(serviceCleared, content);

                // Break out of loop if we found service and cleared cache
                if (serviceCleared)
                {
                    break;
                }
            }

            // Clear submenu cache
            string cacheType = "submenu";
            ClearOutputCache(cacheType);
        }


        private void ContentService_UnPublished(IPublishingStrategy sender, PublishEventArgs<IContent> e)
        {
            // Initiate service flag
            bool serviceCleared = false;

            // Iterate over the (un)published items
            foreach (var content in e.PublishedEntities)
            {
                // Look for a service hit
                serviceCleared = CheckService(serviceCleared, content);

                // Break out of loop if we found service and cleared cache
                if (serviceCleared)
                {
                    break;
                }
            }

            // Clear submenu cache
            string cacheType = "submenu";
            ClearOutputCache(cacheType);
        }


        private void ContentService_Saved(IContentService sender, SaveEventArgs<IContent> e)
        {
            // Initiate service flag
            bool serviceCleared = false;

            // Iterate over the saved items
            foreach (var content in e.SavedEntities)
            {
                // Look for a service hit
                serviceCleared = CheckService(serviceCleared, content);

                // Break out of loop if we found service and cleared cache
                if (serviceCleared)
                {
                    break;
                }
            }

            // Clear submenu cache
            string cacheType = "submenu";
            ClearOutputCache(cacheType);
        }


        private void ContentService_Deleted(IContentService sender, DeleteEventArgs<IContent> e)
        {
            // Initiate service flag
            bool serviceCleared = false;

            // Iterate over the deleted items
            foreach (var content in e.DeletedEntities)
            {
                // Look for a service hit
                serviceCleared = CheckService(serviceCleared, content);

                // Break out of loop if we found service and cleared cache
                if (serviceCleared)
                {
                    break;
                }
            } 
            
            // Clear submenu cache
            string cacheType = "submenu";
            ClearOutputCache(cacheType);
        }


        private bool CheckService(bool cleared, IContent content)
        {
            string cacheType = "services";

            // Look for a Service or ServiceType content type   
            if (content.ContentType.Alias == "Service" || content.ContentType.Alias == "ServiceType")
            {
                //LogHelper.Info(typeof(RegisterEvents), "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Något har hänt med doctype " + content.ContentType.Alias);

                ClearOutputCache(cacheType);
                cleared = true;
            }   
            return cleared;
        }


        public void ClearOutputCache(string cacheType)
        {

            switch (cacheType)
            {
                case "services":

                    // Remove items from outputcache          
                    HttpResponse.RemoveOutputCacheItem("/umbraco/surface/mapssurface/getservicesbyareajson");  /* Fetches services for an area map */
                    HttpResponse.RemoveOutputCacheItem("/umbraco/surface/mapssurface/getbusstopinfo"); /* Fetches info including Resrobot id for a busstop marker's popup window */
                    HttpResponse.RemoveOutputCacheItem("/umbraco/surface/mapinteractivitysurface/getservicetypesjson"); /* Fetches service types including counts by area for an area map */

                    //// Get the url for the action method
                    //var requestContext = new System.Web.Routing.RequestContext(
                    //   new HttpContextWrapper(System.Web.HttpContext.Current),
                    //   new System.Web.Routing.RouteData());
                    //var Url = new UrlHelper(requestContext);
                    //var staleItem = Url.Action("GetServicesByAreaJson", "MapsSurfaceController", new
                    //{
                    //    language = {language},
                    //    nodeId = {NodeId                
                    //});   
                    break;

                case "submenu":

                    // Remove items from outputcache          
                    HttpResponse.RemoveOutputCacheItem("/umbraco/surface/submenusurface/getsubmenujson");  /* Fetches the sub menu for a specific landing page. */
                    break;
            }
        }


        private void IndexNestedObject(Dictionary<string, string> fields, object obj, string prefix, string language, IndexingNodeDataEventArgs e)
        {
                     
            var objType = obj.GetType();
            if (objType == typeof(JObject))
            {
                var jObj = obj as JObject;
                if (jObj != null)
                {




                    bool added = false;


                    foreach (var kvp in jObj)
                    { 

                        var propKey = prefix + "_" + kvp.Key;
                        var valueType = kvp.Value.GetType();
                        if (typeof(JContainer).IsAssignableFrom(valueType))
                        {
                            IndexNestedObject(fields, kvp.Value, propKey, language, e);
                            //IndexNestedObject(fields, kvp.Value, propKey, language, combinedFields, combinedFields2, combinedFields3);
                        }
                        else
                        {
                            fields.Add(propKey, kvp.Value.ToString().StripHtml());

                            if(kvp.Key == "contactPicker")
                            {

                                // Initiate string builders
                                var combinedFields = new StringBuilder();
                                var combinedFields2 = new StringBuilder();
                                var combinedFields3 = new StringBuilder();
                               
  

                                var nodeIds = kvp.Value.ToString().Split(',');

                                // Iterate over the id:s and get the person's name and function
                                foreach (string nodeId in nodeIds)
                                {
                                    var node = new Node(Convert.ToInt32(nodeId));

                                    // Name                                
                                    combinedFields.AppendLine(node.GetProperty(MyConstants.ContactNamePropertyAlias).ToString());
                                    //combinedFields.AppendLine(node.Name);

                                    switch (language)
                                    {
                                        case "SV":

                                            // Function
                                            combinedFields2.AppendLine(node.GetProperty(MyConstants.ContactFunctionPropertyAlias).ToString());

                                            // Work assignments (optional, textbox multiple)
                                            var assignments = node.GetProperty(MyConstants.ContactworkAssignmentsPropertyAlias);
                                            if (assignments != null)
                                            {
                                                combinedFields3.AppendLine(assignments.ToString());
                                            }
                                            break;

                                        case "EN":

                                            // Function
                                            combinedFields2.AppendLine(node.GetProperty(MyConstants.ContactFunctionENPropertyAlias).ToString());

                                            // Work assignments (optional, textbox multiple)
                                            var assignmentsEn = node.GetProperty(MyConstants.ContactworkAssignmentsENPropertyAlias);
                                            if (assignmentsEn != null)
                                            {
                                                combinedFields3.AppendLine(assignmentsEn.ToString());
                                            }
                                            break;
                                    }
                                }

                                if (added == false)
                                {
                                    added = true;

                                    // Add the fetched name values to a new Examine index element
                                    e.Fields.Add("_CoworkerName", combinedFields.ToString());

                                    //// Add the fetched function values to a new Examine index element, due to language
                                    //switch (language)
                                    //{
                                    //    case "SV":
                                    //        fields.Add("_PersonFunction", combinedFields2.ToString());
                                    //        fields.Add("_PersonWorkAssignments", combinedFields3.ToString());
                                    //        break;
                                    //    case "EN":
                                    //        fields.Add("_PersonFunctionEn", combinedFields2.ToString());
                                    //        fields.Add("_PersonWorkAssignmentsEn", combinedFields3.ToString());
                                    //        break;
                                    //}

                                    combinedFields.Clear();
                                    combinedFields2.Clear();
                                    combinedFields3.Clear();
                                }
                            }
                        }
                    }

                   
                       

                   
                }
            }
            else if (objType == typeof(JArray))
            {
                var jArr = obj as JArray;
                if (jArr != null)
                {
                    for (var i = 0; i < jArr.Count; i++)
                    {
                        var itm = jArr[i];
                        var propKey = prefix + "_" + i;
                        var valueType = itm.GetType();
                        if (typeof(JContainer).IsAssignableFrom(valueType))
                        {
                            IndexNestedObject(fields, itm, propKey, language, e);
                            //IndexNestedObject(fields, itm, propKey, language, combinedFields, combinedFields2, combinedFields3);
                        }
                        else
                        {
                            fields.Add(propKey, itm.ToString().StripHtml());
                        }
                    }
                }
            }   
        }
    } 
}
