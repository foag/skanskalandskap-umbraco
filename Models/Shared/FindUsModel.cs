﻿using System.Web;

namespace SkanskaLandskap15.Models.Shared
{
    public class FindUsModel : AreaModel
    {
        public string FindUsIntro { get; set; }
        public IHtmlString FindUsText { get; set; }
        public bool ByCarLinkExists { get; set; }
        public LinkModel ByCarLink { get; set; }
        
        public string GoByPublicLinkTerm { get; set; }
        public string GoByCarLinkTerm { get; set; }

        public string BusIconPath { get; set; }
        public string CarIconPath { get; set; }

        public int ParkingNodeId { get; set; }
        public int BusstopNodeId { get; set; }
    }
}