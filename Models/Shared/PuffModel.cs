﻿using System.Collections.Generic;
using System.Web;

namespace SkanskaLandskap15.Models.Shared
{
    public class PuffModel
    {
        public dynamic Image { get; set; }
        public string ImagePath { get; set; }
        public bool PortraitImage { get; set; }
        public string Title { get; set; }
        public string Intro { get; set; }
        public IHtmlString Text { get; set; }
        public bool LinksExists { get; set; }
        public List<LinkModel> LinksCollection { get; set; }
        public List<ContactModel> PersonsCollection { get; set; }
        public string IconPath { get; set; }
        public IHtmlString LeftColumnText { get; set; }
        public IHtmlString RightColumnText { get; set; }       
    }
}