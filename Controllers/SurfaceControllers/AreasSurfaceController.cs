﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.Mvc;



namespace SkanskaLandskap15.Controllers.SurfaceControllers
{
    public class AreasSurfaceController : SurfaceController
    { 


        ///<summary>
        /// Fetches list of all areas.
        /// </summary> 
        /// <param name="language">Current language.</param>
        /// <returns>A json list of MapMarker objects.</returns>      
        [HttpGet]       
        public JsonResult GetAreas(string language, bool sortByStatus)
        {
            // Initiate the return value
            var areasList = new AreasListModel();

            // The culture of an Ajax request thread is neutral and therefore we need to set it 
            // explicitly to be able to get the correct Umbraco context.
            if (Request.IsAjaxRequest())
            {
                UmbracoHelperExtensions.EnsureCulture(Thread.CurrentThread, language);
            }

            // Get the list           
            areasList = GetAreasList(language, sortByStatus);

            return Json(new { areas = areasList }, JsonRequestBehavior.AllowGet);
        }
        


        ///<summary>
        /// Fetches information for a list of area puffs. 
        /// </summary> 
        /// <param name="language">Current language.</param>
        /// <param name="sortByStatus">Whether to sort by status or alphabetically.</param>
        /// <param name="contentType">The document type alias of the calling page. Might be used 
        /// for customizing the return value. Optional.</param>
        /// <returns>A list of Area objects.</returns>   
        public AreasListModel GetAreasList(string language, bool sortByStatus, string contentType = "")
        {
            // Initiate the return model
            var listModel = new AreasListModel();
            
            // Instanciate an area list 
            var areasList = new List<AreaModel>();

            // Get a reference to the Umbraco helper         
            UmbracoHelper helper = new UmbracoHelper(UmbracoContext.Current);

            string parentDocType = string.Empty;
            switch (language)
            {
                case "SV":
                    parentDocType = MyConstants.StartPageDocTypeAlias;
                    break;
                case "EN":
                    parentDocType = MyConstants.HomePageDocTypeAlias;
                    break;
            }

            // Get the node id of the areas landing page due to language
            int id = Umbraco.TypedContentSingleAtXPath("//" + parentDocType + "/" + MyConstants.AreasLandingPageDocTypeAlias).Id;

            // Get node content 
            var content = helper.TypedContent(id);

            // Inititiate ienumerable
            IEnumerable<AreaModel> listAreaData = null;

            // Filter out the necessary properties from the area nodes due to sort method
            switch (sortByStatus)
            {
                // Sort by status
                case true:                   
                    listAreaData = content
                        .Children
                        .Where(c => c.DocumentTypeAlias == MyConstants.AreaPageDocTypeAlias)
                        .OrderBy(c => c.GetPropertyValue<string>(MyConstants.AreaStatusPropertyAlias))
                        .Select(obj => new AreaModel()
                        {
                            Name = obj.Name,
                            Title = obj.GetPropertyValue<string>(MyConstants.TitlePropertyAlias),
                            SubTitle = obj.GetPropertyValue<string>(MyConstants.SubtitlePropertyAlias),
                            Intro = obj.GetPropertyValue<string>(MyConstants.IntroPropertyAlias),
                            AltIntro = obj.GetPropertyValue<string>(MyConstants.IntroAltPropertyAlias),
                            ImagePicker = obj.GetPropertyValue<string>(MyConstants.ImagePickerPropertyAlias),
                            MapLocation = obj.GetPropertyValue<string>(MyConstants.MapLocationPropertyAlias),
                            UrlName = obj.UrlName,
                            NodeId = obj.Id
                        }).ToList();
                    break;

                // Sort by alphabetical order
                default:
                    // Filter out the necessary properties from the area nodes
                    listAreaData = content
                        .Children
                        .Where(c => c.DocumentTypeAlias == MyConstants.AreaPageDocTypeAlias)
                        .OrderBy(c => c.Name)
                        .Select(obj => new AreaModel()
                        {
                            Name = obj.Name,
                            Title = obj.GetPropertyValue<string>(MyConstants.TitlePropertyAlias),
                            SubTitle = obj.GetPropertyValue<string>(MyConstants.SubtitlePropertyAlias),
                            Intro = obj.GetPropertyValue<string>(MyConstants.IntroPropertyAlias),
                            AltIntro = obj.GetPropertyValue<string>(MyConstants.IntroAltPropertyAlias),
                            ImagePicker = obj.GetPropertyValue<string>(MyConstants.ImagePickerPropertyAlias),
                            MapLocation = obj.GetPropertyValue<string>(MyConstants.MapLocationPropertyAlias),
                            UrlName = obj.UrlName,
                            NodeId = obj.Id
                        }).ToList();
                    break;
            } 
           

            // Iterate over the area properties to make some manipulations
            listAreaData.ForEach(item =>
            {
                // Create an Area object
                AreaModel model = new AreaModel();

                /* IMAGE */

                // Make sure at least one image exist
                if (!string.IsNullOrWhiteSpace(item.ImagePicker))
                {
                    // Store the main images to a list
                    string[] areaImagesList = item.ImagePicker.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                    // Get first image's path and add to model
                    model.Image = helper.Media(areaImagesList[0]);
                    model.ImagePath = model.Image.GetCropUrl("umbracoFile", "Inspirationsbild tumnagel");
                }
                

                /* MAP LOCATION */
             
                // Get map data - or not - due to content type
                switch(contentType)
                {
                    // Method is called from the StartMaster controller, which means we do not need to return map data.
                    case MyConstants.StartPageDocTypeAlias:
                    case MyConstants.HomePageDocTypeAlias:
                        item.MapLocation = string.Empty;
                        break;

                    // Method is called from the areas landing page
                    case MyConstants.AreasLandingPageDocTypeAlias:
                        if (string.IsNullOrWhiteSpace(item.MapLocation))
                        {
                            item.MapLocation = string.Empty;
                        }
                        else
                        {
                            // Create a MapMarker object
                            var objMapMarker = new MapMarkerModel();

                            // Get coordinates
                            string mapLocation = item.MapLocation.Trim();

                            // Assign coordinates to current MapMarker object
                            string[] mapLocationInfo = mapLocation.Split(new Char[] { ',' });
                            objMapMarker.Latitude = mapLocationInfo[0];
                            objMapMarker.Longitude = mapLocationInfo[1];

                            // Assign some content for the infobox
                            objMapMarker.Content = item.Title + "<br />" + item.Intro;

                            // Assign title, icon file and tag name 
                            objMapMarker.Title = item.Title;
                            objMapMarker.Icon = "areas";
                            objMapMarker.Tag = "area";

                            // Get UrlName
                            objMapMarker.UrlName = item.UrlName;
                            objMapMarker.NodeId = item.NodeId;

                            // Add object to return model                   
                            model.MapMarkerData = objMapMarker;
                        }
                        break;

                    // Method is called from any other page
                    default:
                        item.MapLocation = string.Empty;
                        break;
                }         
               


                /* OTHER PROPERTIES */

                model.Title = item.Title;
                model.SubTitle = item.SubTitle;
                model.Name = item.Name;               
                model.Url = helper.NiceUrl(item.NodeId);
                model.UrlName = item.UrlName;
                model.NodeId = item.NodeId;                            

                // Use the puff-specific intro, if any 
                if (!string.IsNullOrEmpty(item.AltIntro))
                {
                    item.Intro = item.AltIntro;
                }
                model.Intro = UmbracoHelperExtensions.TruncateAtWord(item.Intro, 150);

                // Add all properties to the return list
                areasList.Add(model);
            });

            // Add areas list to return model
            listModel.AreasCollection = areasList;
            
            return listModel;
        }



        /* Used by MapInteractivitySurfaceController */
        public int GetAreasLandingPageNodeId(string language, UmbracoHelper helper)
        {

            // Get document type for root (start) node due to language 
            string parentDocType = string.Empty;
            switch (language)
            {
                case "SV":
                    parentDocType = MyConstants.StartPageDocTypeAlias;
                    break;
                case "EN":
                    parentDocType = MyConstants.HomePageDocTypeAlias;
                    break;
            }

            // Get the node id of the current areas landing page
            int id = helper.TypedContentAtXPath("//" + parentDocType + "/" + MyConstants.AreasLandingPageDocTypeAlias).FirstOrDefault().Id;

            return id;
        }
    }
}