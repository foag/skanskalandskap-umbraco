﻿using System;
using System.Collections.Generic;


namespace SkanskaLandskap15.Models.Shared
{
    public class NewsListModel
    {
        public List<NewsItemModel> NewsItemsCollection { get; set; }
        public Int32 PageNumber { get; set; }
        public bool LoadMore { get; set; }
        public string LoadMoreInfo { get; set; }
        public string NoNewsMessage { get; set; }
    }
}
