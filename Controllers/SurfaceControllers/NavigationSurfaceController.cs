﻿using System.Collections.Generic;
using System.Linq;
using SkanskaLandskap15.Classes.Mappers;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;




namespace SkanskaLandskap15.Controllers.SurfaceControllers
{
    public class NavigationSurfaceController : SurfaceController
    {

        public IEnumerable<NavigationItemModel> mapMainNavigation(IPublishedContent root, IPublishedContent currentPage)
        {
            // Get all published top nodes 
            return from c in root.Children
                   where c.IsVisible()
                   // Use the mapper to map items
                   select NavigationItemMappers.Map(new NavigationItemModel(), c, currentPage);
        }      



        /////<summary>
        ///// Fetches the sub menu for a specific Content node.
        ///// </summary>
        ///// <param name="currentPage">Current page.>/param>
        ///// <returns>A list of NavigationItem objects.</returns>                
        //public IEnumerable<NavigationItemModel> GetSubMenu(IPublishedContent currentPage)
        //{
        //    // Initiate the return list
        //    IEnumerable<NavigationItemModel> subMenu = null;

        //    // Get an Umbraco helper
        //    var helper = new UmbracoHelper(UmbracoContext.Current);

        //    // Get Level 2 node id of current branch
        //    var nodeId = currentPage.AncestorOrSelf(2).Id;

        //    // Get a reference to the Level 2 Content node
        //    var content = helper.TypedContent(nodeId);

        //    // Make sure this is not an area page (landing or detailed)          
        //    if (currentPage.DocumentTypeAlias != MyConstants.AreasLandingPageDocTypeAlias 
        //        && currentPage.DocumentTypeAlias != MyConstants.AreaPageDocTypeAlias)
        //    {
        //        // Get the menu
        //        subMenu = mapSubNavigation(content, currentPage);
        //    }           
            
        //    return subMenu;
        //}



        //public static IEnumerable<NavigationItemModel> mapSubNavigation(IPublishedContent content, IPublishedContent currentPage, bool areaPart = false)
        //{

        //    // Get the published child nodes 
        //    return from c in content.Children
        //        where c.IsVisible()

        //            // Exclude a few pages
        //            && c.DocumentTypeAlias != MyConstants.NewsPageDocTypeAlias
        //            && c.DocumentTypeAlias != MyConstants.EventPageDocTypeAlias

        //            // Use the mapper to map items:
        //            select NavigationItemMappers.Map(new NavigationItemModel()
        //            {
        //                // Map children the same way as we have already mapped parents
        //                Children = mapSubNavigation(c, currentPage)
        //            },
        //                c, currentPage);           


        //    //// Make mapping due to page type
        //    //switch (areaPart)
        //    //{
        //    //    // Area page (landing or detailed)
        //    //    case true:

        //    //        // Get the published child nodes
        //    //        return from c in content.Children
        //    //               where c.IsVisible()
     
        //    //               // Order area page by status                          
        //    //               orderby c.GetPropertyValue<string>(MyConstants.AreaStatusPropertyAlias)

        //    //               // Use the mapper to map items:
        //    //               select NavigationItemMappers.Map(new NavigationItemModel(), c, currentPage, areaPart);

        //    //    // Other pages
        //    //    default:

        //    //        // Get the published child nodes 
        //    //        return from c in content.Children
        //    //               where c.IsVisible()

        //    //                   // Exclude a few pages
        //    //                   && c.DocumentTypeAlias != MyConstants.AreasLandingPageDocTypeAlias
        //    //                   && c.DocumentTypeAlias != MyConstants.AreaPageDocTypeAlias
        //    //                   && c.DocumentTypeAlias != MyConstants.NewsPageDocTypeAlias
        //    //                   && c.DocumentTypeAlias != MyConstants.EventPageDocTypeAlias

        //    //               // Use the mapper to map items:
        //    //               select NavigationItemMappers.Map(new NavigationItemModel()
        //    //               {
        //    //                   // Map children the same way as we have already mapped parents
        //    //                   Children = mapSubNavigation(c, currentPage)
        //    //               },
        //    //                   c, currentPage);
        //    //}
        //}

    }
}