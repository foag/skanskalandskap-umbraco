﻿
namespace SkanskaLandskap15.Models.Shared
{
    public class SubMenuLinkCategoryModel
    {
        public string CategoryName { get; set; }
        public string CategoryId { get; set; }
    }
}