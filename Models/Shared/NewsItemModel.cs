﻿using System.Collections.Generic;
using System.Web;


namespace SkanskaLandskap15.Models.Shared
{
    public class NewsItemModel
    {
        public int NodeId { get; set; }
        public string Url { get; set; }
        public string UrlTitleTerm { get; set; }
        public string Title { get; set; }
        public string Intro { get; set; }
        public IHtmlString Text { get; set; }
        public dynamic NewsImage { get; set; }
        public string ImageCaption { get; set; }
        public string NewsFlash { get; set; }
        public string PublishingDate { get; set; }
        public string PublishedTerm { get; set; }
        public bool FeaturedNewsItem { get; set;} 
        public List<NewsItemModel> NewsCollection { get; set; }
        
        public string GridClass { get; set; }      

        public string NewsTitleTerm { get; set; }
        public string ReadMoreTerm { get; set; }
        public string SeeMoreNewsTerm { get; set; }
        public string MoreNewsTerm { get; set; }
        public string AllNewsTerm { get; set; }

        public string NewsLandingPageUrl { get; set; }
        public string SitePart { get; set; }
    }
}
