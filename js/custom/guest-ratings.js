
    function updateStarRatingBar(id, value, textshown, errortext, thanktext, name) {

        // Get current language
		var language = Global.language;

        // No previous voting (using current browser)
		if (readStarRatingCookie(id) == null) {

            // Get the confirm message from Umbraco's Dictionary via a global js variable.
		    var messageConfirm = "";

		    messageConfirm = GuestRatings.ConfirmMessage.format(name, value);

            // Hand out a confirm dialog
		    var answer = confirm(decodeHtml(messageConfirm));

		    if (answer) {

		        // Get the site part that's utilizing the rating function (= AccommodationPage)
		        var sitePart = $('#node-content-type').text();

		        // Create parameters
		        var params = {
		            nodeId: id,
                    rating: value,
		            sitePart: sitePart,
		            language: Global.language
		        };

		        $.getJSON('/umbraco/Surface/GuestRatingsSurface/UpdateBar', params, function (data) {

		            if (data.result == true) {
		                rateBar = document.getElementById(id);
		                rateBar.style.width = value * 20 + "%";

		                createStarRatingCookie(id, value, 100);

		                if (textshown == 1) {

		                    // Temporarily hide the stars message
		                    starsMsg = document.getElementById("stars-message");
		                    starsMsg.style.display = 'none';

		                    spanMsg = document.getElementById("voters-message");

                            // Get the thank-you and see-results messages from Umbraco's Dictionary via global js variables
		                    spanMsg.innerHTML = unescape(GuestRatings.ThankYouMessage + "<br /><a id='see-result' href='javascript:void(0);'>" + GuestRatings.SeeResults + "</a>.");
		                }
		            }
		        });
			}
		}
        // Previous voting exist
        else {

            if(textshown == 1) {
                spanMsg = document.getElementById("voters-message");

                // Get the no-no message from Umbraco's Dictionary via a global js variable.
                spanMsg.innerHTML = GuestRatings.OnlyOnceMessage;
	       }
        }
    }


    function createStarRatingCookie(name,value,days) {
	    if (days) {
		    var date = new Date();
		    date.setTime(date.getTime()+(days*24*60*60*1000));
		    var expires = "; expires="+date.toGMTString();
	    }
	    else var expires = "";
	    document.cookie = name+"="+value+expires+"; path=/";
    }


    function readStarRatingCookie(name) {
	    var nameEQ = name + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0;i < ca.length;i++) {
		    var c = ca[i];
		    while (c.charAt(0)==' ') c = c.substring(1,c.length);
		    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	    }
	    return null;
    }


    (function ($) {

        // Attach a click event to the "See results/Se resultat" link
        $(document).on('click', '#see-result', function () {

            // Get the site part that's utilizing the rating function (AccommodationPage)
            var sitePart = $('#node-content-type').text();

            // Create parameters
            var params = {
                nodeId: Global.currentNodeId,
                sitePart: sitePart,
                language: Global.language
            };

            // Get updated rating information
            $.getJSON('/umbraco/Surface/GuestRatingsSurface/GuestRatingsJson', params, function (data) {

                // Replace the rating bar's percentage value
                $('.current-rating').css({ 'width': data.model.Percentage + "%" })

                // Display and replace the average value message
                $('#stars-message').show().html(data.model.AverageValueMessage);

                // Replace the thank you-message to an updated voters message
                $('#voters-message').text(data.model.NumOfVotersMessage);
            });
        });



        // Attach click events to the stars of the rating bar
        var nodeId = Global.currentNodeId;
        var nodeTitle = $('#node-title').text();

        $(document).on('click', '.one-star', function (e) {
            e.stopPropagation();
            updateStarRatingBar(nodeId, 1, 1, "", "", nodeTitle);
        });
        $(document).on('click', '.two-stars', function (e) {
            e.stopPropagation();
            updateStarRatingBar(nodeId, 2, 1, "", "", nodeTitle);
        });
        $(document).on('click', '.three-stars', function (e) {
            e.stopPropagation();
            updateStarRatingBar(nodeId, 3, 1, "", "", nodeTitle);
        });
        $(document).on('click', '.four-stars', function (e) {
            e.stopPropagation();
            updateStarRatingBar(nodeId, 4, 1, "", "", nodeTitle);
        });
        $(document).on('click', '.five-stars', function (e) {
            e.stopPropagation();
            updateStarRatingBar(nodeId, 5, 1, "", "", nodeTitle);
        });

    })(jQuery);



    // String format function
    String.prototype.format = function () {
        var s = this,
            i = arguments.length;

        while (i--) {
            s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
        }
        return s;
    };


    // Decode html including strings containing html-coded Swedish characters
    function decodeHtml(html) {
        var txt = document.createElement("textarea");
        txt.innerHTML = html;
        return txt.value;
    }
