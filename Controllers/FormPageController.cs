﻿using System;
using System.Web;
using System.Web.Mvc;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Models;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Web;



namespace SkanskaLandskap15.Controllers
{
    public class FormPageController : InteriorMasterController
    {       
       
        public ActionResult FormPage()
        {
            // Initiate the return model
            var model = new FormPageModel();
                       
            // Title
            model.Title = CurrentPage.GetPropertyValue<String>(MyConstants.TitlePropertyAlias);

            // Text
            model.Text = CurrentPage.GetPropertyValue<IHtmlString>(MyConstants.TextPropertyAlias);

            // Form           
            FormModel formModel = new FormModel();
            formModel.FormGuid = CurrentPage.GetPropertyValue<Guid>("FormPicker");
            model.UmbracoForm = formModel;
            //umbraco.library.RenderMacroContent(src.BodyText, src.Id)

            return View(model);
        }
    }
}