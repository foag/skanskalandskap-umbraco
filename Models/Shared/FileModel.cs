﻿
namespace SkanskaLandskap15.Models.Shared
{
    public class FileModel 
    {        
        public string Name { get; set; }
        public string Path { get; set; }
        public string Extension { get; set; }
        public string Size { get; set; }
        public string Info { get; set; }
        public string IconPath { get; set; }
    }
}