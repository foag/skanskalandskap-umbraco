﻿using System.Collections.Generic;

namespace SkanskaLandskap15.Models.Shared
{
    public class SubPagesListModel
    {
        // public bool TipsExists { get; set; }
        public string TitleTerm { get; set; }
        public List<SubPagePuffModel> SelectedPuffsCollection { get; set; }
        public List<SubPagePuffModel> SelectedAreasCollection { get; set; }
        public List<SubPagePuffModel> SelectedAccommodationsCollection { get; set; }
        public List<SubPagePuffModel> PuffsCollection { get; set; }
        public bool AccommodationsInTheAreaType { get; set; }
        public bool Gallery { get; set; }
    }
}