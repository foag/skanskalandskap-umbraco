﻿using System;
using System.Text;
using Examine;
using umbraco.NodeFactory;
using Umbraco.Core;
using Umbraco.Web;
using UmbracoExamine;

namespace SkanskaLandskap15.Classes.Utilities
{

    ///<summary>
    /// This class is used mainly to make some properties, included through MNTP datatypes, searchable.
    ///</summary>

    public class AppEvents : IApplicationEventHandler
    {
        public void OnApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }

        public void OnApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }

        public void OnApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {

            // Get Umbraco helper instance
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language ("SV" or "EN")
            var currentLanguage = helper.GetDictionaryValue("Language");
            
            // Handle MNTP properties for the Swedish indexer
            ExamineManager.Instance.IndexProviderCollection["SiteContentIndexer"].GatheringNodeData
                += (sender, e) => ExamineEvents.GatheringContentData(sender, e, helper, "SV");

            // Handle MNTP properties for the English indexer
            ExamineManager.Instance.IndexProviderCollection["SiteContentEnglishIndexer"].GatheringNodeData
                += (sender, e) => ExamineEvents.GatheringContentData(sender, e, helper, "EN");
        }       
    }


    public static class ExamineEvents
    {
        public static void GatheringContentData(object sender, IndexingNodeDataEventArgs e, UmbracoHelper helper, string language)
        {

            //var serializer = new JavaScriptSerializer();

            // Make sure index type is "content"
            if (e.IndexType == IndexTypes.Content)
            {   
                // Get Examine fields
                var fields = e.Fields;
                
                // Initiate string builders
                var combinedFields = new StringBuilder();
                var combinedFields2 = new StringBuilder();
                var combinedFields3 = new StringBuilder();
               
                // Iterate over the Examine fields
                foreach (var keyValuePair in fields)
                {
                    // Get the field key (e.g. nodeName, Title etc)
                    string propertyAlias = keyValuePair.Key;
                    
                    // Make manipulations for MNTP keys
                    switch (propertyAlias)
                    {
                        // This is the MNTP persons picker
                        case MyConstants.ContactPickerPropertyAlias:

                            // Get the comma-separated node ids
                            var nodeIds = keyValuePair.Value.Split(',');

                            // Iterate over the id:s and get the person's name and function
                            foreach (string nodeId in nodeIds)
                            {
                                var node = new Node(Convert.ToInt32(nodeId));

                                // Name                                
                                combinedFields.AppendLine(node.GetProperty(MyConstants.ContactNamePropertyAlias).ToString());
                                //combinedFields.AppendLine(node.Name);

                                switch(language)
                                {
                                    case "SV":

                                        // Function
                                        combinedFields2.AppendLine(node.GetProperty(MyConstants.ContactFunctionPropertyAlias).ToString());

                                        // Work assignments (optional, textbox multiple)
                                        var assignments = node.GetProperty(MyConstants.ContactworkAssignmentsPropertyAlias);
                                        if (assignments != null)
                                        {
                                            combinedFields3.AppendLine(assignments.ToString());
                                        }
                                        break;
                                    
                                    case "EN":
                                        
                                        // Function
                                        combinedFields2.AppendLine(node.GetProperty(MyConstants.ContactFunctionENPropertyAlias).ToString());

                                        // Work assignments (optional, textbox multiple)
                                        var assignmentsEn = node.GetProperty(MyConstants.ContactworkAssignmentsENPropertyAlias);
                                        if (assignmentsEn != null)
                                        {
                                            combinedFields3.AppendLine(assignmentsEn.ToString());
                                        }
                                        break;
                                } 
                            }                          
                           break;

                        //// An attempt to make a nuPicker datatype searchable. Breaks at the deserialize operation. 
                        //case "CabinFacilities":                         

                            //// Get the json 
                            //var deserialized = serializer.Deserialize<List<TheType>>(keyValuePair.Value);   
                            //var deserialized = JsonConvert.DeserializeObject<List<TheType>>(keyValuePair.Value);

                           //// Iterate over the list
                           //foreach (var item in deserialized)
                           //{
                           //    var node = new Node(Convert.ToInt32(item.key));

                           //    switch (language)
                           //    {
                           //        case "SV":
                           //            combinedFields3.AppendLine(node.Name + " ");
                           //            //combinedFields3.AppendLine(item.label);
                           //            break;
                           //        case "EN":
                           //            combinedFields3.AppendLine(node.GetProperty("TermEnglish").ToString());
                           //            break;
                           //    }
                           //}
                           //break;
                    }                   
                }

                // Add the fetched name values to a new Examine index element
                e.Fields.Add("_PersonName", combinedFields.ToString());

                // Add the fetched function values to a new Examine index element, due to language
                switch (language)
                {
                    case "SV":
                        e.Fields.Add("_PersonFunction", combinedFields2.ToString());
                        e.Fields.Add("_PersonWorkAssignments", combinedFields3.ToString());
                        break;
                    case "EN":
                        e.Fields.Add("_PersonFunctionEn", combinedFields2.ToString());
                        e.Fields.Add("_PersonWorkAssignmentsEn", combinedFields3.ToString());
                        break;
                }

                //e.Fields.Add("_CabinFacilities", combinedFields3.ToString());
            }
        } 
    }

    //public class TheType
    //{
    //    public string key { get; set; }
    //    public string label { get; set; }
    //}
}