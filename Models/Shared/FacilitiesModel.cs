﻿using System.Collections.Generic;

namespace SkanskaLandskap15.Models.Shared
{
    public class FacilitiesModel
    {        
        public List<FacilityTypeModel> FacilitiesCollection { get; set; }
    }
}