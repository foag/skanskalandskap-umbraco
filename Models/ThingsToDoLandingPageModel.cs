﻿using SkanskaLandskap15.Models.Masters;
using SkanskaLandskap15.Models.Shared;


namespace SkanskaLandskap15.Models
{

    public class ThingsToDoLandingPageModel : InteriorMasterModel
    {
        public string SelectedNodes { get; set; }
        public string MoreTipsTerm { get; set; }
        public SubPagesListModel SubPages { get; set; }
    }
}