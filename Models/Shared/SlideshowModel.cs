﻿using System.Collections.Generic;

namespace SkanskaLandskap15.Models.Shared
{
    public class SlideshowModel
    {                  
        public bool SlidesExists { get; set; }
        public List<SlideModel> SlidesCollection { get; set; }
        public string Title { get; set; }
        public string ReadMoreTerm { get; set; }
        public bool Hidden { get; set; }
    }
}