﻿using System.Web.Mvc;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Controllers.SurfaceControllers;
using SkanskaLandskap15.Models;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Web;




namespace SkanskaLandskap15.Controllers
{
    public class AccommodationsLandingPageController : InteriorMasterController
    {
        
        public ActionResult AccommodationsLandingPage()
        {
            // Initiate the return model
            var model = new AccommodationsLandingPageModel();

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language ("SV" or "EN")
            string currentLanguage = helper.GetDictionaryValue("Language");
            
            // Assign the content type
            model.ContentType = CurrentPage.DocumentTypeAlias;

            // Initiate a SubPagesList object
            var subPages = new SubPagesListModel();

            // Get controller reference
            PuffsSurfaceController psc = new PuffsSurfaceController();

            // Get and add a list of all sub pages to the sub pages list object
            subPages.PuffsCollection = psc.GetAllSubPagePuffs(CurrentPage.Id, CurrentPage.DocumentTypeAlias, helper, currentLanguage);

            // Get title for the puffs section 
            subPages.TitleTerm = CurrentPage.GetPropertyValue<string>(MyConstants.TitleListPagePropertyAlias).Trim();
            //subPages.TitleTerm = helper.GetDictionaryValue("AccommodationPuffsTitle");

            // Add the sub pages list object to the return model
            model.SubPages = subPages;
                               
            return View(model);
        }


    }
}


