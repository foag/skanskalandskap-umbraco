﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CompleteRatings.ascx.cs" Inherits="dashboardcontrols.CompleteRatings" %>
<%@ Import Namespace="dashboardcontrols" %>


    <style type="text/css">        
        
        #datarea-wrapper {width: 100%; margin-left:20px;}

        .dataarea {margin-bottom:60px; float:left; min-width:400px;}
        
        .dataarea div table, .dataarea div th, .dataarea div td { border: none; }
        
        .dataarea div table tbody tr th:nth-of-type(1){width:70px;}
        .dataarea div table tbody tr th:nth-of-type(2){width:200px;}
        .dataarea div table tbody tr th:nth-of-type(3){width:90px;}
        .dataarea div table tbody tr th:nth-of-type(4){width:90px;}
        
        .dataarea div table tbody tr {height:25px;}        

        @media only screen and (max-width: 1000px) {
            .dataarea {
                margin-left: 0px !important;
            }
        }    
    </style>

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
             <div id="datarea-wrapper">

                  <%if (this.lblErrorMessage.Text != String.Empty)
                  { %>
                    <div style="font-size:14px; padding:14px 3px; color:red; font-weight:bold;">
                        <asp:Label ID="lblErrorMessage" runat="server" />
                    </div> 
                <%}%>

                <div class="dataarea">   
                    <h2>Strövområden</h2>   
                    <asp:GridView ID="gdvStrovomraden" runat="server"
                        AutoGenerateColumns="false"  
                        GridLines="Both" 
                        AllowSorting="true" 
                        ShowHeader="true"
                        EmptyDataText="Sidor saknas."
                        OnSorting="gdvStrovomraden_Sorting" 
                        RowStyle-HorizontalAlign="Center">  
                        <Columns>  
                            <asp:BoundField DataField="Placement" HeaderText="Placering" SortExpression="Placement" ItemStyle-HorizontalAlign="Center" />               
                            <asp:BoundField DataField="LinkedName" HeaderText="Strövområde" SortExpression="HiddenName" ItemStyle-HorizontalAlign="Center" HtmlEncode="false" />
                            <asp:BoundField DataField="AverageValue" HeaderText="Medelvärde" SortExpression="AverageValue" /> 
                            <asp:BoundField DataField="Votes" HeaderText="Röster" SortExpression="Votes" /> 
                        </Columns>
                    </asp:GridView>
                </div>

                <div class="dataarea" style="margin-left:100px;">    
                    <h2>Recreation Areas</h2>                           
                    <asp:GridView ID="gdvRecreationAreas" runat="server" 
                        AutoGenerateColumns="false"  
                        GridLines="Both" 
                        AllowSorting="true" 
                        ShowHeader="true"
                        EmptyDataText="Sidor saknas."
                        OnSorting="gdvRecreationAreas_Sorting" 
                        RowStyle-HorizontalAlign="Center">    
                        <Columns>  
                            <asp:BoundField DataField="Placement" HeaderText="Placering" SortExpression="Placement" ItemStyle-HorizontalAlign="Center" />               
                            <asp:BoundField DataField="LinkedName" HeaderText="Strövområde" SortExpression="HiddenName"
                                ItemStyle-HorizontalAlign="Center" HtmlEncode="false" />
                            <asp:BoundField DataField="AverageValue" HeaderText="Medelvärde" SortExpression="AverageValue" />
                            <asp:BoundField DataField="Votes" HeaderText="Röster" SortExpression="Votes" /> 
                        </Columns>
                    </asp:GridView>
                </div>
                <div style="clear:left;"></div>

                <div class="dataarea">    
                    <h2>Boende</h2>                  
                    <asp:GridView ID="gdvBoende" runat="server" 
                        AutoGenerateColumns="false"  
                        GridLines="Both" 
                        AllowSorting="true" 
                        ShowHeader="true"
                        EmptyDataText="Sidor saknas."
                        OnSorting="gdvBoende_Sorting" 
                        RowStyle-HorizontalAlign="Center">    
                        <Columns>  
                            <asp:BoundField DataField="Placement" HeaderText="Placering" SortExpression="Placement" ItemStyle-HorizontalAlign="Left" />               
                            <asp:BoundField DataField="LinkedName" HeaderText="Stuga" SortExpression="HiddenName"
                                ItemStyle-HorizontalAlign="Left" HtmlEncode="false" />
                            <asp:BoundField DataField="AverageValue" HeaderText="Medelvärde" SortExpression="AverageValue" />
                            <asp:BoundField DataField="Votes" HeaderText="Röster" SortExpression="Votes" /> 
                        </Columns>                         
                    </asp:GridView>
                </div>
                
                <div class="dataarea" style="margin-left:100px;">    
                    <h2>Where To Stay</h2>                    
                    <asp:GridView ID="gdvWhereToStay" runat="server" 
                        AutoGenerateColumns="false"  
                        GridLines="Both" 
                        AllowSorting="true" 
                        ShowHeader="true"
                        EmptyDataText="Sidor saknas."
                        OnSorting="gdvWhereToStay_Sorting" 
                        RowStyle-HorizontalAlign="Center">    
                        <Columns>  
                            <asp:BoundField DataField="Placement" HeaderText="Placering" SortExpression="Placement" ItemStyle-HorizontalAlign="Left" />               
                            <asp:BoundField DataField="LinkedName" HeaderText="Stuga" SortExpression="HiddenName"
                                ItemStyle-HorizontalAlign="Left" HtmlEncode="false" />
                            <asp:BoundField DataField="AverageValue" HeaderText="Medelvärde" SortExpression="AverageValue" />
                            <asp:BoundField DataField="Votes" HeaderText="Röster" SortExpression="Votes" /> 
                        </Columns>
                    </asp:GridView>
                </div>
                <div style="clear:left;"></div>

                <div class="dataarea">    
                    <h2>Göra?</h2>                             
                    <asp:GridView ID="gdvGora" runat="server" 
                        AutoGenerateColumns="false"  
                        GridLines="Both" 
                        AllowSorting="true" 
                        ShowHeader="true"
                        EmptyDataText="Sidor saknas."
                        OnSorting="gdvGora_Sorting" 
                        RowStyle-HorizontalAlign="Center">    
                        <Columns>  
                            <asp:BoundField DataField="Placement" HeaderText="Placering" SortExpression="Placement" ItemStyle-HorizontalAlign="Left" />               
                            <asp:BoundField DataField="LinkedName" HeaderText="Aktivitet" SortExpression="HiddenName"
                                ItemStyle-HorizontalAlign="Left" HtmlEncode="false" />
                            <asp:BoundField DataField="AverageValue" HeaderText="Medelvärde" SortExpression="AverageValue" />
                            <asp:BoundField DataField="Votes" HeaderText="Röster" SortExpression="Votes" /> 
                                <asp:BoundField DataField="PublishedValue" HeaderText="Publicerad" SortExpression="PublishedValue" /> 
                        </Columns>                            
                    </asp:GridView>
                </div>               

                <div class="dataarea" style="margin-left:100px;">    
                    <h2>Things To Do</h2>                         
                    <asp:GridView ID="gdvThingsToDo" runat="server" 
                        AutoGenerateColumns="false"  
                        GridLines="Both" 
                        AllowSorting="true" 
                        ShowHeader="true"
                        EmptyDataText="Sidor saknas."
                        OnSorting="gdvThingsToDo_Sorting" 
                        RowStyle-HorizontalAlign="Center">    
                        <Columns>  
                            <asp:BoundField DataField="Placement" HeaderText="Placering" SortExpression="Placement" ItemStyle-HorizontalAlign="Left" />               
                            <asp:BoundField DataField="LinkedName" HeaderText="Aktivitet" SortExpression="HiddenName"
                                ItemStyle-HorizontalAlign="Left" HtmlEncode="false" />
                            <asp:BoundField DataField="AverageValue" HeaderText="Medelvärde" SortExpression="AverageValue" />
                            <asp:BoundField DataField="Votes" HeaderText="Röster" SortExpression="Votes" />
                            <asp:BoundField DataField="PublishedValue" HeaderText="Publicerad" SortExpression="PublishedValue" /> 
                        </Columns>                            
                    </asp:GridView>
                </div>
                <div style="clear:left;"></div>
                                                
            </div>               
         </ContentTemplate>
    </asp:UpdatePanel>                     
