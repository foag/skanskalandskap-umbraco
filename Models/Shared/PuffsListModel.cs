﻿using System.Collections.Generic;

namespace SkanskaLandskap15.Models.Shared
{
    public class PuffsListModel
    {
        public string MoreInfoTitle { get; set; }
        public bool PuffsExists { get; set; }
        public string PuffsTitle { get; set; }
        public List<PuffModel> PuffsCollection { get; set; }
        public bool Gallery { get; set; }
    }
}