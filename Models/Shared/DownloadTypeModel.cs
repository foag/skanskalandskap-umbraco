﻿
namespace SkanskaLandskap15.Models.Shared
{
    public class DownloadTypeModel
    {
        public int NodeId { get; set; }
        public string TypeId { get; set; }
        public string TypeTitle { get; set; }       
    }
}
