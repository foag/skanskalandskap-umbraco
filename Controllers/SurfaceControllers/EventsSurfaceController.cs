﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.Mvc;



namespace SkanskaLandskap15.Controllers.SurfaceControllers
{
    public class EventsSurfaceController : SurfaceController
    {


        ///<summary>
        /// Fetches a list of upcoming events, typically for start page or Fulltofta page.  
        /// </summary>
        /// <param name="model">An empty EventItem object.</param>
        /// <param name="contentType">Indicates the type of page that wants to display the upcoming events 
        /// list and from which the EventItem object is sent ("StartPage", "AreaPage" - i.e. Fulltofta).
        /// </param> 
        /// <param name="areaPage">The name of the area page when content type is AreaPage. Optional.</param>
        /// <returns>The EventsUpcoming partial populated with an EventItem object.</returns>      
        [ChildActionOnly]
        public ActionResult EventsUpcoming(EventItemModel model, string contentType, string areaPage = "")
        { 
            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language ("SV" or "EN")
            var currentLanguage = helper.GetDictionaryValue("Language");

            // Create a Swedish culture object
            CultureInfo swedish = new CultureInfo("sv-SE");


            /* SET NUMBER OF EVENTS DUE TO SITEPART */

            // Initiate variable
            int numOfEvents = 0;

            //// Adjust sitePart parameter
            //string areaPage = string.Empty;
            //if (contentType == "AreaPage")
            //     = "";

            switch (contentType)
            {
                case "StartPage":

                    // Get id of the start page node
                    int nodeIdStartPage = Umbraco.TypedContentAtXPath("//" + MyConstants.StartPageDocTypeAlias).First().Id;

                    // Get content based on start page node id
                    var contentStartPage = helper.TypedContent(nodeIdStartPage);

                    // Get the number of events to be displayed (set in backoffice, start page node)
                    numOfEvents = contentStartPage.GetPropertyValue<int>(MyConstants.NumOfEventsPropertyAlias);
                    break;

                case "AreaPage":  // Fulltofta
                    numOfEvents = MyConstants.EventItemsOnAreaPage;
                    break;
            }


            // Get the node id of the News and Events landing page
            int nodeId = Umbraco.TypedContentAtXPath("//" + MyConstants.NewsAndEventsLandingPageDocTypeAlias).First().Id;

            // Get content based on News and Events page node id
            var content = helper.TypedContent(nodeId);
            
            IEnumerable<EventItemModel> eventsList = content
                .Descendants()
                .Where(c => c.DocumentTypeAlias == MyConstants.EventPageDocTypeAlias
                    && c.GetPropertyValue<DateTime>(MyConstants.EventEndPropertyAlias) >= DateTime.Now)
                .OrderBy(c => c.GetPropertyValue<DateTime>(MyConstants.EventStartPropertyAlias))
                .Take(numOfEvents)
                .Select(obj => new EventItemModel()
                {
                    NodeId = obj.Id,
                    Tags = obj.GetPropertyValue<string>(MyConstants.TagEventPropertyAlias)
                }).ToList();


            // Manage AreaPage (= Fulltofta) events
            if (contentType == MyConstants.AreaPageDocTypeAlias)
            {
                // Initiate a temporary EventItem list for tagged events
                List<EventItemModel> taggedList = new List<EventItemModel>();

                // Instanciate serializer
                JavaScriptSerializer ser = new JavaScriptSerializer();
                
                // Iterate over the event items to select events with a specific area tag  
                foreach (var item in eventsList)
                {
                    // Initiate Tag flag
                    bool tagFlag = false;

                    if (item.Tags != null)
                    {  
                        // Deserialize tags value
                        List<string> tags = ser.Deserialize<List<string>>(item.Tags);

                        // Iterate over the selected tags (one or many)
                        foreach (var itemTag in tags)
                        {
                            // Look for the specified tag
                            if (itemTag == areaPage)
                            {
                                tagFlag = true;
                                break;
                            }
                        }

                        // Event was tagged
                        if(tagFlag)
                        {
                            taggedList.Add(item);
                        }
                    }
                }

                // Replace original list with the tagged items list 
                eventsList = taggedList;
            }

            // Initiate a temporary EventItem list
            List<EventItemModel> tempList = new List<EventItemModel>();

            // Create a DateTimeFormatInfo object to make it possible to get month name as text...
            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();

            // ...in Swedish, provided this is current language 
            if (currentLanguage == "SV")
            {
                dtfi = swedish.DateTimeFormat;
            }

            // Iterate over the event items list
            foreach (var item in eventsList)
            {
                // Instanciate an EventItem object
                EventItemModel modelEi = new EventItemModel();

                // Get content based on current node id
                var itemContent = helper.Content(item.NodeId);

                // Construct readable date information                 
                modelEi = EventPageController.CreateNiceEventDates(itemContent, modelEi, dtfi);

                // Title
                modelEi.Title = itemContent.GetPropertyValue(MyConstants.TitlePropertyAlias);

                // Url
                modelEi.Url = umbraco.library.NiceUrl(Convert.ToInt32(itemContent.Id));

                // Location
                dynamic pickedItems = itemContent.GetPropertyValue(MyConstants.AreaPickerPropertyAlias);

                bool areaPicked = false;
                foreach (var itemP in pickedItems.PickedKeys)
                {
                    // A recreational area was selected
                    areaPicked = true;
                    modelEi.LocationName = helper.TypedContent(itemP).Name;
                    break;
                }
                if (areaPicked == false)
                {
                    // Free-text location
                    modelEi.LocationName = itemContent.GetPropertyValue<String>(MyConstants.LocationFreeTextPropertyAlias).Trim();
                }

                // Add event item object to the temporary list
                tempList.Add(modelEi);
            }

            // Add the temporary list to the return model
            model.UpcomingEventsCollection = tempList;


            // Set GUI titles due to site part 
            switch (contentType)
            {
                case "StartPage":

                    // Add the Upcoming Events area's title
                    model.TitleTerm = helper.GetDictionaryValue("EventsTitleStartPage");

                    if(tempList.Count > 0)
                    {
                        // Add the title for the Events Landing Page link
                        model.SeeMoreEventsTerm = helper.GetDictionaryValue("SeeMoreEvents");

                        // Create the url to the Events landing page
                        model.EventsLandingPageUrl = helper.NiceUrl(nodeId);                       
                    }
                    else
                    {
                        model.NoEventsMessage = helper.GetDictionaryValue("NoEventsMessageShort");
                    }                  

                    break;

                case "AreaPage":

                    // Add the Upcoming Events area's title
                    model.TitleTerm = String.Format(helper.GetDictionaryValue("EventsTitleAreaPage"), CurrentPage.Name);

                    break;
            }

            // Set the part of the site where the upcoming events list should be displayed
            model.ContentType = contentType;

            // Send the EventItem model to the EventsUpcoming partial view
            return PartialView("EventsUpcoming", model);
        }




        ///<summary>
        /// Fetches a subset of event item puffs based on the page number parameter (aimed mainly
        /// for the Load more function on the News and Events Landing page).
        /// </summary>
        /// <param name="nodeId">The node id of current (Events landing) page.</param>
        /// <param name="pageNumber">An integer indicating the page/subset to be fetched 
        /// (should be > 1).</param>
        /// <returns>An EventsList object.</returns>      
        public ActionResult GetEventsListSubset(int nodeId, int pageNumber, string language)
        {
            // Initiate the return model
            var model = new EventsListModel();

            // The culture of an Ajax request thread is neutral and therefore we need to set it 
            // explicitly to be able to get the correct Umbraco context.
            if (Request.IsAjaxRequest())
            {
                UmbracoHelperExtensions.EnsureCulture(Thread.CurrentThread, language);
            }
                       
            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get all content based on current (landing) page
            var content = helper.TypedContent(nodeId);

            // Create a Swedish culture object
            CultureInfo swedish = new CultureInfo("sv-SE");

            // Create a DateTimeFormatInfo object to make it possible to get month name as text...
            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();

            // ...in Swedish, provided this is current language 
            if (language == "SV")
            {
                dtfi = swedish.DateTimeFormat;
            }

            // Get a reference to the EventsLandingPage controller
            NewsAndEventsLandingPageController nelpc = new NewsAndEventsLandingPageController();

            // Get the subset size (the number of news items to be displayed at a time)
            int recordsPerPage = nelpc.GetRecordsPerPage(content, "events");            
            
            // Get the published and not outdated event items (just the node id:s)
            IEnumerable<EventItemModel> eventsList = content
                .Descendants()
                .Where(c => c.DocumentTypeAlias == MyConstants.EventPageDocTypeAlias && c.GetPropertyValue<DateTime>(MyConstants.EventEndPropertyAlias) >= DateTime.Now)
                .OrderBy(c => c.GetPropertyValue<DateTime>(MyConstants.EventStartPropertyAlias))
                .Select(obj => new EventItemModel()
                {
                    NodeId = obj.Id
                }).ToList();

            // Assign the total number of event items to a variable
            int numberOfRecords = Convert.ToInt32(eventsList.Count());

            // Get a reference to the EventPage Controller
            //EventPageController epc = new EventPageController(); 

            // Set some variables 
            List<EventItemModel> tempList = new List<EventItemModel>();
            int lowerLimit = (pageNumber * recordsPerPage) + 1;
            int upperLimit = lowerLimit + recordsPerPage;
            int counter = 0;

            // Set event display type
            string requestType = "puff";
            
            // Iterate over the event items and get the content for a specific subset
            eventsList.ForEach(item =>
            {
                // Enumerate counter
                counter += 1;

                // Make sure only items within the specified range is affected
                if (counter >= lowerLimit && counter < upperLimit) { 
                   
                    // Get all content based on current node id
                    var itemContent = helper.TypedContent(item.NodeId);

                    // Get all content based on current node id
                    EventItemModel eventItem = EventPageController.GetEventContent(itemContent, helper, requestType, dtfi); 

                   // Make the date value readable
                    //eventItem = EventPageController.CreateNiceDateInfo(itemContent, eventItem, dtfi);

                    // Set the url to be able to make the event item linked
                    eventItem.Url = Umbraco.NiceUrl(item.NodeId);

                    tempList.Add(eventItem);
                }

                // Break out of the loop when upper limit is reached
                if (counter == upperLimit)
                {
                    return;
                }
            });

            // Add the temp list to the model
            model.EventItemsCollection = tempList;            

            // Check whether a Load More button should be displayed
            model.LoadMore = false;
            if (Convert.ToDouble(numberOfRecords) / (recordsPerPage * (pageNumber + 1)) > 1)
            {
                // Set flag and link title
                model.LoadMore = true;
                model.LoadMoreInfo = umbraco.library.GetDictionaryItem("LoadMoreEvents");

                // Enumerate the page number before assigning it to the return model
                model.PageNumber = pageNumber + 1;
            } 

            return PartialView("EventsList", model);
        }
     
    }
}