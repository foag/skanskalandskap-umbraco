﻿
namespace SkanskaLandskap15.Models.Shared
{
    public class FacilityTypeModel 
    {
        public int NodeId { get; set; }
        public string Name { get; set; }
        public string IconPath { get; set; }
        public string NumOfBeds { get; set; }
    }
}