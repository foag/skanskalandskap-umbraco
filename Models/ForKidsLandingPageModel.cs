﻿using System.Collections.Generic;
using SkanskaLandskap15.Models.Masters;
using SkanskaLandskap15.Models.Shared;


namespace SkanskaLandskap15.Models
{
    public class ForKidsLandingPageModel : InteriorMasterModel
    {
        public string SelectedNodes { get; set; }
        //public string MoreTipsTerm { get; set; }
        //public string MoreTipsUrl { get; set; }
        //public SubPagesListModel SubPagesSelected { get; set; }
        //public SubPagesListModel SubPagesRemaining { get; set; }
        //public DownloadableModel DownloadableItem { get; set; }
    }
}