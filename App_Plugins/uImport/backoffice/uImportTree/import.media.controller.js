﻿angular.module("umbraco").directive('file', function () {
    return {
        scope: {
            file: '='
        },
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {

                scope.file = el[0].files[0];
                scope.$apply();

            });
        }
    };
});

angular.module("umbraco").service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function (file, uploadUrl, id, navigationService, notificationsService,node) {
        var fd = new FormData();
        fd.append('file', file);

        var btn = document.getElementById('importBtn');
        var spinner = document.getElementById('loadingSpinner');
        btn.disabled = true;
        btn.className = btn.className + " disabled";
        spinner.style.display = "block";

        $http.post(uploadUrl + '?id=' + id, fd, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        })
        .success(function () {
            spinner.style.display = "none";
            btn.disabled = false;
            btn.className = btn.className.replace(/\bdisabled\b/, '');

            notificationsService.success("Success", "Import finished!");
            navigationService.reloadNode(node);

        })
        .error(function () {
            spinner.style.display = "none";
            btn.disabled = false;
            btn.className = btn.className.replace(/\bdisabled\b/, '');
            notificationsService.error("Error", "Import Failed");
        });
    }
}]);

angular.module("umbraco").controller("uImport.MediaImportController",
        function ($scope, $routeParams, fileUpload, navigationService, notificationsService) {

            $scope.param = {};
            $scope.encodings = [
                { id: 'ibm850', name: 'ibm850' },
                { id: 'utf-8', name: 'utf-8' }
            ];

            $scope.encoding = 'ibm850';

            $scope.uploadFile = function () {

                var id = $scope.currentNode.id;

                var file = $scope.param.file;
                var node = $scope.currentNode;
                var uploadUrl = "/umbraco/backoffice/uImport/uImportApi/Import";

                if (typeof file != 'undefined') {
                    fileUpload.uploadFileToUrl(file, uploadUrl, id, navigationService, notificationsService, node);
                } else {
                    notificationsService.error("Error", "No file selected!");
                }

                
            };

        });