'use strict';

;(function ($) {
  $(function () {
    var $wrapper   = $('#search-wrapper'),
        $searchbox = $('#searchbox');;

    /* Remove the browser's built-in autocomplete capabilities
    ----------------------------------------------------------*/

    // Header search box
    $searchbox.attr('autocomplete', 'off');
    // Also remove possibly search term
    $searchbox.val('');

    // Search results page's search box
    if ($('#searchbox-on-resultpage').length > 0) {
        $('#searchbox-on-resultpage').attr('autocomplete', 'off');
        // Also make search term selected
        $('#searchbox-on-resultpage').focus().select();
    }



    /* Attach click events to the search input elements
    ----------------------------------------------------------*/

    var pageType = '';
    // General search box
    $('#search-button').on('click', function () {
      if ($wrapper.hasClass('is-open') && $searchbox.val()) {
        CreateFormAndSubmit(GetSearchFormData(pageType));
      } else {
        $wrapper.toggleClass('is-open');
      }
    });

    // Search results page's search box
    $('#resultpage-search #search-submit').on('click', function () {
        pageType = 'searchresults';
        CreateFormAndSubmit(GetSearchFormData(pageType))
    });



    /* Get autosuggestions
    ----------------------------------------------------------*/

    // Header search box
    $searchbox.focus(function () {

        // To make the links go away!
        // $('#header-search ul').addClass('select');

        // $searchbox.animate({
        //     width: 180
        // }, 500, function () {
        //     // Animation complete.
        // });
    });

    $searchbox.autocomplete({
        minLength: 2,
        source: "/umbraco/Surface/SearchSurface/CreateAutoSuggestions/?language=" + Global.language,
        focus: function (event, ui) {
            // Prevent searchbox from being updated before enter is pressed (when key-navigating in the list)
            event.preventDefault();
        },
        select: function (event, ui) {
            // Direct to the search processing when user selects in the autosuggestions list
            location.href = GetSearchResultsView() + "?term=" + ui.item.value.replace('&', '%26').replace(' ', '%20') + "&type=auto";
        }
    });

    // Search results page's search box
    $("#searchbox-on-resultpage").autocomplete({
        minLength: 2,
        source: "/umbraco/Surface/SearchSurface/CreateAutoSuggestions/?language=" + Global.language,
        //extraParams: { language: Global.language },
        focus: function (event, ui) {
            // Prevent searchbox from being updated before enter is pressed (when key-navigating in the list)
            event.preventDefault();
        },
        select: function (event, ui) {
            // Direct to the search processing when user picks an item in the autosuggestions list
            location.href = GetSearchResultsView() + "?term=" + ui.item.value.replace('&', '%26').replace(' ', '%20') + "&type=auto";
        }
    });



    /* Reset the header search box when clicking the page
    ----------------------------------------------------------*/
    // $('body').click(function () {
    //
    //     // To make the links go away!
    //     $("#header-search ul").removeClass("select");
    //
    //     var searchbox = $('#searchbox');
    //     searchbox.value = searchbox.defaultValue;
    //
    //     $('#searchbox').animate({
    //         width: 120
    //     }, 500, function () {
    //         // Animation complete.
    //     });
    // });


    // $('#searchbox').click(function (e) {
    //     e.stopPropagation();
    // });



    /* Make it possible to process a search by pressing the Enter key
       (basically needed for IE7/8 only).
    ----------------------------------------------------------*/

    // Header search box
    $('#searchbox').keydown(function (e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            CreateFormAndSubmit(GetSearchFormData(pageType));
            return false;
        }
    });

    // Search results page's search box
    $('#searchbox-on-resultpage').keydown(function (e) {
        pageType = "searchresults";
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            CreateFormAndSubmit(GetSearchFormData(pageType));
            return true;
        }
    });



    /* IE8 and IE7 hack
     ----------------------------------------------------------*/

    // Initiate a global variable
    var isArrowKey = false;

	// This function makes IE7 and IE8 submit the search form when user presses the Enter key.
    // It takes into account if the user presses Enter in the autosuggestion list right before.
    $('input').keydown(function (e) {
        if (browser.lteIE8()) {
            switch (e.keyCode) {
                case 13:    //Enter
                    // Previous keystroke is not an Up or Down arrow so let's submit the form.
                    if (isArrowKey == false) {
                        CreateFormAndSubmit(GetSearchFormData(pageType));
                        return false;
                    }
                    // Enter is pressed to select an item in the history list
                    else {
                        isArrowKey = false;
                        return true;
                    }
                    break;
                case 38:    // Up arrow
                case 40:    // Down arrow
                    isArrowKey = true;
                    return true;
                    break;
                default:
                    return true;
            }
        }
    });



    /* Event triggered by the click on a pagination link (in the SearchResults partial view). It retrieves a new page
       (= set of search results) via ajax and appends it to the specified element. */
    $(document).on("click", "#resultPager a[href]", function () {
        $.ajax({
            url: $(this).attr("href"),
            data: { nodeId: Global.currentNodeId, language: Global.language },
            type: 'GET',
            cache: false,
            success: function (result) {
                $('#paged-result').html(result);
                HighlightSearchTerm();
            }
        });
        return false;
    });





    /* Highlightning of search term(s)
    ----------------------------------------------------------*/

    // Higlight on the search results page
    if ($('#searchbox-on-resultpage').length > 0) {
        HighlightSearchTerm()
    }


    function HighlightSearchTerm() {

        var hls_query = $.trim($('#hidden-search-term').text().replace("*", " "));

        if (typeof (hls_query) != 'undefined') {
            if (hls_query.indexOf(" ") > 0) {
                var multipleTerms = hls_query.split(" ");

                for (i in multipleTerms) {
                    $('#search-result').highlight(multipleTerms[i], 1, 'hls');
                }
            }
            else {
                $("#search-result").highlight(hls_query, 1, "hls");
            }
        }

        // Add the search term as querystring to each search result link
        $('div.search-result-item a.title').attr('href', function (index, attr) {
            return attr + '?hl_term=' + hls_query;
        });
    }


    // Highlight when navigating to a page via a search result link
    if (typeof (Global.hlTerm) != 'undefined') {
        var hl_term = $.trim(Global.hlTerm);

        // Make sure there is a search term
        if (hl_term != "") {

            // Check whether search term can be marked. Make sure there is a contentarea class and no Contour form.
            var markTerm = false;
            if (!browser.lteIE8()) {

                if ($(".content-wrap").size() > 0) {
                //if ($(".content-wrap").size() > 0 && $(".contourNavigation").size() == 0) {
                    markTerm = true;
                }
            }
            // Special treatment for IE8 AND page with Disqus comments
            else {
                if ($(".content-wrap").size() > 0 && $(".contourNavigation").size() == 0 && $("div.comments").size() == 0) {
                    markTerm = true;
                }
            }

            if (markTerm == true) {
                // Multiple search terms
                if (hl_term.indexOf(" ") > 0) {
                    var multipleTerms = hl_term.split(" ");
                    for (i in multipleTerms) {
                        $('.content-wrap').highlight(multipleTerms[i], 1, 'highlight');
                    }
                }
                // A single search term
                else {
                    $(".content-wrap").highlight(hl_term, 1, "highlight");
                }

                // Remove highlightning from headings with a blue background color
                if ($("div.news h2.top").size() > 0) {
                    $("div.news h2.top").find("span.highlight").removeClass('highlight');
                }
                if ($("a h2").size() > 0) {
                    $("a h2").find("span.highlight").removeClass('highlight');
                }
            }
        }
    }
  });
})(jQuery);


/*  NOTE: The \b operator in the regex does not find search terms with åäöÅÄÖ as the initial letter. You could
use \s but this fails to find this type of word when there's no space character right before it (you could
get a good-enough effect by changing to case-sensitive). For some weird reason, \B seem to work the best. */
jQuery.fn.extend({
    highlight: function (search, insensitive, hls_class) {
        // Check for swedish character as the initial letter in the search term.
        if (CheckException(search) == true) {
            var regex = new RegExp("(<[^>]*>)|(\\B" + search.replace(/([-.*+?^${}()|[\]\/\\])/g, "\\$1") + ")", insensitive ? "ig" : "g");
        }
        else {
            var regex = new RegExp("(<[^>]*>)|(\\b" + search.replace(/([-.*+?^${}()|[\]\/\\])/g, "\\$1") + ")", insensitive ? "ig" : "g");
        }
        return this.html(this.html().replace(regex, function (a, b, c) {
            return (a.charAt(0) == "<") ? a : "<span class=\"" + hls_class + "\">" + c + "</span>";
        }));
    }
});



function CheckException(searchTerm) {

    // Initiate return value
    exception = false;

    // Create array containing the problem characters
    var arrExceptions = new Array("å", "ä", "ö", "Å", "Ä", "Ö");

    // Loop through the characters array
    for (i = 0; i < arrExceptions.length; i++) {
        // First letter is a swedish character
        if (searchTerm.indexOf(arrExceptions[i]) == 0) {
            exception = true;
        }
        if (exception == true) {
            break;
        }
    }
    return exception;
}


function GetSearchResultsView() {
    var searchResultFile = "/sokresultat";
    if (Global.language == "EN") {
        searchResultFile = "/search-results";
    }
    return searchResultFile;
}


// Helper function to initiate an object holding the search form properties due to page type
function GetSearchFormData(pageType) {
    var objSearchFormData = new Object();
    objSearchFormData.wrapperId = "search-form-wrapper";
    objSearchFormData.textboxId = "searchbox";
    if (pageType == "searchresults")
    {
        objSearchFormData.textboxId = "searchbox-on-resultpage";
    }
    objSearchFormData.hiddenElementName = "search";
    objSearchFormData.method = "post";
    objSearchFormData.action = GetSearchResultsView();
    return objSearchFormData;
}


// Helper function to create a form, add a hidden element and then submit the form
function CreateFormAndSubmit(objFormData) {
    var submitForm = GetNewSubmitForm(objFormData.wrapperId);
    var searchBox = document.getElementById(objFormData.textboxId);
    var searchTerm = TrimAllWords(searchBox.value);
    if (searchTerm != "") {
        CreateNewFormElement(submitForm, objFormData.hiddenElementName, searchTerm);
        submitForm.method = objFormData.method;
        // Add querystring to have the search term registered at Google Analytics
        submitForm.action = objFormData.action + "?term=" + searchTerm.replace('&', '%26');
        submitForm.submit();
    }
}


// Helper function to create and place the form
function GetNewSubmitForm(wrapperId) {
    var submitForm = document.createElement("form");
    var searchWrapper = document.getElementById(wrapperId);
    searchWrapper.appendChild(submitForm);
    return submitForm;
}


// Helper function to add a hidden element to the form
function CreateNewFormElement(inputForm, elementName, elementValue) {
    var newElement = document.createElement('input');
    newElement.setAttribute('type', 'hidden');
    newElement.setAttribute('name', elementName);
    inputForm.appendChild(newElement);
    newElement.value = elementValue;
    return newElement;
}
