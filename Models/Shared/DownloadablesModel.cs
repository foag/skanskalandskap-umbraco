﻿using System.Collections.Generic;


namespace SkanskaLandskap15.Models.Shared
{
    public class DownloadablesModel
    {
        public List<DownloadableModel> DownloadablesCollection { get; set; }
    }
}
