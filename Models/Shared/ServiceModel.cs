﻿using System.Web;

namespace SkanskaLandskap15.Models.Shared
{
    public class ServiceModel
    {
        public int NodeId { get; set; }
        public string Title { get; set; }
        public IHtmlString Intro { get; set; }
        public string ServiceType { get; set; }
        public ServiceStandardModel StandardService { get; set; }
        public ServiceBusStopModel BusStopService { get; set; }
    }
}
