﻿
namespace SkanskaLandskap15.Models.Shared
{
    public class DownloadableModel
    {
        public string Url { get; set; }       
        public string UrlTitle { get; set; }
        public string FileExtension { get; set; }
        public string FileSize { get; set; }
    }
}
