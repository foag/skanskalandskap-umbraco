﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Controllers.SurfaceControllers;
using SkanskaLandskap15.Models;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Web;


namespace SkanskaLandskap15.Controllers
{
    public class GeneralSubPageController : InteriorMasterController
    {

        public ActionResult GeneralSubPage()
        {
            var model = new GeneralSubPageModel();

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language ("SV" or "EN") 
            string currentLanguage = helper.GetDictionaryValue("Language");

            // UrlName and Content type
            model.UrlName = CurrentPage.UrlName;
            model.ContentType = CurrentPage.DocumentTypeAlias;

            // Create controller references
            MediaSurfaceController msc = new MediaSurfaceController();
            PuffsSurfaceController psc = new PuffsSurfaceController();


            /* SLIDESHOW  */

            // Images for slideshow        
            string imagePicker = CurrentPage.GetPropertyValue<string>(MyConstants.ImagePickerPropertyAlias);

            // Initiate flag
            model.SlideshowExists = false;

            // Make sure images exists (required but you never know...)
            if (!string.IsNullOrWhiteSpace(imagePicker))
            {
                // Set flag          
                model.SlideshowExists = true;

                // Get a populated Slideshow object and add it to the return model
                model.Slideshow = msc.GetSlideshow(imagePicker, helper);
            }



            /* TEXTS */                    

            // Richtext content
            model.Text = CurrentPage.GetPropertyValue<IHtmlString>(MyConstants.TextPropertyAlias);



            /* FORM  (FÖR SKOLOR ONLY)  */
           
            FormModel formModel = new FormModel();
            formModel.FormGuid = CurrentPage.GetPropertyValue<Guid>("FormPicker");
            model.UmbracoForm = formModel;
            //umbraco.library.RenderMacroContent(src.BodyText, src.Id)
            model.TitleForm = CurrentPage.GetPropertyValue<string>("TitleForm");
            model.IntroForm = CurrentPage.GetPropertyValue<IHtmlString>("IntroForm");



            /* AREA(S) (OPTIONAL TIPS) */

            // Explicitly set tips flag
            model.AreasTipsExists = false;

            // Initiate a puff list
            List<SubPagePuffModel> selectedAreasCollection = new List<SubPagePuffModel>();

            // Get a string of comma-separated sub page node id:s
            var strAreaNodes = CurrentPage.GetPropertyValue<string>(MyConstants.TipsAreasPropertyAlias);

            // Check whether at least one sub page is selected
            if (!string.IsNullOrEmpty(strAreaNodes))
            {
                // Initiate a SubPagesList object
                var subPages = new SubPagesListModel();              

                // Get a list of selected puffs/sub pages                 
                selectedAreasCollection = psc.GetAreaPuffs(strAreaNodes, helper);

                // Make sure there is at least one published sub page node in the collection.
                // (You're able to select unpublished nodes in MNTP.)
                if (selectedAreasCollection.Any())
                {
                    // Set flag
                    model.AreasTipsExists = true;

                    // Add list of selected puffs to the sub pages list object
                    subPages.SelectedAreasCollection = selectedAreasCollection;

                    // Add sub pages object to return model
                    model.SubPagesAreas = subPages;
                }
            }



            /* PUFFS ARE RETRIEVED FROM THE VIEW */

            // Here we need to check whether puffs exists, though. This is to be able to handle
            // the common section title for puffs and downloads.
            model.PuffsExists = psc.PuffsExists(CurrentPage.Id, helper);
                       


            /* DOWNLOADS  */

            // Pdf:s for the Download section        
            string downloadsPicker = CurrentPage.GetPropertyValue<string>(MyConstants.DownloadsPropertyAlias);

            // Initiate flag
            model.DownloadsExists = false;

            // Check whether downloads is selected
            if (!string.IsNullOrWhiteSpace(downloadsPicker))
            {              
                // Get the media files collection and add to return model
                model.MediaFiles = msc.GetDownloadFiles(downloadsPicker, helper, currentLanguage);

                // Make sure at least one of the selected files actually exists in Media 
                if (model.MediaFiles.MediaFilesCollection.Count() > 0)
                {
                    // Set flag          
                    model.DownloadsExists = true;
                }
            }


            // Puffs and/or downloads exists so get the common title
            if (model.PuffsExists || model.DownloadsExists)
            {
                model.PuffsAndDownloadsTitle = helper.GetDictionaryValue("Other");
            }          
          


            /* ACCOMMODATIONS IN THE AREA (OPTIONAL TIPS) */

            // Explicitly set tips flag
            model.AccommodationTipsExists = false;

            // Initiate a puff list
            List<SubPagePuffModel> selectedCollection = new List<SubPagePuffModel>();

            // Get a string of comma-separated sub page node id:s
            var strNodes = CurrentPage.GetPropertyValue<string>(MyConstants.TipsAccommodationsPropertyAlias);

            // Check whether at least one sub page is selected
            if (!string.IsNullOrEmpty(strNodes))
            {
                // Initiate a SubPagesList object
                var subPages = new SubPagesListModel();

                // Get a list of selected puffs/sub pages 
                selectedCollection = psc.GetSelectedAccommodationSubPages(strNodes, helper);

                // Make sure there is at least one published sub page node in the collection.
                // (You're able to select unpublished nodes in MNTP.)
                if (selectedCollection.Any())
                {
                    // Set flag
                    model.AccommodationTipsExists = true;

                    // Add list of selected puffs to the sub pages list object
                    subPages.SelectedAccommodationsCollection = selectedCollection;

                    // Set a flag to indicate this special type of puffs
                    subPages.AccommodationsInTheAreaType = true;

                    // Add sub pages object to return model
                    model.SubPagesAccommodations = subPages;
                }
            }

            // Get fixed value for the accommodation tips section
            if (model.AccommodationTipsExists)
            {
                model.AccommodationsTipsTitle = helper.GetDictionaryValue("AccommodationsInTheArea");
            }            

            
            return View(model);
        } 
    }
}