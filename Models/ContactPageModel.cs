﻿using System.Collections.Generic;
using System.Web;
using SkanskaLandskap15.Models.Masters;
using SkanskaLandskap15.Models.Shared;


namespace SkanskaLandskap15.Models
{
    public class ContactPageModel : InteriorMasterModel
    {
        public string SubPageTag { get; set; }
        public dynamic ImagePicker { get; set; }

        public bool ContactInfoExists { get; set; }
        public string ContactInfoTitle { get; set; }
        public List<PuffModel> ContactInfoCollection { get; set; }

        public bool ContactsExists { get; set; }
        public string ContactPersonsTitle { get; set; }
        public List<ContactModel> ContactsCollection { get; set; }
    }
}