﻿
 
// Global browser detection functions
var browser = {
    isIE8: function () {
        var flagIsIE8 = false;
        if (navigator.userAgent.toLowerCase().indexOf('msie') != -1) {
            if (!document.createElement('SVG').getAttributeNS) {
                if (document.createElement('DATA').getAttributeNS) {
                    flagIsIE8 = true;
                }
            }
        }
        return flagIsIE8;
    },
    lteIE8: function(){
        var flagLteIE8 = false;
        if (navigator.userAgent.toLowerCase().indexOf('msie') != -1) {
            if (!document.createElement('SVG').getAttributeNS) {                
                flagLteIE8 = true;
            }
        }
        return flagLteIE8;
    }
};


// Trim of string containing multiple words
function TrimAllWords(str) {
    while (str.substring(0, 1) == ' ') {
        str = str.substring(1, str.length);
    }
    while (str.substring(str.length - 1, str.length) == ' ') {
        str = str.substring(0, str.length - 1);
    }
    return str;
}


// String format function
String.prototype.format = function () {
    var s = this,
        i = arguments.length;

    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
};


// Decode html including strings containing html-coded Swedish characters
function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}




/* INTERACTIVE PANEL */

// Initiate panel
var sidebar = $('#sidebar').sliiide({ place: 'top', exit_selector: '.slider-exit', toggle: '#panel-toggle', no_scroll: false, body_slide: false });

// Open panel
$(document).on('click', '#panel-toggle', function (e) {
    sidebar.activate();;
});

// Close panel by clicking icon
$(document).on('click', '#slider-exit', function (e) {
    sidebar.deactivate();    
});

//// Close panel by clicking the panel - but...
//$(document).on('click', '#sidebar', function (e) {
//    sidebar.deactivate();
//});

//// ...make exception for the map...
//$(".interactive-map").click(function (e) {
//    e.stopPropagation(); 
//});


$(document).on('click', '.interactive-map', function (e) {
    e.stopPropagation();
    //sidebar.activate();
    //$('.panel-container div').css('visibility', '');
});


// Note: Clicking one of the EasyTabs tabs - or any button within the panel - makes the sliiide panel 
// deactivate itself. Reactivating and removing the css hidden property prevents this behavour. 

// Tabs...
$(document).on('click', '#tab-container .etabs .tab', function (e) {
    e.stopPropagation();
    sidebar.activate();
    $('.panel-container div').css('visibility', '');
});

// Buttons...
$(document).on('click', '.display-buttons button', function (e) {
    e.stopPropagation();
    sidebar.activate();
    $('.panel-container div').css('visibility', '');
});



//var settings = {
//    // The selector for the sidebar toggle to activate or deactivate the menu.
//    // A click listener will be added to this element.
//    toggle: "#panel-toggle",
//    // The selector for an exit button in the div if needed. When the exit element
//    // is clicked the sidebar will deactivate (suitable for an exit element inside
//    // the side bar.
//    exit_selector: ".slider-exit",
//    // How long it takes to slide the menu.
//    animation_duration: "0.5s",
//    // Where is the menu sliding from, possible options are (left | right | top | bottom).
//    place: "top",
//    // Animation curve for the sliding animation.
//    animation_curve: "cubic-bezier(0.54, 0.01, 0.57, 1.03)",
//    // Set it to true if you want to use the effect where the entire page slides and not just the div.
//    body_slide: false,
//    // Set to true if you want the scrolling disabled while the menu is active.
//    no_scroll: true,
//};
//$('#sidebar').sliiide(settings); //initialize sliiide



(function ($) {
    

    /* EASYTABS */

    // We're on a page using the EasyTabs plugin
    if ($("#tab-container").length > 0) {
        $('#tab-container').easytabs();      
    }       


    /* DOWNLOADABLE CONTENT */

    // We're on the Downloadable Content page
    if ($("#download-list").length > 0) {
            
        // Manage tab system
        $('#tabs').tabs({
            hide: 'fade',
            show: 'fade',
            fx: { opacity: 'toggle' }
        });
    }

})(jQuery);