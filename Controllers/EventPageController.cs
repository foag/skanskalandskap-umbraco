﻿using System;
using System.Globalization;
using System.Web;
using System.Web.Mvc;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Controllers.SurfaceControllers;
using SkanskaLandskap15.Models;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Core.Models;
using Umbraco.Web;


namespace SkanskaLandskap15.Controllers
{
    public class EventPageController : InteriorMasterController
    {

        public ActionResult EventPage()
        {
            // Initiate the return model
            var model = new EventPageModel();

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);
            
            // Set event display type
            string requestType = "full";

            // Get the event item's content            
            model.EventItem = GetEventContent(CurrentPage, helper, requestType);

            model.PageTitleTerm = helper.GetDictionaryValue("EventPageTitle");

            return View(model);
        }




        /*******************************************************************************
          Note: This method is called from NewsAndEventsLandingPageController as well
        ********************************************************************************/
        public static EventItemModel GetEventContent(IPublishedContent content, UmbracoHelper helper, string requestType, DateTimeFormatInfo dtfi = null)
        {

            // Initiate the return model
            EventItemModel model = new EventItemModel();
            
            //Create controller reference
            MediaSurfaceController msc = new MediaSurfaceController();
                      
            // Image 
            model.EventImage = helper.Media(content.GetPropertyValue(MyConstants.ImagePickerPropertyAlias));

           
            // Get a populated Media image object
            ImageModel objImage = msc.GetMediaImageProperties(model.EventImage, helper);

            // Create a nice image caption
            if (objImage.Caption != string.Empty)
            {
                model.ImageCaption = objImage.Caption;

                if (objImage.Photographer != string.Empty)
                {
                    model.ImageCaption += " " + objImage.Photographer;
                }
            }
            else
            {
                if (objImage.Photographer != string.Empty)
                {
                    model.ImageCaption = objImage.Photographer;
                }
            }
          
                                
            // Construct date and time information due to request type
            switch (requestType)
            {
                case "full":
                    model = CreateDateTimeInfo(content, model, helper);
                    break;
                default:
                    model = CreateNiceEventDates(content, model, dtfi);
                    break;
            }    
           

            // Title
            model.Title = content.GetPropertyValue<String>(MyConstants.TitlePropertyAlias);
            
            // Location
            dynamic pickedItems = content.GetPropertyValue(MyConstants.AreaPickerPropertyAlias);
                   
            bool areaPicked = false;
            foreach (var item in pickedItems.PickedKeys)
            {
                // A recreational area was selected
                areaPicked = true;
                model.LocationName = helper.TypedContent(item).Name; 
                model.LocationUrl = helper.NiceUrl(Convert.ToInt32(item));
                break;
            }
            if (areaPicked == false)
            {
                // Free-text location
                model.LocationName = content.GetPropertyValue<String>(MyConstants.LocationFreeTextPropertyAlias).Trim();

                // Explicitly set the Url property as empty
                model.LocationUrl = String.Empty;
            }
                          
            // Text
            switch(requestType)
            {
                case "full":
                    model.Text = content.GetPropertyValue<IHtmlString>(MyConstants.TextPropertyAlias);   
                    break;
                case "puff":
                    model.Intro = UmbracoHelperExtensions.TruncateAtWord(content.GetPropertyValue<string>(MyConstants.IntroPropertyAlias), 155); 
                    break;
                case "short":
                    break;
            }    

            // Get the time and place headers
            model.DateTerm = helper.GetDictionaryValue("Date");
            model.TimeTerm = helper.GetDictionaryValue("Time");
            model.LocationTerm = helper.GetDictionaryValue("Location");

            model.Url = helper.NiceUrl(content.Id);
            model.UrlTitleTerm = helper.GetDictionaryValue("ReadMore");

            return model;
        }

        
             
        public static EventItemModel CreateDateTimeInfo(IPublishedContent content, EventItemModel model, UmbracoHelper helper)
        {
            // Get Start and End date and time (date is required)
            DateTime eventStart = content.GetPropertyValue<DateTime>(MyConstants.EventStartPropertyAlias);
            DateTime eventEnd = content.GetPropertyValue<DateTime>(MyConstants.EventEndPropertyAlias);

            // Get Hide End date and time flag
            //bool hideEventEnd = content.GetPropertyValue<bool>(MyConstants.HideEventEndPropertyAlias);

            // Split the datetime values into date and time strings           
            string[] startDateTimeParts = eventStart.ToString().Split(' ');
            string startDateValue = startDateTimeParts[0];
            string startTimeValue = startDateTimeParts[1];

            string[] endDateTimeParts = eventEnd.ToString().Split(' ');
            string endDateValue = endDateTimeParts[0];
            string endTimeValue = endDateTimeParts[1];

            // Set month and day
            string startMonthValue = eventStart.Month.ToString();
            string startDayValue = eventStart.Day.ToString();

            string endMonthValue = eventEnd.Month.ToString();
            string endDayValue = eventEnd.Day.ToString();
           
            // Set hour and minute
            string startHourValue = eventStart.Hour.ToString();
            string startMinuteValue = eventStart.Minute.ToString();
            
            // Add a zero for minutes less than 10
            if(Convert.ToInt32(startMinuteValue) < 10)
            {
                startMinuteValue = "0" + startMinuteValue;
            }

            string endHourValue = eventEnd.Hour.ToString();
            string endMinuteValue = eventEnd.Minute.ToString();
            
            // Add zero for minutes less than 10
            if (Convert.ToInt32(endMinuteValue) < 10)
            {
                endMinuteValue = "0" + endMinuteValue;
            }

            // Set the start date information
            model.DateInfo = startDayValue + "/" + startMonthValue;
            
            //// Start and End date is not the same and End date should not be hidden...
            //if(startDateValue != endDateValue && hideEventEnd == false)
            // Start and End date is not the same...
            if (startDateValue != endDateValue)
            {
                //...so add End date
                model.DateInfo += "-" + endDayValue + "/" + endMonthValue;
            } 
            
            // Initiate time value as empty string
            model.TimeInfo = String.Empty;

            // Check whether time info should be displayed            
            if (startTimeValue != "00:00:00")
            {
                // Set the Start time information
                model.TimeInfo = startHourValue + ":" + startMinuteValue;

                //// End date should not be hidden
                //if (hideEventEnd == false)
                //{
                //    //...so add End time
                //    model.TimeInfo += "-" + endHourValue + ":" + endMinuteValue;
                //}
                               
                //...and add End time
                model.TimeInfo += "-" + endHourValue + ":" + endMinuteValue;                
            }

            // Finally check whether current event is outdated
            model.OutdatedInfo = String.Empty;
            if (Convert.ToDateTime(endDateValue) < DateTime.Now)
            {
                model.OutdatedInfo = helper.GetDictionaryValue("OutdatedEventMessage") + " " + endDateValue;
            }

            return model;
        }




        public static EventItemModel CreateNiceEventDates(IPublishedContent content, EventItemModel model, DateTimeFormatInfo dtfi)
        {               

            // Get Start and End date/time (date is required)
            DateTime eventStart = content.GetPropertyValue<DateTime>(MyConstants.EventStartPropertyAlias);
            DateTime eventEnd = content.GetPropertyValue<DateTime>(MyConstants.EventEndPropertyAlias);

            // Split the datetime values into date strings           
            string[] startDateTimeParts = eventStart.ToString().Split(' ');
            string startDateValue = startDateTimeParts[0];
            string[] endDateTimeParts = eventEnd.ToString().Split(' ');
            string endDateValue = endDateTimeParts[0];
            
            // Set month and day
            string startMonthValue = dtfi.GetMonthName(Convert.ToInt32(eventStart.Month)).ToString().Substring(0, 3);
            string startDayValue = eventStart.Day.ToString();
            string endMonthValue = dtfi.GetMonthName(Convert.ToInt32(eventEnd.Month)).ToString().Substring(0, 3);
            string endDayValue = eventEnd.Day.ToString();

            // Initiate the date information
            model.DateInfo = startDayValue; 

            // Start and End date is not the same 
            if (startDateValue != endDateValue)
            {
                // Same month
                if (startMonthValue == endMonthValue)
                {
                    model.DateInfo += "-" + endDayValue + " " + endMonthValue;
                }
                // Different months
                else
                {
                    model.DateInfo += " " + startMonthValue + " - " + endDayValue + " " + endMonthValue;
                }
            }
            // Start and End date is the same
            else if (startDateValue == endDateValue) 
            {
                model.DateInfo += " " + startMonthValue;
            }

            return model;
        }
    }
}