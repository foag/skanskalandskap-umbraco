﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Models;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;




namespace SkanskaLandskap15.Controllers
{

    public class NewsAndEventsLandingPageController : InteriorMasterController
    {

        ///<summary>
        /// Fetches all information for the News & Events landing page including lists of sub pages.
        /// </summary>      
        /// <returns>A NewsAndEventsLandingPage object.</returns> 
        public ActionResult NewsAndEventsLandingPage()
        {

            // Initiate the return model
            var model = new NewsAndEventsLandingPageModel();

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language ("SV" or "EN")
            var currentLanguage = helper.GetDictionaryValue("Language");

            // Assign the content type
            model.ContentType = CurrentPage.DocumentTypeAlias;


            /* EVENTS LIST */

            // Create an empty Events List object
            EventsListModel modelEventsList = new EventsListModel();

            // Get the Events List model
            modelEventsList = GetEventsList(modelEventsList, helper, currentLanguage);

            // Should the event items collection be empty, add a message
            if (modelEventsList.EventItemsCollection.Count() == 0)
            {
                modelEventsList.NoEventsMessage = helper.GetDictionaryValue("NoEventsMessage");
            }

            // Add the Events List object to the return model         
            model.EventsList = modelEventsList;

            // Add some fixed texts
            model.EventsTitleTerm = CurrentPage.GetPropertyValue<string>(MyConstants.TitleTipsPropertyAlias);
            model.MoreEventsTerm = helper.GetDictionaryValue("MoreEvents");


            /* NEWS LIST */

            // Create an empty News List object
            NewsListModel modelNewsList = new NewsListModel();

            // Get the News List model, including the default subset of news items
            modelNewsList = GetNewsList(modelNewsList, helper, currentLanguage);

            // Should the news items collection be empty, add a message
            if (modelNewsList.NewsItemsCollection.Count() == 0)
            {
                modelNewsList.NoNewsMessage = helper.GetDictionaryValue("NoNewsMessage");
            }

            // Add the News List object to the return model         
            model.NewsList = modelNewsList;

            // Add some fixed texts
            model.NewsTitleTerm = CurrentPage.GetPropertyValue<string>(MyConstants.TitleListPropertyAlias);
            model.MoreNewsTerm = helper.GetDictionaryValue("MoreTips");

            return View(model);
        }




        protected EventsListModel GetEventsList(EventsListModel model, UmbracoHelper helper, string language)
        {

            // Explicitly set the page number
            model.PageNumber = 1;

            // Get the default number of events to display
            model.RecordsPerPage = GetRecordsPerPage(CurrentPage, "events");

            // Create a Swedish culture object
            CultureInfo swedish = new CultureInfo("sv-SE");

            // Create a DateTimeFormatInfo object to make it possible to get month name as text...
            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();

            // ...in Swedish, provided this is current language 
            if (language == "SV")
            {
                dtfi = swedish.DateTimeFormat;
            }

            // Get all content based on current (landing) page node
            var content = helper.TypedContent(CurrentPage.Id);

            // Get all published and not outdated events (just the node id:s) 
            IEnumerable<EventItemModel> eventsList = content
                .Descendants()
                .Where(c => c.DocumentTypeAlias == MyConstants.EventPageDocTypeAlias && c.GetPropertyValue<DateTime>(MyConstants.EventEndPropertyAlias) >= DateTime.Now)
                .OrderBy(c => c.GetPropertyValue<DateTime>(MyConstants.EventStartPropertyAlias))
                .Select(obj => new EventItemModel()
                {
                    NodeId = obj.Id
                }).ToList();

            // Create an empty temporary list for Event Items
            List<EventItemModel> tempList = new List<EventItemModel>();

            // Set event display type
            string requestType = "puff";

            // Iterate over the event items and get their properties
            eventsList.ForEach(item =>
            {
                // Get all content based on current node id
                var itemContent = helper.TypedContent(item.NodeId);

                // Get current node's content
                EventItemModel eventsItem = EventPageController.GetEventContent(itemContent, helper, requestType, dtfi);

                // Set the url to be able to make the event item linked
                eventsItem.Url = helper.NiceUrl(item.NodeId);

                // Add event item to the temp list
                tempList.Add(eventsItem);
            });

            // Add the temporary list to the Events List object
            model.EventItemsCollection = tempList;

            // Check whether a Load more button should be displayed
            bool loadMore = CheckLoadMore(content, model.RecordsPerPage, "events");
            model.LoadMore = loadMore;

            // If true, add the Load more link's title
            if (loadMore)
            {
                model.LoadMoreInfo = helper.GetDictionaryValue("MoreEvents");
            }

            return model;
        }



        protected NewsListModel GetNewsList(NewsListModel model, UmbracoHelper helper, string language)
        {
            // Explicitly set the page number
            model.PageNumber = 1;

            // Get the default number of news items to display
            int recordsPerPage = GetRecordsPerPage(CurrentPage, "news");
                        
            // Get all content based on current (landing) page node
            var content = helper.TypedContent(CurrentPage.Id);

            // Get the specified subset of published news items (just the node id:s)
            IEnumerable<NewsItemModel> newsList = content
                .Descendants()
                .Where(c => c.DocumentTypeAlias == MyConstants.NewsPageDocTypeAlias)
                .OrderByDescending(c => c.GetPropertyValue<DateTime>(MyConstants.PublishingDatePropertyAlias))
                .Take(recordsPerPage)
                .Select(obj => new NewsItemModel()
                {
                    NodeId = obj.Id
                }).ToList();

            // Create a temporary list
            List<NewsItemModel> tempList = new List<NewsItemModel>();

            // Set event display type
            string requestType = "puff";
           
            // Iterate over the news items and get the properties
            newsList.ForEach(item =>
            {
                // Get all content based on current node id
                var itemContent = helper.TypedContent(item.NodeId);

                // Get current node's content
                NewsItemModel newsItem = NewsPageController.GetNewsContent(itemContent, helper, requestType);

                // Set the url to be able to make the news item linked
                newsItem.Url = helper.NiceUrl(item.NodeId);

                // Add news item to the temp list
                tempList.Add(newsItem);
            });

            // Add the temporary list to the News List object
            model.NewsItemsCollection = tempList;

            // Check whether a Load more button should be displayed
            bool loadMore = CheckLoadMore(content, recordsPerPage, "news");
            model.LoadMore = loadMore;

            // If true, add the Load more link's title
            if (loadMore)
            {
                model.LoadMoreInfo = helper.GetDictionaryValue("LoadMoreNews");
            }
            return model;
        }     



        protected bool CheckLoadMore(IPublishedContent content, int recordsPerPage, string typeOfItems)
        {
            // Initiate return value
            bool loadMore = false;

            // Initiate variable
            int numberOfRecords = 0;

            // Get node id:s due to type of items (events or news) 
            switch(typeOfItems)
            {
                case "events":
                    // Get all published and not outdated events
                    IEnumerable<EventItemModel> eventsList = content
                        .Descendants()
                        .Where(c => c.DocumentTypeAlias == MyConstants.EventPageDocTypeAlias && c.GetPropertyValue<DateTime>(MyConstants.EventEndPropertyAlias) >= DateTime.Now)
                        .Select(obj => new EventItemModel()
                        {
                            NodeId = obj.Id
                        }).ToList();

                    // Assign the total number of published items to a variable
                    numberOfRecords = Convert.ToInt32(eventsList.Count());
                    break;

                case "news":
                    // Get all (published) news items
                    IEnumerable<NewsItemModel> newsList = content
                        .Descendants()
                        .Where(c => c.DocumentTypeAlias == MyConstants.NewsPageDocTypeAlias)
                        .Select(obj => new NewsItemModel()
                        {
                            NodeId = obj.Id
                        }).ToList();

                    // Assign the total number of published items to a variable
                    numberOfRecords = Convert.ToInt32(newsList.Count());
                    break;
            } 

            // Check whether a Load More button should be displayed          
            if (numberOfRecords > recordsPerPage)
            {
                loadMore = true;
            }

            return loadMore;
        }



        public int GetRecordsPerPage(IPublishedContent content, string typeOfItems)
        {
            // Initiate return value
            Int32 recordsPerPage = 0;

            // Get pagination subset's size due to type of items (events or news)
            switch(typeOfItems)
            {
                case "events":
                    recordsPerPage = MyConstants.EventRecordsPerPage;                    
                    break;

                case "news":

                    // Get pagination value from backoffice
                    recordsPerPage = content.GetPropertyValue<Int32>(MyConstants.PaginationNewsPropertyAlias);

                    // If no value was given, set default value
                    if (recordsPerPage == 0)
                    {
                        recordsPerPage = MyConstants.NewsRecordsPerPage;
                    }
                    break;
            }           
            return recordsPerPage;
        }

    }
}