﻿using System.Collections.Generic;
using System.Web;
using SkanskaLandskap15.Models.Shared;


namespace SkanskaLandskap15.Models.Masters
{
    public class MasterModel 
    {
        public string SiteTitle { get; set; }
        public string SiteDescription { get; set; }
        public string SiteLogo { get; set; }
        public string FoundationName { get; set; }
        public string HomeTerm { get; set; }
        
        public string LanguageSwitchUrl { get; set; }
        public string LanguageSwitchText { get; set; }
        public string SearchUrlName { get; set; }
        public string SearchPlaceHolderTerm { get; set; }
        
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Intro { get; set; }
        public dynamic TopImage { get; set; }
        public GuestRatingsModel GuestRatings { get; set; }

        public string TitleHeader { get; set; }
        public string IntroLanding { get; set; }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }       
        public string UrlName { get; set; }
        public string Language { get; set; }
        public string HighlightTerm { get; set; }
        //public int SubMenuCacheDuration { get; set; }

        public string SitePart { get; set; }
        public string ContentType { get; set; }
        public int Level { get; set; }
        public string Slug { get; set; }
       
        public string FoundationText { get; set; }
        public IHtmlString ContactText { get; set; }
        public bool LinksExists { get; set; }
        public List<LinkModel> LinksCollection { get; set; }
        public string FooterFoundationTitleTerm { get; set; }
        public string FooterContactTitleTerm { get; set; }
             
        public IEnumerable<NavigationItemModel> MainNavigation { get; set; }
        
        public string GoogleAnalyticsKey { get; set; }
        public string NoJavaScriptWarning { get; set; }
    }
}