﻿
namespace SkanskaLandskap15.Models.Shared
{
    public class MapMarkerModel
    {       
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Title { get; set; }
        public string Intro { get; set; } 
        public string Content { get; set; }           
        public string Icon { get; set; }
        public string Tag { get; set; }   
        public int NodeId { get; set; }
        public string UrlName { get; set; }
        public string ResRobotZID { get; set; }
        public string AreaActivities { get; set; }
    }
}
