﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using RJP.MultiUrlPicker.Models;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Core.Dynamics;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;




namespace SkanskaLandskap15.Controllers.SurfaceControllers
{
    public class MediaSurfaceController : SurfaceController
    {


        ///<summary>
        /// Fetches the properties for a Slideshow module (typically located on the start page 
        /// but might be added to any page). 
        /// </summary>       
        /// <returns>The populated Slideshow object.</returns>      
        [ChildActionOnly]
        public ActionResult SlideshowModule()
        {  

            // Initiate the return model
            var model = new SlideshowModel();

            // Get hidden flag
            model.Hidden = CurrentPage.GetPropertyValue<bool>(MyConstants.PublishSlideshowModulePropertyAlias);

            // Make sure editor has not marked slideshow module as hidden
            if (!model.Hidden) 
            {              
                // Get an Umbraco helper
                var helper = new UmbracoHelper(UmbracoContext.Current);

                // Get read more-link title from dictionary
                model.ReadMoreTerm = helper.GetDictionaryValue("ReadMore");

                // Get module title
                model.Title = CurrentPage.GetPropertyValue<string>(MyConstants.TitleSlideshowModulePropertyAlias).Trim();

                // Get the slides collection (Nested Content datatype)
                var slides = CurrentPage.GetPropertyValue<IEnumerable<IPublishedContent>>(MyConstants.ContentSlideshowModulePropertyAlias);

                // Initiate flag
                model.SlidesExists = false;

                // Make sure at least one slide exist
                if(slides != null && slides.Count() > 0)
                {
                    // Set flag
                    model.SlidesExists = true;

                    // Initiate a temp list
                    var tempListSlides = new List<SlideModel>(); 

                    // Get controller reference
                    PuffsSurfaceController psc = new PuffsSurfaceController();

                    // Iterate over the slides
                    foreach(var slide in slides){

                        // Instanciate a Slide object
                        var objSlide = new SlideModel();

                        // Instanciate an Image object
                        var objImage = new ImageModel();

                        // Get image
                        objImage.MediaNode = helper.Media(slide.GetPropertyValue<string>(MyConstants.ImagePickerSlideshowModulePropertyAlias));

                        // Add Image object to Slide object
                        objSlide.Image = objImage;

                        // Get subtitle
                        objSlide.SubTitle = slide.GetPropertyValue<string>(MyConstants.SubtitleSlideshowModulePropertyAlias);
                        
                        // Get text
                        objSlide.Text = slide.GetPropertyValue<string>(MyConstants.TextPropertyAlias);

                        // Get link(s)
                        var links = slide.GetPropertyValue<MultiUrls>(MyConstants.MultiUrlPickerPropertyAlias);
                                      
                        // Initiate links flag
                        objSlide.LinksExists = false;

                        // Check whether links exists (optional)
                        if (links.Any())
                        {
                            // Set flag
                            objSlide.LinksExists = true;

                            // Get links and add to the slide object
                            objSlide.LinksCollection = psc.GetLinks(links, helper);

                            // Add links to slide object
                            //objSlide.LinksCollection = tempListLinks;

                        }
                        // Add slide to temp list
                        tempListSlides.Add(objSlide);
                    }

                    // Add slides list to return model
                    model.SlidesCollection = tempListSlides;
                }
            }

            // Send the Slideshow model to the partial
            return PartialView("SlideshowModule", model);
        }



        ///<summary>
        /// Fetches the properties for a standard slideshow. 
        /// </summary> 
        /// <param name="imagePicker">Comma-separated list of Media node id:s.</param>
        /// <param name="helper">Instance of the Umbraco helper. Optional.</param>
        /// <returns>The populated Slideshow object.</returns>      
        public SlideshowModel GetSlideshow(string imagePicker, UmbracoHelper helper = null)
        {

            // Initiate the return model
            SlideshowModel model = new SlideshowModel();
            
            // If helper parameter is missing, create an Umbraco helper
            if (helper == null)
            {
                helper = new UmbracoHelper(UmbracoContext.Current);
            }

            // Get current language ("SV" or "EN")
            var currentLanguage = helper.GetDictionaryValue("Language");

            // Get photo term from dictionary
            string photo = helper.GetDictionaryValue("Photo");

            // Get list of media node id:s           
            var imagesList = imagePicker.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                             
            // Create dynamic list of media nodes based on the node id:s 
            dynamic listImages = helper.Media(imagesList);

            // Create a temp list
            List<SlideModel> listTemp = new List<SlideModel>();
                
            // Iterate over the dynamic list and get the image properties
            foreach (var item in listImages)
            {
                // Instanciate slide object
                SlideModel objSlide = new SlideModel();

                ImageModel objImage = GetMediaImageProperties(item, helper, photo, currentLanguage);
                               
                //// Initiate image object
                //ImageModel objImage = null;

                //// Populate Image object due to current language
                //switch(currentLanguage)
                //{ 
                //    case "SV":
                //        objImage = new ImageModel
                //        {
                //            MediaNode = item,
                //            Name = item.Name,                       
                //            Caption = item.GetPropertyValue<string>(MyConstants.MediaFileCaptionSV).Trim(),
                //            Photographer = item.GetPropertyValue<string>(MyConstants.MediaFilePhotographer.Trim())
                //        };
                //        break;

                //    case "EN":
                //        objImage = new ImageModel
                //        {
                //            MediaNode = item,
                //            Name = item.Name,
                //            Caption = item.GetPropertyValue<string>(MyConstants.MediaFileCaptionEN).Trim(),
                //            Photographer = item.GetPropertyValue<string>(MyConstants.MediaFilePhotographer.Trim())
                //        };
                //        break;
                //}

                //// Add dictionary term
                //if(objImage.Photographer != string.Empty)
                //{
                //    objImage.Photographer = photo + ": " + objImage.Photographer;
                //}

                // Add image object to slide object
                objSlide.Image = objImage;

                // Add slide object to temp list
                listTemp.Add(objSlide);
            }

            // Add temp list to the Images object
            model.SlidesCollection = listTemp;

            return model;
        }



        ///<summary>
        /// Fetches a collection of media files, typically for the Downloads section. 
        /// </summary> 
        /// <param name="downloadsPicker">Comma-separated list of Media node id:s.</param>
        /// <param name="helper">Instance of the Umbraco helper. Optional.</param>
        /// <param name="language">Current language (SV/EN).</param>
        /// <returns>The populated MediaFiles object.</returns>      
        public MediaFilesModel GetDownloadFiles(string downloadsPicker, UmbracoHelper helper, string language)
        {    
            // Initiate return model
            MediaFilesModel model = new MediaFilesModel();

            // Create a MediaFile list
            List<MediaFileModel> listDownloads = new List<MediaFileModel>();

            // Get list of media node id:s           
            var pdfList = downloadsPicker.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            // Create dynamic list of media nodes based on the node id:s 
            dynamic listPdfs = helper.Media(pdfList);
                        
            // Iterate over the dynamic list 
            foreach (var item in listPdfs)
            {
                // Make sure node id actually exists in Media
                if(item.Parent != null)
                {
                    // Instanciate file object
                    MediaFileModel objFile = new MediaFileModel();

                    // Get the file properties
                    objFile = GetMediaFileProperties(item, language);

                    // Add File object to list
                    listDownloads.Add(objFile);
                }
            }

            // Add list to the return model
            model.MediaFilesCollection = listDownloads;

            return model;
        }



        /* HELPER METHODS */


        public ImageModel GetMediaImageProperties(dynamic mediaFile, UmbracoHelper helper, string photo = "", string language = "")
        {
            // Initiate return model
            ImageModel model = new ImageModel();

            // Get current language ("SV" or "EN")
            if (language == string.Empty)
            {
                language = helper.GetDictionaryValue("Language");
            }

            // Get photo term from dictionary
            if (photo == string.Empty)
            {
                photo = helper.GetDictionaryValue("Photo");
            }

            // Populate Image object due to current language
            switch (language)
            {
                case "SV":
                    model = new ImageModel
                    {
                        MediaNode = mediaFile,
                        Name = mediaFile.Name,
                        Caption = mediaFile.GetPropertyValue<string>(MyConstants.MediaFileCaptionSV).Trim(),
                        Photographer = mediaFile.GetPropertyValue<string>(MyConstants.MediaFilePhotographer.Trim())
                    };
                    break;

                case "EN":
                    model = new ImageModel
                    {
                        MediaNode = mediaFile,
                        Name = mediaFile.Name,
                        Caption = mediaFile.GetPropertyValue<string>(MyConstants.MediaFileCaptionEN).Trim(),
                        Photographer = mediaFile.GetPropertyValue<string>(MyConstants.MediaFilePhotographer.Trim())
                    };
                    break;
            }

            // Add dictionary term
            if (model.Photographer != string.Empty)
            {
                model.Photographer = photo + ": " + model.Photographer;
            }

            return model;
        }



        public MediaFileModel GetMediaFileProperties(dynamic mediaFile, string language)
        {
            // Initiate return model
            MediaFileModel model = new MediaFileModel();

            // Make sure a file was actually selected 
            if (mediaFile is DynamicNull)
            {
                model.Path = string.Empty;
            }
            else
            {
                // Get file name due lo language
                switch(language)
                { 
                    case "SV":

                        model.Name = mediaFile.Name;
                        break;
                    
                    case "EN":

                        model.Name = mediaFile.GetPropertyValue<string>(MyConstants.MediaFileNameInEnglish).Trim();

                        // Use Swedish name if English name is missing
                        if(model.Name == string.Empty)
                        {
                            model.Name = mediaFile.Name;
                        }
                        break;
                }

                // Get other properties
                model.Path = mediaFile.Url;
                model.Size = UmbracoHelperExtensions.GetReadableFileSizeViaBytes(Convert.ToInt64(mediaFile.GetPropertyValue<int>(MyConstants.MediaFileSize))).ToString();
                model.Extension = mediaFile.GetPropertyValue<string>(MyConstants.MediaFileExtension);
                model.Width = mediaFile.GetPropertyValue<string>(MyConstants.MediaFileWidth);
                model.Height = mediaFile.GetPropertyValue<string>(MyConstants.MediaFileHeight);
                model.Info = " (." + model.Extension + " " + model.Size + ")";

                // Detect portrait format
                model.Portrait = false;
                if(Convert.ToInt32(model.Width) < Convert.ToInt32(model.Height))
                {
                    model.Portrait = true;
                }

                // Get icon file path
                switch (model.Extension)
                {                    
                    case "pdf":

                        // Assign media file's parent folder's name to variable
                        string parentName = mediaFile.Parent.Name; 

                        // Set icon file due to parent folder (mainly for the Downloads section)
                        switch(parentName)
                        { 
                            case "Kartor":
                                model.IconPath = Paths.GetIconPath("marker");
                                break;

                            case "Ut i Skåne":
                                model.IconPath = Paths.GetIconPath("folder");
                                break;

                            default:                        
                                model.IconPath = Paths.GetIconPath("document");
                                break;
                        }
                        break;

                    // Any other file extension
                    default:
                        model.IconPath = Paths.GetIconPath("document");
                        break;
                }
            }

            // Return model
            return model;
        }
   
               
    }
}