﻿using System.Web;

namespace SkanskaLandskap15.Models.Shared
{
    public class FindUsModuleModel 
    {
        public string Type { get; set; }
        public string Title { get; set; }
        public IHtmlString Text { get; set; }      
    }
}