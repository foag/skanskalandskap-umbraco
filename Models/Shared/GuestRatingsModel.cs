﻿
namespace SkanskaLandskap15.Models.Shared
{
    public class GuestRatingsModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ContentType { get; set; }
        public string TitleTerm { get; set; }
        public int Percentage { get; set; }
        public string AverageValue { get; set; }
        public int NumOfVoters { get; set; }
        public string AverageValueMessage { get; set; }
        public string NumOfVotersMessage { get; set; }
        public bool GuestRatingsFlag { get; set; }

        public string ConfirmMessage { get; set; }
        public string ThankYouMessage { get; set; }
        public string OnlyOnceMessage { get; set; }
        public string SeeResults { get; set; }
    }
}