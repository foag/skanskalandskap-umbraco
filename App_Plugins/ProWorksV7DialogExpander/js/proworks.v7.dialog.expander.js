//bootstrap controller onto page
$(document).ready(function () {
	var bodyEl = $('body');

	bodyEl.on('DOMNodeInserted', 'div.umb-modal', function () {
	    var $this = $(this);
	    if ($this.length > 0) {
	        if ($this.find(".pw-qp-dialog-size-toggle").length === 0) {

	            var $toggleLink = $("<a href class='btn pw-qp-dialog-size-toggle' style='position: absolute; left: 0px; height: 50px; top: 40%; padding: 30px 0px 0px;'><i class='icon icon-navigation-left'></i></a>");
	            $this.append($toggleLink);
	            $toggleLink.on("click", function () {
	                if ($this.is(".pw-qp-wide")) {
	                    $this.attr('style', '');
	                    $this.removeClass("pw-qp-wide");
	                    $toggleLink.html("<i class='icon icon-navigation-left'></i>");
                    } else {
	                    $this.attr('style', 'width: 750px !important; margin-left: -750px;');
	                    $this.addClass("pw-qp-wide");
	                    $toggleLink.html("<i class='icon icon-navigation-right'></i>");
	                }
	            });

	        }
	    }
	});

});