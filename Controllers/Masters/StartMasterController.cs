﻿using System.Web.Mvc;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.SurfaceControllers;
using SkanskaLandskap15.Models.Masters;
using Umbraco.Web;



namespace SkanskaLandskap15.Controllers.Masters
{
    public class StartMasterController : MasterController
    {

        protected ViewResult View(StartMasterModel model)
        {
            return this.View(null, model);
        }

        
        protected ViewResult View(string view, StartMasterModel model)
        {

            // Get a reference to the Umbraco helper         
            UmbracoHelper helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language ("SV" or "EN")
            var currentLanguage = helper.GetDictionaryValue("Language");


            /* MAP SECTION */

            // Create a controller reference
            AreasSurfaceController asc = new AreasSurfaceController();

            // Get a list of all areas for the start page map.         
            model.AreasList = asc.GetAreasList(currentLanguage, false, CurrentPage.DocumentTypeAlias);
            
            // Get title for the areas map section
            model.AreaMapTitleTerm = helper.GetDictionaryValue("StartPageMapTitle");

            // Get title for the filter areas link
            model.FilterAreasLinkTerm = helper.GetDictionaryValue("StartPageAreasLink");

            // Get url to the areas page due to language
            model.AreasPagePath = helper.NiceUrl(helper.ContentAtXPath("//" + CurrentPage.DocumentTypeAlias + "/" + MyConstants.AreasLandingPageDocTypeAlias).FirstOrDefault().Id);

            return base.View(view, model);
        }     
    }
}