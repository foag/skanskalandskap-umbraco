﻿using System.Web.Mvc;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Models;
using Umbraco.Web;



namespace SkanskaLandskap15.Controllers
{

    public class ForKidsLandingPageController : InteriorMasterController
    {

        /////<summary>
        ///// Fetches information for the For Kids landing page.
        ///// </summary>      
        ///// <returns>A ForKidsLandingPage object.</returns> 
        public ActionResult ForKidsLandingPage()
        {
            // Initiate the return model
            var model = new ForKidsLandingPageModel();

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language ("SV" or "EN")
            //var currentLanguage = helper.GetDictionaryValue("Language");

            // Assign the content type
            model.ContentType = CurrentPage.DocumentTypeAlias;
            
            // Get a string of comma-separated sub page node id:s
            model.SelectedNodes = CurrentPage.GetPropertyValue<string>(MyConstants.tipsForKidsLandingPagePropertyAlias);

            return View(model);
        }
    }
}