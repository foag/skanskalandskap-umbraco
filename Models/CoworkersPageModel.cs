﻿using System.Collections.Generic;
using System.Web;
using SkanskaLandskap15.Models.Masters;
using SkanskaLandskap15.Models.Shared;


namespace SkanskaLandskap15.Models
{
    public class CoworkersPageModel : InteriorMasterModel
    {

        public bool CoworkerGroupsExists { get; set; }
        public List<PuffModel> CoworkerGroupsCollection { get; set; }
    }
}