﻿angular.module("umbraco").controller("uImport.MediaExportController",
        function ($scope, $routeParams, navigationService, notificationsService) {

            $scope.export = function () {

                var url = "/umbraco/backoffice/uImport/uImportApi/Export";

                var id = $scope.currentNode.parentId;

                if (typeof id === "undefined" || id === null) {
                    id = -1;
                }

                window.location = "/umbraco/uImport/uImport/Export?id=" + id;

            };

        });