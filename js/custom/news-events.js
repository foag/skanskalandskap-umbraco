(function ($) {

    /* Here we load a subset of News or Event items - the number is specified in
       Umbracos backoffice - when the visitor clicks the Load more button. */


    // We're on the News And Events landing page
    if ($("#events-content").length > 0 || $("#news-content").length > 0)
    {

        // Assign the wrapper element to an object
        $eventsContent = $("#events-content");

        // Manage the load operation
        LoadSubset($eventsContent, 'events');

        // Assign the wrapper element to an object
        var $newsContent = $('#news-content');

        // Manage the load operation
        LoadSubset($newsContent, 'news');
    }


    function LoadSubset($wrapper, loadType) {

        // Attach a click event to the Load more button
        $wrapper.on("click", ".pager a", function (e) {

            e.preventDefault();

            // Get current page/paging number
            var pageNumber = $('.page-number:last').text();

            // Remove the Load button from existing content
            $(".pager").css({ 'display': 'none' });

            // Add a list element to hold the new subset
            $wrapper.append("<ul></ul>");

            // Set path and offset value due to load type
            var path = "";
            var offsetValue = 0;
            switch (loadType) {
                case 'events':
                    path = '/umbraco/surface/EventsSurface/GetEventsListSubset';
                    offsetValue = 500;
                    break;
                case 'news':
                    path = '/umbraco/Surface/NewsSurface/GetNewsListSubset';
                    offsetValue = 500;
                    break;
            }

            // Create parameters
            var params = {
                nodeId: Global.currentNodeId,
                pageNumber: pageNumber,
                language: Global.language
            };

            // Get the new subset
            $wrapper.find('ul:last').load(path, params, function (data, status, xhr) {

                // Move the loaded content a bit upwards
                $('html, body').animate({ scrollTop: '+=700' }, 500);
                //window.scrollBy(0, 400);
            });
        });
    }
})(jQuery);
