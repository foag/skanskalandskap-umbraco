﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.Mvc;




namespace SkanskaLandskap15.Controllers.SurfaceControllers
{
    public class NewsSurfaceController : SurfaceController
    {

        ///<summary>
        /// Fetches the news items that was selected in Umbraco's backoffice (Start page node)
        /// </summary>
        /// <param name="model">An empty NewsItem object.</param>
        /// <param name="contentType">Indicates the site part that wants to display the latest news
        /// list (typically the Start page).</param>       
        /// <returns>The NewsLatest partial populated with a NewsItem object.</returns>      
        [ChildActionOnly]
        public ActionResult LatestNewsSelected(NewsItemModel model, string contentType)
        {

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get id of the start page node
            int nodeId = helper.TypedContentAtXPath("//" + MyConstants.StartPageDocTypeAlias).First().Id;

            // Get content based on start page node id
            var content = helper.Content(nodeId);

            /**************************************************************************************************
               The following section implements the MultiNode Tree Picker datatype, but should work for the 
               nuPickers Xml PrefetchList datatype as well. 
            **************************************************************************************************/

            // Get a string of comma-separated NewsItem node id:s
            var strNewsItems = content.GetPropertyValue<string>(MyConstants.NewsPickerPropertyAlias);

            // Initiate a temporary list
            List<NewsItemModel> tempList = new List<NewsItemModel>();

            // Make sure at least one NewsItem is selected (required but you never know...)
            if (!string.IsNullOrEmpty(strNewsItems))
            {
                // Set news item's display type
                string requestType = "puff";

                // Convert string to list
                var listNewsItems = strNewsItems.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                // Convert list to a typed collection of published nodes
                var collectionNewsItems = helper.TypedContent(listNewsItems);

                // Iterate over the selected nodes 
                foreach (var item in collectionNewsItems)
                {

                    // Make sure current news item node is published (it's possible to select a
                    // non-published news item in backoffice).
                    if (item != null)
                    {
                        // Get all content based on current node id
                        var itemContent = helper.TypedContent(item.Id);

                        // Initiate a NewsItem object
                       // NewsItemModel NewsItem = new NewsItemModel();

                        // Get the news item's content
                        NewsItemModel newsItem = NewsPageController.GetNewsContent(itemContent, helper, requestType);

                        // Add the news item object to the temp list
                        tempList.Add(newsItem);                         
                    }
                }
            }

            // Add the temporary list to the return model
            model.NewsCollection = tempList;

            // Check whether first news item should be featured
            model.FeaturedNewsItem = content.GetPropertyValue<bool>(MyConstants.featuredNewsItemPropertyAlias);

            // Get the id of the News landing page node
            int nodeIdNewsLandingPage = helper.TypedContentAtXPath("//" + MyConstants.NewsAndEventsLandingPageDocTypeAlias).First().Id;

            // Create the url to the News landing page
            model.NewsLandingPageUrl = helper.NiceUrl(nodeIdNewsLandingPage);


            // Add the Latest News area's title
            model.NewsTitleTerm = helper.GetDictionaryValue("NewsTitle");

            // Add the title for the Read More link
            model.ReadMoreTerm = helper.GetDictionaryValue("ReadMore");

            // Add the title for the News Landing Page link
            model.SeeMoreNewsTerm = helper.GetDictionaryValue("SeeMoreNews");

            // Set the part of the site where the latest news list should be displayed
            model.SitePart = contentType;

            // Send the NewsItem model to the partial
            return PartialView("NewsLatest", model);
        }



        ///<summary>
        /// Fetches a list of the latest news items (puffs) that was tagged for a specific 
        /// site part, i.e. Fulltofta or För skolor (the sub meny).
        /// </summary>
        /// <param name="model">An empty NewsItem object.</param>
        /// <param name="tag">Indicates the site part that wants to display the latest news
        /// list ("Fulltofta", "För skolor").</param>       
        /// <returns>The NewsLatest partial populated with a NewsItem object.</returns>      
        [ChildActionOnly]
        public ActionResult LatestNewsTagged(NewsItemModel model, string tag)
        {
            // Add sitePart parameter to the return model
            model.SitePart = tag;

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get number of items to display
            int numOfItems = MyConstants.NewsItemsOnAreaPage;

            // Get the id of the News landing page node
            int nodeId = helper.TypedContentAtXPath("//" + MyConstants.NewsAndEventsLandingPageDocTypeAlias).First().Id;
            
            // Get all content for the News landing page
            var content = helper.TypedContent(nodeId);

            // Get the published news items (just the node id:s) for the specified site part
            List<NewsItemModel> newsList = content
                .Descendants()
                .Where(c => c.DocumentTypeAlias == MyConstants.NewsPageDocTypeAlias)
                .OrderByDescending(c => c.GetPropertyValue<DateTime>(MyConstants.PublishingDatePropertyAlias))
                //.Take(MyConstants.NewsItemsOnAreaPage)
                .Select(obj => new NewsItemModel()
                {
                    NodeId = obj.Id
                }).ToList();
          
            // Initiate a temporary list 
            List<NewsItemModel> tempList = new List<NewsItemModel>();
            
            // Iterate over the news items, filter out the tagged items and get the properties 
            newsList.ForEach(item =>
            {
                // Make sure upper limit is not reached 
                if (tempList.Count() < numOfItems)
                {
                    // Get content based on current node id
                    var itemContent = helper.TypedContent(item.NodeId);

                    // Initiate Tag flag
                    bool tagFlag = false;

                    // Get tags
                    var tagsValue = itemContent.GetPropertyValue<string>(MyConstants.TagNewsPropertyAlias);

                    if (tagsValue != null)
                    {

                        // Instanciate serializer
                        JavaScriptSerializer ser = new JavaScriptSerializer();

                        // Deserialize tags value
                        List<string> tags = ser.Deserialize<List<string>>(tagsValue);

                        // Iterate over the selected tags (one or many)
                        foreach (var itemTag in tags)
                        {
                            // Look for the specified tag
                            if (itemTag == tag)
                            {
                                tagFlag = true;
                                break;
                            }
                        }
                    }

                    //// Get tags
                    //var tags = itemContent.GetPropertyValue<string>(MyConstants.TagNewsPropertyAlias);

                    //// Make sure current news is tagged for anything
                    //if (tags != null)
                    //{
                    //    // Iterate over the selected tags (one or many)
                    //    foreach (var itemTag in tags.Split(','))
                    //    {
                    //        // Look for the specified tag
                    //        if (itemTag == tag)
                    //        {
                    //            tagFlag = true;
                    //            break;
                    //        }
                    //    }
                    //}

                    // Current news item was marked with the specified tag 
                    if (tagFlag == true)
                    {                        
                        // Set news display type
                        string requestType = "puff";

                        // Get the news item's content
                        NewsItemModel newsItem = NewsPageController.GetNewsContent(itemContent, helper, requestType);

                        // Add the news item object to the temp list
                        tempList.Add(newsItem);
                    }
                }
            });

            // Add the temp list to the return model            
            model.NewsCollection = tempList;

            // Add the news section's title due to site part
            switch (tag)
            {
                case "Fulltofta":                                       
                    model.NewsTitleTerm = helper.GetDictionaryValue("NewsFulltofta");
                    break;

                case "Utomhuspedagogik":
                    //model.NewsTitleTerm = helper.GetDictionaryValue("NewsFoundation");
                    break;
            }

            // Add the title for the Read More link
            model.ReadMoreTerm = helper.GetDictionaryValue("ReadMore");
            
            // Send the NewsItem model to the partial
            return PartialView("NewsLatest", model);
        }





        ///<summary>
        /// Fetches a subset of news items - full version - based on the page number parameter 
        /// (mainly aimed for the Load more function on the News and Events landing page).
        /// </summary>
        /// <param name="nodeId">The node id of current landing page.</param>
        /// <param name="pageNumber">An integer indicating the page/subset to be fetched 
        /// (should be > 1).</param>
        /// <param name="language">Indicates current language (SV/EN).</param>
        /// <returns>A NewsList object.</returns> 
        public ActionResult GetNewsListSubset(int nodeId, int pageNumber, string language)
        {

            // Initiate the return model
            var model = new NewsListModel();

            // The culture of an Ajax request thread is neutral and therefore we need to set it 
            // explicitly to be able to get the correct Umbraco context.
            if (Request.IsAjaxRequest())
            {
                UmbracoHelperExtensions.EnsureCulture(Thread.CurrentThread, language);
            }

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get all content based on current (landing) page
            var content = helper.TypedContent(nodeId);

            // Get a reference to the NewsLandingPage controller
            NewsAndEventsLandingPageController nelpc = new NewsAndEventsLandingPageController();

            // Get the subset size (the number of news items to be displayed at a time)
            int recordsPerPage = nelpc.GetRecordsPerPage(content, "news");

            // Get the published news items (just the node id:s)
            IEnumerable<NewsItemModel> newsList = content
                .Descendants()
                .Where(c => c.DocumentTypeAlias == MyConstants.NewsPageDocTypeAlias)
                .OrderByDescending(c => c.GetPropertyValue<DateTime>(MyConstants.PublishingDatePropertyAlias))
                .Select(obj => new NewsItemModel()
                {
                    NodeId = obj.Id
                }).ToList();

            // Assign the total number of published news items to a variable
            int numberOfRecords = Convert.ToInt32(newsList.Count());

            // Set some variables 
            List<NewsItemModel> tempList = new List<NewsItemModel>();
            int lowerLimit = (pageNumber * recordsPerPage) + 1;
            int upperLimit = lowerLimit + recordsPerPage;
            int counter = 0;
            string requestType = "puff";

            // Iterate over the news items and get the content for a specific subset
            newsList.ForEach(item =>
            {
                // Enumerate counter
                counter += 1;

                // Make sure only items within the specified range is affected
                if (counter >= lowerLimit && counter < upperLimit)
                {

                    // Get content based on current node id
                    var itemContent = helper.TypedContent(item.NodeId);

                    // Get the properties for current news item
                    NewsItemModel newsItem = NewsPageController.GetNewsContent(itemContent, helper, requestType);

                    // Set the url to be able to make the news item linked
                    newsItem.Url = helper.NiceUrl(item.NodeId);

                    // Add the news item object to the temporary list
                    tempList.Add(newsItem);
                }

                // Break out of loop when the upper limit is reached
                if (counter == upperLimit)
                {
                    return;
                }
            });

            // Add the temp list to the return model
            model.NewsItemsCollection = tempList;

            // Check whether a Load More button should be displayed
            model.LoadMore = false;
            if (Convert.ToDouble(numberOfRecords) / (recordsPerPage * (pageNumber + 1)) > 1)
            {
                // Set flag and link title
                model.LoadMore = true;
                model.LoadMoreInfo = helper.GetDictionaryValue("LoadMoreNews");

                // Enumerate the page number before assigning it to the return model
                model.PageNumber = pageNumber + 1;
            }

            return PartialView("NewsList", model);
        }



        ///<summary>
        /// Fetches a subset of news items that was marked with a specific tag.
        /// </summary>
        /// <param name="nodeId">The node id of current landing page.</param>
        /// <param name="pageNumber">An integer indicating the page/subset to be fetched 
        /// (should be > 1).</param>
        /// <param name="language">Indicates current language (SV/EN).</param>
        /// <returns>A NewsList object.</returns> 
        public NewsListModel GetNewsByTag(string tag, int numOfItems, UmbracoHelper helper)
        {  
            // Initiate the return model
            NewsListModel model = new NewsListModel();

            // Get id of the News And Events landing page node
            int nodeId = helper.TypedContentAtXPath("//" + MyConstants.NewsAndEventsLandingPageDocTypeAlias).First().Id;

            // Get content based on node id
            var content = helper.TypedContent(nodeId);

            // Get the published news items (just the node id:s) 
            IEnumerable<NewsItemModel> newsList = content
                .Descendants()
                .Where(c => c.DocumentTypeAlias == MyConstants.NewsPageDocTypeAlias)
                .OrderByDescending(c => c.GetPropertyValue<DateTime>(MyConstants.PublishingDatePropertyAlias))
                .Select(obj => new NewsItemModel()
                {
                    NodeId = obj.Id
                }).ToList();

            // Create a temporary list
            List<NewsItemModel> tempList = new List<NewsItemModel>();

            // Iterate over the news items, filter out the tagged items and get the properties 
            newsList.ForEach(item =>
            {
                // Make sure upper limit is not reached 
                if (tempList.Count() < numOfItems)
                {
                    // Get content based on current node id
                    var itemContent = helper.TypedContent(item.NodeId);

                    // Initiate Tag flag
                    bool tagFlag = false;

                    // Get tags
                    var tagsValue = itemContent.GetPropertyValue<string>(MyConstants.TagNewsPropertyAlias);

                    if (tagsValue != null)
                    {
                        // Instanciate serializer
                        JavaScriptSerializer ser = new JavaScriptSerializer();

                        // Deserialize tags value
                        List<string> tags = ser.Deserialize<List<string>>(tagsValue);

                        // Iterate over the selected tags (one or many)
                        foreach (var itemTag in tags)
                        {
                            // Look for the specified tag
                            if (itemTag == tag)
                            {
                                tagFlag = true;
                                break;
                            }
                        }
                    }

                    //// Get tags
                    //var tags = itemContent.GetPropertyValue<string>(MyConstants.TagNewsPropertyAlias);

                    //// Make sure current news is tagged for anything
                    //if (tags != null)
                    //{
                    //    // Iterate over the selected tags (one or many)
                    //    foreach (var itemTag in tags.Split(','))
                    //    {
                    //        // Look for the specified tag
                    //        if (itemTag == tag)
                    //        {
                    //            tagFlag = true;
                    //            break;
                    //        }
                    //    }
                    //}

                    // Current news item was marked with the specified tag 
                    if (tagFlag == true)
                    {
                        // Set news display type
                        string requestType = "puff";

                        // Get the news item's content
                        NewsItemModel newsItem = NewsPageController.GetNewsContent(itemContent, helper, requestType);

                        // Add the news item object to the temp list
                        tempList.Add(newsItem);
                    }
                }
            });

            // Add the temporary list to the News List object
            model.NewsItemsCollection = tempList;

            return model;
        }     







        ///<summary>
        /// Returns the news item properties for a news flash puff.
        /// </summary>        
        //public static NewsItemModel GetNewsFlashContent(IPublishedContent content, UmbracoHelper helper)
        //{

        //    // Initiate the return model
        //    NewsItemModel model = new NewsItemModel();

        //    // Title
        //    model.Title = content.GetPropertyValue<String>(MyConstants.TitlePropertyAlias);

        //    // Intro
        //    model.NewsFlash = content.GetPropertyValue<String>(MyConstants.NewsFlashPropertyAlias);

        //    if(string.IsNullOrEmpty(model.NewsFlash))
        //    {
        //        model.NewsFlash = content.GetPropertyValue<String>(MyConstants.IntroPropertyAlias);
        //    }

        //    // Image 
        //    //model.NewsImage = helper.Media(content.GetPropertyValue(MyConstants.NewsEventsImagePropertyAlias));
        //    model.NewsImage = helper.Media(content.GetPropertyValue(MyConstants.ImagePickerPropertyAlias));
            
        //    // Publishing date
        //    string publishingDate = content.GetPropertyValue<string>(MyConstants.PublishingDatePropertyAlias);
        //    if (string.IsNullOrWhiteSpace(publishingDate))
        //    {
        //        model.PublishingDate = String.Empty;
        //    }
        //    else
        //    {
        //        // Remove time value
        //        model.PublishingDate = publishingDate.Substring(0, 10);
        //    }

        //    // Set the url to be able to make the news item linked
        //    model.Url = helper.NiceUrl(content.Id);

        //    return model;
        //}



        /////<summary>
        ///// Returns the news item properties for a simple list link.
        ///// </summary> 
        //public static NewsItemModel GetNewsLinkContent(IPublishedContent content, UmbracoHelper helper)
        //{

        //    // Initiate the return model
        //    NewsItemModel model = new NewsItemModel();

        //    // Title
        //    model.Title = content.GetPropertyValue<String>(MyConstants.TitlePropertyAlias);

        //    // Publishing date
        //    string publishingDate = content.GetPropertyValue<string>(MyConstants.PublishingDatePropertyAlias);
        //    if (string.IsNullOrWhiteSpace(publishingDate))
        //    {
        //        model.PublishingDate = String.Empty;
        //    }
        //    else
        //    {
        //        // Remove time value
        //        model.PublishingDate = publishingDate.Substring(0, 10);
        //    }

        //    // Set the url to be able to make the news item linked
        //    model.Url = helper.NiceUrl(content.Id);

        //    return model;
        //}
    }
}