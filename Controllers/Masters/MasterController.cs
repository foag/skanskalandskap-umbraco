﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RJP.MultiUrlPicker.Models;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.SurfaceControllers;
using SkanskaLandskap15.Models.Masters;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Core.Dynamics;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;



namespace SkanskaLandskap15.Controllers.Masters
{
    public class MasterController : RenderMvcController
    {

        protected ViewResult View(MasterModel model)
        {           
            return this.View(null, model);
        }


        protected ViewResult View(string view, MasterModel model)
        {

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language ("SV" or "EN")
            var currentLanguage = helper.GetDictionaryValue("Language");

            // Get the root node (start page)
            var root = this.CurrentPage.AncestorOrSelf(1);

            // Get some properties
            model.Url = root.Url;
            model.Id = CurrentPage.Id;            
            model.Level = CurrentPage.Level;
            model.UrlName = CurrentPage.UrlName; 
            model.ContentType = CurrentPage.DocumentTypeAlias;
            string rootDocTypeAlias = GetRootContentType(currentLanguage);

            // Get content based on current page's node id 
            var content = helper.TypedContent(CurrentPage.Id);  

            // Get Start page content (to be able to get configurations)
            var contentStartPage = helper.TypedContent(helper.TypedContentAtXPath("//" + MyConstants.StartPageDocTypeAlias).First().Id);


            /*** SITE INFORMATION ***/
                   
           // Get data from the start page (root) node 
            model.SiteTitle = root.GetPropertyValue<string>(MyConstants.SiteTitlePropertyAlias);
            if (string.IsNullOrWhiteSpace(model.SiteTitle))
            {
                model.SiteTitle = root.Name;
            }

            model.SiteDescription = root.GetPropertyValue<string>(MyConstants.SiteDescriptionPropertyAlias);
            if (string.IsNullOrWhiteSpace(model.SiteDescription))
            {               
                model.SiteDescription = String.Empty;               
            }


            /*** GOOGLE ANALYTICS KEY ***/

            model.GoogleAnalyticsKey = contentStartPage.GetPropertyValue<string>(MyConstants.googleAnalyticsKeyPropertyAlias);


            /*** NO JAVASCRIPT WARNING ***/

            model.NoJavaScriptWarning = helper.GetDictionaryValue("NoJavaScriptWarning");


            /*** LOGO ***/ 

            // Get logo file path due to language
            model.SiteLogo = Paths.GetLogoPath(currentLanguage);
            model.FoundationName = helper.GetDictionaryValue("FoundationName");


            /*** LANGUAGE SWITCH ***/

            // When running on development server we need to hard code the language switch url 
            // since multiple domain names are used in Umbraco's "Culture and hostnames".
            string devNameSW = Paths.GetDevDomainName("SV");
            string devNameEN = Paths.GetDevDomainName("EN");

            // Get domain name
            string domainName = Request.ServerVariables["SERVER_NAME"];

            // At Swedish dev site part
            if (domainName == devNameSW)
            {
                model.LanguageSwitchUrl = "http://" + devNameEN;
            }
            // At English dev site part
            else if (domainName == devNameEN)
            {
                model.LanguageSwitchUrl = "http://" + devNameSW;
            }
            // Live site (or local project) 
            else
            {
                // Get a list of the root nodes
                List<IPublishedContent> rootNodes = helper.TypedContentAtRoot().ToList();

                // Make sure there are more than 2 root nodes (one language and the Resources node)
                if (rootNodes.Count > 1)
                {
                    // Iterate over the nodes to find the opposite root node relative to current language
                    foreach (var node in rootNodes)
                    {
                        if (currentLanguage == "SV" && node.UrlName == "home" || currentLanguage == "EN" && node.UrlName == "start")
                        {
                            model.LanguageSwitchUrl = helper.NiceUrl(node.Id);
                            // Break out of loop
                            break;
                        }
                    }
                }
            }

            // Get the language switch text
            model.LanguageSwitchText = helper.GetDictionaryValue("LangSwitchText"); 


            //int switchTargetNodeId = 0;
            //switch (currentLanguage)
            //{
            //    case "SV":                    
            //        // Get id of the English start page node
            //        switchTargetNodeId = helper.TypedContentAtXPath("//" + MyConstants.HomePageDocTypeAlias).First().Id;
            //        break;

            //    case "EN":
            //        // Get id of the Swedish start page node
            //        switchTargetNodeId = helper.TypedContentAtXPath("//" + MyConstants.StartPageDocTypeAlias).First().Id;
            //        break;
            //}

            //// Get domain name
            //string domainName = Request.ServerVariables["SERVER_NAME"];

            //// When running the site on the development server we need to hard code the language switch 
            //// links since multiple domain names are used in Umbraco's backoffice.
            //if (domainName == Paths.GetDevDomainName("SV"))
            //{
            //    model.LanguageSwitchUrl = "http://" + Paths.GetDevDomainName("EN");
            //}
            //else if (domainName == Paths.GetDevDomainName("EN"))
            //{
            //    model.LanguageSwitchUrl = "http://" + Paths.GetDevDomainName("SV");
            //}
            //else
            //{
            //    model.LanguageSwitchUrl = helper.NiceUrl(switchTargetNodeId);
            //}
           
            //// Language switch text
            //model.LanguageSwitchText = helper.GetDictionaryValue("LangSwitchText");

          
            //NavigationItemModel objNavItem = new NavigationItemModel();
            //objNavItem.LanguageSwitchUrl = model.LanguageSwitchUrl;
            //objNavItem.LanguageSwitchText = model.LanguageSwitchText;
            //model.MainNavigation = objNavItem;



            /*** SEARCH FUNCTION ***/

            // Get search node's urlname due to language 
            model.SearchUrlName = "/" + helper.TypedContentAtXPath("//" + rootDocTypeAlias + "/" + MyConstants.SearchResultsPageDocTypeAlias).First().UrlName;

            // Get placeholder text for the search box
            model.SearchPlaceHolderTerm = helper.GetDictionaryValue("Search");
                            
           

            /*** PAGE HEADER IMAGE ***/

            model.TopImage = GetTopImage(model, content, helper, rootDocTypeAlias);           



            /*** VALUES FOR OG TAGS (with exception for search results page) ***/

            if(CurrentPage.DocumentTypeAlias != MyConstants.SearchResultsPageDocTypeAlias)
            { 
                model.Title = CurrentPage.GetPropertyValue<string>(MyConstants.TitlePropertyAlias).Trim();
                model.Intro = CurrentPage.GetPropertyValue<string>(MyConstants.IntroPropertyAlias);
            }


            /*** TITLE & INTRO (AND IN ONE CASE GUEST RATINGS) ***/

            // Level 1 and 2 pages
            if(model.Level <= 2)
            {
                // Handle the search results page
                if (model.ContentType == MyConstants.SearchResultsPageDocTypeAlias)
                {
                    // Get node id of current start page
                    int nodeId = helper.TypedContentAtXPath("//" + rootDocTypeAlias).First().Id;

                    // Get content
                    var contentRoot = helper.TypedContent(nodeId);

                    // Get start/home page's title and intro 
                    model.TitleHeader = contentRoot.GetPropertyValue<string>(MyConstants.TitlePropertyAlias).Trim();
                    model.Intro = contentRoot.GetPropertyValue<string>(MyConstants.IntroPropertyAlias).Trim();
                }
                // Any other start and landing page (Title and Intro is mandatory)
                else 
                { 
                    model.TitleHeader = content.GetPropertyValue<string>(MyConstants.TitlePropertyAlias).Trim();
                    model.Intro = content.GetPropertyValue<string>(MyConstants.IntroPropertyAlias).Trim();
                }
            }

            // Info pages  
            else if (model.Level > 2 && model.ContentType == MyConstants.functionPageDocTypeAlias)
            {
                // Get node id of current start page
                int nodeId = helper.TypedContentAtXPath("//" + rootDocTypeAlias).First().Id;

                // Get content
                var contentRoot = helper.TypedContent(nodeId);

                // Get start/home page's title and intro 
                model.TitleHeader = contentRoot.GetPropertyValue<string>(MyConstants.TitlePropertyAlias).Trim();
                model.IntroLanding = contentRoot.GetPropertyValue<string>(MyConstants.IntroPropertyAlias).Trim();
            }


            // // Area and Accommodation sub pages  
            //else if (model.Level > 2 && (model.ContentType == MyConstants.AreaPageDocTypeAlias || model.ContentType == MyConstants.AccommodationPageDocTypeAlias))
            // Area sub pages  
            else if (model.Level > 2 && model.ContentType == MyConstants.AreaPageDocTypeAlias)
            {
                // Title is mandatory
                model.Title = CurrentPage.GetPropertyValue<string>(MyConstants.TitlePropertyAlias).Trim();
                model.TitleHeader = model.Title;
                model.Subtitle = CurrentPage.GetPropertyValue<string>(MyConstants.SubtitlePropertyAlias).Trim();

                //switch (model.ContentType)
                //{
                //    // Area page (Subtitle is mandatory)
                //    case MyConstants.AreaPageDocTypeAlias:
                //        model.Subtitle = CurrentPage.GetPropertyValue<string>(MyConstants.SubtitlePropertyAlias).Trim();
                //        break;

                //    // Accommodation page (Guest ratings is mandatory)
                //    case MyConstants.AccommodationPageDocTypeAlias:                       

                //        // Explicitly set the GuestRatings object to null
                //        model.GuestRatings = null;
                        
                //        // Create a Guest Ratings object
                //        GuestRatingsModel objGuestRatings = new GuestRatingsModel();
                //        model.GuestRatings = GuestRatingsSurfaceController.GetRatingValues(objGuestRatings, model.Id, currentLanguage);                       
                //        break;
                //}
            }

            // For any other sub page, its landing page's Title and Intro - both mandatory - should be displayed in page header
            else
            {

                // Get current page's landing node
                var landingNode = content.Ancestor(2);

                // Get landing node's content
                var contentLanding = helper.TypedContent(landingNode.Id);

                // Add Title and Intro to return model
                model.TitleHeader = contentLanding.GetPropertyValue(MyConstants.TitlePropertyAlias).ToString();
                model.IntroLanding = contentLanding.GetPropertyValue(MyConstants.IntroPropertyAlias).ToString();
            }


            /*** GLOBAL JAVASCRIPT VARIABLES ***/           

            // Return current language for use as a global javascript variable
            model.Language = currentLanguage;
                                   
            // Get the search term to be highlighted; only present in the querystring 
            // when user clicked a search result link    
            string searchTerm = String.Empty;
            if (!String.IsNullOrEmpty(Request.QueryString["hl_term"]))
            {
                searchTerm = Request.QueryString["hl_term"];
            }
            model.HighlightTerm = searchTerm;

            //model.SubMenuCacheDuration = contentStartPage.GetPropertyValue<int>(MyConstants.submenuCacheDurationPropertyAlias);

            // Create and add slug to the return model
            model.Slug = CreateSlug(model);
                                   

            
            /*** MAIN MENU ***/

            // Get reference to controller
            NavigationSurfaceController nsc = new NavigationSurfaceController();

            // Get main menu
            model.MainNavigation = nsc.mapMainNavigation(root, CurrentPage);
            
            // Get title for Home link
            model.HomeTerm = helper.GetDictionaryValue("Home");
           


            /*** PAGE FOOTER ***/

            // Get texts (mandatory)
            model.FoundationText = root.GetPropertyValue<string>(MyConstants.FooterFoundationTextPropertyAlias);
            model.ContactText = root.GetPropertyValue<IHtmlString>(MyConstants.FooterContactTextPropertyAlias);

            //Links (Optional)
            var links = root.GetPropertyValue<MultiUrls>(MyConstants.FooterMultiUrlPickerPropertyAlias);

            // Initiate links flag
            model.LinksExists = false;

            // Check whether links exists 
            if (links.Any())
            {
                // Set flag
                model.LinksExists = true;

                // Get controller reference
                PuffsSurfaceController psc = new PuffsSurfaceController();

                // Get links and add to the puff object
                model.LinksCollection = psc.GetLinks(links, helper);
            }

            // Get the fixed footer titles
            model.FooterFoundationTitleTerm = helper.GetDictionaryValue("FooterTitleFoundation");
            model.FooterContactTitleTerm = helper.GetDictionaryValue("FooterTitleContact");

            return base.View(view, model);
        }





        protected dynamic GetTopImage(MasterModel model, IPublishedContent content, UmbracoHelper helper, string rootDocTypeAlias)
        {
            // Initiate return value
            dynamic topImage = null;

            // Assign level and content type to variables
            int level = model.Level;
            string contentType = model.ContentType;


            // Start and Home pages
            if (model.Level == 1)
            {
                // Top image is mandatory so just get it and show it.               
                topImage = helper.Media(content.GetPropertyValue(MyConstants.TopImagePropertyAlias));
            }

            // Landing pages and sub pages for Area and Accommodation
            if ((level == 2 && contentType != MyConstants.SearchResultsPageDocTypeAlias) ||
                (level == 3 && (contentType == MyConstants.AreaPageDocTypeAlias || contentType == MyConstants.AccommodationPageDocTypeAlias || 
                contentType == MyConstants.AccommodationPageBookDocTypeAlias || contentType == MyConstants.AccommodationPageOtherDocTypeAlias)))
            {

                // Custom top image is possible but not mandatory   
                topImage = helper.Media(content.GetPropertyValue(MyConstants.TopImagePropertyAlias));

                // If top image was not selected, find the closest parent top image
                if (topImage.GetType() == typeof(DynamicNull))
                {

                    // Get all top images up the node tree 
                    List<MasterModel> tempList = content
                        .Ancestors()
                        //.Where(x => x.DocumentTypeAlias != MyConstants.NewsAndEventsRedirectDocTypeAlias 
                        //    && x.DocumentTypeAlias != MyConstants.InfoPagesDocTypeAlias
                        //    && x.DocumentTypeAlias != MyConstants.FunctionPagesDocTypeAlias)
                        .OrderBy(x => x.Id)
                        .Select(obj => new MasterModel()
                        {
                            TopImage = helper.Media(obj.GetPropertyValue(MyConstants.TopImagePropertyAlias))
                        }).ToList();

                    // Get first valid image and break out of loop
                    tempList.ForEach(item =>
                    {
                        if (item.TopImage.GetType() != typeof(DynamicNull))
                        {
                            topImage = (dynamic)item.TopImage;
                            return;
                        }
                    });
                }
            }

            // General sub pages, contact persons and coworkers pages, and search result page (does not have custom top image)
            if ((level == 3 && contentType == MyConstants.GeneralSubPageDocTypeAlias)
                || (level == 3 && contentType == MyConstants.ContactPersonsPageDocTypeAlias)
                || (level == 3 && contentType == MyConstants.CoworkersPageDocTypeAlias)
                || (level == 2 && contentType == MyConstants.SearchResultsPageDocTypeAlias))
            {

                // Get all top images up the node tree 
                List<MasterModel> tempList = content
                    .Ancestors()
                    .OrderBy(x => x.Id)
                    .Select(obj => new MasterModel()
                    {
                        TopImage = helper.Media(obj.GetPropertyValue(MyConstants.TopImagePropertyAlias))
                    }).ToList();

                // Get first valid image and break out of loop
                tempList.ForEach(item =>
                {
                    if (item.TopImage.GetType() != typeof(DynamicNull))
                    {
                        topImage = (dynamic)item.TopImage;
                        return;
                    }
                });
            }


            // Info pages in the Function pages folder (image should be fetched directly from the root page)
            if (level == 3 && contentType == MyConstants.functionPageDocTypeAlias)
            {
                // Get node id of current start page
                int nodeId = helper.TypedContentAtXPath("//" + rootDocTypeAlias).First().Id;

                // Get content
                var contentRoot = helper.TypedContent(nodeId);

                // Top image is mandatory so just get it and show it.               
                topImage = helper.Media(contentRoot.GetPropertyValue(MyConstants.TopImagePropertyAlias));
            }

            // Event and News page
            if(level == 4)
            {
                // Get all top images up the node tree 
                List<MasterModel> tempList = content
                    .Ancestors()
                    .Where(x => x.Level <= 2)
                    .OrderBy(x => x.Id)
                    .Select(obj => new MasterModel()
                    {
                        TopImage = helper.Media(obj.GetPropertyValue(MyConstants.TopImagePropertyAlias))
                    }).ToList();

                // Get first valid image and break out of loop
                tempList.ForEach(item =>
                {
                    if (item.TopImage.GetType() != typeof(DynamicNull))
                    {
                        topImage = (dynamic)item.TopImage;
                        return;
                    }
                });
            }
          
            return topImage;
        }



        protected string CreateSlug(MasterModel model)
        {
            // Initiate return value
            string slug = string.Empty;

            // Initiate path variable
            string path = string.Empty;

            // Use urlname as path at level 1
            if (model.Level == 1)
            { 
                path = model.UrlName;
            }
            else
            {
                // Get absolute path  
                path = Request.Url.AbsolutePath.Trim();

                // Remove possible initial slash
                if (path.Substring(0, 1) == "/")
                {
                    path = path.Remove(0, 1);
                }

                // Remove possible final slash
                if (path.Substring(path.Length-1, 1) == "/")
                {
                    path = path.Remove(path.Length-1, 1);
                }
                
                // Replace possible inner slash(es) with hyphen(s)
               path = path.Replace("/", "-");
            }

            // Add content type to path
            slug = model.ContentType.ToLower() + " " + path;

            return slug;
        }



        public string GetRootContentType(string language)
        {
            // Initiate return content type
            string contentType = string.Empty;

            switch (language)
            {
                case "SV":
                    contentType = MyConstants.StartPageDocTypeAlias;
                    break;
                case "EN":
                    contentType = MyConstants.HomePageDocTypeAlias;
                    break;
            }
            return contentType;
        }



        public static string GetTrafikLabApiKey()
        {
            //Initiate return value
            string apiKey = string.Empty;

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get start page's node id
            int nodeId = helper.TypedContentAtXPath("//" + MyConstants.StartPageDocTypeAlias).First().Id;

            // Get content based on node id
            var content = helper.TypedContent(nodeId);

            apiKey = content.GetPropertyValue<string>(MyConstants.trafiklabApiKeyPropertyAlias);

            return apiKey;
        }



        public static int GetOutputCacheDuration(string cacheSubject)
        {
            // Initiate return value
            int duration = 0;

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Initiate variables
            int nodeId = 0;
            IPublishedContent content;

            // Get the duration value due to subject of caching
            switch (cacheSubject)
            {
                case "services" :

                    // Get start page's node id
                    nodeId = helper.TypedContentAtXPath("//" + MyConstants.StartPageDocTypeAlias).First().Id;

                    // Get content based on node id
                    content = helper.TypedContent(nodeId);

                    // Get duration value
                    duration = content.GetPropertyValue<int>(MyConstants.servicesCacheDurationPropertyAlias);
                    break;

                case "submenu":
                    
                    // Get start page's node id
                    nodeId = helper.TypedContentAtXPath("//" + MyConstants.StartPageDocTypeAlias).First().Id;

                    // Get content based on node id
                    content = helper.TypedContent(nodeId);

                    // Get duration value
                    duration = content.GetPropertyValue<int>(MyConstants.submenuCacheDurationPropertyAlias);
                    break;

                default:
                    break;
            }

            return duration;
        }    


    }
}
