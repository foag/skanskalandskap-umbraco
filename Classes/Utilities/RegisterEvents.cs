﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Examine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using umbraco.NodeFactory;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Publishing;
using Umbraco.Core.Services;
using Umbraco.Core.Logging;



namespace SkanskaLandskap15.Classes.Utilities
{

    ///<summary>
    /// In the ApplicationStarted method below, Umbraco property fields - contained in Nested Content datatypes - are made searchable. 
    /// All you manually needs to do is to add the Nested Content property alias to ExamineIndex.config and to the searchable properties 
    /// list in SearchSurfaceController.
    ///</summary>
    public class RegisterEvents : ApplicationEventHandler
    {
   
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            base.ApplicationStarted(umbracoApplication, applicationContext);

            // Listen for when content is published          
            ContentService.Published += ContentService_Published;

            // Listen for when content iss unpublished
            ContentService.UnPublished += ContentService_UnPublished;

            // Listen for when content is saved
            ContentService.Saved += ContentService_Saved;

            // Listen for when content is deleted
            ContentService.Deleted += ContentService_Deleted;


            // Make Swedish content, included in Nested Content data types, searchable
            ExamineManager.Instance.IndexProviderCollection["SiteContentIndexer"]
                .GatheringNodeData += (sender, e) =>
                {
                    // Extract JSON properties
                    var fieldKeys = e.Fields.Keys.ToArray();
                    foreach (var key in fieldKeys)
                    {
                        var value = e.Fields[key];
                        if (value.DetectIsJson())
                        {
                            IndexNestedObject(e.Fields, JsonConvert.DeserializeObject(value), key, "SV");
                        }
                    }
                };

            // Make Enlish content, included in Nested Content data types, searchable
            ExamineManager.Instance.IndexProviderCollection["SiteContentEnglishIndexer"]
                .GatheringNodeData += (sender, e) =>
                {
                    // Extract JSON properties
                    var fieldKeys = e.Fields.Keys.ToArray();
                    foreach (var key in fieldKeys)
                    {
                        var value = e.Fields[key];
                        if (value.DetectIsJson())
                        {
                            IndexNestedObject(e.Fields, JsonConvert.DeserializeObject(value), key, "EN");
                        }
                    }
                };
        }


        private void ContentService_Published(IPublishingStrategy sender, PublishEventArgs<IContent> e)
        {
            // Initiate service flag
            bool serviceCleared = false;

            // Iterate over the published items
            foreach (var content in e.PublishedEntities)
            {
                // Look for a service hit
                serviceCleared = CheckService(serviceCleared, content);

                // Break out of loop if we found service and cleared cache
                if (serviceCleared)
                {
                    break;
                }
            }

            // Clear the sub menu cache
            ClearOutputCacheSubMenu();

        }


        private void ContentService_UnPublished(IPublishingStrategy sender, PublishEventArgs<IContent> e)
        {
            // Initiate service flag
            bool serviceCleared = false;

            // Iterate over the (un)published items
            foreach (var content in e.PublishedEntities)
            {
                // Look for a service hit
                serviceCleared = CheckService(serviceCleared, content);

                // Break out of loop if we found service and cleared cache
                if (serviceCleared)
                {
                    break;
                }
            }

            // Clear the sub menu cache
            ClearOutputCacheSubMenu();
        }


        private void ContentService_Saved(IContentService sender, SaveEventArgs<IContent> e)
        {
            // Initiate service flag
            bool serviceCleared = false;

            // Iterate over the saved items
            foreach (var content in e.SavedEntities)
            {
                // Look for a service hit
                serviceCleared = CheckService(serviceCleared, content);

                // Break out of loop if we found service and cleared cache
                if (serviceCleared)
                {
                    break;
                }
            }

            // Clear the sub menu cache
            ClearOutputCacheSubMenu();
        }


        private void ContentService_Deleted(IContentService sender, DeleteEventArgs<IContent> e)
        {
            // Initiate service flag
            bool serviceCleared = false;

            // Iterate over the deleted items
            foreach (var content in e.DeletedEntities)
            {
                // Look for a service hit
                serviceCleared = CheckService(serviceCleared, content);

                // Break out of loop if we found service and cleared cache
                if (serviceCleared)
                {
                    break;
                }
            }

            // Clear the sub menu cache
            ClearOutputCacheSubMenu();
        }


        private bool CheckService(bool cleared, IContent content)
        { 
            // Look for a Service or ServiceType content type   
            if (content.ContentType.Alias == "Service" || content.ContentType.Alias == "ServiceType")
            {
                //LogHelper.Info(typeof(RegisterEvents), "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Något har hänt med doctype " + content.ContentType.Alias);

                ClearOutputCache();
                cleared = true;
            }   
            return cleared;
        }


        public void ClearOutputCache()
        {
            // Remove items from outputcache          
            HttpResponse.RemoveOutputCacheItem("/umbraco/surface/mapssurface/getservicesbyareajson");  /* Fetches services for an area map */           
            HttpResponse.RemoveOutputCacheItem("/umbraco/surface/mapssurface/getbusstopinfo"); /* Fetches info including Resrobot id for a busstop marker's popup window */
            HttpResponse.RemoveOutputCacheItem("/umbraco/surface/mapinteractivitysurface/getservicetypesjson"); /* Fetches service types including counts by area for an area map */
                       
            //// Get the url for the action method
            //var requestContext = new System.Web.Routing.RequestContext(
            //   new HttpContextWrapper(System.Web.HttpContext.Current),
            //   new System.Web.Routing.RouteData());
            //var Url = new UrlHelper(requestContext);
            //var staleItem = Url.Action("GetServicesByAreaJson", "MapsSurfaceController", new
            //{
            //    language = {language},
            //    nodeId = {NodeId                
            //});           
        }


        public void ClearOutputCacheSubMenu()
        {
            // Remove items from outputcache          
            HttpResponse.RemoveOutputCacheItem("/umbraco/surface/submenusurface/getsubmenujson");  /* Fetches sub menu for specific landing page */
        }     
       

        public void IndexNestedObject(Dictionary<string, string> fields, object obj, string prefix, string language)
        {
            var objType = obj.GetType();
            if (objType == typeof(JObject))
            {
                var jObj = obj as JObject;
                if (jObj != null)
                {
                    foreach (var kvp in jObj)
                    {
                        var propKey = prefix + "_" + kvp.Key;
                        var valueType = kvp.Value.GetType();
                        if (typeof(JContainer).IsAssignableFrom(valueType))
                        {
                            IndexNestedObject(fields, kvp.Value, propKey, language);
                        }
                        else
                        {
                            fields.Add(propKey, kvp.Value.ToString().StripHtml());                         
                        }
                    }
                }
            }
            else if (objType == typeof(JArray))
            {
                var jArr = obj as JArray;
                if (jArr != null)
                {
                    for (var i = 0; i < jArr.Count; i++)
                    {
                        var itm = jArr[i];
                        var propKey = prefix + "_" + i;
                        var valueType = itm.GetType();
                        if (typeof(JContainer).IsAssignableFrom(valueType))
                        {
                            IndexNestedObject(fields, itm, propKey, language);
                        }
                        else
                        {
                            fields.Add(propKey, itm.ToString().StripHtml());
                        }
                    }
                }
            }
        }     

    } 
}
