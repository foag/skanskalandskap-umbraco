﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Controllers.SurfaceControllers;
using SkanskaLandskap15.Models;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Core.Models;
using Umbraco.Web;




namespace SkanskaLandskap15.Controllers
{

    // ENGLISH START PAGE


    public class HomePageController : StartMasterController
    {
        public ActionResult HomePage()
        {
            // Initiate the return model
            var model = new HomePageModel();                     

            // Get reference to the Umbraco helper             
            UmbracoHelper helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language ("SV" or "EN")
            var currentLanguage = helper.GetDictionaryValue("Language");

            // Set the site part (used by the NewsLatest partial view and the GetAreaMarkersList method)
            model.ContentType = CurrentPage.DocumentTypeAlias;


            /* TIPS */

            // Get a string of comma-separated sub page node id:s (optional)
            var selectedNodes = CurrentPage.GetPropertyValue<string>(MyConstants.TipsThingsToDoStartPagePropertyAlias);

            if (!string.IsNullOrEmpty(selectedNodes))
            {
                model.SelectedNodes = selectedNodes;

                // Get fixed values for the tips section
                model.TipsTitle = CurrentPage.GetPropertyValue<string>(MyConstants.TitleTipsPropertyAlias);
                model.MoreTipsTerm = helper.GetDictionaryValue("MoreTips");

                // Get the url of the ThingsToDo landing page due to language
                MasterController mc = new MasterController();
                string rootDocTypeAlias = mc.GetRootContentType(currentLanguage);
                int nodeId = helper.TypedContentAtXPath("//" + rootDocTypeAlias + "/" + MyConstants.ThingsToDoLandingPageDocTypeAlias).First().Id;
                model.MoreTipsUrl = helper.NiceUrl(nodeId);
            }


            /* FIXED LOCAL PUFFS (optional, max 3) */

            // Get the puffs collection (Nested Content datatype)
            var fixedPuffs = CurrentPage.GetPropertyValue<IEnumerable<IPublishedContent>>(MyConstants.PuffFixedModulePropertyAlias);

            // Initiate flag
            model.FixedPuffsExists = false;

            // Make sure at least one puff exists
            if (fixedPuffs != null)
            {

                // Create controller reference
                PuffsSurfaceController psc = new PuffsSurfaceController();

                // Initiate a temp list
                List<PuffModel> tempList = new List<PuffModel>();

                // Initiate counter
                int counter = 0;

                // Iterate over the puffs
                foreach (var item in fixedPuffs)
                {
                    // Enumerate counter
                    counter += 1;

                    // Break out of loop when three puffs is reached
                    if (counter > 3)
                    {
                        break;
                    }

                    // Set flag
                    model.FixedPuffsExists = true;                  

                    // Instanciate a Puff object
                    var objPuff = new PuffModel();

                    // Get puff properties 
                    objPuff = psc.GetPuff(item, helper);

                    // Add puff to temp list
                    tempList.Add(objPuff);                   
                }

                // Add temp puffs list to return model
                //model.FixedPuffsCollection = tempList;

                // Create a PuffsList object to contain the list when it's sent to a partial
                PuffsListModel objPuffsList = new PuffsListModel();

                // Add list
                objPuffsList.PuffsCollection = tempList;

                // Set gallery flag (= list the puffs as gallery in small devices)
                objPuffsList.Gallery = true;

                // Add object to return model
                model.PuffsList = objPuffsList;
            }

            return View(model);
        }
    }
}