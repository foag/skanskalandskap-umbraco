// Initiate a global map object
var map = {};

// Initiate a global markers array
var markers = [];


// Google maps API initialisation
function initializeOsm(mapdata) {

    mapCanvas = document.getElementById("osm_map");

    // Collect the Google map types and add OpenStreetMap
    var mapTypeIds = [];
    for (var type in google.maps.MapTypeId) {
        mapTypeIds.push(google.maps.MapTypeId[type]);
    }
    mapTypeIds.push("OSM");

    // Create map with basic coordinates
    map = new google.maps.Map(mapCanvas, {
        center: new google.maps.LatLng(mapdata.lat, mapdata.lng),
        zoom: mapdata.zoom,
        mapTypeId: "OSM",
        mapTypeControl: true,
        mapTypeControlOptions: {
            mapTypeIds: mapTypeIds
        },
        streetViewControl: true
    });


    // Make sure url name exist
    if (mapdata.urlname != ''){

        // Add kml layer for the recreation area's area
        var borderLayer = new google.maps.KmlLayer({
            url: 'http://www.skanskalandskap.se/mapdata/borders/' + mapdata.urlname + '.kml',
            preserveViewport: true,
            suppressInfoWindows: true,
            clickable: true,
            map: map
        });

        // Add click event for kml layer to open info window
        borderLayer.addListener('click', function () {
            var infoWindow = new google.maps.InfoWindow({
                'maxWidth': 250,
                'position': new google.maps.LatLng(mapdata.lat, mapdata.lng),
                'content': 'Testar med lite text.',
                'map': map
            });
        }, this);
    }

    // Define OSM map type pointing at the OpenStreetMap tile server
    map.mapTypes.set("OSM", new google.maps.ImageMapType({
        getTileUrl: function (coord, zoom) {
            return "http://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
        },
        tileSize: new google.maps.Size(256, 256),
        name: "OpenStreetMap",
        maxZoom: 18
    }));
}



(function ($) {

    // Make sure there is an element for the OSM map
    if ($('#osm_map').length > 0) {

      console.log(Global.currentNodeId);

        // Create the default map
        $.getJSON('/umbraco/Surface/MapsSurface/GetBasicData', 'nodeId=' + Global.currentNodeId, function (data) {

            // Assign the fetched map and page data to variables
            var coordLat = data.map.Latitude;
            var coordLng = data.map.Longitude;
            var zoom = data.map.Zoom;
            var urlName = data.map.UrlName;

            // Create map after page was loaded
            google.maps.event.addDomListener(window, 'load', initializeOsm({ 'lat': coordLat, 'lng': coordLng, 'zoom': zoom, 'urlname': urlName }))
        });
    }

    // Attach click events to the service buttons in the interactive panel
    $(document).on('click', '#services-osm .click-me', function (e) {
        e.stopPropagation();
        ManageServiceRequestOsm($(this), map);
    });

})(jQuery);




function ManageServiceRequestOsm($requestElement, $map) {

    // Get service type's url name
    var serviceType = $requestElement.nextAll('span:first').text();

    // Check whether current button is active
    var active = $requestElement.hasClass('active');

    // At first button click, display markers; at second click, hide them.
    switch (active) {

        // First click
        case false:

            // Show markers
            DisplayServiceOsm(serviceType, $map);

            // Add active flag
            $requestElement.addClass('active');

            break;

        // Second click
        case true:

            // Hide markers
            HideServiceOsm(serviceType, $map);

            // Remove active flag
            $requestElement.removeClass('active');

            break;
    }
}


function DisplayServiceOsm(serviceType, $map) {

    // Create url
    var url = "/umbraco/Surface/MapsSurface/GetMarkersForService";

    var params = {
        serviceType: serviceType,
        language: Global.language,
        nodeId: Global.currentNodeId,
    };

    // Get map data for a specific service provided by a specific area
    $.getJSON(url, params, function (data) {

        // Initiate a counter
        //var counter = 0;

        // Iterate over the returned objects
        $.each(data.markers, function (i, item) {

            // Drop markers consecutively by using window.setTimeout
            // window.setTimeout(function () {

                // Create marker
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(item.Latitude, item.Longitude),
                    title: item.Title,
                    tag: item.Tag,
                    icon: {
                        url: item.Icon,
                        size: new google.maps.Size(80,80)
                    },
                    //icon: new google.maps.MarkerImage(item.Icon),
                    zIndex: google.maps.Marker.MAX_ZINDEX + 1,
                    //animation: google.maps.Animation.DROP,
                    map: $map
                });

                // Add marker to the markers global array
                markers.push(marker);

                // Initiate variable for the infoWindow content
                var infoWindowContent = "";

                // Bus stop service type
                if (serviceType == "hallplats") {

                    // Clone the html element for the busstop infowindow (in the MapInteractivity partial)
                    $('.dataContainer').first().clone(true).appendTo('.busstop-list');

                    // Get the new (last) html elements
                    var dataContainer = $('.dataContainer').last();

                    // Add infowindow header
                    $(dataContainer).find('.popup-header span').text(item.Title);

                    // Add infowindow information text
                    $(dataContainer).find('.popup-text span').html(item.Content);

                    // Add route pdf link, if any.
                    if (item.PublicRoutePdf.Path != "")
                    {
                        $(dataContainer).find('.document-icon').attr('src', item.PublicRoutePdf.IconPath);
                        $(dataContainer).find('.route-pdf').attr('href', item.PublicRoutePdf.Path);
                        $(dataContainer).find('.route-pdf').attr('target', '_blank');
                        $(dataContainer).find('.route-pdf').text(item.DownloadRouteLinkTerm + item.PublicRoutePdf.Info);
                    }

                    // Add resrobot id string
                    $(dataContainer).find('#resRobotZID').val(item.ResRobotZID);

                    infoWindowContent = $(dataContainer).html();
                }
                // Any other type
                else
                {
                    infoWindowContent = item.Content;
                }

                // Add click event for marker to open info window
                marker.addListener('click', function () {
                    var infoW = new google.maps.InfoWindow({
                        'maxWidth': 500,
                        'position': new google.maps.LatLng(item.Latitude, item.Longitude),
                        'content': infoWindowContent,
                        'map': $map
                    });
                }, this);
            //}, 200 * counter);

            // Enumerate counter
            //counter += 1;
        });
    });
}


function HideServiceOsm(serviceType, $map) {

    // Remove markers with a specific tag value
    for (var i = markers.length; i--;) {
        if (markers[i].tag == serviceType) {
            markers[i].setMap(null);
            markers.splice(i, 1);
            if (serviceType == "hallplats") {
                $('.dataContainer').last().remove();
            }
        }
    }
}
