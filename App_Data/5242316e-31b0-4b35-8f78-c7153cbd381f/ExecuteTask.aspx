﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExecuteTask.aspx.cs" Inherits="CMSImport.Pages.ExecuteTask" MasterPageFile="/umbraco/masterpages/umbracoPage.Master"%>
<%@ Register Src="../UserControls/ImportSteps/Importing.ascx" TagName="Import" TagPrefix="CMSImport" %>
<%@ Register Src="../UserControls/ImportSteps/ConfirmSelectedOptions.ascx" TagName="Confirm" TagPrefix="CMSImport" %>
 <%@ Register Src="../UserControls/StateError.ascx" TagName="StateError"
    TagPrefix="CMSImport" %>
<%@ Register Assembly="controls" Namespace="umbraco.uicontrols" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
    <cc1:UmbracoPanel ID="UmbracoPanel" runat="server">
    <CMSImport:StateError id="StateError" runat="server" />
        <cc1:Pane ID="ReadPane" runat="server">
            <asp:Image ID="LogoImage" CssClass="cmsimportLogo" runat="server" />
            <h2><asp:Literal ID="TitleLiteral" runat="server"/></h2>
            <asp:PlaceHolder ID="ConfirmPlaceholder" runat="server">
                <cc1:PropertyPanel ID="PropertyPanel2" runat="server" Text="">
                    <CMSImport:Confirm id="Confirm" runat="server" />
                </cc1:PropertyPanel>
<br /><br />
                <cc1:PropertyPanel ID="buttonsPanel" runat="server" Text="" style="position:relative;">
                <asp:Button ID="ImportButton" runat="server" OnClick="ImportButton_Click" OnClientClick="ShowInProgress();"/>
                &nbsp;&nbsp;&nbsp;
<div id="ShowInprogressDialog" style="position:absolute; top:-120px;Left:80px;width:400px;height:200px;border-style: solid;background-color:#F8F8FA;z-index:99;border-color:#D9D7D7;border-width:1px;display:none;"><div style="padding-top:40px;padding-left:80px;"><p><asp:Literal ID="ImportingDialogLiteral" runat="server" Text="Importing Data don't interrupt this process"/></p>
		<img src="/umbraco_client/images/progressBar.gif" alt="Importing"/><br /></div></div>
</cc1:PropertyPanel>
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="CMSImportPlaceHolder" runat="server">
                <cc1:PropertyPanel ID="PropertyPanel1" runat="server" Text="">
                    <CMSImport:Import id="Import" runat="server" />
                </cc1:PropertyPanel>
            </asp:PlaceHolder>
</cc1:Pane>
</cc1:UmbracoPanel>
</asp:Content>