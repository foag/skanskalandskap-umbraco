﻿
namespace SkanskaLandskap15.Models.Shared
{
    public class ServiceStandardModel : ServiceModel
    {
        public int ServiceTypeNodeId { get; set; }
        public MapMarkerModel MapMarker { get; set; }    
        public string MapLocation { get; set; }
    }
}
