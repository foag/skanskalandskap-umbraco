﻿using System;
using MightyLittleGeodesy.Positions;


namespace SkanskaLandskap15.Classes.Utilities
{

    public class ConvertCoords
    {

        public ConvertCoords() { }
     
       
	    public static string ConvertRT902GWS84(string coord1, string coord2) {

            // Remove possibly empty space
            coord1 = coord1.Trim();
            coord2 = coord2.Trim(); 

            // Initiate the return string
            string fixedPos = string.Empty;
           
            // Make sure the coordinate values have seven characters each
            if (coord1.Length == 7 && coord2.Length == 7)
            {
                // Convert the coordinates to integer values
                int coord1Number = Convert.ToInt32(coord1);
                int coord2Number = Convert.ToInt32(coord2);

                // Initiate variables and... 
                int largestCoord = 0;
                int smallestCoord = 0;

                //...make sure they are sent to the convert function in correct order
                if (coord1Number > coord2Number)
                {
                    largestCoord = coord1Number;
                    smallestCoord = coord2Number;
                }
                else
                {
                    largestCoord = coord2Number;
                    smallestCoord = coord1Number;
                }
                
                // Perform the conversion                
                RT90Position position = new RT90Position(largestCoord, smallestCoord);
                WGS84Position wgsPos = position.ToWGS84();                
                double lat = Math.Round(wgsPos.Latitude, 4);
                double lon = Math.Round(wgsPos.Longitude, 4);

                // Replace comma with dot
                string latitude = lat.ToString().Replace(",", ".");
                string longitude = lon.ToString().Replace(",", ".");

                // Add a zoom value - any value will do - to make the coordinates work on iOS5 devices as well
                fixedPos = latitude + "," + longitude + ",13"; 
            }
            else
            {
                fixedPos = "bad_input";
            }
            return fixedPos;
	    }
    }
}