﻿using System.Collections.Generic;
using System.Web;
using SkanskaLandskap15.Models.Masters;
using SkanskaLandskap15.Models.Shared;


namespace SkanskaLandskap15.Models
{
    public class ContactPersonsPageModel : InteriorMasterModel
    {        
        public List<ContactModel> ContactsCollection { get; set; }
    }
}