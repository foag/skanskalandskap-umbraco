﻿using System.Collections.Generic;


namespace SkanskaLandskap15.Models.Shared
{

    public class SubMenuModel
    {
        public string PuffsTitleTerm { get; set; }
        public string LinksTitleTerm { get; set; }
        public bool PuffsExists { get; set; }
        public bool LinksExists { get; set; }
        public List<SubMenuPuffModel> PuffsCollection { get; set; }
        public List<SubMenuLinkModel> LinksCollection { get; set; }
        public List<SubMenuLinkCategoryModel> LinkCategories { get; set; }

        public List<SubMenuLinkModel> LinksCollectionColumn1 { get; set; }
        public List<SubMenuLinkModel> LinksCollectionColumn2 { get; set; }
        public List<SubMenuLinkModel> LinksCollectionColumn3 { get; set; }
    }
}