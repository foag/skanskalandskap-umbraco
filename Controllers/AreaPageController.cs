﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RJP.MultiUrlPicker.Models;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Controllers.SurfaceControllers;
using SkanskaLandskap15.Models;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Core.Models;
using Umbraco.Web;



namespace SkanskaLandskap15.Controllers
{
    public class AreaPageController : InteractiveMasterController
    {
      
        public ActionResult AreaPage()
        {

            // Initiate the return model
            var model = new AreaPageModel();

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language ("SV" or "EN") 
            string currentLanguage = helper.GetDictionaryValue("Language");

            // Get controller reference
            MasterController mc = new MasterController();
            
            // Add a sitepart property for the Fulltofta page 
            if(CurrentPage.UrlName == "fulltofta")
            {
                model.SitePart = "Fulltofta";
            }            
            
            // Set Name, UrlName, Content type and Language
            model.Name = CurrentPage.Name;
            model.UrlName = CurrentPage.UrlName;
            model.ContentType = CurrentPage.DocumentTypeAlias;
            model.Language = currentLanguage;
                        

            /* TIPS SECTION (OPTIONAL) */

            // Explicitly set property as empty
            model.SelectedNodes = string.Empty;

            // Get a string of comma-separated sub page node id:s (optional)
            string selectedNodes = CurrentPage.GetPropertyValue<string>(MyConstants.TipsThingsToDoPropertyAlias);

            if(selectedNodes != string.Empty)
            {
                model.SelectedNodes = selectedNodes;

                // Get fixed values for the tips section
                model.TipsTitle = CurrentPage.GetPropertyValue<string>(MyConstants.TitleTipsPropertyAlias);
                model.MoreTipsTerm = helper.GetDictionaryValue("MoreTips");

                // Get the url of the ThingsToDo landing page due to language                
                string rootDocTypeAlias = mc.GetRootContentType(currentLanguage);
                int nodeId = helper.TypedContentAtXPath("//" + rootDocTypeAlias + "/" + MyConstants.ThingsToDoLandingPageDocTypeAlias).First().Id;
                model.MoreTipsUrl = helper.NiceUrl(nodeId);
            } 
      
            
            /* TEXTS  */

            // Richtext content
            model.DoAndExperience = CurrentPage.GetPropertyValue<IHtmlString>(MyConstants.DoAndExperiencePropertyAlias);

            
            /* FIND US */            
            
            // Add a populated Find Us object to the return model
            model.FindUs = GetFindUsInfo(helper, currentLanguage);

               
            /* DICTIONARY TERMS  */

            model.AreaTerm = helper.GetDictionaryValue("RecreationArea");
            model.AboutTerm = helper.GetDictionaryValue("AboutArea");
            model.BackTerm = helper.GetDictionaryValue("Back");
            model.FilterTerm = helper.GetDictionaryValue("FilteringLinkTitleAreaMap");
            model.DoAndExperienceTerm = helper.GetDictionaryValue("DoAndExperience");
            model.MoreActivitiesTerm = helper.GetDictionaryValue("MoreActivities");
            model.FindUsTerm = helper.GetDictionaryValue("FindUs");
                 
            
            
            /* DOWNLOAD & ORDER  */

            // Inititiate common variable
            string downloadsPicker = string.Empty;

            // Manage the section due to language (there should be no ordering link and info on English area pages)
            switch(currentLanguage)
            {
                case "SV":

                    // Create a link to the ordering page by getting a node id that was set in the parent node (areas landing page)
                    string orderingPageNodeId = CurrentPage.Parent.GetPropertyValue<string>(MyConstants.SelectOrderingPagePropertyAlias);

                    // Initiate flag
                    model.OrderingPageExists = false;

                    if (!string.IsNullOrEmpty(orderingPageNodeId))
                    {
                        // Set flag
                        model.OrderingPageExists = true;

                        // Value is an area's node id             
                        Int32 nodeId = Convert.ToInt32(orderingPageNodeId);

                        // Get ordering page's url
                        model.OrderingPageUrl = helper.TypedContent(nodeId).Url;

                        // Get ordering page's url
                        model.OrderingPageLinkTitle = helper.GetDictionaryValue("OrderStuff");

                        // Get intro text from parent node
                        model.OrderingText = CurrentPage.Parent.GetPropertyValue<string>(MyConstants.textPropertyAlias);
                    }

                    // Get selected Media nodes for the Download section        
                    downloadsPicker = CurrentPage.GetPropertyValue<string>(MyConstants.DownloadsPropertyAlias);

                    // Initiate flag
                    model.DownloadsExists = false;

                    // Check whether downloads exists
                    if (!string.IsNullOrWhiteSpace(downloadsPicker))
                    {

                        // Create controller reference
                        MediaSurfaceController msc = new MediaSurfaceController();

                        // Get the media files collection and add to return model
                        model.MediaFiles = msc.GetDownloadFiles(downloadsPicker, helper, currentLanguage);

                        // Make sure at least one of the selected files actually exists in Media 
                        if (model.MediaFiles.MediaFilesCollection.Count() > 0)
                        {
                            // Set flag          
                            model.DownloadsExists = true;
                        }
                    }

                    // Get header from Dictionary
                    if (model.OrderingPageExists || model.DownloadsExists)
                    {
                        model.DownloadsTitle = helper.GetDictionaryValue("DownloadAndOrder");
                    }
            
                    break;

                case "EN":

                    // Get selected Media nodes for the Download section        
                    downloadsPicker = CurrentPage.GetPropertyValue<string>(MyConstants.DownloadsPropertyAlias);

                    // Initiate flag
                    model.DownloadsExists = false;

                    // Check whether downloads exists
                    if (!string.IsNullOrWhiteSpace(downloadsPicker))
                    {

                        // Create controller reference
                        MediaSurfaceController msc = new MediaSurfaceController();

                        // Get the media files collection and add to return model
                        model.MediaFiles = msc.GetDownloadFiles(downloadsPicker, helper, currentLanguage);

                        // Make sure at least one of the selected files actually exists in Media 
                        if (model.MediaFiles.MediaFilesCollection.Count() > 0)
                        {
                            // Set flag          
                            model.DownloadsExists = true;
                        }
                    }

                    // Get header from Dictionary
                    if (model.DownloadsExists)
                    {
                        model.DownloadsTitle = helper.GetDictionaryValue("Download");
                    }
                    break;
            }

          

            /* SERVICES  */

            // Get "Show on map" title
            model.ShowOnMapTerm = helper.GetDictionaryValue("ShowOnMap");

            // Get map marker icon
            model.MarkerIconPath = Paths.GetIconPath("marker");

            // Get cache duration
            model.ServicesCacheDuration = MasterController.GetOutputCacheDuration("services");
            
            // Create controller reference
            MapInteractivitySurfaceController mapisc = new MapInteractivitySurfaceController();

            // Get a list of current area's services including counts and icons  
            model.ServiceTypes = mapisc.GetServiceTypes(currentLanguage, CurrentPage.Id);



            /* INFORMATION  */

            PuffsListModel modelPuffs = new PuffsListModel();                  
          
            // Get warning module's title
            model.WarningsTitle = CurrentPage.GetPropertyValue<string>(MyConstants.TitleWarningsPropertyAlias).Trim();

            // Get the puffs collection (Nested Content datatype)
            var warnings = CurrentPage.GetPropertyValue<IEnumerable<IPublishedContent>>(MyConstants.WarningsModulePropertyAlias);

            // Initiate flag
            model.WarningsExists = false;

            // Make sure at least one puff exists
            if (warnings != null)
            {               

                // Create controller reference
                PuffsSurfaceController psc = new PuffsSurfaceController();

                // Initiate a temp list
                List<PuffModel> tempListWarnings = new List<PuffModel>();

                // Iterate over the puffs
                foreach (var item in warnings)
                {
                    // Get unpublish datetime for current warning
                    DateTime unpublish = item.GetPropertyValue<DateTime>(MyConstants.WarningUnpublishPropertyAlias);

                    // Manage default value if no datetime was selected
                    bool dateExists = false; 
                    if(unpublish.ToString() != "0001-01-01 00:00:00")
                    {
                        dateExists = true;
                    }

                    // Make sure there is either no datetime set (optional) or datetime is later than now
                    if(!dateExists || (dateExists && unpublish > DateTime.Now))
                    {
                        // Set flag
                        model.WarningsExists = true;

                        // Instanciate a Puff object
                        var objPuff = new PuffModel();
                    
                        // Get puff properties 
                        objPuff = psc.GetWarningProperties(item, objPuff, helper);
                                   
                        // Add puff to temp list
                        tempListWarnings.Add(objPuff);
                    }                     
                }

                // Add temp puffs list to return model
                model.WarningsCollection = tempListWarnings;
            }
            
            // Opening hours and contact information
            model.GeneralInfo = CurrentPage.GetPropertyValue<IHtmlString>(MyConstants.GeneralInfoPropertyAlias);

            // Title for the general information. Should be visible only with mobile resolution.
            model.GeneralInfoTitle = CurrentPage.GetPropertyValue<string>(MyConstants.GeneralInfoTitlePropertyAlias);

            // Useful information about the area (nature reserve etc)
            model.UsefulInfo = CurrentPage.GetPropertyValue<IHtmlString>(MyConstants.UsefulInformationPropertyAlias);

            // Get section header if field is not empty
            if (!String.IsNullOrWhiteSpace(model.UsefulInfo.ToString()))
            {
                model.UsefulInformationTerm = helper.GetDictionaryValue("UsefulInformation");                   
            }
            

            /* SLIDESHOW  */

            // Images for slideshow        
            string imagePicker = CurrentPage.GetPropertyValue<string>(MyConstants.ImagePickerPropertyAlias);

            // Initiate flag
            model.SlideshowExists = false;

            // Make sure images exists (required but you never know...)
            if (!string.IsNullOrWhiteSpace(imagePicker))
            {
                // Set flag          
                model.SlideshowExists = true;

                // Create controller reference
                MediaSurfaceController msc = new MediaSurfaceController();

                // Get a populated Slideshow object and add it to the return model
                model.Slideshow = msc.GetSlideshow(imagePicker, helper);
            }


            /* PUFFS SLIDESHOW IS RETRIEVED FROM THE VIEW */


            /* ACCOMMODATIONS IN THE AREA (OPTIONAL TIPS) */

            // Explicitly set tips flag
            model.AccommodationTipsExists = false;

            // Initiate a puff list
            List<SubPagePuffModel> selectedCollection = new List<SubPagePuffModel>();

            // Get a string of comma-separated sub page node id:s
            var strNodes = CurrentPage.GetPropertyValue<string>(MyConstants.TipsAccommodationsPropertyAlias);

            // Check whether at least one sub page is selected
            if (!string.IsNullOrEmpty(strNodes))
            {
                // Initiate a SubPagesList object
                var subPages = new SubPagesListModel();

                // Get controller reference
                PuffsSurfaceController psc = new PuffsSurfaceController();

                // Get a list of selected puffs/sub pages 
                selectedCollection = psc.GetSelectedAccommodationSubPages(strNodes, helper);

                // Make sure there is at least one published sub page node in the collection.
                // (You're able to select unpublished nodes in MNTP.)
                if (selectedCollection.Any())
                {
                    // Set flag
                    model.AccommodationTipsExists = true;

                    // Add list of selected puffs to the sub pages list object
                    subPages.SelectedAccommodationsCollection = selectedCollection;

                    // Set a flag to indicate this special type of puffs
                    subPages.AccommodationsInTheAreaType = true;

                    // Add sub pages object to return model
                    model.SubPagesAccommodations = subPages;                   
                }
            }

            // Get fixed value for the accommodation tips section
            if (model.AccommodationTipsExists)
            {
                model.AccommodationsTipsTitle = helper.GetDictionaryValue("AccommodationsInTheArea");               
            }
            

            /* MISC  */
          
            // Add a MapInteractivity object to the return model
            model.MapInteractivity = new MapInteractivityModel();

            // Set the map element's id            
            model.MapType = "osm_map";

            // Add event and news item objects to the return model but only for the Swedish Fulltofta page  
            if(currentLanguage == "SV" && CurrentPage.UrlName == "fulltofta")
            {                
                // Add an empty EventItem object 
                model.EventItem = new EventItemModel();

                // Add an empty NewsItem object 
                model.NewsItem = new NewsItemModel();
            }
           
            // Add current language
            model.Language = currentLanguage;

            // Add a ServiceList object
            model.ServiceList = new ServiceListModel();

            // Add title and intro for the services filtering dialog (in small devices)
            model.FilteringTitleTerm = helper.GetDictionaryValue("MobileFilteringServicesTitle");
            model.FilteringIntroTerm = helper.GetDictionaryValue("MobileFilteringServicesIntro");

            return View(model);
        }





        /* HELPER METHODS  */

        
        public FindUsModel GetFindUsInfo(UmbracoHelper helper, string language)
        {
            // Initiate a return model
            FindUsModel model = new FindUsModel();

            // Get controller reference
            MediaSurfaceController msc = new MediaSurfaceController();


            /* INTRO TEXT */

            // Get intro, if any
            model.FindUsIntro = CurrentPage.GetPropertyValue<String>(MyConstants.findUsIntroPropertyAlias);


            /* DESCRIPTION TEXT */

            // Get text, if any
            model.FindUsText = CurrentPage.GetPropertyValue<IHtmlString>(MyConstants.FindUsTextPropertyAlias);


            /* BY CAR LINK */

            // Get "by car" link (in this case only a single link can be selected).
            var links = CurrentPage.GetPropertyValue<MultiUrls>(MyConstants.MultiUrlPickerPropertyAlias);

            // Initiate link flag
            model.ByCarLinkExists = false;

            // Check whether link exists (optional)
            if (links.Any())
            {
                // Set flag
                model.ByCarLinkExists = true;

                // Create a Link object
                LinkModel objLink = new LinkModel();

                // Populate link object with properties
                objLink.Url = links.First().Url;
                objLink.Title = links.First().Name;
                objLink.Target = links.First().Target;

                // Add link object to puff object
                model.ByCarLink = objLink;           
            }
                                  
            
            /* DICTIONARY TERMS AND ICONS */
            
            // The "public" link (always present) 
            model.GoByPublicLinkTerm = helper.GetDictionaryValue("ByTrainAndBus");
            model.BusIconPath = Paths.GetIconPath("bus");

            // The "by car" link (optional) 
            if(model.ByCarLinkExists)
            {
                model.GoByCarLinkTerm = helper.GetDictionaryValue("ByCar");
                model.CarIconPath = Paths.GetIconPath("car");
            }                     
           
            
            /* MAP INTERACTION DATA */

            // Get id of the service types parent node
            int parentNodeId = helper.TypedContentSingleAtXPath("//" + MyConstants.ServiceTypesDocTypeAlias).Id;

            // Get content based on node id
            var content = helper.TypedContent(parentNodeId);

            // Get object with node id for the Busstop service type
            ServiceTypeModel objBusstop = content
                .Children
                .Where(c => c.UrlName == "hallplats")
                .Select(obj => new ServiceTypeModel()
                {
                    NodeId = obj.Id
                }).First();

            // Add node id:s for the affected service types
            model.BusstopNodeId = objBusstop.NodeId;

            return model;
        }         

    }
}