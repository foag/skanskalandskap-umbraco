﻿
namespace SkanskaLandskap15.Models.Shared
{
    public class SubMenuPuffModel
    {
        public int NodeId { get; set; }
        public string Tag { get; set; }
        public dynamic Image { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
    }
}