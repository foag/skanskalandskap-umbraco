﻿
namespace SkanskaLandskap15.Models.Shared
{
    public class MediaFileModel
    {        
        public string Name { get; set; }
        public string Path { get; set; }
        public string Extension { get; set; }
        public string Size { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public bool Portrait { get; set; }
        public string Info { get; set; }
        public string IconPath { get; set; }
    }
}