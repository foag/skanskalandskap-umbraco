﻿using System.Collections.Generic;
using SkanskaLandskap15.Models.Masters;
using SkanskaLandskap15.Models.Shared;


namespace SkanskaLandskap15.Models
{
    public class ForSchoolsLandingPageModel : InteriorMasterModel
    {
        public NewsListModel NewsList { get; set; }

        public string NewsTitleTerm { get; set; }
        public bool NewsExists { get; set; }
        //public string MoreTipsUrl { get; set; }
        //public SubPagesListModel SubPagesSelected { get; set; }
        //public SubPagesListModel SubPagesRemaining { get; set; }
        //public string SelectedNodes { get; set; }

        //public DownloadableModel DownloadableItem { get; set; }
    }
}