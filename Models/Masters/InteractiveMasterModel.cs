﻿using SkanskaLandskap15.Models.Shared;


namespace SkanskaLandskap15.Models.Masters
{
    public class InteractiveMasterModel : MasterModel
    {         
        public dynamic ImageSingle { get; set; }
        public string MapType { get; set; }

        public string ShowAllTerm { get; set; }
        public string HideAllTerm { get; set; }
        public string ResetTerm { get; set; }
        public string LoadingTerm { get; set; }
        public string BusStopsErrorTerm { get; set; }
        public string GeneralMapErrorTerm { get; set; }
        public string DetailedDirectionsTerm { get; set; }
        public string HideDetailedDirectionsTerm { get; set; }        
        public string CloseFilteringLinkTerm { get; set; }
        public string ShowAreasLinkTerm { get; set; }

        public MapInteractivityModel MapInteractivity { get; set; }
    }
}