(function ($) {

    // Initiate a global map object
    var map = {};

    function initialize(mapdata) {
        var mapCanvas = document.getElementById('u-map');
        var mapOptions = {
            center: new google.maps.LatLng(mapdata.lat, mapdata.lng),
            zoom: mapdata.zoom,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(mapCanvas, mapOptions)
    }


    // Make sure there is an element for the custom map  
    if ($('#u-map').length > 0) {
             
        $.getJSON('/umbraco/Surface/UMapSurface/GetBasicData', 'nodeId=' + Global.currentNodeId, function (data) {
            // Assign the fetched map and page data to variables
            //var coordLat = data.map.Latitude;
            //var coordLng = data.map.Longitude;
            //var zoom = data.map.Zoom;
            //var urlName = data.map.UrlName;
            //var addMarkers = data.map.AddMarkers;

            // Create map after page was loaded
            //google.maps.event.addDomListener(window, 'load', initialize({ 'lat': coordLat, 'lng': coordLng, 'zoom': zoom }));

            // Add kml layer to display area borders
            //var borderLayer = new google.maps.KmlLayer({
            //    url: 'http://www.skanskalandskap.se/mapdata/borders/' + urlName + '.kml',
            //    preserveViewport: true,
            //    suppressInfoWindows: true,
            //    clickable: false
            //});
            //borderLayer.setMap(map);

            //if (addMarkers == true) {
            //    alert("Add markers");
            //}
        });
    }
})(jQuery);