﻿using System.Web;
using System.Collections.Generic;

namespace SkanskaLandskap15.Models.Shared
{
    public class PuffsSlideModel
    {                  
        public string Title { get; set; }
        public IHtmlString Text { get; set; }
        public ImageModel Image { get; set; }
        public bool LinksExists { get; set; }       
        public List<LinkModel> LinksCollection { get; set; }
        public string IconPath { get; set; }
    }
}