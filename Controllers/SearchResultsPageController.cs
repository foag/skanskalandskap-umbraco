﻿using System;
using System.Web.Mvc;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Models;




namespace SkanskaLandskap15.Controllers
{
    public class SearchResultsPageController : MasterController
    {
        

        public ActionResult SearchResultsPage()
        {
            // Initiate return model
            var model = new SearchResultsPageModel();

            // Initiate the access-without-search flag, aimed to facilitate the styling
            // of the search results page when accessed from e.g. a 404 page. 
            model.AccessWithoutSearch = false;

            // Initiate the search page title
            string titleTerm = String.Empty;          
         
            // Visitor navigates to the search page without performing a search            
            if (String.IsNullOrEmpty(Request.QueryString["term"]))
            {
                // Set the access-without search flag
                model.AccessWithoutSearch = true;

                // Get title from Umbraco's dictionary
                titleTerm = Umbraco.GetDictionaryValue("SearchTheSite");
            }
            // A search is performed
            else
            {
                // Get title from Umbraco's dictionary
                titleTerm = Umbraco.GetDictionaryValue("SearchResult");
            }
         
          
            // Assign page title to the return model
            model.TitleTerm = titleTerm;
           
            return View(model);
        }
    }
}