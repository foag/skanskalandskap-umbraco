﻿using System;
using System.Globalization;
using System.Threading;




namespace SkanskaLandskap15.Classes.Utilities
{
    public class UmbracoHelperExtensions
    {

        public UmbracoHelperExtensions() { }

            

        public static string TruncateAtWord(string value, int length)
        {

            if (value == null || value.Trim().Length <= length)
            {
                return value;
            }
            else
            {
                int index = value.Trim().LastIndexOf(" ");

                while ((index + 3) > length)
                {
                    index = value.Substring(0, index).Trim().LastIndexOf(" ");
                }

                if (index > 0)
                {
                    value = value.Substring(0, index) + "...";
                    return value;
                }
                else
                {
                    value = value.Substring(0, length - 3) + "...";
                    return value;
                }
            }
        }


        // Set's the culture of a thread (needed when Ajax call is made to a SurfaceController).
        public static void EnsureCulture(Thread currentThread, string language)
        {
            // Initiate the culture name variable
            string cultureName = string.Empty;

            // Get culture name based on current language
            switch (language)
            {
                case "EN":
                    cultureName = "en-US";
                    break;
                case "SV":
                    cultureName = "sv-SE";
                    break;
            }
            
            // Set culture and UI culture
            currentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureName);
            // Note second parameter which specifies whether to use the user-selected culture settings from the system.
            currentThread.CurrentUICulture = new CultureInfo(cultureName, false);
        }



        public static double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }



        public static string GetReadableFileSizeViaBytes(long Bytes)
        {
            if (Bytes >= 1073741824)
            {
                Decimal size = Decimal.Divide(Bytes, 1073741824);
                return String.Format("{0:##.##} gb", size);
            }
            else if (Bytes >= 1048576)
            {
                Decimal size = Decimal.Divide(Bytes, 1048576);
                return String.Format("{0:##.#} mb", size);
            }
            else if (Bytes >= 1024)
            {
                Decimal size = Decimal.Divide(Bytes, 1024);
                return String.Format("{0:##} kb", size);
            }
            else if (Bytes > 0 & Bytes < 1024)
            {
                Decimal size = Bytes;
                return String.Format("{0:##.##} bytes", size);
            }
            else
            {
                return "0 Bytes";
            }
        }



        public static string GetNiceFileSize(string strUBytes)
        {
            // Convert bytes string to long data type
            long lngUBytes = Convert.ToInt64(strUBytes);

            // Convert bytes value to megabytes in double data type 
            double dblMegaBytes = (double)UmbracoHelperExtensions.ConvertBytesToMegabytes(lngUBytes);

            // Round the megabyte value to one decimal and return as string
            return Math.Round(dblMegaBytes, 1).ToString();
        }     
    }
}