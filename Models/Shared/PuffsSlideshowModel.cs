﻿using System.Collections.Generic;

namespace SkanskaLandskap15.Models.Shared
{
    public class PuffsSlideShowModel
    {                  
        public bool SlidesExists { get; set; }
        public List<PuffsSlideModel> SlidesCollection { get; set; }
        public string Title { get; set; }
    }
}