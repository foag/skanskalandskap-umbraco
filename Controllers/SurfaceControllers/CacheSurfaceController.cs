﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Core;
//using Umbraco.Core.CacheHelper;
using Umbraco.Web;
using Umbraco.Web.Mvc;



namespace SkanskaLandskap15.Controllers.SurfaceControllers
{


    public class CacheSurfaceController : SurfaceController
    {
       

        // Initiate global database variables        
        //protected SqlConnection conn { get; set; }
        //String ConnectionString = ConfigurationManager.ConnectionStrings["umbracoDbDSN"].ToString();

        public CacheSurfaceController() { }

       
        public void StoreTrafiklabInfo()
        {
            //Response.Write("TESTING TESTING");
            //Debug.WriteLine("JOB IS EXECUTING");

            string test = "test";


            // Get a reference to the Umbraco helper         
            UmbracoHelper helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language ("SV" or "EN") 
            string currentLanguage = helper.GetDictionaryValue("Language");

            // Get controller reference
            MapsSurfaceController msc = new MapsSurfaceController();


            // MAKE SURE TRAFIKLAB IS AVAILABLE

            // Set type of request (by name or by nearby)
            string typeOfRequest = "by name";

            // Get api url for the Trafiklab request
            string apiUrl = Paths.GetTrafikLabUrl(typeOfRequest);

            // Initiate Trafiklab availability flag
            bool flagTrafiklab = false;

            // Check whether the service is up and running
            flagTrafiklab = msc.CheckTrafiklab(apiUrl, currentLanguage);

            // Send e-mail warning if store operation failed
            if (!flagTrafiklab)
            {

                return;
            }

            // The Trafiklab service seem to be up and running
            if (flagTrafiklab)
            {

                // Save to database all busstop services for all Swedish area nodes
                // -----------------------
                // Title, Intro, Latitude, Longitude



                //....SWEDISH
                // Get the node id of the Swedish landing page
                int nodeIdLanding = helper.TypedContentSingleAtXPath("//StartPage/" + MyConstants.AreasLandingPageDocTypeAlias).Id;

                // Get landing page node content 
                var content = helper.TypedContent(nodeIdLanding);

                // Inititiate ienumerable
                IEnumerable<ServiceBusStopModel> listTemp = null;
                List<ServiceBusStopModel> listBusStopServices = new List<ServiceBusStopModel>();

                // Get complete list of all busstop services and some of the properties
                listTemp = content
                    .Descendants()
                    .Where(c => c.DocumentTypeAlias == MyConstants.ServiceDocTypeAlias && c.GetPropertyValue<string>(MyConstants.ServiceTypeDocTypeAlias) == "Hållplats")
                    .Select(obj => new ServiceBusStopModel()
                    {
                        AreaNodeId = obj.Parent.Id,
                        NodeId = obj.Id,
                        Title = obj.GetPropertyValue<string>(MyConstants.TitlePropertyAlias),
                        Intro = obj.GetPropertyValue<IHtmlString>(MyConstants.IntroPropertyAlias),
                        ServiceType = obj.GetPropertyValue<String>(MyConstants.ServiceTypePropertyAlias)
                    }).ToList();

                // Now get the rest of the properties from Trafiklab while iterating over the busstops list
                listTemp.ForEach(item =>
                {
                    // Get a service type object for current service
                    ServiceTypeModel objServiceType = msc.GetServiceTypeByLanguage(helper, currentLanguage, item.ServiceType);

                    ServiceBusStopModel objBusStopService = msc.GetBusStopService(item, objServiceType, helper, currentLanguage, apiUrl, flagTrafiklab);

                    listBusStopServices.Add(objBusStopService);
                });

                //// Add list to object
                //ServiceBusStopsListModel objBusStopsList{
                //    BusStopsCollection = listBusStopServices
                //};

                //....ENGLISH



                // SAVE TO DB
                //CacheSurfaceController csc = new CacheSurfaceController();
                bool dbSuccess = SaveToDb(listBusStopServices, currentLanguage);


                // SAVE TO CACHE
                //bool cacheSuccess = SaveToCache(listBusStopServices, currentLanguage);

            }
        }

           
                  


        protected bool SaveToDb(List<ServiceBusStopModel> listBusStopServices, string language)
        {

            SqlConnection conn;
            String connectionString = ConfigurationManager.ConnectionStrings["umbracoDbDSN"].ToString();

            // Create local variables for this static method
            //CacheSurfaceController csc = new CacheSurfaceController();
            //SqlConnection conn = conn;
            //String connectionString = csc.ConnectionString;
            //CacheSurfaceController csc = new CacheSurfaceController();
            //SqlConnection conn = csc.conn;
            //String connectionString = csc.ConnectionString;
            string sqlStatement = string.Empty;
            bool cacheExists = false;

            try
            {
                // Create a new SQL Connection object
                using (conn = new SqlConnection(connectionString))
                {
                    // Open the database connection
                    conn.Open();

                    //*** Check whether coordinates has been stored before ***

                    // Create the sql statement
                    sqlStatement = "SELECT COUNT(NodeId) FROM TrafiklabCache WHERE Latest = 'x'";

                    // Create a new SQL Command
                    using (SqlCommand cmd = new SqlCommand(sqlStatement, conn))
                    {
                        // Execute the command
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                // Set flag
                                cacheExists = true;
                            }
                            // Close the data reader
                            dr.Close();
                        }


                        //*** Reset old cache ***

                        // If cache exists, reset the Latest flag 
                        if(cacheExists)
                        {
                            // Create the sql statement
                            sqlStatement = "UPDATE TrafiklabCache SET Latest = ''";

                            // Change the SQL Command 
                            cmd.CommandText = sqlStatement;

                            // Execute the command
                            cmd.ExecuteNonQuery();
                        }


                        //*** Insert new cache ***

                        // Create the sql statement
                        sqlStatement = "INSERT INTO TrafiklabCache(AreaNodeId, ServiceNodeId, Latitude, Longitude, Title, Intro, Language, Latest) "
                            + "VALUES(@NodeId, @Latitude, @Longitude, @Title, @Intro, @Language, 'x')";

                        // Change the SQL Command 
                        cmd.CommandText = sqlStatement;

                        // Parameterize
                        cmd.Parameters.Add("@AreaNodeId", SqlDbType.SmallInt);
                        cmd.Parameters.Add("@ServiceNodeId", SqlDbType.SmallInt);
                        cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 50);
                        cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 50);
                        cmd.Parameters.Add("@Title", SqlDbType.VarChar, 200);
                        cmd.Parameters.Add("@Intro", SqlDbType.Text);
                        cmd.Parameters.Add("@Language", SqlDbType.Char, 2);

                        // Iterate over the busstop services list
                        foreach (var item in listBusStopServices)
                        {
                            cmd.Parameters["@AreaNodeId"].Value = item.AreaNodeId;
                            cmd.Parameters["@ServiceNodeId"].Value = item.NodeId;
                            cmd.Parameters["@Latitude"].Value = item.MapMarker.Latitude;
                            cmd.Parameters["@Longitude"].Value = item.MapMarker.Longitude;
                            cmd.Parameters["@Title"].Value = item.MapMarker.Title;
                            cmd.Parameters["@Intro"].Value = item.MapMarker.Intro;
                            cmd.Parameters["@Language"].Value = language;

                            //// Parameterize
                            //cmd.Parameters.Add("@NodeId", SqlDbType.SmallInt);
                            //cmd.Parameters["@NodeId"].Value = item.NodeId;

                            //cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 50);
                            //cmd.Parameters["@Latitude"].Value = item.MapMarker.Latitude;
                            //cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 50);
                            //cmd.Parameters["@Longitude"].Value = item.MapMarker.Longitude;

                            //cmd.Parameters.Add("@Title", SqlDbType.VarChar, 200);
                            //cmd.Parameters["@Title"].Value = item.MapMarker.Title;

                            //cmd.Parameters.Add("@Intro", SqlDbType.Text);
                            //cmd.Parameters["@Intro"].Value = item.MapMarker.Intro;

                            //cmd.Parameters.Add("@Language", SqlDbType.Char, 2);
                            //cmd.Parameters["@Language"].Value = language;

                            // Execute the command
                            cmd.ExecuteNonQuery();
                        }


                        //*** Remove old cache, if there was any ***
                        if(cacheExists)
                        {
                            sqlStatement = "DELETE FROM TrafiklabCache WHERE Latest = ''";

                            // Change the SQL Command 
                            cmd.CommandText = sqlStatement;

                            // Execute the command
                            cmd.ExecuteNonQuery();
                        }
                    }

                    // Close the database connection
                    conn.Close();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }


        
        protected static bool SaveToCache(List<ServiceBusStopModel> listBusStopServices, string language)
        {
            // Initiate result flag
            bool successFlag = false;

            //ApplicationContext.ApplicationCache.RuntimeCache

            //CacheHelper.InsertCacheItem<T>();

            return successFlag;
        }


        
    }
}


      


            //using (var message = new MailMessage("user@gmail.com", "user@live.co.uk"))
            //{
            //    message.Subject = "Test";
            //    message.Body = "Test at " + DateTime.Now;
            //    using (SmtpClient client = new SmtpClient
            //    {
            //        EnableSsl = true,
            //        Host = "smtp.gmail.com",
            //        Port = 587,
            //        Credentials = new NetworkCredential("user@gmail.com", "password")
            //    })
            //    {
            //        client.Send(message);
            //    }
            //}
//        }
//    }
//}