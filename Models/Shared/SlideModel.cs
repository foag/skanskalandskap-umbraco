﻿using System.Collections.Generic;

namespace SkanskaLandskap15.Models.Shared
{
    public class SlideModel
    {                  
        public string SubTitle { get; set; }
        public string Text { get; set; }
        public bool LinksExists { get; set; }       
        public List<LinkModel> LinksCollection { get; set; }

        public ImageModel Image { get; set; }
    }
}