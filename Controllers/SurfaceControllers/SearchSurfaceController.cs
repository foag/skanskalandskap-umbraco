﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Examine;
using Examine.LuceneEngine.SearchCriteria;
using PagedList;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Web;
using Umbraco.Web.Mvc;



namespace SkanskaLandskap15.Controllers.SurfaceControllers
{
   
    public class SearchSurfaceController : SurfaceController 
    {

        ///<summary>
        /// Fetches a search result based on a search term (querystring).
        /// </summary>
        /// <param name="nodeId">The node id of current (Content node) page.</param>
        /// <param name="language">Current language ("SV" or "EN").</param>
        /// <param name="accessWithoutSearch">Boolean which evaluates to true when the search results page is 
        /// accessed from e.g. a 404 page.</param>
        /// <returns>A SearchResultsModel object.</returns>  
        public ActionResult SearchResults(int? nodeId, string language, bool? accessWithoutSearch)
        {
            // Initiate the return model
            SearchResultsModel model = new SearchResultsModel();
            
            // The culture of an Ajax request thread is neutral and therefore we need to set it 
            // explicitly to be able to get the correct Umbraco context.
            if (Request.IsAjaxRequest())
            {
                UmbracoHelperExtensions.EnsureCulture(Thread.CurrentThread, language);
            }
            else
            {
                //Response.Redirect("http://skanskalandskap.se.local/sok?term=fulltofta&directed=true");
            }

            // Get an Umbraco Helper
            UmbracoHelper helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language ("SV" or "EN")
            var currentLanguage = helper.GetDictionaryValue("Language");

            // Get controller reference
            MasterController mc = new MasterController();

            // Get search node's urlname due to language 
            model.SearchUrlName = "/" + helper.TypedContentAtXPath("//" + mc.GetRootContentType(currentLanguage) + "/" + MyConstants.SearchResultsPageDocTypeAlias).First().UrlName;

            // Get placeholder text for the search box
            model.SearchPlaceHolderTerm = helper.GetDictionaryValue("Search");
                       
            // Initiate querystring variables
            string searchTerm = string.Empty;
            string searchType = string.Empty;
            string page = string.Empty;

            // Get search term
            if (!String.IsNullOrEmpty(Request.QueryString["term"]))
            {
                searchTerm = Request.QueryString["term"];
            }
            else if (!String.IsNullOrEmpty(Request.Form["search"]))
            {
                searchTerm = Request.Form["search"];
            }

            // Get search type (i.e. from the autosuggestions list or custom)
            searchType = Request.QueryString["type"];

            // Get page number (for pagination function)
            page = Request.QueryString["page"];

            // Add search term to model
            model.SearchTerm = searchTerm;

            // Initiate the Number (of search results) property
            model.Number = 0;
            
            // Initiate the access-without-search flag, aimed to facilitate the styling
            // of the search results page when accessed from e.g. a 404 page. 
            model.AccessWithoutSearch = false;

            // Set the access-without-search flag
            if (accessWithoutSearch != null) { 
                model.AccessWithoutSearch = (bool)accessWithoutSearch;
            } 
            
            // Get the content of the current page's Content node (Swedish or English search results page)    
            var content = helper.Content(nodeId);

            // Get page size
            int pageSize = PageSize(content);

            // Add page size to model
            model.PageSize = pageSize;


            // Make sure a search term exist before processing the search
            if (searchTerm != string.Empty)
            {
                // Initiate an Examine search result list
                List<SearchResult> examineCollection = null;

                // Process the search
                examineCollection = ProcessSearch(searchTerm, searchType, currentLanguage, helper);

                // Count number of search items
                Int32 numOfItems = examineCollection.Count();
               
                // Create the general search result message
                model.Message = CreateMessage(numOfItems, searchTerm, helper);

                // Make sure there is a result before making manipulations
                if (numOfItems > 0)
                {
                    // Add number of items to model
                    model.Number = numOfItems;

                    // Create a list for SearchResultItem objects
                    List<SearchResultItemModel> listSearchResultItems = new List<SearchResultItemModel>();

                    // Get the length of the preview text
                    int lengthPreview = PreviewTextLength(content);
                    //int lengthPreview = PreviewTextLength(help, nodeId);

                    // Initiate a counter
                    int counter = 0;

                    // Iterate over the Examine result
                    foreach (SearchResult item in examineCollection)
                    {
                        // Create a new SearchResultItem object
                        SearchResultItemModel modelItem = new SearchResultItemModel();

                        // Enumerate counter
                        counter += 1;
                                               
                        // Add number to title 
                        modelItem.Title = counter + ". " + item.Fields[MyConstants.TitlePropertyAlias];
                        
                        // Create search result url
                        modelItem.Url = Umbraco.NiceUrl(Convert.ToInt32(item.Fields["id"]));

                        // Construct preview text
                        modelItem.PreviewText = GetPreviewText(item, lengthPreview);

                        // Add object to list
                        listSearchResultItems.Add(modelItem);
                    }                                    
                    
                    // Manage page number                  
                    if(String.IsNullOrEmpty(page))
                    {
                        page = "1";
                    }
                    Int32 pageNumber = Convert.ToInt32(page);

                    // Turn the SearchResultItem's list into a paged list 
                    IPagedList pagedCollection = (IPagedList<SearchResultItemModel>)listSearchResultItems.ToPagedList<SearchResultItemModel>(pageNumber, pageSize);
                    
                    // Add paged list to the return model
                    model.SearchResultsCollection = (IPagedList<SearchResultItemModel>)pagedCollection;

                    model.PaginationMessage = CreatePaginationMessage(pagedCollection, helper); 
                  
                    // Add some parameters for the pager
                    model.NodeId = content.Id;
                    model.Language = currentLanguage;
                }

                // No result
                else
                {
                    model.Message = CreateMessage(0, searchTerm, helper);
                }
            }
            // No search term available
            else
            {
                model.Message = String.Empty;
            }

            // Send the SearchResults partial view populated with the SearchResults model
            return PartialView("SearchResults", model);
        }



        
        protected string CreateMessage(Int32 number, string term, UmbracoHelper helper)
        {
            // Initiate message variable
            string message = String.Empty;
            string pagesValue = string.Empty;

            if (number > 0)
            {
                // One or many pages in the result
                pagesValue = helper.GetDictionaryValue("Pages");
                if (number == 1)
                {
                    pagesValue = helper.GetDictionaryValue("Page");
                }

                message = String.Format(helper.GetDictionaryValue("ResultMessage"), term, number, pagesValue);
            }
            else
            {
                message = String.Format(helper.GetDictionaryValue("NoResultMessage"), term);             
            }
            return message;
        }



        protected string GetPreviewText(SearchResult item, int length, bool autoSuggestion = false)
        {
            // Initiate return value
            String previewText = String.Empty;

            // Set the truncate addition
            string truncateAddition = "...";

            // Don't mess with subtitles for autosuggestions
            if (autoSuggestion == false) { 

                // Add possibly Subtitle property to the return value
                if (item.Fields.ContainsKey(MyConstants.SubtitlePropertyAlias))
                {
                    previewText = item.Fields[MyConstants.SubtitlePropertyAlias].Trim();
              
                    // Make sure there's a final dot
                    if(previewText.Substring(previewText.Length) != ".")
                    {
                        previewText += ".";
                    }
                }
            }

            // Add possibly Intro or Text property to the return value
            if (item.Fields.ContainsKey(MyConstants.IntroPropertyAlias))
            {
                if (previewText == String.Empty)
                {
                    previewText = item.Fields[MyConstants.IntroPropertyAlias].Trim();
                }
                else
                {
                    previewText += " " + item.Fields[MyConstants.IntroPropertyAlias].Trim();
                }
            }
            else
            {
                if (item.Fields.ContainsKey(MyConstants.TextPropertyAlias))
                {
                    if (previewText == String.Empty)
                    {
                        previewText = item.Fields[MyConstants.TextPropertyAlias].Trim();
                    }
                    else
                    {                    
                        previewText += " " + item.Fields[MyConstants.TextPropertyAlias].Trim();
                    }
                }
            }
                        
            // Only show a specified number of characters in the preview text 
            if (previewText.Length > length)
            {
                if (autoSuggestion == false)
                {
                    previewText = previewText.Substring(0, length) + truncateAddition;
                }
                else
                {
                    previewText = UmbracoHelperExtensions.TruncateAtWord(previewText, length);
                }
            }
            return previewText;
        }



        protected int PreviewTextLength(dynamic content)
        {
            // Initiate return value
            int lengthPreview = 0;

            // Get number of max preview text characters from backoffice (SearchResults node)
            lengthPreview = content.GetPropertyValue<int>(MyConstants.PreviewTextLengthPropertyAlias);

            // Assign the default value if no backoffice value was set 
            if (lengthPreview == 0)
            {
                lengthPreview = MyConstants.PreviewTextLength;
            }
            return lengthPreview;
        }



        protected int PageSize(dynamic content)
        {
            // Initiate return value
            int pageSize = 0;

            // Get the page size from backoffice (SearchResults node)
            pageSize = content.GetPropertyValue<int>(MyConstants.PageSizePropertyAlias);

            // Assign the default value if no backoffice value was set 
            if (pageSize == 0)
            {
                pageSize = MyConstants.PageSize;
            }
            return pageSize;
        }



        protected string CreatePaginationMessage(IPagedList pagedCollection, UmbracoHelper helper)
        {
            // Initiate the return string
            string paginationMessage = String.Empty;

            // Get some figures to be included in the message
            int firstItemOnPage = pagedCollection.FirstItemOnPage;
            int lastItemOnPage = pagedCollection.LastItemOnPage;
            int totalItemCount = pagedCollection.TotalItemCount;

            // Create a pagination message for current (pagination) page            
            if (firstItemOnPage != lastItemOnPage)
            {
                // Ex: "Visar 1-10 av 21 resultat"
                paginationMessage = String.Format(helper.GetDictionaryValue("PaginationMessage"), firstItemOnPage, lastItemOnPage, totalItemCount);
            }
            else
            {
                // Ex: "Visar 21 av 21 resultat"
                paginationMessage = String.Format(helper.GetDictionaryValue("PaginationMessageShorter"), firstItemOnPage, totalItemCount);
            }
            return paginationMessage;
        }



        public List<SearchResult> ProcessSearch(string searchTerm, string searchType, string language, UmbracoHelper helper)
        { 
            // Initialize some default values            
            string searchProvider = String.Empty;
            IEnumerable<SearchResult> ExamineCollection = new List<SearchResult>();
            List<SearchResult> FinalCollection = new List<SearchResult>();
            
            // Select search provider due to current language
            switch (language)
            {
                case "SV":
                    searchProvider = "SiteContentSearcher";
                    break;
                case "EN":
                    searchProvider = "SiteContentEnglishSearcher";
                    break;
            }

            // Create a list of searchable properties
            List<string> fieldsList = new List<string>
            {
                "nodeName", 
                "Title",
                "Subtitle", 
                "Intro", 
                "Text", 
                "TitlePuff", 
                "TitleTips",
                "TitleList",
                "TitleListPage",
                "titleGroup",
                "introGroup",
                "doAndExperience",
                "findUsIntro",
                "FindUsText",
                "GeneralInfo",
                "usefulInformation",
                "CabinPrices",
                "CabinReserve",
                "TitleForm",
                "IntroForm",
                "titleContactInfo",
                "titleContactPersons",
                "SubPageTag",
                "_GlobalPuffTitle",
                "_GlobalPuffText",
                "_PersonName",
                //"_CabinFacilities",
                "puffSlideShowModule",
                "PuffModule",
                "WarningsModule",
                "contactInfoModule",
                "coworkersGroupsModule"
            };            

            // Add some properties due to current language
            switch (language)
            {
                case "SV":
                    fieldsList.Add("_PersonFunction");
                    fieldsList.Add("_PersonWorkAssignments");
                    break;
                case "EN":
                    fieldsList.Add("_PersonFunctionEn");
                    fieldsList.Add("_PersonWorkAssignmentsEn");
                    //fieldsList.Add("TermEnglish");
                    break;
            }

            // Convert list to array
            string[] fieldsComplete = fieldsList.ToArray();
                      
            // Setup the search criteria by pointing to the language-specific searcher (set up in ExamineSettings.config)
            var searcher = ExamineManager.Instance.SearchProviderCollection[searchProvider];
            var criteria = searcher.CreateSearchCriteria(LuceneSearchExtensions.ToBooleanOperation(Lucene.Net.Search.BooleanClause.Occur.SHOULD));

            // Catch errors that might appear when the filter is compiled ("*" and "?" are not allowed as search terms) 
            // or when the Search operation is performed (does not allow reserved words like "and" and "in"). 
            try
            {
                // Perform a multiple terms search if user has entered more than one word 
                if (searchTerm.IndexOf(' ') > 0 && searchType != "auto" && searchType != String.Empty)
                {
                    // Initiate filter variable
                    Examine.SearchCriteria.IBooleanOperation filter = null;

                    // Split the search terms into a string array
                    string[] terms = searchTerm.Split(' ');

                    // Assign the searchable properties to a local variable
                    var fields = fieldsComplete;
                   
                    // Not working version
                    //---------------------------------------------------------
                    //// Initiate lists
                    //var searchFields = new List<string>();
                    //var searchTerms = new List<string>();

                    //// Interate over the search terms and add them to the lists
                    //foreach (var t in terms)
                    //{
                    //    searchTerms.AddRange(fields.Select(_ => t));
                    //    searchFields.AddRange(fields);
                    //}

                    // Pass the lists to GroupedOr, compile and execute the search. 
                    //var query = criteria.GroupedOr(searchFields, searchTerms.Select(x => x.MultipleCharacterWildcard()).ToArray()).Not().Field("umbracoNaviHide", "1");
                    //ExamineCollection = searcher.Search(query.Compile());
                    //-----------------------------------------------------------
                                   
                    // Iterate over the splitted search terms
                    foreach (var t in terms)
                    {
                        if (filter == null)
                        {
                            //filter = criteria.GroupedOr(fields.ToArray(), t.MultipleCharacterWildcard()).Or().GroupedOr(fields, t.Fuzzy(y));
                            filter = criteria.GroupedOr(fields.ToArray(), t.MultipleCharacterWildcard());
                        }
                        else
                        {
                            //filter = filter.Or().GroupedOr(fields.ToArray(), t.MultipleCharacterWildcard()).Or().GroupedOr(fields, t.Fuzzy(y));
                            filter = filter.Or().GroupedOr(fields.ToArray(), t.MultipleCharacterWildcard());
                        }
                    }
                    filter = filter.Not().Field("umbracoNaviHide", "1");
                    ExamineCollection = searcher.Search(filter.Compile());
                }

                // Perform a single word search
                else
                {
                    
                    //float fuzzyValue = 0.5f;

                    // Initiate filter variable
                    Examine.SearchCriteria.ISearchCriteria filter = null;

                    // Split the complete list into two lists
                    List<string> fieldsPrioritizedList = new List<string>();
                    List<string> fieldsRestList = new List<string>();

                    int counter = 0;
                    foreach(string term in fieldsComplete)
                    {
                        counter += 1;

                        if(counter <= 2)
                        {
                            fieldsPrioritizedList.Add(term);
                        }
                        else
                        {
                            fieldsRestList.Add(term);
                        }
                    }

                    // Convert lists to arrays
                    string[] fieldsPrioritized = fieldsPrioritizedList.ToArray();
                    string[] fieldsRest = fieldsRestList.ToArray();

                    // Configure a filter that will query the specified fields. 
                    // Hits on a title should be prioritized over hits on other properties (see second Boost parameter). 
                    filter = criteria
                        .GroupedOr(fieldsPrioritized, LuceneSearchExtensions.Boost(searchTerm.ToLower(), 8))
                        .Or()
                        .GroupedOr(fieldsRest, LuceneSearchExtensions.Boost(searchTerm.ToLower(), 5))
                        .Or()
                        .GroupedOr(fieldsComplete, LuceneSearchExtensions.MultipleCharacterWildcard(searchTerm.ToLower()))
                        .Or()
                        .GroupedOr(fieldsComplete, LuceneSearchExtensions.Escape(searchTerm.ToLower()))
                        .Not()
                        .Field("umbracoNaviHide", "1") // filter out the hidden pages
                        .Compile();


                    //// Configure a filter that will query the specified fields. 
                    //// Hits on a title should be prioritized over hits on other properties (see second Boost parameter). 
                    //filter = criteria
                    //    .GroupedOr(new string[] { "nodeName", "Title" }, LuceneSearchExtensions.Boost(searchTerm.ToLower(), 8))
                    //    .Or()
                    //    .GroupedOr(new string[] { "Subtitle", "Intro", "Text", "Nature", "CulturalHistory", "Activities", "TitleSlideshowModule" }, LuceneSearchExtensions.Boost(searchTerm.ToLower(), 5))
                    //    .Or()
                    //    .GroupedOr(fieldsComplete, LuceneSearchExtensions.MultipleCharacterWildcard(searchTerm.ToLower()))
                    //    .Or()
                    //    .GroupedOr(fieldsComplete, LuceneSearchExtensions.Escape(searchTerm.ToLower()))
                    //    .Not()
                    //    .Field("umbracoNaviHide", "1") // filter out the hidden pages
                    //    .Compile();
                 
                    //if (searchType == "auto")
                    //{
                    //    // Configure a filter that will query the title field or node name.
                    //    filter = criteria
                    //        .GroupedOr(new string[] { "nodeName", "Title" }, searchTerm)
                    //        .Compile();
                    //}
                    //else
                    //{
                    //    //float fuzzyValue = 0.5f;

                    //    // Configure a filter that will query the specified fields. 
                    //    // Hits on a title should be prioritized over hits on other properties (see second Boost parameter). 
                    //    filter = criteria
                    //        .GroupedOr(new string[] { "nodeName", "Title" }, LuceneSearchExtensions.Boost(searchTerm.ToLower(), 8))
                    //        .Or()
                    //        .GroupedOr(new string[] { "Subtitle", "Intro", "Text", "Nature", "CulturalHistory", "Activities", "FindUs", "UsefulInformation" }, LuceneSearchExtensions.Boost(searchTerm.ToLower(), 5))
                    //        .Or()
                    //        .GroupedOr(fieldsComplete, LuceneSearchExtensions.MultipleCharacterWildcard(searchTerm.ToLower()))
                    //        .Or()
                    //        .GroupedOr(fieldsComplete, LuceneSearchExtensions.Escape(searchTerm.ToLower()))
                    //        .Not()
                    //        .Field("umbracoNaviHide", "1") // filter out the hidden pages
                    //        .Compile();
                    //}

                    ExamineCollection = searcher.Search(filter);         
                }
            }
            catch
            {
                return new List<SearchResult>();
            }

            // Move all SearchResult objects to the final collection while making some checks and exemptions.
            foreach (SearchResult item in ExamineCollection)
            {
                // Make sure the appropriate key exist for each result 
                if (item.Fields.ContainsKey(MyConstants.TitlePropertyAlias))
                {
                    // Sort out results that points to (still published) nodes in the Recycle Bin of the Content tree.                   
                    if (helper.NiceUrl(item.Id) != "#")
                    {
                        FinalCollection.Add(item);
                    }
                }
            }
            return FinalCollection;
        }




        public void CreateAutoSuggestions(string language, int limit = 0)
        {

            // The culture of an Ajax request thread is neutral and therefore we need to set it 
            // explicitly to be able to get the correct Umbraco context.
            if (Request.IsAjaxRequest())
            {
                UmbracoHelperExtensions.EnsureCulture(Thread.CurrentThread, language);
            }         

            // Get an Umbraco Helper
            UmbracoHelper helper = new UmbracoHelper(UmbracoContext.Current);

            // Initiate some values
            string SearchTerm = String.Empty;
            string searchProvider = String.Empty;
            int numOfResults = 0;
            IEnumerable<SearchResult> ExamineCollection = new List<SearchResult>();
            List<SearchResult> ResultList = new List<SearchResult>();
            List<SearchAutoSuggestionModel> ReturnList = new List<SearchAutoSuggestionModel>();

            // Grab the search term from the URL query string 'term'; break out if it's null or empty 
            SearchTerm = Request.QueryString["term"];
            if (string.IsNullOrEmpty(SearchTerm)) return;

            // Select search provider due to current language ("SV" or "EN")
            switch (language)
            {
                case "SV":
                    searchProvider = "SiteContentSearcher";
                    break;
                case "EN":
                    searchProvider = "SiteContentEnglishSearcher";
                    break;
            }  

            // Setup the search criteria by pointing to the language-specific searcher (set up in ExamineSettings.config)
            var searcher = ExamineManager.Instance.SearchProviderCollection[searchProvider];

            // Initiate a search criteria object
            var criteria = searcher.CreateSearchCriteria();

            // Configure a filter that will query the specified fields.  
            var filter = criteria
                .GroupedOr(new string[] { "nodeName", "Title" }, LuceneSearchExtensions.MultipleCharacterWildcard(SearchTerm.ToLower()))
                .Not()
                .Field("umbracoNaviHide", "1") // filter out hidden pages
                .Compile();

            // Execute the actual search
            ExamineCollection = searcher.Search(filter);
   
            // Count the number of valid results
            int counterA = 0;
            int counterTest = 0;
            foreach (SearchResult item in ExamineCollection)
            {
                // Make sure the appropriate key exist for each result 
                if (item.Fields.ContainsKey(MyConstants.TitlePropertyAlias))
                {
                    counterTest += 1;

                    // Filter out results that points to (still published) nodes in the Recycle Bin of the Content tree.           
                    if (helper.NiceUrl(item.Id) != "#")
                    {
                        // Enumerate counter
                        counterA += 1;
                    }
                }
            }
            // Assign the total number of results to a variable
            numOfResults = counterA;          
                      
            // Make sure there was a result
            if (numOfResults > 0)
            {
                // Initiate counter
                int counterB = 0;

                // Move SearchResult objects to a temporary list while making some checks and exemptions.
                // Break out of loop if the number of result items exceeds requested limit
                foreach (SearchResult item in ExamineCollection)
                {
                    // Make sure the appropriate key exist for each result 
                    if (item.Fields.ContainsKey(MyConstants.TitlePropertyAlias))
                    {
                        // Filter out results that points to (still published) nodes in the Recycle Bin of the Content tree.           
                        if (helper.NiceUrl(item.Id) != "#")
                        {
                            // Add search result item to list 
                            ResultList.Add(item);

                            // Enumerate counter
                            counterB += 1;

                            // Break out of loop if limit is reached
                            if (limit > 0 && counterB == limit)
                            {
                                break;
                            }
                        }
                    }
                }

                // Get number of characters for the preview text
                int previewTextLength = MyConstants.AutosuggestionPreviewTextLength;

                // Set autosuggestion flag
                bool autosuggestion = true;

                // Iterate over the Examine result
                foreach (SearchResult item in ResultList)
                {
                    // Create a new SearchAutoSuggestion object
                    SearchAutoSuggestionModel model = new SearchAutoSuggestionModel();

                    // Add title 
                    model.Title = item.Fields[MyConstants.TitlePropertyAlias];
                   
                    // Create search result url
                    model.Url = helper.NiceUrl(Convert.ToInt32(item.Fields["id"]));

                    // Construct preview text               
                    model.PreviewText = GetPreviewText(item, previewTextLength, autosuggestion);

                    // Add object to list
                    ReturnList.Add(model);
                }
            }

            // Add return values to dictionary
            Dictionary<string, object> dict = new Dictionary<string, object>();
            dict.Add("result", ReturnList);
            dict.Add("total", numOfResults);            

            // Convert the dictionary content into a JSON string
            JavaScriptSerializer oSerializer = new JavaScriptSerializer();
            string jsonText = oSerializer.Serialize((object)dict);

            // Write the JSON string to whatever receiver 
            Response.Write(jsonText);
        }
    }
}