﻿using System;
using System.Web;


namespace SkanskaLandskap15.Classes.Utilities
{
    
    public static class MyConstants {


        // Default number of characters in the search result's preview texts
        public const int PreviewTextLength = 300;
        public const int AutosuggestionPreviewTextLength = 55;

        // Default number of search results in the pager function
        public const int PageSize = 10;

        // Default size of the news and events subsets, respectively
        public const int NewsRecordsPerPage = 9;
        public const int EventRecordsPerPage = 3;

        // Default number of news items in the news section of an area page (Fulltofta)
        public const int NewsItemsOnAreaPage = 3;

        // Default number of events in the Calendar of an area page (Fulltofta)
        public const int EventItemsOnAreaPage = 5;

        public const int PuffsInSubMenu = 3;

        // Default number of meters for the busstop search radius
        public const int BusstopRadius = 3000;


        // Content type aliases         
        public const string AreasLandingPageDocTypeAlias = "AreasLandingPage";
        public const string AreaPageDocTypeAlias = "AreaPage";
        public const string AccommodationsLandingPageDocTypeAlias = "AccommodationsLandingPage";
        public const string AccommodationPageDocTypeAlias = "AccommodationPage";
        public const string AccommodationPageBookDocTypeAlias = "accommodationPageBook";
        public const string AccommodationPageOtherDocTypeAlias = "accommodationPageOther";
        public const string ThingsToDoLandingPageDocTypeAlias = "ThingsToDoLandingPage";        
        public const string GeneralSubPageDocTypeAlias = "GeneralSubPage";
        public const string NewsAndEventsLandingPageDocTypeAlias = "NewsAndEventsLandingPage";
        public const string ForKidsLandingPageDocTypeAlias = "ForKidsLandingPage";
        public const string ForSchoolsLandingPageDocTypeAlias = "ForSchoolsLandingPage";
        public const string UsefulInformationLandingPageDocTypeAlias = "UsefulInformationLandingPage";
        public const string AboutUsLandingPageDocTypeAlias = "AboutUsLandingPage"; 
        public const string ContactPersonsPageDocTypeAlias = "ContactPersonsPage";
        public const string CoworkersPageDocTypeAlias = "coworkersPage"; 
        public const string EventsLandingPageDocTypeAlias = "EventsLandingPage";
        public const string EventsListDocTypeAlias = "EventsList";
        public const string EventPageDocTypeAlias = "EventPage";
        public const string FormPageDocTypeAlias = "FormPage";      
        public const string HomePageDocTypeAlias = "HomePage";
        public const string functionPageDocTypeAlias = "functionPage";
        public const string FunctionPagesDocTypeAlias = "FunctionPages";
        public const string NewsLandingPageDocTypeAlias = "NewsLandingPage";
        public const string NewsPageDocTypeAlias = "NewsPage";
        public const string SearchResultsPageDocTypeAlias = "SearchResultsPage";
        public const string ServiceDocTypeAlias = "Service";
        public const string StartPageDocTypeAlias = "StartPage"; 
        public const string TextPageDocTypeAlias = "TextPage";
        public const string ActivityTypesDocTypeAlias = "activityTypesFolder";
        public const string ActivityTypeDocTypeAlias = "ActivityType";
        public const string ServiceTypesDocTypeAlias = "serviceTypesFolder";
        public const string ServiceTypeDocTypeAlias = "ServiceType";
        public const string LocalPuffSlideShowDocTypeAlias = "localPuffSlideShow";
        public const string guestRatingsConfigDocTypeAlias = "guestRatingsConfig";


        // Media type aliases         
        public const string MediaFolderTypeAlias = "Folder";
             

        // Template aliases
        public const string AreaPageTemplateAlias = "AreaPage";
        public const string ThingsToDoTemplateAlias = "ThingsToDoPage";
        public const string WhereToStayTemplateAlias = "WhereToStayPage";        


        // Content Property aliases
        public const string SiteTitlePropertyAlias = "SiteTitle";
        public const string SiteDescriptionPropertyAlias = "SiteDescription";

        public const string googleAnalyticsKeyPropertyAlias = "googleAnalyticsKey";
        public const string trafiklabApiKeyPropertyAlias = "trafiklabApiKey";
        public const string servicesCacheDurationPropertyAlias = "servicesCacheDuration";
        public const string submenuCacheDurationPropertyAlias = "submenuCacheDuration";
             
        public const string TitlePropertyAlias = "Title";
        public const string titlePropertyAlias = "title";
        public const string SubtitlePropertyAlias = "Subtitle";
        public const string subTitlePropertyAlias = "subTitle";
        public const string IntroPropertyAlias = "Intro";
        public const string introPropertyAlias = "intro";
        public const string IntroAltPropertyAlias = "IntroAlt";
        public const string TextPropertyAlias = "Text";
        public const string textPropertyAlias = "text";
        public const string TopImagePropertyAlias = "TopImage";
        public const string ImagePickerPropertyAlias = "ImagePicker";
        public const string imagePickerPropertyAlias = "imagePicker";
        public const string ImageSinglePropertyAlias = "ImageSingle";
        public const string ImageCaptionPropertyAlias = "ImageCaption";
        public const string MapLocationPropertyAlias = "MapLocation";
        public const string MapBusstopRadiusPropertyAlias = "SearchRadiusBuses";
        public const string ServiceNamePropertyAlias = "ServiceName";
        public const string ServiceDescriptionPropertyAlias = "ServiceDescription";
        public const string MapLocationServicePropertyAlias = "MapLocationService";
        public const string HideInListingPropertyAlias = "HideInListing";

        public const string MapRT90YCoordinatePropertyAlias = "yCoordinateRT90";
        public const string MapRT90XCoordinatePropertyAlias = "xCoordinateRT90";

        public const string SelectOrderingPagePropertyAlias = "selectOrderingPage";
        public const string DoAndExperiencePropertyAlias = "doAndExperience";
        public const string findUsIntroPropertyAlias = "findUsIntro";
        public const string FindUsTextPropertyAlias = "FindUsText";        
        public const string DownloadsPropertyAlias = "downloads";
        public const string TitleWarningsPropertyAlias = "TitleWarnings";
        public const string WarningsModulePropertyAlias = "WarningsModule";
        public const string WarningUnpublishPropertyAlias = "WarningUnpublish";
        public const string GeneralInfoPropertyAlias = "GeneralInfo";
        public const string GeneralInfoTitlePropertyAlias = "GeneralInfoTitle";
        public const string UsefulInformationPropertyAlias = "usefulInformation";
        public const string ServiceTypePropertyAlias = "ServiceType";
        public const string AreaStatusPropertyAlias = "AreaStatus";
        public const string PuffFixedModulePropertyAlias = "puffFixedModule";
                
        public const string TipsAccommodationsPropertyAlias = "TipsAccommodations";
        public const string TipsAreasPropertyAlias = "TipsAreas";
        public const string TipAreaPropertyAlias = "TipArea";

        public const string TipsThingsToDoStartPagePropertyAlias = "TipsThingsToDoStartPage";
        public const string TipsThingsToDoPropertyAlias = "tipsThingsToDo";
        public const string TipsSubPagesPropertyAlias = "tipsSubPages";
        public const string tipsForKidsLandingPagePropertyAlias = "tipsForKidsLandingPage";
        public const string TitleTipsPropertyAlias = "TitleTips";
        public const string titleTipsPropertyAlias = "titleTips";
        public const string SubPageTagPropertyAlias = "SubPageTag";

        public const string TitlePuffPropertyAlias = "TitlePuff";
        public const string titlePuffPropertyAlias = "titlePuff";
        public const string TitlePuffsPropertyAlias = "TitlePuffs";
        public const string TitleListPropertyAlias = "titleList";
        public const string TitleListPagePropertyAlias = "titleListPage";
        public const string PuffModulePropertyAlias = "PuffModule";
        public const string GlobalPuffsPropertyAlias = "GlobalPuffs";

        public const string CabinFacilitiesPropertyAlias = "CabinFacilities";
        public const string CabinPetOptionsPropertyAlias = "CabinPetOptions";
        public const string CabinBedOptionsPropertyAlias = "CabinBedOptions";
        public const string CabinPricesPropertyAlias = "CabinPrices";
        public const string CabinReservePropertyAlias = "CabinReserve";
        public const string AccommodationTagPropertyAlias = "taggaSidan"; 

        public const string NewsPickerPropertyAlias = "NewsPicker";        
        public const string TagNewsPropertyAlias = "TagNews";
        public const string TagEventPropertyAlias = "TagEvent";
        public const string NewsFlashPropertyAlias = "NewsFlash";       
        public const string PublishingDatePropertyAlias = "PublishingDate";
        public const string PaginationNewsPropertyAlias = "PaginationNews";       
        public const string EventStartPropertyAlias = "EventStart";
        public const string EventEndPropertyAlias = "EventEnd";
        public const string NumOfEventsPropertyAlias = "NumOfEvents";
        //public const string PaginationEventsPropertyAlias = "PaginationEvents";
        public const string FulltoftaNaturcentrumFlagPropertyAlias = "FulltoftaNaturcentrumFlag";
        public const string AreaPickerPropertyAlias = "AreaPicker";
        public const string LocationFreeTextPropertyAlias = "LocationFreeText";
        public const string featuredNewsItemPropertyAlias = "featuredNewsItem";
       
        public const string FormContentPropertyAlias = "FormContent";
        public const string PageSizePropertyAlias = "PageSize";
        public const string PreviewTextLengthPropertyAlias = "PreviewTextLength";

        public const string titleContactInfoPropertyAlias = "titleContactInfo";
        public const string titleContactPersonsPropertyAlias = "titleContactPersons";
        public const string contactInfoModulePropertyAlias = "contactInfoModule";
        public const string coworkersGroupsModulePropertyAlias = "coworkersGroupsModule";
        public const string titleGroupPropertyAlias = "titleGroup";
        public const string introGroupPropertyAlias = "introGroup";
        public const string contactPuffLeftColumnPropertyAlias = "contactPuffLeftColumn";
        public const string contactPuffRightColumnPropertyAlias = "contactPuffRightColumn";
        public const string ContactPickerPropertyAlias = "ContactPicker";
        public const string contactPickerPropertyAlias = "contactPicker";
        public const string ContactPortraitPropertyAlias = "ContactPortrait";
        public const string ContactNamePropertyAlias = "Name";
        public const string ContactEmailPropertyAlias = "Email";
        public const string ContactFunctionPropertyAlias = "Function";
        public const string ContactworkAssignmentsPropertyAlias = "workAssignments";  
        public const string ContactPhone1PropertyAlias = "Phone1";
        public const string ContactPhone2PropertyAlias = "Phone2";
        public const string ContactFunctionENPropertyAlias = "FunctionEn";
        public const string ContactworkAssignmentsENPropertyAlias = "workAssignmentsEn"; 
        public const string ContactPhoneEN1PropertyAlias = "PhoneEn1";
        public const string ContactPhoneEN2PropertyAlias = "PhoneEn2";
        
        public const string DownloadTeaserTextPropertyAlias = "TeaserText";
        public const string DownloadTeaserImagePropertyAlias = "TeaserImage";
        public const string DownloadFolderPickerPropertyAlias = "DownloadFolderPicker";
        public const string TermEnglishPropertyAlias = "TermEnglish";

        public const string ActivityTypeIconPropertyAlias = "ActivityTypeIcon"; 
        public const string ServiceTypeIconPropertyAlias = "ServiceTypeIcon";
        public const string AreaActivitiesPropertyAlias = "AreaActivities";
        public const string FacilityTypeIconPropertyAlias = "FacilityTypeIcon"; 

        public const string PublishSlideshowModulePropertyAlias = "PublishSlideshowModule";
        public const string TitleSlideshowModulePropertyAlias = "TitleSlideshowModule";
        public const string ContentSlideshowModulePropertyAlias = "SlideshowModule";
        public const string ImagePickerSlideshowModulePropertyAlias = "ImagePicker";
        public const string SubtitleSlideshowModulePropertyAlias = "Subtitle";
        public const string MultiUrlPickerPropertyAlias = "MultiUrlPicker";
        public const string multiUrlPickerPropertyAlias = "multiUrlPicker";
        public const string hidePuffSlidePropertyAlias = "hidePuffSlide";

        public const string PuffsSlideshowModulePropertyAlias = "puffSlideShowModule";

        public const string FooterFoundationTextPropertyAlias = "FooterFoundationText";
        public const string FooterContactTextPropertyAlias = "FooterContactText";
        public const string FooterMultiUrlPickerPropertyAlias = "FooterMultiUrlPicker";

        public const string introInfoPropertyAlias = "introInfo";
        public const string cabinNodesConnectionsPropertyAlias = "cabinNodesConnections";
        public const string cabinNodesSwedishPropertyAlias = "cabinNodesSwedish";
        public const string cabinNodesEnglishPropertyAlias = "cabinNodesEnglish";

        
        
        // Media property aliases
        //public const string MediaDownloadFolderNameEN = "FolderNameEn";
        public const string MediaFileNameInEnglish = "nameInEnglish";
        public const string MediaFileCaptionSV = "FileDescriptionSV";
        public const string MediaFileCaptionEN = "FileDescriptionEN";
        public const string MediaFilePhotographer = "imgPhotographer";
        public const string MediaFile = "umbracoFile";
        public const string MediaFileExtension = "umbracoExtension";
        public const string MediaFileSize = "umbracoBytes";
        public const string MediaFileWidth = "umbracoWidth";
        public const string MediaFileHeight = "umbracoHeight";


      
        // Misc
        public const string DownloadablesTabSystemId = "tabs";
        public const string ForSchoolsNewsTag = "Utomhuspedagogik";
        public const string TrafiklabTestStation = "Malmö Centralstation";
    }      
}