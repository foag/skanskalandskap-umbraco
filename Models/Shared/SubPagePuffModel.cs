﻿using System.Web;
using System.Collections.Generic;

namespace SkanskaLandskap15.Models.Shared
{
    public class SubPagePuffModel
    {
        public string Tag { get; set; }
        public dynamic Image { get; set; }
        public string ImagePath { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Intro { get; set; }
        public IHtmlString Text { get; set; } 
        public string Url { get; set; }
        public GuestRatingsModel GuestRatings { get; set; }
        public List<FacilityTypeModel> FacilitiesCollection { get; set; }
        public string ReadMoreTerm { get; set; }
        public string MapThumbnailPath { get; set; }

        public bool LinkExists { get; set; }
        public LinkModel Link { get; set; }

        public int NodeId { get; set; }
        public int ParentNodeId { get; set; }
        public string ContentType { get; set; }  
    }
}