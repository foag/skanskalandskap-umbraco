﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Controllers.SurfaceControllers;
using SkanskaLandskap15.Models;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Web;




namespace SkanskaLandskap15.Controllers
{
    public class AccommodationPageController : InteriorMasterController
    {

        public ActionResult AccommodationPage()
        {
            // Initiate the return model
            var model = new AccommodationPageModel();

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language ("SV" or "EN") 
            string currentLanguage = helper.GetDictionaryValue("Language");
                  
            
            // UrlName and Content type
            model.UrlName = CurrentPage.UrlName;
            model.ContentType = CurrentPage.DocumentTypeAlias;


            /* TAG (= AREA NODE ID) */

            // Initiate flags
            model.AreaTypeTag = false;
            model.ShowExtendedTexts = false;
                      
            // Get tag
            var tagValue = CurrentPage.GetPropertyValue<string>(MyConstants.SubPageTagPropertyAlias);

            // Make sure tag is not empty (mandatory but you never know...)
            if (!string.IsNullOrEmpty(tagValue))
            {
                model.AreaTypeTag = true;
                model.ShowExtendedTexts = true;
            }


            /* SLIDESHOW */

            // Images for slideshow        
            string imagePicker = CurrentPage.GetPropertyValue<string>(MyConstants.ImagePickerPropertyAlias);

            // Initiate flag
            model.SlideshowExists = false;

            // Make sure images exists (required but you never know...)
            if (!string.IsNullOrWhiteSpace(imagePicker))
            {
                // Set flag          
                model.SlideshowExists = true;

                // Create controller reference
                MediaSurfaceController msc = new MediaSurfaceController();

                // Get a populated Slideshow object and add it to the return model
                model.Slideshow = msc.GetSlideshow(imagePicker, helper);
            }
           

            /* FACILITIES */

            // Initiate a Facilities object
            FacilitiesModel objFacilities = new FacilitiesModel();

            // Create a temporary list
            List<FacilityTypeModel> listCabinCapacities = new List<FacilityTypeModel>();

            // Create controller reference
            PuffsSurfaceController psc = new PuffsSurfaceController();

            // Get main facilities (checkbox list)
            dynamic pickedFacilities = CurrentPage.GetPropertyValue(MyConstants.CabinFacilitiesPropertyAlias);

            // Add main facilities to the Facility Types list          
            listCabinCapacities = psc.GetCabinCapacities(pickedFacilities, listCabinCapacities, helper, currentLanguage);

            // Get pet option (radiobutton list)
            dynamic pickedPetOption = CurrentPage.GetPropertyValue(MyConstants.CabinPetOptionsPropertyAlias);

            // Add pet option to the Facility Types list
            listCabinCapacities = psc.GetCabinCapacities(pickedPetOption, listCabinCapacities, helper, currentLanguage);

            // Get bed option (dropdown list)
            dynamic pickedBedOption = CurrentPage.GetPropertyValue(MyConstants.CabinBedOptionsPropertyAlias);

            // Add bed option to the Facility Types list
            bool bedOption = true;
            listCabinCapacities = psc.GetCabinCapacities(pickedBedOption, listCabinCapacities, helper, currentLanguage, bedOption);

            // Add list to Facilities object
            objFacilities.FacilitiesCollection = listCabinCapacities;

            // Add Facilities object to return model
            model.Facilities = objFacilities;

            model.FacilitiesExists = false;
            if(listCabinCapacities.Count > 0)
            {
                model.FacilitiesExists = true;
            }

            // Get title for the Facilities section
            model.CabinCapacitiesTerm = helper.GetDictionaryValue("CabinCapacities");

                                
            /* RICHTEXT CONTENT  */

            model.Text = CurrentPage.GetPropertyValue<IHtmlString>(MyConstants.TextPropertyAlias);
            model.CabinPrices = CurrentPage.GetPropertyValue<IHtmlString>(MyConstants.CabinPricesPropertyAlias);
            model.CabinReserve = CurrentPage.GetPropertyValue<IHtmlString>(MyConstants.CabinReservePropertyAlias);
                       
            // Content title terms
            model.CabinPricesTerm = helper.GetDictionaryValue("Prices");
            model.CabinReserveTerm = helper.GetDictionaryValue("Reserve");



            /*** GUEST RATINGS ***/           

            // Add a GuestRatings object to the return model
            model.GuestRatings = new GuestRatingsModel();     



            /* AREA (OPTIONAL TIPS) */

            // Explicitly set flag
            model.ShowAreaPuff = false;
                       
            // Make sure an area was actually selected
            if (model.AreaTypeTag)
            {
                // Get boolean that determines whether there should be a puff for the selected area
                bool showAreaPuff = CurrentPage.GetPropertyValue<bool>(MyConstants.TipAreaPropertyAlias);
                               
                if (showAreaPuff)
                { 
                    // Tag value is an area's node id             
                    Int32 areaId = Convert.ToInt32(tagValue);

                    // Initiate a puff list
                    List<SubPagePuffModel> selectedAreasCollection = new List<SubPagePuffModel>();

                    // Get a list of selected puffs/sub pages, i.e. in this case a single area page                    
                    selectedAreasCollection = psc.GetAreaPuffs(areaId.ToString(), helper);

                    // Make sure the selected area node is published. (You're able to select unpublished nodes in MNTP.)
                    if (selectedAreasCollection.Any())
                    {
                        // Set flag
                        model.ShowAreaPuff = true;

                        // Initiate a SubPagesList object
                        var subPages = new SubPagesListModel();

                        // Add list to the sub pages list object
                        subPages.SelectedAreasCollection = selectedAreasCollection;

                        // Add sub pages object to return model
                        model.SubPagesAreas = subPages;
                    }
                }                
            }


            /* PUFFS ARE RETRIEVED FROM THE VIEW */

            // Here we need to check whether puffs exists, though. This is to be able to handle
            // the common section title for puffs and downloads.
            model.PuffsExists = psc.PuffsExists(CurrentPage.Id, helper);



            /* DOWNLOADS  */

            // Pdf:s for the Download section        
            string downloadsPicker = CurrentPage.GetPropertyValue<string>(MyConstants.DownloadsPropertyAlias);

            // Initiate flag
            model.DownloadsExists = false;

            // Check whether downloads exists
            if (!string.IsNullOrWhiteSpace(downloadsPicker))
            {

                // Create controller reference
                MediaSurfaceController msc = new MediaSurfaceController();

                // Get the media files collection and add to return model
                model.MediaFiles = msc.GetDownloadFiles(downloadsPicker, helper, currentLanguage);

                // Make sure at least one of the selected files actually exists in Media 
                if (model.MediaFiles.MediaFilesCollection.Count() > 0)
                {
                    // Set flag          
                    model.DownloadsExists = true;
                }
            }

            // Puffs and/or downloads exists so get the common title
            if (model.PuffsExists || model.DownloadsExists)
            {
                model.PuffsAndDownloadsTitle = helper.GetDictionaryValue("Other");
            }          
                     
            return View(model);
        }  
    

    }
}