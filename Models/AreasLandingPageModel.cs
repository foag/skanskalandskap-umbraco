﻿using System.Collections.Generic;
using SkanskaLandskap15.Models.Masters;
using SkanskaLandskap15.Models.Shared;


namespace SkanskaLandskap15.Models
{
    public class AreasLandingPageModel : InteractiveMasterModel
    {
        public AreasListModel AreasList { get; set; }

        public string MapLinkTerm { get; set; }
        public string ListLinkTerm { get; set; }
        public string FilteringLinkTerm { get; set; }
        public string FilteringTitleTerm { get; set; }
        public string FilteringIntroTerm { get; set; }
    }
}