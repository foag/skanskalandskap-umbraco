﻿
namespace SkanskaLandskap15.Models.Shared
{
    public class LinkModel
    {              
        public string Title { get; set; }
        public string Url { get; set; }
        public string Target { get; set; }          
    }
}