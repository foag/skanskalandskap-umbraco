﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using RJP.MultiUrlPicker.Models;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Models;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Web;


namespace SkanskaLandskap15.Controllers
{

    // SWEDISH START PAGE

    public class StartPageController : StartMasterController
    {

        /* Note: Top Image is managed in the MasterController */

        public ActionResult StartPage()
        {
            // Initiate the return model
            var model = new StartPageModel();

            // Get reference to the Umbraco helper             
            UmbracoHelper helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language ("SV" or "EN")
            var currentLanguage = helper.GetDictionaryValue("Language");

            // Set the site part (used by the NewsLatest partial view and the GetAreaMarkersList method)
            model.ContentType = CurrentPage.DocumentTypeAlias;


            /* TIPS */

            // Get a string of comma-separated sub page node id:s (mandatory)
            model.SelectedNodes = CurrentPage.GetPropertyValue<string>(MyConstants.TipsThingsToDoStartPagePropertyAlias);

            // Get fixed values for the tips section
            model.TipsTitle = CurrentPage.GetPropertyValue<string>(MyConstants.TitleTipsPropertyAlias);
            model.MoreTipsTerm = helper.GetDictionaryValue("MoreTips");

            // Get the url of the ThingsToDo landing page due to language
            MasterController mc = new MasterController();
            string rootDocTypeAlias = mc.GetRootContentType(currentLanguage);
            int nodeId = helper.TypedContentAtXPath("//" + rootDocTypeAlias + "/" + MyConstants.ThingsToDoLandingPageDocTypeAlias).First().Id;
            model.MoreTipsUrl = helper.NiceUrl(nodeId);


            /* FIXED PUFF */

            // Create a puff object
            SubPagePuffModel puff = new SubPagePuffModel();

            // Get puff properties
            puff.Image = helper.Media(CurrentPage.GetPropertyValue<string>(MyConstants.ImagePickerPropertyAlias));
            puff.Title = CurrentPage.GetPropertyValue<string>(MyConstants.TitlePuffPropertyAlias);
            puff.Text = CurrentPage.GetPropertyValue<IHtmlString>(MyConstants.TextPropertyAlias);

            // Get link(s). In this case only a single link can be selected.
            var links = CurrentPage.GetPropertyValue<MultiUrls>(MyConstants.MultiUrlPickerPropertyAlias);

            // Initiate link flag
            puff.LinkExists = false;

            // Check whether link exists (optional)
            if (links.Any())
            {
                // Set flag
                puff.LinkExists = true;

                // Instanciate a Link object
                LinkModel link = new LinkModel();

                // Populate link object with properties
                link.Url = links.First().Url;
                link.Title = links.First().Name;
                link.Target = links.First().Target;

                // Add link object to puff object
                puff.Link = link;
            }

            // Add puff object to return model
            model.FixedPuff = puff;


            /* PUFFS SLIDESHOW IS RETRIEVED FROM THE VIEW */


            /* MISC */

            // Add an empty NewsItem object to the return model
            model.NewsItem = new NewsItemModel();

            // Add an empty EventItem object to the return model
            model.EventItem = new EventItemModel();

            return View(model);
        }

    }
}