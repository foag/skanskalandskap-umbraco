﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Xml.XPath;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;


namespace SkanskaLandskap15.Controllers.SurfaceControllers
{
    

    public class MapsSurfaceController : SurfaceController
    {

        // Classes to support dynamically created properties (Duration) for the OutputCache Attribute.
        public static class CacheConfig
        {
            public static int Duration = MasterController.GetOutputCacheDuration("services");
        }

        public class MyOutputCacheAttribute : OutputCacheAttribute
        {
            public MyOutputCacheAttribute()
            {
                this.Duration = CacheConfig.Duration;
            }
        }
               
        ///<summary>
        /// Fetches a list of all services for an area page and returns it as json result. 
        /// Note: Data for busstop's InfoWindows is not included in the result.  
        /// </summary>
        /// <param name="nodeId">The area page's node id.</param>
        /// <param name="language">Current language (SV/EN).</param>       
        /// <returns>The ServiceList object as json.</returns> 
        [HttpGet]
        [MyOutputCache(VaryByParam = "nodeId;language", Location = OutputCacheLocation.Server)]
        //[OutputCache(Duration = 86400, VaryByParam = "nodeId;language", Location = OutputCacheLocation.Server)]
        public JsonResult GetServicesByAreaJson(int nodeId, string language)
        {

            // Initiate the return model
            ServiceListModel model = new ServiceListModel();

            // The culture of an Ajax request thread is neutral and therefore we need to set it 
            // explicitly to be able to get the correct Umbraco context.
            if (Request.IsAjaxRequest())
            {
                UmbracoHelperExtensions.EnsureCulture(Thread.CurrentThread, language);
            }

            // Get reference to the Umbraco helper             
            UmbracoHelper helper = new UmbracoHelper(UmbracoContext.Current);


            /* PREPARE FOR BUS STOP SERVICES */
              
            // Initiate variables
            string apiUrl = string.Empty;
            bool flagTrafiklab = false;
          
            // Set type of request (by name or by nearby)
            string typeOfRequest = "by name";

            // Get api url for the Trafiklab request
            apiUrl = Paths.GetTrafikLabUrl(typeOfRequest);

            // Explicitly initiate the Trafiklab availability
            model.TrafiklabAvailable = false;

            // Check whether Trafiklab services is up and running
            flagTrafiklab = CheckTrafiklab(apiUrl, language);
            model.TrafiklabAvailable = flagTrafiklab;                


            // Get content based on the area's node id
            IPublishedContent areaContent = helper.TypedContent(nodeId);

            List<ServiceModel> listServices = new List<ServiceModel>();

            // Initiate a temporary services list
            var listServicesTemp = new List<ServiceModel>();          

            // Create a list containing current area's services
            listServices = areaContent
                .Children
                .Where(c => c.DocumentTypeAlias == MyConstants.ServiceDocTypeAlias)
                .Select(obj => new ServiceModel()
                {
                    NodeId = obj.Id,
                    ServiceType = obj.GetPropertyValue<string>(MyConstants.ServiceTypePropertyAlias),
                    Title = obj.GetPropertyValue<string>(MyConstants.ServiceNamePropertyAlias)
                }).ToList();
                                      

            // Iterate over the service nodes and get properties
            listServices.ForEach(item =>
            {
                // Initiate a service object
                ServiceModel objService = new ServiceModel();

                // Get a service type object for current service
                ServiceTypeModel modelServiceType = GetServiceTypeByLanguage(helper, language, item.ServiceType);

                // Get properties due to service type (busstop or any other, respectively)
                switch (item.ServiceType)
                {
                    // Busstop service
                    case "Hållplats":

                        //// Initiate a busstop object
                        //ServiceBusStopModel objBusStopService = new ServiceBusStopModel();

                        // Get a populated busstop service object                                      
                        ServiceBusStopModel objBusStopService = GetBusStopService(item, modelServiceType, helper, language, apiUrl, flagTrafiklab);

                        // Add busstop service object to the service object     
                        objService.BusStopService = objBusStopService;

                        // Add model object to list
                        listServicesTemp.Add(objService);

                        break;

                    // Any other service
                    default:

                        // Get a populated standard service object
                        ServiceStandardModel objStandardService = GetStandardService(item, modelServiceType, helper);

                        // Make sure coordinates exists (if the map tab was not even opened, the default map coordinates does not exists)
                        if (objStandardService.MapMarker != null)
                        {
                            // Add standard service object to the service object
                            objService.StandardService = objStandardService;

                            // Add model object to list
                            listServicesTemp.Add(objService);
                        }
                        break;
                }
            });          

            // Add list of services to the return model            
            model.ServiceCollection = listServicesTemp;

            return Json(new { services = model }, JsonRequestBehavior.AllowGet);
        }



     
             
        ///<summary>
        /// Fetches information for a bus stop's info window based on the service's node id 
        /// (aimed for an AJAX request).
        /// </summary>
        /// <param name="nodeId">The id of the service node.</param>
        /// <param name="language">Indicates current language (SV/EN).</param>
        /// <returns>The BusStopService partial populated with a ServiceBusStop object.</returns> 
        [HttpGet]
        [MyOutputCache(VaryByParam = "nodeId;language", Location = OutputCacheLocation.Server)]
        //[OutputCache(Duration = 86400, VaryByParam = "nodeId;language", Location = OutputCacheLocation.Server)]
        public ActionResult GetBusStopInfo(int nodeId, string language)
        {

            // Initiate the return model
            var model = new ServiceBusStopModel();

            // The culture of an Ajax request thread is neutral and therefore we need to set it 
            // explicitly to be able to get the correct Umbraco context.
            if (Request.IsAjaxRequest())
            {
                UmbracoHelperExtensions.EnsureCulture(Thread.CurrentThread, language);
            }

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Set type of request (by name or by nearby)
            string typeOfRequest = "by name";
            
            // Get an api url for the Trafiklab request
            string apiUrl = Paths.GetTrafikLabUrl(typeOfRequest);

            // Check whether Trafiklab services is up and running (maybe a cached version of
            // the busstop was clicked)
            //bool flagTrafiklab = CheckTrafiklab(apiUrl, language);

            // Get content based on the service's node id
            IPublishedContent content = helper.TypedContent(nodeId);

            // Get the id of the service's parent's node (= current area)
            int areaNodeId = content.Parent.Id;

            // Get content based on the area's node id
            IPublishedContent areaContent = helper.TypedContent(areaNodeId);

            // Initiate FindUs object
            FindUsModel findUs = null;

            //// Get the area's route description pdf file, if any
            //dynamic pdfPublicRoute = helper.Media(areaContent.GetPropertyValue<string>(MyConstants.RouteDescriptionPropertyAlias));

            //if (pdfPublicRoute != null)
            //{
            //    // Create a controller reference
            //    MediaSurfaceController msc = new MediaSurfaceController();

            //    // Get file properties          
            //    MediaFileModel routePdf = msc.GetMediaFileProperties(pdfPublicRoute, language);

            //    // Create a FindUs object the bus stop service 
            //    findUs = new FindUsModel
            //    {
            //        DownloadRouteLinkTerm = helper.GetDictionaryValue("DownloadRouteLinkTitle"),
            //        RouteDescriptionFile = routePdf
            //    };
            //}

            // Populate the return model
            model = new ServiceBusStopModel
            {
                Title = content.GetPropertyValue<string>(MyConstants.ServiceNamePropertyAlias),
                Intro = content.GetPropertyValue<IHtmlString>(MyConstants.ServiceDescriptionPropertyAlias), 
                ServiceType = content.GetPropertyValue<string>(MyConstants.ServiceTypePropertyAlias),
                FindUs = findUs,
                MapMarker = new MapMarkerModel(),
                GetHereFromTerm = helper.GetDictionaryValue("GetHereFrom"),
                SearchTerm = helper.GetDictionaryValue("Find")
            };

            // Get the service type's title due to language
             model.BusStopTerm = GetBusStopTitleByLanguage(helper, language, model.ServiceType);

             // Get the TrafikLab part of the Info Window content
             model = GetTrafiklabInfo(model, helper, language, apiUrl);

            return PartialView("BusStopService", model);
        }




        /* HELPER METHODS */      

        public ServiceStandardModel GetStandardService(ServiceModel objService, ServiceTypeModel objServiceType, UmbracoHelper helper)
        {

            // Initiate variables
            string mapLocation = string.Empty;
            string returnValue = string.Empty;
            bool coordsExist = false;

            // Get current service's content
            var content = helper.TypedContent(objService.NodeId);

            // Create and populate the return model
            ServiceStandardModel model = new ServiceStandardModel
            {
                ServiceTypeNodeId = objServiceType.NodeId,
                ServiceType = objService.ServiceType,
                Title = objService.Title,
                Intro = content.GetPropertyValue<IHtmlString>(MyConstants.ServiceDescriptionPropertyAlias),
                MapLocation = content.GetPropertyValue<string>(MyConstants.MapLocationServicePropertyAlias)
            };
            
            // Get the RT90 properties, if any
            string coordRT90Y = content.GetPropertyValue<string>(MyConstants.MapRT90YCoordinatePropertyAlias).Trim();
            string coordRT90X = content.GetPropertyValue<string>(MyConstants.MapRT90XCoordinatePropertyAlias.Trim());

            // Check whether RT90 coordinates exists
            if (Convert.ToInt32(coordRT90X) > 0 && Convert.ToInt32(coordRT90Y) > 0)
            {
                // Convert from RT90 to GWS84 (supported by Google maps)
                returnValue = ConvertCoords.ConvertRT902GWS84(coordRT90Y, coordRT90X);

                // Make sure the coordinates string length was valid
                if (returnValue != "bad_input")
                {
                    coordsExist = true;
                    mapLocation = returnValue;
                }
            }

            // If not, look for coordinates in the Google Maps datatype (there should always be the 
            // default coordinates, but you never know...)
            else if (!string.IsNullOrWhiteSpace(model.MapLocation))
            {
                coordsExist = true;
                mapLocation = model.MapLocation.Trim();
            }
            else
            {
                model.MapLocation = string.Empty;
            }

            // Create a MapMarker object provided coordinates exist
            if (coordsExist == true)
            {
                // Instanciate object
                var objMapMarker = new MapMarkerModel();

                // Assign coordinates to current object
                string[] mapLocationInfo = mapLocation.Split(new Char[] { ',' });
                objMapMarker.Latitude = mapLocationInfo[0];
                objMapMarker.Longitude = mapLocationInfo[1];

                // Assign some content for a possible infobox
                // objMapMarker.Content = objServiceType.Title + ": " + model.Title + "<br />" + model.Intro;
                objMapMarker.Title = model.Title;
                objMapMarker.Intro = model.Intro.ToString();

                // Assign area, icon file and tag names                
                objMapMarker.Icon = objServiceType.IconPath;
                objMapMarker.Tag = objServiceType.UrlName;

                // Add current MapMarker object to the return model
                model.MapMarker = objMapMarker;
            }

            return model;
        }

        
     
        public ServiceBusStopModel GetBusStopService(ServiceModel objService, ServiceTypeModel objServiceType, UmbracoHelper helper, string language, string apiUrl, bool flagTrafiklab)
        {

            // Initiate and populate the return model
            ServiceBusStopModel model = new ServiceBusStopModel
            {
                NodeId = objService.NodeId,
                ServiceTypeNodeId = objServiceType.NodeId,
                ServiceType = objService.ServiceType,
                Title = objService.Title
            };

            // Get info from Trafiklab provided their service is available
            if (flagTrafiklab)
            { 
                // Get Trafiklab url
                string stationURL = CreateTrafiklabUrl(apiUrl, model.Title, language);                

                // Get the xml result
                XPathNodeIterator stationResponse = umbraco.library.GetXmlDocumentByUrl(stationURL);

                // Iterate over the xml result     
                foreach (XPathNavigator item in stationResponse.Current.Select("//*/*"))
                {
                    // Instanciate map marker object
                    var objMapMarker = new MapMarkerModel();

                    // Add coordinates
                    objMapMarker.Latitude = item.SelectSingleNode("@lat").ToString();
                    objMapMarker.Longitude = item.SelectSingleNode("@lon").ToString();

                    // Create header for the infobox
                    objMapMarker.Title = objServiceType.Title + ": " + item.SelectSingleNode("@name").ToString();

                    // Assign xml properties 
                    string xmlNodeId = item.SelectSingleNode("@id").ToString();

                    // Assign icon file and tag names 
                    objMapMarker.Icon = objServiceType.IconPath;
                    objMapMarker.Tag = objServiceType.UrlName;

                    // Add current MapMarker object to return model
                    model.MapMarker = objMapMarker;
                }
            }

            return model;
        }

                       
        protected ServiceBusStopModel GetTrafiklabInfo(ServiceBusStopModel model, UmbracoHelper helper, string language, string apiUrl)
        {

            // Get Trafiklab url
            string stationURL = CreateTrafiklabUrl(apiUrl, model.Title, language);
                       
            // Get the xml result
            XPathNodeIterator stationResponse = umbraco.library.GetXmlDocumentByUrl(stationURL);

            // Iterate over the xml result     
            foreach (XPathNavigator item in stationResponse.Current.Select("//*/*"))
            {
                // Instanciate map marker object
                var objMapMarker = new MapMarkerModel();

                // Add coordinates
                objMapMarker.Latitude = item.SelectSingleNode("@lat").ToString();
                objMapMarker.Longitude = item.SelectSingleNode("@lon").ToString();

                // Create header for the infobox
                objMapMarker.Title = model.BusStopTerm + ": " + item.SelectSingleNode("@name").ToString();

                // Add info text
                objMapMarker.Content = model.Intro.ToString();

                //// Add the area's public route pdf file object, if any
                //if (model.FindUs != null)
                //{
                //    objMapMarker.PublicRoutePdf = model.FindUs.RouteDescriptionFile;

                //    // Add the pdf link title
                //    objMapMarker.DownloadRouteLinkTerm = model.FindUs.DownloadRouteLinkTerm;
                //}

                // Assign xml properties 
                string xmlNodeId = item.SelectSingleNode("@id").ToString();
                               
                // Create a Resrobot id string 
                StringBuilder sb = new StringBuilder();
                sb.Append("A=1");
                sb.Append("@O=@");
                sb.Append("@X=" + objMapMarker.Longitude);
                sb.Append("@Y=" + objMapMarker.Latitude);
                sb.Append("@U=80");
                sb.Append("@L=" + xmlNodeId);
                sb.Append("@B=1");
                sb.Append("@V=74.9,");
                sb.Append("@p=1335531707@");
                objMapMarker.ResRobotZID = sb.ToString();

                // Add current MapMarker object to return model
                model.MapMarker = objMapMarker;
            }

            return model;
        }



        public ServiceTypeModel GetServiceTypeByLanguage(UmbracoHelper helper, string language, string serviceType)
        {

            // Initiate return model
            ServiceTypeModel model = new ServiceTypeModel();

            // Get id of the service types parent node
            int id = helper.TypedContentSingleAtXPath("//" + MyConstants.ServiceTypesDocTypeAlias).Id;

            // Get content based on the service type's node id
            var content = helper.TypedContent(id);

            // Get the service type's title due to language
            switch (language)
            {
                // Swedish - use node's name as the services title
                case "SV":

                    model = content
                       .Children
                       .Where(c => c.DocumentTypeAlias == MyConstants.ServiceTypeDocTypeAlias && c.Name == serviceType)
                       .Select(obj => new ServiceTypeModel()
                       {
                           NodeId = obj.Id,
                           Title = obj.Name,
                           UrlName = obj.UrlName,
                           IconPath = helper.Media(obj.GetPropertyValue(MyConstants.ServiceTypeIconPropertyAlias)).Url
                       }).FirstOrDefault();
                    break;

                // English - use the specific English term for title
                case "EN":

                    model = content
                     .Children
                     .Where(c => c.DocumentTypeAlias == MyConstants.ServiceTypeDocTypeAlias && c.Name == serviceType)
                     .Select(obj => new ServiceTypeModel()
                     {
                         NodeId = obj.Id,
                         Title = obj.GetPropertyValue<string>(MyConstants.TermEnglishPropertyAlias),
                         UrlName = obj.UrlName,
                         IconPath = helper.Media(obj.GetPropertyValue(MyConstants.ServiceTypeIconPropertyAlias)).Url
                     }).FirstOrDefault();
                    break;
            }

            return model;
        }



        protected string GetBusStopTitleByLanguage(UmbracoHelper helper, string language, string serviceType)
        {

            // Get id of the service types parent node
            int id = helper.TypedContentSingleAtXPath("//" + MyConstants.ServiceTypesDocTypeAlias).Id;

            // Get content based on the service type's node id
            var content = helper.TypedContent(id);

            // Initiate a ServiceType object
            ServiceTypeModel model = new ServiceTypeModel();

            // Get the service type's title due to language
            switch (language)
            {
                // Swedish - use node's name as the services title
                case "SV":

                    model = content
                       .Children
                       .Where(c => c.DocumentTypeAlias == MyConstants.ServiceTypeDocTypeAlias && c.Name == serviceType)
                       .Select(obj => new ServiceTypeModel()
                       {
                           NodeId = obj.Id,
                           Title = obj.Name
                       }).FirstOrDefault();
                    break;

                // English - use the specific English term for title
                case "EN":

                    model = content
                     .Children
                     .Where(c => c.DocumentTypeAlias == MyConstants.ServiceTypeDocTypeAlias && c.Name == serviceType)
                     .Select(obj => new ServiceTypeModel()
                     {
                         NodeId = obj.Id,
                         Title = obj.GetPropertyValue<string>(MyConstants.TermEnglishPropertyAlias)
                     }).FirstOrDefault();
                    break;
            }

            return model.Title;
        }


        public bool CheckTrafiklab(string apiUrl, string language)
        {

            // Initiate return value
            bool available = false;
            
            // Get Trafiklab url
            string stationURL = CreateTrafiklabUrl(apiUrl, MyConstants.TrafiklabTestStation, language);
            
            // Initiate variables          
            bool unavailable = false;
            int stations = 0;
            
            // Try get a response from Trafiklab 
            try
            {
                // Get the xml result
                XPathNodeIterator stationResponse = umbraco.library.GetXmlDocumentByUrl(stationURL);

                // Iterate over the xml result     
                foreach (XPathNavigator item in stationResponse.Current.Select("//*/*"))
                {
                    // Enumerate the stations counter
                    stations += 1;

                    // There was a valid xml result but content is missing  
                    if (item.SelectSingleNode("@name").ToString() == string.Empty)
                    {
                        // Set flag and break out of loop
                        unavailable = true;
                        break;
                    }
                }
            }
            // Any error were thrown
            catch
            {
                unavailable = true;
            }

            // If service is available, set return value to true
            if (unavailable == false && stations > 0)
            {
                available = true;
            }
                        
            return available;
        }
    


        ///<summary>
        /// Fetches the basic information for a Google map, i.e. latitude, longitude and zoom.
        /// </summary>
        /// <param name="nodeId">The node id of current page.</param>
        /// <returns>A json MapBasicData object.</returns>      
        [HttpGet]
        public JsonResult GetBasicData(int nodeId)
        {
            // Create return object
            var model = new MapBasicDataModel();

            // Get reference to the Umbraco helper             
            UmbracoHelper helper = new UmbracoHelper(UmbracoContext.Current);

            // Get node content  
            var content = helper.TypedContent(nodeId);

            // Initiate variable
            string mapLocation = String.Empty;

            // Get the Content node's map location 
            mapLocation = content.GetPropertyValue<string>(MyConstants.MapLocationPropertyAlias).Trim();

            // Assign coordinates and zoom value to model
            if (mapLocation != String.Empty)
            {
                string[] mapLocationInfo = mapLocation.Split(new Char[] { ',' });
                model.Latitude = mapLocationInfo[0];
                model.Longitude = mapLocationInfo[1];
                model.Zoom = Convert.ToInt32(mapLocationInfo[2]);
                model.UrlName = content.UrlName;
            }

            return Json(new { map = model }, JsonRequestBehavior.AllowGet);
        }




        ///<summary>
        /// Fetches markers for all areas.
        /// </summary> 
        /// <param name="language">Current language.</param>
        /// <returns>A json list of MapMarker objects.</returns>      
        [HttpGet]
        public JsonResult GetAreaMarkers(string language)
        {
            // Initiate the return value
            var markersList = new List<MapMarkerModel>();

            // Get the list
            markersList = GetAreaMarkersList(language);

            return Json(new { markers = markersList }, JsonRequestBehavior.AllowGet);
        }




        ///<summary>
        /// Fetches marker information for all areas. Note: the method is called not only from 
        /// this controller but from the StartPageController as well.
        /// </summary> 
        /// <param name="language">Current language.</param>
        /// <param name="sitePart">The document type alias of the calling page. Might be used 
        /// for customizing the return value for the start page map. Optional.</param>
        /// <returns>A list of MapMarker objects.</returns>   
        public List<MapMarkerModel> GetAreaMarkersList(string language, string sitePart = "")
        {
            // Initiate the return value
            var markersList = new List<MapMarkerModel>();

            // Get a reference to the Umbraco helper         
            UmbracoHelper helper = new UmbracoHelper(UmbracoContext.Current);

            // Get reference to controller
            AreasSurfaceController asc = new AreasSurfaceController();

            // Get node id of the areas landing page due to language
            int id = asc.GetAreasLandingPageNodeId(language, helper);

            // Get node content 
            var content = helper.TypedContent(id);

            // Set some variables            
            string tagName = "area";
            string iconPath = Paths.GetStandardAreaMarkerPath();

            //// Filter out the necessary map properties from the area nodes
            //IEnumerable<InteractiveMasterModel> listMarkerData = content
            //    .Children
            //    .Where(c => c.DocumentTypeAlias == MyConstants.AreaPageDocTypeAlias)
            //    .Select(obj => new InteractiveMasterModel()
            //    {
            //        Title = obj.Name,
            //        Intro = obj.GetPropertyValue<string>(MyConstants.IntroPropertyAlias),
            //        MapLocation = obj.GetPropertyValue<string>(MyConstants.MapLocationPropertyAlias),
            //        AreaActivities = obj.GetPropertyValue<string>(MyConstants.AreaActivitiesPropertyAlias),
            //        // Assign the page's UrlName to model to be able to add a kml layer 
            //        UrlName = obj.UrlName,
            //        Id = obj.Id
            //    }).ToList();

            // Filter out the necessary map properties from the area nodes
            IEnumerable<AreaModel> listMarkerData = content
                .Children
                .Where(c => c.DocumentTypeAlias == MyConstants.AreaPageDocTypeAlias)
                .Select(obj => new AreaModel()
                {
                    Title = obj.Name,
                    Intro = obj.GetPropertyValue<string>(MyConstants.IntroPropertyAlias),
                    MapLocation = obj.GetPropertyValue<string>(MyConstants.MapLocationPropertyAlias),
                    AreaActivities = obj.GetPropertyValue<string>(MyConstants.AreaActivitiesPropertyAlias),
                    // Assign the page's UrlName to model to be able to add a kml layer 
                    UrlName = obj.UrlName,
                    NodeId = obj.Id
                }).ToList();

            // Iterate over the map properties
            listMarkerData.ForEach(item =>
            {
                if (string.IsNullOrWhiteSpace(item.MapLocation))
                {
                    item.MapLocation = string.Empty;
                }
                else
                {
                    // Create a MapMarker object
                    var objMapMarker = new MapMarkerModel();

                    // Get coordinates
                    string mapLocation = item.MapLocation.Trim();

                    // Assign coordinates to current MapMarker object
                    string[] mapLocationInfo = mapLocation.Split(new Char[] { ',' });
                    objMapMarker.Latitude = mapLocationInfo[0];
                    objMapMarker.Longitude = mapLocationInfo[1];

                    // Assign some content for the infobox
                    objMapMarker.Content = item.Title + "<br />" + item.Intro;

                    // Assign title, icon file and tag name 
                    objMapMarker.Title = item.Title;
                    objMapMarker.Icon = iconPath;
                    objMapMarker.Tag = tagName;
                    objMapMarker.AreaActivities = item.AreaActivities;

                    // Get UrlName
                    objMapMarker.UrlName = item.UrlName;
                    objMapMarker.NodeId = item.NodeId;

                    // Add MapMarker object to the list
                    markersList.Add(objMapMarker);
                }
            });
            return markersList;
        }




        ///<summary>
        /// Fetches markers for a specific service type that's provided by a specific area. It's possible 
        /// to get the services for all area nodes as well (see parameter nodeId).
        /// </summary>
        /// <param name="serviceType">The requested service type, more precisely the service type node's 
        /// UrlName.</param>
        /// <param name="language">Current language.</param>
        /// <param name="nodeId">Id of current area. Optional.</param>
        /// <returns>A list of marker data in Json format.</returns>      
        [HttpGet]
        public JsonResult GetMarkersForService(string serviceType, string language, int nodeId = 0)
        {
            // Initiate the return value
            var markersList = new List<MapMarkerModel>();

            // The culture of an Ajax request thread is neutral and therefore we need to set it 
            // explicitly to be able to get the correct Umbraco context.
            if (Request.IsAjaxRequest())
            {
                UmbracoHelperExtensions.EnsureCulture(Thread.CurrentThread, language);
            }

            // Get a reference to the Umbraco helper         
            UmbracoHelper helper = new UmbracoHelper(UmbracoContext.Current);


            /* CREATE SERVICE TYPE OBJECT */

            // Get id of the Service Types parent node
            int nodeIdServiceTypes = Umbraco.TypedContentSingleAtXPath("//" + MyConstants.ServiceTypesDocTypeAlias).Id;

            // Get service type content based on parent node's id
            var content = helper.TypedContent(nodeIdServiceTypes);

            // Initiate object
            ServiceTypeModel modelServiceType = null;

            // Create a service type's object due to language 
            switch (language)
            {
                // Swedish - use node's name as the service type's title
                case "SV":

                    modelServiceType = content
                       .Children
                       .Where(c => c.UrlName == serviceType)
                       .Select(obj => new ServiceTypeModel()
                       {
                           // Get node name
                           Name = obj.Name,
                           Title = obj.Name,
                           MapIcon = helper.Media(obj.GetPropertyValue<string>(MyConstants.ServiceTypeIconPropertyAlias)),
                           UrlName = obj.UrlName,
                       }).First();
                    break;

                // English - use the specific English term for title
                case "EN":

                    modelServiceType = content
                      .Children
                      .Where(c => c.UrlName == serviceType)
                      .Select(obj => new ServiceTypeModel()
                      {
                          // Get specific English name
                          Name = obj.Name,
                          Title = obj.GetPropertyValue<string>(MyConstants.TermEnglishPropertyAlias),
                          MapIcon = helper.Media(obj.GetPropertyValue<string>(MyConstants.ServiceTypeIconPropertyAlias)),
                          UrlName = obj.UrlName,
                      }).First();
                    break;
            }


            /* COLLECT REQUESTED SERVICES FOR AREA(S) */

            // Initiate variable
            IPublishedContent areaContent = null;

            // Get markers due to type of request
            switch (nodeId)
            {
                // Markers for all areas
                case 0:

                    // Get document type for start node due to language. 
                    string parentDocType = string.Empty;
                    switch (language)
                    {
                        case "SV":
                            parentDocType = MyConstants.StartPageDocTypeAlias;
                            break;
                        case "EN":
                            parentDocType = MyConstants.HomePageDocTypeAlias;
                            break;
                    }

                    // Get the node id of the areas landing page due to language
                    int id = helper.TypedContentSingleAtXPath("//" + parentDocType + "/" + MyConstants.AreasLandingPageDocTypeAlias).Id;

                    // Get node content 
                    var contentAreas = helper.TypedContent(id);

                    // Get a list of all area node id:s
                    IEnumerable<AreaModel> listAreaIds = contentAreas
                        .Children
                        .Where(c => c.DocumentTypeAlias == MyConstants.AreaPageDocTypeAlias)
                        .Select(obj => new AreaModel()
                        {
                            NodeId = obj.Id
                        }).ToList();

                    // Iterate over the area node id:s
                    listAreaIds.ForEach(item =>
                    {
                        // Get node content based on current area's node id
                        areaContent = helper.TypedContent(item.NodeId);

                        // Get a services list for each area
                        markersList = GetServicesList(serviceType, markersList, areaContent, modelServiceType, helper, language);
                    });
                    break;

                // Markers for a single area page based on the nodeId input parameter
                default:

                    // Get node content based on the area's node id
                    areaContent = helper.TypedContent(nodeId);

                    // Get a services list for the specified area
                    markersList = GetServicesList(serviceType, markersList, areaContent, modelServiceType, helper, language);

                    break;
            }

            return Json(new { markers = markersList }, JsonRequestBehavior.AllowGet);
        }


        

        public List<MapMarkerModel> GetServicesList(String serviceType, List<MapMarkerModel> markersList, IPublishedContent areaContent, ServiceTypeModel objServiceType, UmbracoHelper helper, string language)
        {
            // Get the list due to service type
            switch (serviceType)
            {
                // Bus stop
                case "hallplats":

                    // Set type of request
                    string typeOfRequest = "by name";

                    // Get list based on type of request
                    markersList = GetBusstopServicesList(typeOfRequest, markersList, areaContent, objServiceType, helper, language);
                    break;

                // Any other service
                default:
                    markersList = GetStandardServicesList(markersList, areaContent, objServiceType, helper);
                    break;
            }

            return markersList;
        }



        public List<MapMarkerModel> GetStandardServicesList(List<MapMarkerModel> markersList, IPublishedContent areaContent, ServiceTypeModel objServiceType, UmbracoHelper helper)
        {

            // Initiate variables
            string mapLocation = string.Empty;
            string returnValue = string.Empty;
            bool coordsExist = false;

            // Filter out services based on the requested service type's node name and create a list
            IEnumerable<ServiceStandardModel> listMarkerData = areaContent
                .Children
                .Where(c => c.DocumentTypeAlias == MyConstants.ServiceDocTypeAlias && c.GetPropertyValue<string>(MyConstants.ServiceTypeDocTypeAlias) == objServiceType.Name)
                .Select(obj => new ServiceStandardModel()
                {
                    NodeId = obj.Id,
                    Title = obj.GetPropertyValue<string>(MyConstants.ServiceNamePropertyAlias),
                    Intro = obj.GetPropertyValue<IHtmlString>(MyConstants.ServiceDescriptionPropertyAlias),
                    MapLocation = obj.GetPropertyValue<string>(MyConstants.MapLocationServicePropertyAlias)
                }).ToList();

            // Iterate over the service properties and add them to the return list
            listMarkerData.ForEach(itemMap =>
            {

                var itemContent = helper.TypedContent(itemMap.NodeId);

                // Get the RT90 properties
                string coordRT90Y = itemContent.GetPropertyValue<string>(MyConstants.MapRT90YCoordinatePropertyAlias).Trim();
                string coordRT90X = itemContent.GetPropertyValue<string>(MyConstants.MapRT90XCoordinatePropertyAlias.Trim());

                // Check whether RT90 coordinates exist
                if (Convert.ToInt32(coordRT90X) > 0 && Convert.ToInt32(coordRT90Y) > 0)
                {
                    // Convert from RT90 to GWS84 (supported by Google maps)
                    returnValue = ConvertCoords.ConvertRT902GWS84(coordRT90Y, coordRT90X);

                    // Make sure the coordinates string length was valid
                    if (returnValue != "bad_input")
                    {
                        coordsExist = true;
                        mapLocation = returnValue;
                    }
                }

                // If not, look for coordinates in the Google Maps datatype (there should always be the default 
                // coordinates, but you never know...)
                else if (!string.IsNullOrWhiteSpace(itemMap.MapLocation))
                {
                    coordsExist = true;
                    mapLocation = itemMap.MapLocation.Trim();
                }
                else
                {
                    itemMap.MapLocation = string.Empty;
                }

                // Create a MapMarker object provided coordinates exist
                if (coordsExist == true)
                {
                    // Instanciate object
                    var objMapMarker = new MapMarkerModel();

                    // Assign coordinates to current object
                    string[] mapLocationInfo = mapLocation.Split(new Char[] { ',' });
                    objMapMarker.Latitude = mapLocationInfo[0];
                    objMapMarker.Longitude = mapLocationInfo[1];

                    // Assign some content for a possible infobox
                    objMapMarker.Content = objServiceType.Title + ": " + itemMap.Title + "<br />" + itemMap.Intro;

                    // Assign area, icon file and tag names 
                    objMapMarker.Title = itemMap.Title;
                    objMapMarker.Icon = objServiceType.MapIcon.Url;
                    objMapMarker.Tag = objServiceType.UrlName;

                    // Add current MapMarker object to the list
                    markersList.Add(objMapMarker);
                }
            });
            return markersList;
        }





        public List<MapMarkerModel> GetBusstopServicesList(string typeOfRequest, List<MapMarkerModel> markersList, IPublishedContent areaContent, ServiceTypeModel objServiceType, UmbracoHelper helper, string language)
        {
            // Get bus stop term from Dictionary
            string busstopTerm = helper.GetDictionaryValue("Busstop");

            // Get TrafikLab url due to type of request (by name or by nearby)
            string apiUrl = Paths.GetTrafikLabUrl(typeOfRequest);

            // Get the list due to type of request
            switch (typeOfRequest)
            {
                case "by name":
                    markersList = GetBusstopsByName(markersList, areaContent, objServiceType, apiUrl, busstopTerm, language, helper);
                    break;
                case "by nearby":
                    markersList = GetBusstopsByNearby(markersList, areaContent, objServiceType, apiUrl, busstopTerm, language);
                    break;
            }
            return markersList;
        }


                    

        public List<MapMarkerModel> GetBusstopsByNearby(List<MapMarkerModel> markersList, IPublishedContent areaContent, ServiceTypeModel objServiceType, string apiUrl, string busstopTerm, string language)
        {

            // Initiate variables
            string mapLocation = String.Empty;
            int radius = 0;
            string searchRadius = string.Empty;
            string coordsY = String.Empty;
            string coordsX = String.Empty;

            // Get area's map coordinates and radius value
            mapLocation = areaContent.GetPropertyValue<string>(MyConstants.MapLocationPropertyAlias).Trim();
            radius = areaContent.GetPropertyValue<int>(MyConstants.MapBusstopRadiusPropertyAlias);

            // Assign coordinates to variables
            if (mapLocation != String.Empty)
            {
                string[] mapLocationInfo = mapLocation.Split(new Char[] { ',' });
                coordsY = mapLocationInfo[0];
                coordsX = mapLocationInfo[1];
            }
                       
            // Manage the bus stop search radius                       
            if (radius > 0)
            {
                searchRadius = radius.ToString();
            }
            // Get default value if radius was not set in backoffice
            else
            {
                searchRadius = MyConstants.BusstopRadius.ToString();
            }

            string stationListURL = CreateTrafiklabUrlByNearby(apiUrl, coordsY, coordsX, 25, searchRadius, language);                

            // Get the xml result
            XPathNodeIterator stationListResponse = umbraco.library.GetXmlDocumentByUrl(stationListURL);

            // Iterate over the xml result     
            foreach (XPathNavigator item in stationListResponse.Current.Select("//*/*"))
            {
                // Instanciate object for current bus stop
                var objMapMarker = new MapMarkerModel();

                // Add coordinates
                objMapMarker.Latitude = item.SelectSingleNode("@lat").ToString();
                objMapMarker.Longitude = item.SelectSingleNode("@lon").ToString();

                // Create header for the infobox
                objMapMarker.Title = busstopTerm + ": " + item.SelectSingleNode("@name").ToString();

                // Assign xml properties 
                string xmlNodeId = item.SelectSingleNode("@id").ToString();
                
                // Create a Resrobot id string 
                StringBuilder sb = new StringBuilder();
                sb.Append("A=1");
                sb.Append("@O=@");
                sb.Append("@X=" + objMapMarker.Longitude);
                sb.Append("@Y=" + objMapMarker.Latitude);
                sb.Append("@U=80");
                sb.Append("@L=" + xmlNodeId);
                sb.Append("@B=1");
                sb.Append("@V=74.9,");
                sb.Append("@p=1335531707@");
                objMapMarker.ResRobotZID = sb.ToString();

                // Assign icon file and tag names 
                objMapMarker.Icon = objServiceType.MapIcon.Url;
                objMapMarker.Tag = objServiceType.UrlName;

                // Add current MapMarker object to the list
                markersList.Add(objMapMarker);
            }

            return markersList;
        }




        public List<MapMarkerModel> GetBusstopsByName(List<MapMarkerModel> markersList, IPublishedContent areaContent, ServiceTypeModel objServiceType, string apiUrl, string busstopTerm, string language, UmbracoHelper helper)
        {

            //// Get the area's public route pdf file
            //dynamic pdfRoute = helper.Media(areaContent.GetPropertyValue(MyConstants.RouteDescriptionPropertyAlias));

            // Get link title for the pdf 
            //string downloadRouteLinkTitle = helper.GetDictionaryValue("DownloadRouteLinkTitle");

            // Create a controller reference
            MediaSurfaceController msc = new MediaSurfaceController();

            //// Get file properties and add file object to return model           
            //MediaFileModel routePdf = msc.GetMediaFileProperties(pdfRoute, language);

            // Filter out services based on the requested service type's node name and create a list
            IEnumerable<ServiceBusStopModel> listServiceData = areaContent
                .Children
                .Where(c => c.DocumentTypeAlias == MyConstants.ServiceDocTypeAlias && c.GetPropertyValue<string>(MyConstants.ServiceTypeDocTypeAlias) == objServiceType.Name)
                .Select(obj => new ServiceBusStopModel()
                {
                    Title = obj.GetPropertyValue<string>(MyConstants.ServiceNamePropertyAlias),
                    Intro = obj.GetPropertyValue<IHtmlString>(MyConstants.ServiceDescriptionPropertyAlias),
                }).ToList();

            // Iterate over the service properties and add them to the return list
            listServiceData.ForEach(itemMap =>
            {

                string stationURL = CreateTrafiklabUrl(apiUrl, itemMap.Title, language);               

                // Get the xml result
                XPathNodeIterator stationResponse = umbraco.library.GetXmlDocumentByUrl(stationURL);

                // Iterate over the xml result     
                foreach (XPathNavigator item in stationResponse.Current.Select("//*/*"))
                {
                    // Instanciate object for current bus stop
                    var objMapMarker = new MapMarkerModel();

                    // Add coordinates
                    objMapMarker.Latitude = item.SelectSingleNode("@lat").ToString();
                    objMapMarker.Longitude = item.SelectSingleNode("@lon").ToString();

                    // Create header for the infobox
                    objMapMarker.Title = busstopTerm + ": " + item.SelectSingleNode("@name").ToString();

                    // Add info text
                    objMapMarker.Content = itemMap.Intro.ToString();

                    // Add the area's public route pdf file object
                    //objMapMarker.PublicRoutePdf = routePdf;

                    // Add the pdf link title
                    //objMapMarker.DownloadRouteLinkTerm = downloadRouteLinkTitle;

                    // Assign xml properties 
                    string xmlNodeId = item.SelectSingleNode("@id").ToString();

                    // Create a Resrobot id string 
                    StringBuilder sb = new StringBuilder();
                    sb.Append("A=1");
                    sb.Append("@O=@");
                    sb.Append("@X=" + objMapMarker.Longitude);
                    sb.Append("@Y=" + objMapMarker.Latitude);
                    sb.Append("@U=80");
                    sb.Append("@L=" + xmlNodeId);
                    sb.Append("@B=1");
                    sb.Append("@V=74.9,");
                    sb.Append("@p=1335531707@");
                    objMapMarker.ResRobotZID = sb.ToString();

                    // Assign icon file and tag names 
                    objMapMarker.Icon = objServiceType.MapIcon.Url;
                    objMapMarker.Tag = objServiceType.UrlName;

                    // Add current MapMarker object to the list
                    markersList.Add(objMapMarker);
                }
            });
            return markersList;
        }


        protected string CreateTrafiklabUrl(string apiUrl, string title, string language)
        {

            // Initiate variables
            string stationUrl = string.Empty;
            StringBuilder sb = new StringBuilder();
           
            // Create complete TrafikLab url
            sb.Append(apiUrl);
            // Url encode title to get rid of (for Trafiklab) baffling Swedish characters 
            sb.Append("&input=" + Server.UrlEncode(title));
            sb.Append("&maxNo=" + 1);
            sb.Append("&lang=" + language.ToLower());
            // Add xml format info here instead of setting xml as file extension to avoid a second url-encode.
            sb.Append("&format=xml");
            stationUrl = sb.ToString();

            return stationUrl;
        }


        protected string CreateTrafiklabUrlByNearby(string apiUrl, string coordsY, string coordsX, int maxNo, string searchRadius, string language)
        {

            // Initiate variables
            StringBuilder sb = new StringBuilder();
            string stationListURL = string.Empty;

            // Create url to get an xml result from tafiklab (Alternative: &format=json)
            sb.Append(apiUrl);
            sb.Append("&originCoordLat=" + coordsY);
            sb.Append("&originCoordLong=" + coordsX);
            sb.Append("&maxNo=" + 25);
            sb.Append("&r=" + searchRadius);
            sb.Append("&lang=" + language.ToLower());
            // Add xml format info here instead of setting xml as file extension to avoid a second url-encode.
            sb.Append("&format=xml");
            stationListURL = sb.ToString();

            return stationListURL;
        }

    }
}