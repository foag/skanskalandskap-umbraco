﻿using System.Collections.Generic;
using System.Web;
using SkanskaLandskap15.Models.Masters;
using SkanskaLandskap15.Models.Shared;


namespace SkanskaLandskap15.Models
{
    public class AreaPageModel : InteractiveMasterModel
    {
        public bool TipsExists { get; set; }
        public string TipsTitle { get; set; }
        public string MoreTipsTerm { get; set; }
        public string MoreTipsUrl { get; set; }
        public string SelectedNodes { get; set; }

        public string MapLocation { get; set; }
        public MapMarkerModel MapMarker { get; set; }

        public IHtmlString DoAndExperience { get; set; }
        public FindUsModel FindUs { get; set; }
        public bool DownloadsExists { get; set; }
        public string DownloadsTitle { get; set; }
        public bool OrderingPageExists { get; set; }         
        public string OrderingPageUrl { get; set; }
        public string OrderingPageLinkTitle { get; set; }
        public string OrderingText { get; set; }
        public MediaFilesModel MediaFiles { get; set; }

        public string ShowOnMapTerm { get; set; }
        public string MarkerIconPath { get; set; }
        public MapInteractivityModel ServiceTypes { get; set; }
        public int ServicesCacheDuration { get; set; }
        
        public bool WarningsExists { get; set; }
        public string WarningsTitle { get; set; }
        public List<PuffModel> WarningsCollection { get; set; }
        public IHtmlString GeneralInfo { get; set; }
        public string GeneralInfoTitle { get; set; }
        public IHtmlString UsefulInfo { get; set; }

        public SlideshowModel Slideshow { get; set; }
        public bool SlideshowExists { get; set; }

        public string AreaTerm { get; set; }
        public string AboutTerm { get; set; }
        public string BackTerm { get; set; } 
        public string DoAndExperienceTerm { get; set; }
        public string MoreActivitiesTerm { get; set; }
        public string FindUsTerm { get; set; }
        public string UsefulInformationTerm { get; set; }
        public string FilterTerm { get; set; }
        public string FilteringTitleTerm { get; set; }
        public string FilteringIntroTerm { get; set; }
          
        public NewsItemModel NewsItem { get; set; }
        public EventItemModel EventItem { get; set; }
        public ServiceListModel ServiceList { get; set; }

        public bool AccommodationTipsExists { get; set; }
        public string AccommodationsTipsTitle { get; set; }
        public SubPagesListModel SubPagesAccommodations { get; set; } 
       
    }
}