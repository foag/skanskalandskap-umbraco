﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;



namespace SkanskaLandskap15.Controllers.SurfaceControllers
{
   
    public class GuestRatingsSurfaceController : SurfaceController 
    {

        // Initiate global database variables        
        protected SqlConnection conn { get; set; }
        String ConnectionString = ConfigurationManager.ConnectionStrings["umbracoDbDSN"].ToString();
        String SqlStatement = String.Empty;        
        
        
        ///<summary>
        /// Fetches guest ratings information and returns a partial view.
        /// </summary>
        /// <param name="model">Empty GuestRatings object.</param>
        /// <param name="contentType">Indicates the site part that utilizes the rating function 
        /// and from which the empty GuestRatings object is sent.</param>       
        /// <returns>Partial view populatated with the GuestRatings object.</returns>      
        [ChildActionOnly]      
        public ActionResult GuestRatings(GuestRatingsModel model, string contentType)
        {   
            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language ("SV" or "EN")
            var currentLanguage = helper.GetDictionaryValue("Language");

            // Get current page properties 
            model.Title = CurrentPage.Name;          
            model.Id = CurrentPage.Id;
            model.ContentType = CurrentPage.DocumentTypeAlias; 

            // Get rating values (percentage, average and voters)
            model = GetRatingValues(model, CurrentPage.Id, currentLanguage);

            // Database error
            if (model.NumOfVotersMessage == "error")
            {
                model.NumOfVotersMessage = Umbraco.GetDictionaryValue("RatingError");
            }
            else
            {
                model.AverageValueMessage = model.AverageValue + " / 5,0";
                                
                //Create the voters message due to content type, language and previous ratings/no previous ratings
                model.NumOfVotersMessage = CreateVotersMessage(model.NumOfVoters, contentType, helper);
            }
            
            // Construct title for the guest ratings section
            model.TitleTerm = String.Format(helper.GetDictionaryValue("GuestRatingsSectionTitle"), helper.GetDictionaryValue("GuestRatings"), model.Title);  

            return PartialView("GuestRatings", model);           
        }



        ///<summary>
        /// Fetches guest ratings information and sends it to the client, typically used by
        /// the "See result" link.
        /// </summary>
        /// <param name="nodeId">Id of current Content node.</param>
        /// <param name="sitePart">Indicates the site part using the ratings function (areas, accommodations, 
        /// activities).</param>
        /// <param name="language">Current language ("SV" or "EN").</param>
        /// <returns>A GuestRatings object.</returns>             
        [HttpGet]
        public JsonResult GuestRatingsJson(int nodeId, string sitePart, string language)
        {              

            // The culture of an Ajax request thread is neutral and therefore we need to set it 
            // explicitly to be able to get the correct Umbraco context.
            if (Request.IsAjaxRequest())
            {
                UmbracoHelperExtensions.EnsureCulture(Thread.CurrentThread, language);
            }

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Initiate the return model
            GuestRatingsModel model = new GuestRatingsModel();

            // Get rating values (percentage, average and voters)
            model = GetRatingValues(model, nodeId, language);           

            if (model.NumOfVotersMessage == "error")
            {
                model.NumOfVotersMessage = helper.GetDictionaryValue("RatingError");
            }
            else
            {
                model.AverageValueMessage = model.AverageValue + " / 5,0";

                // Create the voters message due to site part and language  
                model.NumOfVotersMessage = CreateVotersMessage(model.NumOfVoters, sitePart, helper);
            }

            // Return model as Json
            return Json(new { model = model }, JsonRequestBehavior.AllowGet);            
        }




        ///<summary>
        /// Fetches the rating values for a specific (accommodation) page.
        /// </summary>
        /// <param name="model">Empty GuestRatings object.</param>
        /// <param name="nodeId">The id of current node.</param>
        /// <returns>The populated GuestRatings object.</returns> 
        public static GuestRatingsModel GetRatingValues(GuestRatingsModel model, int nodeId, string language)
        {
            // Create local variables for this static method
            GuestRatingsSurfaceController gr = new GuestRatingsSurfaceController();
            SqlConnection conn = gr.conn;
            String connectionString = gr.ConnectionString;

            // Set some initiate values
            model.Percentage = 0;
            model.NumOfVoters = 0;
            model.AverageValue = "0,0";
            model.NumOfVotersMessage = String.Empty;           
            Int32 voteValue = 0;
            Int32 voteNr = 0;
            Int32 nodeId1 = 0;
            Int32 nodeId2 = 0;
            bool connected = false;

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get id of the guest ratings config node
            int nodeIdGR = helper.TypedContentAtXPath("//" + MyConstants.guestRatingsConfigDocTypeAlias).First().Id;

            // Get content based on node id
            var content = helper.TypedContent(nodeIdGR);

            // Get the collection of connections between Swedish and English accommodation nodes (Nested Content datatype)
            var connectionsList = content.GetPropertyValue<IEnumerable<IPublishedContent>>(MyConstants.cabinNodesConnectionsPropertyAlias);

            // Make sure at least one connection exists
            if (connectionsList != null)
            {
                // Initiate dynamic option variables
                dynamic pickedOptionA = null;
                dynamic pickedOptionB = null;

                // Iterate over the connections
                foreach (var connection in connectionsList)
                {
                    // Get option from dropdown list due to current language
                    switch (language)
                    {
                        case "SV":
                            pickedOptionA = connection.GetPropertyValue(MyConstants.cabinNodesSwedishPropertyAlias);
                            break;
                        case "EN":
                            pickedOptionA = connection.GetPropertyValue(MyConstants.cabinNodesEnglishPropertyAlias);
                            break;
                    }                    

                    // Make sure an accommodation was picked
                    if (pickedOptionA.PickedKeys != null && pickedOptionA.PickedKeys.Length > 0)
                    {
                        // Look for the current accommodation page id
                        if (Convert.ToInt32(pickedOptionA.PickedKeys[0]) == nodeId)
                        {
                            // Save the first node id to a variable
                            nodeId1 = Convert.ToInt32(pickedOptionA.PickedKeys[0]);

                            // Since we've got a hit so get the corresponding language node, if any
                            switch (language)
                            {
                                case "SV":
                                    pickedOptionB = connection.GetPropertyValue(MyConstants.cabinNodesEnglishPropertyAlias);
                                    break;
                                case "EN":
                                    pickedOptionB = connection.GetPropertyValue(MyConstants.cabinNodesSwedishPropertyAlias);
                                    break;
                            }                           

                            // Make sure an accommodation was picked
                            if (pickedOptionB.PickedKeys != null && pickedOptionB.PickedKeys.Length > 0)
                            {
                                // Save the second node id to a variable
                                nodeId2 = Convert.ToInt32(pickedOptionB.PickedKeys[0]);
                            }
                        }                       
                    }
                    // We've found a valid connection based on current node id so break out of loop 
                    if (nodeId1 > 0 && nodeId2 > 0)
                    {
                        connected = true;
                        break;
                    }
                }
            }

            try
            {
                // Create a new SQL Connection object
                using (conn = new SqlConnection(connectionString))
                {
                    // Open the database connection
                    conn.Open();

                    // Current node id is connected to another accommodation node 
                    if (connected)
                    {                        
                        // Use current node to get vote values
                        var tuple1 = GetVotesById(nodeId1, conn);  
                        voteValue = tuple1.Item1;
                        voteNr = tuple1.Item2;                      
                       
                        // Use corresponding node to get additional vote values
                        var tuple2 = GetVotesById(nodeId2, conn);  
                        voteValue += tuple2.Item1;
                        voteNr += tuple2.Item2;
                    }

                    // Current node has not yet been connected to another node 
                    else 
                    {
                        // Use the original node, that was sent to the method, to get vote values
                        var tuple = GetVotesById(nodeId, conn); 
                        voteValue = tuple.Item1;
                        voteNr = tuple.Item2;
                    }

                    // Close the database connection
                    conn.Close();
                }
            }
            catch
            {
                model.NumOfVotersMessage = "error";
            }
            finally
            {
                if (model.NumOfVotersMessage == String.Empty)
                {  

                    // Initiate variables
                    int score = 0;
                    double scoreNotRounded = 0.0;

                    // Make sure ratings exists for current node.          
                    if (voteNr > 0)
                    {
                        double average = Convert.ToDouble(voteValue) / Convert.ToDouble(voteNr);
                        score = Convert.ToInt16(Math.Round(average, 0));
                        scoreNotRounded = Math.Round(average, 1);

                        model.Percentage = Convert.ToInt32(Math.Round((double)(scoreNotRounded * 20.0)));
                        string averageValue = scoreNotRounded.ToString();

                        // Add a decimal if there's just an integer
                        if (averageValue.Length == 1)
                        {
                            averageValue += ",0";
                        }
                        model.AverageValue = averageValue;
                        model.NumOfVoters = voteNr;
                    }
                }
            }
            return model;
        }




        protected static Tuple<int, int> GetVotesById(int nodeId, SqlConnection conn)
        {
            // Initiate local variables
            Int32 voteValue = 0;
            Int32 voteNr = 0;
            string sqlStatement = string.Empty;

            // Create the sql statement
            sqlStatement = "SELECT voteValue, voteNr FROM xtraStarRating WHERE uniqueId = @id";

            // Create a new SQL Command
            using (SqlCommand cmd = new SqlCommand(sqlStatement, conn))
            {
                // Parameterize
                cmd.Parameters.Add("@id", SqlDbType.NVarChar, 100);
                cmd.Parameters["@id"].Value = nodeId;

                // Execute the command
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        dr.Read();

                        // Get values
                        voteValue = Convert.ToInt32(dr["voteValue"]);
                        voteNr = Convert.ToInt32(dr["voteNr"]);
                    }
                    // Close the data reader
                    dr.Close();
                }
            }
            return new Tuple<int, int>(voteValue, voteNr); 
        }

              

        ///<summary>
        /// Updates the star ratings bar.
        /// </summary>
        /// <param name="nodeId">The id of current Content node.</param>
        /// <param name="rating">The rating (1-5) given by the visitor/guest.</param>
        /// <param name="sitePart">Indicates the site part that's utilizing the rating function and
        /// from which the GuestRatings object is sent (areas, accommodations, activities).</param> 
        /// <param name="language">Current language ("SV" or "EN").</param>
        /// <returns>A boolean indicating whether the update operation was succesful.</returns>      
        [HttpGet]
        public JsonResult UpdateBar(int nodeId, int rating, string sitePart, string language)
        {
            // Initiate return boolean           
            bool result = false;

            // Update the star rating bar
            result = Update(nodeId.ToString(), rating, sitePart, language);

            return Json(new { result = result }, JsonRequestBehavior.AllowGet);
        }



        ///<summary>
        /// Constructs the information text about voters.
        /// </summary>
        /// <param name="numOfVoters">Current number of voters. Might be zero when called from 
        /// the GuestRatings method.</param>
        /// <returns>A message string.</returns>    
        protected string CreateVotersMessage(int numOfVoters, string contentType, UmbracoHelper helper)
        {
            // Initiate return value
            string votersMsg = String.Empty;                  

            // Previous ratings exists
            if (numOfVoters > 0)
            {
                string guestsTerm = String.Empty;
                if (numOfVoters == 1)
                {
                    guestsTerm = helper.GetDictionaryValue("Guest");
                }
                else
                {
                    guestsTerm = helper.GetDictionaryValue("Guests");
                }

                switch (contentType)
                {
                    case MyConstants.AccommodationPageDocTypeAlias:
                        votersMsg = String.Format(helper.GetDictionaryValue("VotersMessageAccommodations"), numOfVoters, guestsTerm);
                        break;
                    case MyConstants.AreaPageDocTypeAlias:
                        votersMsg = String.Format(helper.GetDictionaryValue("VotersMessageAreas"), numOfVoters, guestsTerm);
                        break;
                    case MyConstants.GeneralSubPageDocTypeAlias:
                        votersMsg = String.Format(helper.GetDictionaryValue("VotersMessageActivities"), numOfVoters, guestsTerm);
                        break;
                }
            }
            // No previous ratings
            else
            {
                string welcomeToBeFirstMsg = helper.GetDictionaryValue("WelcomeToBeFirstMessage");
                switch (contentType)
                {
                    case MyConstants.AccommodationPageDocTypeAlias:
                        votersMsg = String.Format(helper.GetDictionaryValue("InitMessageAccommodations"), welcomeToBeFirstMsg);
                        break;
                    case MyConstants.AreaPageDocTypeAlias:
                        votersMsg = String.Format(helper.GetDictionaryValue("InitMessageAreas"), welcomeToBeFirstMsg);
                        break;
                    case MyConstants.GeneralSubPageDocTypeAlias:
                        votersMsg = String.Format(helper.GetDictionaryValue("InitMessageActivities"), welcomeToBeFirstMsg);
                        break;
                }
            }

            return votersMsg;
        }



        ///<summary>
        /// Updates the star ratings bar.
        /// </summary>
        /// <param name="nodeId">The id of current Content node.</param>
        /// <param name="rating">The rating (1-5) given by the visitor/guest.</param>
        /// <param name="sitePart">Indicates the site part using the rating function 
        /// (= AccommodationPage).</param> 
        /// <param name="language">Current language ("SV" or "EN").</param>
        /// <returns>A boolean indicating whether the database operation was succesful.</returns>    
        public static bool Update(string nodeId, int rating, string sitePart, string language)
        {
            // Create local variables for this static method
            GuestRatingsSurfaceController gr = new GuestRatingsSurfaceController();
            SqlConnection conn = gr.conn;
            String connectionString = gr.ConnectionString;
            String sqlStatement = gr.SqlStatement;
         
            // Initiate variables
            bool rated = false;
            Int32 voteValue = 0;
            Int32 voteNr = 0;

            try
            {
                // Create a new SQL Connection object
                using (conn = new SqlConnection(connectionString))
                {
                    // Open the database connection
                    conn.Open();

                    //*** Check whether node has been rated before ***

                    // Create the sql statement
                    sqlStatement = "SELECT voteValue, voteNr FROM xtraStarRating WHERE uniqueId = @id";

                    // Create a new SQL Command
                    using (SqlCommand cmd = new SqlCommand(sqlStatement, conn))
                    {
                        // Parameterize
                        cmd.Parameters.Add("@id", SqlDbType.NVarChar, 100);
                        cmd.Parameters["@id"].Value = nodeId;

                        // Execute the command
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                // Set flag
                                rated = true;

                                dr.Read();

                                // Get values
                                voteValue = Convert.ToInt32(dr["voteValue"]);
                                voteNr = Convert.ToInt32(dr["voteNr"]);
                            }
                            // Close the data reader
                            dr.Close();
                        }

                        switch (rated)
                        {
                            //*** Node is rated for the first time so let's create an initial record ***

                            case false:

                                // Create the sql statement
                                sqlStatement = "INSERT INTO xtraStarRating(uniqueId, voteValue, voteNr, sitePart, language) "
                                    + "VALUES(@UniqueId, @voteValue, @voteNr, @SitePart, @Language)";

                                // Change the SQL Command 
                                cmd.CommandText = sqlStatement;

                                // Parameterize
                                cmd.Parameters.Add("@UniqueId", SqlDbType.VarChar, 100);
                                cmd.Parameters["@UniqueId"].Value = nodeId;
                                cmd.Parameters.Add("@voteValue", SqlDbType.SmallInt);
                                cmd.Parameters["@voteValue"].Value = rating;
                                cmd.Parameters.Add("@voteNr", SqlDbType.SmallInt);
                                cmd.Parameters["@voteNr"].Value = 1;
                                cmd.Parameters.Add("@SitePart", SqlDbType.VarChar, 20);
                                cmd.Parameters["@SitePart"].Value = sitePart;
                                cmd.Parameters.Add("@Language", SqlDbType.Char, 2);
                                cmd.Parameters["@Language"].Value = language;

                                // Execute the command
                                cmd.ExecuteNonQuery();

                                break;

                            //*** Node is previously rated so let's update the number of votes and the total vote value ***

                            case true:

                                sqlStatement = "UPDATE xtraStarRating SET voteValue = @voteValue, voteNr = @voteNr WHERE uniqueId = @UniqueId";

                                // Change the SQL Command 
                                cmd.CommandText = sqlStatement;

                                // Parameterize                          
                                cmd.Parameters.Add("@UniqueId", SqlDbType.VarChar, 100);
                                cmd.Parameters["@UniqueId"].Value = nodeId;
                                cmd.Parameters.Add("@voteValue", SqlDbType.SmallInt);
                                cmd.Parameters["@voteValue"].Value = voteValue + rating;
                                cmd.Parameters.Add("@voteNr", SqlDbType.SmallInt);
                                cmd.Parameters["@voteNr"].Value = voteNr + 1;

                                // Execute the command
                                cmd.ExecuteNonQuery();

                                break;
                        }
                    }

                    // Close the database connection
                    conn.Close();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }







        /* NOT IN USE */
        ///<summary>
        /// Fetches the top list for a specific site part and language.
        /// </summary>
        /// <param name="sitePart">Current site part ("areas", "accommodations" or "activities").</param>
        /// <param name="language">Current language ("SV" or "EN").</param>
        /// <returns>A list of (partially populated) Top List objects.</returns> 
        public static List<TopListModel> CreateTopList(string sitePart, string language)
        {
            // Create local variables for this static method
            GuestRatingsSurfaceController gr = new GuestRatingsSurfaceController();
            SqlConnection conn = gr.conn;
            String connectionString = gr.ConnectionString;
            String sqlStatement = gr.SqlStatement;

            // Initiate the return value
            List<TopListModel> tempList = new List<TopListModel>(); 
           
            try
            {
                // Create a new SQL Connection object
                using (conn = new SqlConnection(connectionString))
                {
                    // Open the database connection
                    conn.Open();

                    // Create the sql statement
                   sqlStatement = "SELECT TOP 5 uniqueId 'NodeId', CAST(voteValue/CONVERT(decimal,voteNr) As dec(12,1)) 'Rating', voteNr 'Votes' "
                    + "FROM xtraStarRating "
                    + "WHERE sitePart = @SitePart "
                    + "AND language = @Language "
                    + "ORDER BY voteValue/CONVERT(decimal,voteNr) DESC, voteValue DESC";
              
                    // Create a new SQL Command
                    using (SqlCommand cmd = new SqlCommand(sqlStatement, conn))
                    {
                        // Parameterize
                        cmd.Parameters.Add("@SitePart", SqlDbType.VarChar, 20);
                        cmd.Parameters["@SitePart"].Value = sitePart;
                        cmd.Parameters.Add("@Language", SqlDbType.Char, 2);
                        cmd.Parameters["@Language"].Value = language;

                        // Execute the command
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    // Instanciate a TopList object
                                    TopListModel obj = new TopListModel();

                                    obj.NodeId = Convert.ToInt32(dr["NodeId"]);
                                    obj.AverageValue = dr["Rating"].ToString();
                                    obj.Votes = Convert.ToInt32(dr["Votes"]);

                                    // Add object to temp list
                                    tempList.Add(obj);
                                }                                                           
                            }
                            // Close the data reader
                            dr.Close();                           
                        }
                    }
                    // Close the database connection
                    conn.Close();
                }
            }
            catch
            {
                // Return a nullified list should there be a database error
                tempList = null;
                return tempList;
            }          
            return tempList;
        }



        /* NOT IN USE */
        ///<summary>
        /// Fetches top list information and sends it to the TopList partial view.
        /// </summary>
        /// <param name="model">Empty TopList object.</param>
        /// <param name="sitePart">Indicates the site part that wants to display the top list and
        /// from which the Top Lists object is sent (areas, accommodations, activities).</param>       
        /// <returns>The GuestRatings object.</returns>      
        [ChildActionOnly]
        public ActionResult TopList(TopListModel model, string sitePart)
        {
            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language ("SV" or "EN")
            var currentLanguage = helper.GetDictionaryValue("Language");

            // Generate the actual list
            List<TopListModel> resultList = CreateTopList(sitePart, currentLanguage);

            // Initiate flags
            bool resultExist = false;
            bool publishedExist = false;

            // Make sure a result exist           
            if (resultList != null && resultList.Count > 0)
            {
                // Set flag
                resultExist = true;

                // Add the title property for the list
                model.Title = helper.GetDictionaryValue("TopListTitle");

                // Initiate the final list
                List<TopListModel> finalList = new List<TopListModel>();

                // Initiate counter
                int counter = 0;

                // Make some manipulations to the list content
                foreach (var item in resultList)
                {
                    // Get a reference to current Content node
                    var content = helper.TypedContent(item.NodeId);

                    // Make sure the node is published
                    if (content != null)
                    {
                        // Enumerate counter
                        counter += 1;

                        // Set list number
                        item.Number = counter + ". ";

                        // Get the node's name
                        item.Title = content.Name;

                        // Create Url by node id
                        item.Url = helper.NiceUrl(item.NodeId);

                        // Create voters information
                        string votesTerm = helper.GetDictionaryValue("Votes");
                        if (item.Votes == 1)
                        {
                            votesTerm = helper.GetDictionaryValue("Vote");
                        }
                        item.VotesMessage = "(" + item.Votes + " " + votesTerm + ")";

                        // Add item to the final list
                        finalList.Add(item);
                    }
                }

                // Make sure the final list contains at least one published node               
                if (finalList.Count > 0)
                {
                    // Set flag
                    publishedExist = true;

                    // Then add the final list to the TopList return model
                    model.TopListItemsCollection = finalList;
                }
            }

            // Should there be no votes at all for current site part and/or current language
            // or should there be a result but without at least one published node
            if (resultExist == false || publishedExist == false)
            {
                // ...add an empty TopListModel list to the return model.
                model.TopListItemsCollection = new List<TopListModel>();
            }

            // Send the Top List model to the TopList partial view
            return PartialView("TopList", model);
        }
    }
}