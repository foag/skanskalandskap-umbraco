﻿using SkanskaLandskap15.Models.Masters;


namespace SkanskaLandskap15.Models
{
    public class SearchResultsPageModel : MasterModel
    {
        public string TitleTerm { get; set; }
        public bool AccessWithoutSearch { get; set; }   
    }
}