﻿angular.module("umbraco")
.controller("ListPicker.EditorController", function ($scope, $timeout) {

    //$scope.model.value = '';

    switch ($scope.model.config.values.type) {

        case 'checkbox':

                angular.forEach($scope.model.config.values.items, function (item, index) {

                    if (!$scope.model.value) {
                        $scope.model.value = [];
                        if (item.checked) {
                            $scope.model.value.push(item.value);
                        }
                    } else {
                        item.checked = $scope.model.value.indexOf(item.value) > -1;
                    }

                });

            break;

        default:

            if (!$scope.model.value) {
                angular.forEach($scope.model.config.values.items, function (item, index) {

                    if (item.checked) {
                        $scope.model.value = item.value;
                    }
                });
            }

            break;

    }

    $scope.update = function () {
        $scope.model.value = [];
        angular.forEach($scope.model.config.values.items, function (item, index) {
            if (item.checked) {
                $scope.model.value.push(item.value);
            }
        });
    }

});