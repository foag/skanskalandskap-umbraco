﻿using System.Web;

namespace SkanskaLandskap15.Models.Shared
{
    public class ContactModel
    {
        public string Name { get; set; }
        public string Function { get; set; }
        public string WorkAssignments { get; set; }       
        public string Organization { get; set; }
        public string Email { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public dynamic ContactPortrait { get; set; }
        public IHtmlString ContactPersonsTips { get; set; }
        public string TipsTitle{ get; set; }       
    }
}