﻿using System.Collections.Generic;

namespace SkanskaLandskap15.Models.Shared
{
    public class ServiceBusStopsListModel
    {       
        public List<ServiceBusStopModel> BusStopsCollection { get; set; }  
    }
}
