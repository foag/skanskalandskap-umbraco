﻿using System.Web.Mvc;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Models;
using Umbraco.Web;




namespace SkanskaLandskap15.Controllers
{
    public class ThingsToDoLandingPageController : InteriorMasterController
    {

        public ActionResult ThingsToDoLandingPage()
        {
            // Initiate the return model
            var model = new ThingsToDoLandingPageModel();

            // Get a string of comma-separated sub page and Calendar node id:s
            model.SelectedNodes = CurrentPage.GetPropertyValue<string>(MyConstants.tipsForKidsLandingPagePropertyAlias);

            return View(model);
        }
      
    }
}


