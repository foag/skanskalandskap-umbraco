/* global google:true */

'use strict';

var GMapsApp = function (settings) {
  this.init(settings);
};

;(function ($) {
  /**
   * Google Maps integration
   */
  GMapsApp.prototype = {

    /**
     * [constructor description]
     * @method constructor
     * @param  {[type]}    id [description]
     * @return {[type]}       [description]
     */
    init : function (settings) {
      this.settings = settings;

      this.mapHandler();

      if (this.settings.markers) {
        this.markers = [];
        // Init Overview Map
        if (this.settings.url === 'strovomraden' || this.settings.url === 'recreation-areas') {
          this.activeFilters = [];
          this.markerOverviewHandler();
        // Init Area Map
        } else {
          this.markerAreaHandler();
        }
      }

      this.layerHandler();

      this.mapLoadedEvent();
    },

    /**
     * [mapHandler description]
     * @method mapHandler
     * @return {[type]}   [description]
     */
    mapHandler : function () {
      var _ = this;

      this.map = new google.maps.Map(document.getElementById(_.settings.id), {
        zoom: (window.innerWidth < 1100) ? 8 : _.settings.zoom,
        center: _.settings.position,
        // mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeId: 'OSM',
        scrollwheel: false,
        draggable: true,
        styles: [{ 'stylers': [{ 'saturation': -100 }] }],
        disableDefaultUI: true,
        zoomControl: true
        // streetViewControl: true
      });

      // Define OSM map type pointing at the OpenStreetMap tile server
      _.map.mapTypes.set('OSM', new google.maps.ImageMapType({
          getTileUrl : function (coord, zoom) {
            return 'http://a.tile.thunderforest.com/transport/' + zoom + '/' + coord.x + '/' + coord.y + '.png';
          },
          tileSize: new google.maps.Size(256, 256),
          name: 'OpenStreetMap',
          maxZoom: 18
      }));

      var center = _.map.getCenter();
      google.maps.event.addDomListener(window, 'resize', function() {
        _.map.setCenter(center);
        _.map.setZoom((window.innerWidth < 1100) ? 8 : _.settings.zoom);
      });

      return this;
    },

    /**
     * [locationHandler description]
     * @method locationHandler
     * @return {[type]}        [description]
     */
    markerOverviewHandler : function () {
      var _ = this;

      _.settings.markers.forEach(function (el, i) {
        _.markers[i] = {};

        _.markers[i].show = true;

        _.markers[i].id = el.NodeId;

        _.markers[i].url = el.UrlName;

        _.markers[i].marker = new google.maps.Marker({
          position: { lat: parseFloat(el.Latitude, 10), lng: parseFloat(el.Longitude, 10) },
          map: _.map,
          title: el.Title,
          icon: {
            url: '/assets/images/area-marker.png',
            size: new google.maps.Size(25, 40),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(13, 40)
            // path: 'M22-48h-44v43h16l6 5 6-5h16z',
            // fillColor: '#25b8ca',
            // fillOpacity: 1,
            // strokeColor: '#24717B',
            // strokeWeight: 1
          }
        });

        // _.markers[i].label = _.markerLabel({
        //   map: _.map,
        //   marker: _.markers[i].marker,
        //   text: '<svg style="position: absolute; left: 50%; top: 50%; margin: -18px 0 0 -15px; width: 30px;" fill="#fff" height="30px" width="30px">' +
        //     '<use xlink:href="/assets/images/area.svg#Lager_1"></use></svg>' +
        //     '<svg width="44px" height="48px" viewBox="0 0 44 48"><g><path d="M44,0 L0,0 L0,43 L16,43 L22,48 L28,43 L44,43 L44,0 Z" fill="#25B8CA" stroke="#24717B" stroke-width="1"></path></g></svg>',
        //   'zIndex': i
        // });
        // _.markers[i].label.bindTo('position', _.markers[i].marker, 'position');

        _.markers[i].infowindow = new google.maps.InfoWindow({
          content: el.Title
        });

        _.markers[i].marker.addListener('click', function () {
          window.location = window.location.pathname + '/' + el.UrlName;
        });

        var scroll = 0;
        _.markers[i].marker.addListener('mouseover', function () {
          _.markers[i].infowindow.open(_.map, _.markers[i].marker);

          if (_._isFunction(_.settings.markerCallback)) {
            _.settings.markerCallback({
              'type' : 'mouseover',
              'id' : _.markers[i].id
            });
          }
        });

        _.markers[i].marker.addListener('mouseout', function () {
          _.markers[i].infowindow.close(_.map, _.markers[i].marker);

          if (_._isFunction(_.settings.markerCallback.hover)) {
            _.settings.markerCallback.hover({
              'type' : 'mouseout',
              'id' : _.markers[i].id
            });
          }
        });

        _.markers[i].filterIds = el.AreaActivities.split(',').map(Number);
      });

      return this;
    },

    /**
     * [locationHandler description]
     * @method locationHandler
     * @return {[type]}        [description]
     */
    markerAreaHandler : function () {
      var _ = this,

          prevInfoWindow = false,
          z = 0;

      _.settings.markers.forEach(function (el, i) {
        var busStopService = el.BusStopService !== null;

        if (el.BusStopService || el.StandardService) {
          el = el.BusStopService || el.StandardService;
        }

        if (!el.MapMarker) {
          if (_._isFunction(_.settings.markerCallback.error)) {
            _.settings.markerCallback.error(busStopService ? "Right now we're having trouble fetching data for busstops." : 'There was an issue fetching some markers.');
          }
        } else {
          _.markers[i] = {};

          _.markers[i].show = true;

          _.markers[i].id = el.NodeId;

          _.markers[i].filterId = el.ServiceTypeNodeId;

          _.markers[i].marker = new google.maps.Marker({
            position: {
              lat: parseFloat(el.MapMarker.Latitude, 10),
              lng: parseFloat(el.MapMarker.Longitude, 10)
            },
            map: _.map,
            title: el.MapMarker.Title,
            icon: {
              path: 'M22-48h-44v43h16l6 5 6-5h16z',
              fillColor: '#25b8ca',
              fillOpacity: 0,
              // strokeColor: '#24717B',
              strokeWeight: 0
            },
            zIndex: i
          });

          _.markers[i].label = _.markerLabel({
            map: _.map,
            marker: _.markers[i].marker,
            text: '<svg style="position: absolute; left: 50%; top: 50%; margin: -15px 0 0 -15px; width: 30px;" fill="#fff" height="24px" width="24px">' +
              '<use xlink:href="' + el.MapMarker.Icon + '#Lager_1"></use></svg>' +
              '<svg width="44px" height="48px" viewBox="0 0 44 48"><g><path d="M44,0 L0,0 L0,43 L16,43 L22,48 L28,43 L44,43 L44,0 Z" fill="#25B8CA" stroke-width="0"></path></g></svg>',
            'zIndex': i
          });
          _.markers[i].label.bindTo('position', _.markers[i].marker, 'position');

          // Add infoWindow with title
          _.markers[i].infoWindow = new google.maps.InfoWindow({
            content: el.Title
          });

          // Add additional info in the infoWindow
          _.markers[i].infoWindowLoaded = false;
          if (!busStopService) {
            if (_._isFunction(_.settings.markerCallback.standardService)) {
              _.settings.markerCallback.standardService({
                'infoWindow' : _.markers[i].infoWindow,
                'data' : {
                  'title' : el.Title,
                  'intro' : el.MapMarker.Intro
                }
              });
              _.markers[i].infoWindowLoaded = true;
            }
          }

          _.markers[i].marker.addListener('click', function (e) {
            // Add infoWindow with ajax loaded info for busstops (only on click)
            if (busStopService && !_.markers[i].infoWindowLoaded) {
              if (_._isFunction(_.settings.markerCallback.busStopService)) {
                _.markers[i].infoWindow.setContent('<p>Laddar...</p>');
                _.settings.markerCallback.busStopService({
                  'infoWindow' : _.markers[i].infoWindow,
                  'data' : {
                    'language' : Global.language,
                    'NodeId' : _.markers[i].id
                  }
                });
                _.markers[i].infoWindowLoaded = true;
              }
            }

            if (prevInfoWindow) {
              prevInfoWindow.close();
            }

            prevInfoWindow = _.markers[i].infoWindow;
            _.markers[i].infoWindow.open(_.map, _.markers[i].marker);
          });
        }

      });

      return this;
    },

    /**
     * [markerLabel description]
     * @method markerLabel
     * @return {[type]}    [description]
     */
    markerLabel : function (settings) {
      // Marker Label Overlay
      var MarkerLabel = function (options) {
        var self = this;
        this.setValues(options);

        // Create the label container
        this.div = document.createElement('div');
        this.div.className = 'map-icon-label';

        // Trigger the marker click handler if clicking on the label
        google.maps.event.addDomListener(this.div, 'click', function (e) {
          (e.stopPropagation) && e.stopPropagation();
          google.maps.event.trigger(self.marker, 'click');
        });
      };

      // Create MarkerLabel Object
      MarkerLabel.prototype = new google.maps.OverlayView;

      // Marker Label onAdd
      MarkerLabel.prototype.onAdd = function() {
        var pane = this.getPanes().overlayImage.appendChild(this.div);
        var self = this;

        this.listeners = [
          google.maps.event.addListener(this, 'position_changed', function() { self.draw(); }),
          google.maps.event.addListener(this, 'text_changed', function() { self.draw(); }),
          google.maps.event.addListener(this, 'zindex_changed', function() { self.draw(); })
        ];
      };

      // Marker Label onRemove
      MarkerLabel.prototype.onRemove = function() {
        this.div.parentNode.removeChild(this.div);

        for (var i = 0, I = this.listeners.length; i < I; ++i) {
          google.maps.event.removeListener(this.listeners[i]);
        }
      };

      // Implement draw
      MarkerLabel.prototype.draw = function() {
        var projection = this.getProjection();
        var position = projection.fromLatLngToDivPixel(this.get('position'));
        var div = this.div;

        this.div.innerHTML = this.get('text').toString();

        div.style.zIndex = this.get('zIndex'); // Allow label to overlay marker
        div.style.position = 'absolute';
        div.style.display = 'block';
        div.style.width = '44px';
        div.style.left = (position.x - (div.offsetWidth / 2)) + 'px';
        div.style.top = (position.y - div.offsetHeight) + 'px';
      };

      return new MarkerLabel(settings);
    },

    /**
     * Shows or Hides the markers on the map
     * @method showHideMarkers
     * @return {[type]}    [description]
     */
    showHideMarkers : function () {
      var _ = this;

      _.markers.forEach(function (marker, i) {
        if (marker.show) {
          _.markers[i].marker.setMap(_.map);
          if (_.markers[i].label) {
            _.markers[i].label.setMap(_.map);
          }
        } else {
          _.markers[i].marker.setMap(null);
          if (_.markers[i].label) {
            _.markers[i].label.setMap(null);
          }
        }
      });

      return this;
    },

    /**
     * Sets marker state depending on previuos state, uses marker.id
     * @method toggleMarkersByFilterId
     * @param  int        id  The Filter ID
     * @return object         Class object
     */
    toggleMarkersByFilterId : function (id) {
      var _ = this;

      // Toggle if a marker should be shown or not
      _.markers.forEach(function (marker, i) {
        if (marker.filterId === id) {
          _.markers[i].show = !_.markers[i].show;
        }
      });

      // Show and Hide markers
      _.showHideMarkers();

      return this;
    },

    /**
     * [toggleMarkersByFilterIds description]
     * @method toggleMarkersByFilterIds
     * @return {[type]}           [description]
     */
    toggleMarkersByFilterIds : function (filters) {
      var _ = this;

      // Set all markers to show
      _._showAllMarkers();

      // Check if a marker should be visible based on active filters
      _.markers.forEach(function (marker, i) {
        filters.forEach(function (id) {
          if (!_.markers[i].show) {
            return;
          }
          _.markers[i].show = (marker.filterIds.indexOf(id) !== -1) ? true : false;
        });
      });

      // Update this.activeFilters
      _.checkActiveFilters(filters);

      // Show and Hide markers
      _.showHideMarkers();

      return this;
    },

    /**
     * Resets all markers to state show = true and shows them
     * @method showAllMarkers
     * @return object     Class object
     */
    showAllMarkers : function () {
      var _ = this;

      // Set all markers to show
      _._showAllMarkers();

      // Show all markers
      _.showHideMarkers();

      return this;
    },

    /**
     * Resets all markers to state show = false and hides them
     * @method _hideAllMarkers
     * @return object     Class object
     */
    hideAllMarkers : function () {
      var _ = this;

      // Set all markers to show
      _._hideAllMarkers();

      // Show all markers
      _.showHideMarkers();

      return this;
    },

    /**
     * Resets all markers to state show = true
     * @method _showAllMarkers
     * @return object     Class object
     */
    _showAllMarkers : function () {
      var _ = this;

      // Set all markers to show
      _.markers.forEach(function (marker, i) {
        _.markers[i].show = true;
      });

      return this;
    },

    /**
     * Resets all markers to state show = true
     * @method _hideAllMarkers
     * @return object     Class object
     */
    _hideAllMarkers : function () {
      var _ = this;

      // Set all markers to show
      _.markers.forEach(function (marker, i) {
        _.markers[i].show = false;
      });

      return this;
    },

    /**
     * Updates this.activeFilters with currently activated filters
     * and counts items tagged with each filter
     * @method checkActiveFilters
     * @param  array           filters  An array of active filters
     * @return object                   Class object
     */
    checkActiveFilters : function (filters) {
      var _ = this;

      if (!filters.length) {
        _.resetFilters();
        return this;
      }

      // Find all active filters in filters variable
      var activeFilters = [];
      _.markers.forEach(function (marker) {
        filters.forEach(function (j) {
          if (marker.filterIds.indexOf(j) !== -1) {
            activeFilters = _._arrayUnique(activeFilters.concat(marker.filterIds));
          }
        });
      });

      // Set filter tagged number to zero
      _.activeFilters.forEach(function (filter, i) {
        _.activeFilters[i] = 0;
      });

      // Find markers tagged with filter and count it
      activeFilters.forEach(function (filter) {
        _.markers.forEach(function (marker) {
          if (marker.filterIds.indexOf(filter) !== -1 && marker.show && _.activeFilters[filter] !== undefined) {
            _.activeFilters[filter]++;
          }
        });
      });

      return this;
    },

    /**
     * Resets this.activeFilters to default the count
     * @method resetFilters
     */
    resetFilters : function () {
      var _ = this;

      // Reset filters
      _.activeFilters = [];

      // Add available filters
      _.settings.activitytypes.forEach(function (activity) {
        if (_.activeFilters[activity.NodeId] === undefined) {
          _.activeFilters[activity.NodeId] = 0;
        }
      });

      // Update count on filters
      _.markers.forEach(function (marker) {
        marker.filterIds.forEach(function (id) {
          if (_.activeFilters[id] !== undefined) {
            _.activeFilters[id]++;
          }
        });
      });

      return this;
    },

    /**
     * [layerHandler description]
     * @method layerHandler
     * @return {[type]}    [description]
     */
    layerHandler : function () {
      var _ = this;

      var borderLayer = function (name) {
        return new google.maps.KmlLayer({
            url: 'http://www.skanskalandskap.se/mapdata/borders/' + name + '.kml',
            preserveViewport: true,
            suppressInfoWindows: true,
            clickable: false,
            map: _.map
        });
      };

      // Overview
      if (_.settings.url === 'strovomraden') {
        _.markers.forEach(function (marker) {
          borderLayer(marker.url);
        });
      // Area
      } else {
        borderLayer(_.settings.url);
      }

      return this;
    },

    /**
     * Triggers an event on window when the map is fully loaded
     * @method mapLoadedEvent
     * @return {[type]}       [description]
     */
    mapLoadedEvent : function () {
      var event;

      if (document.createEvent) {
        event = document.createEvent('HTMLEvents');
        event.initEvent('mapLoaded', true, true);
      } else {
        event = document.createEventObject();
        event.eventType = 'mapLoaded';
      }

      event.eventName = 'mapLoaded';

      google.maps.event.addListenerOnce(this.map, 'idle', function () {
        if (document.createEvent) {
          window.dispatchEvent(event);
        } else {
          window.fireEvent('on' + event.eventType, event);
        }
      });

      return this;
    },

    /**
     * Helper function to remove duplicates in an array
     * @method _arrayUnique
     * @param  {array}     array The array
     * @return {array}           Unique array
     */
    _arrayUnique : function (array) {
      var a = array.concat();
      for (var i = 0; i < a.length; ++i) {
        for (var j = i + 1; j < a.length; ++j) {
          if (a[i] === a[j]) {
            a.splice(j--, 1);
          }
        }
      }

      return a;
    },

    /**
     * Helper function to check if a variable is a function
     * @method
     * @param  {object} value Variable to check
     * @return {boolean}
     */
    _isFunction : function (value) {
      return typeof value === 'function';
    }
  };
})(jQuery);

'use strict';

var simpleAlert = function () {
  this.init();
};

;(function ($) {
  simpleAlert.prototype = {
    init : function () {
      this.$alerts = $('<div></div>').addClass('alerts');
      this.$list = $('<ul></ul>').appendTo(this.$alerts);
      this.$alerts.prependTo('body');
      this.prevMessage = '';
    },

    /**
     * Adds an alert to the queue
     * @method add
     * @param  {string} message   The messgae that should be shown
     * @param  {int}    duration  How long the message should be visible
     * @param  {string} klass     Class to add the message contaioner
     */
    add : function (message, duration, klass) {
      var _ = this,
          $li;

      duration = duration || 5;
      klass = klass || 'notice';

      if (message && message !== _.prevMessage) {
        $li = $('<li></li>').addClass(klass).text(message).hide().appendTo(_.$list).slideDown();
      }

      var show = function () {
        window.setTimeout(function () {
          $li.slideUp(200, function () {
            $li.remove();
          });
        }, duration * 1000);
      };

      if ($li) {
        show();
      }

      _.prevMessage = message;

      return this;
    }
  };
})(jQuery);

/* global jQuery:true */

'use strict';

var Ssl = function () {
  this.init();
};

;(function (Ssl, $) {
  Ssl.prototype = {
    init : function () {
      var _ = this;

      _.isIE = navigator.appVersion.indexOf('MSIE') != -1 || navigator.appVersion.indexOf('Trident') != -1 || navigator.appVersion.indexOf('Edge') != -1;

      // Debug on dev site
      _.debug = window.location.host.indexOf('dev') !== -1;

      // Variables
      _.registerResizeEvents = [];

      _.$document = $(document);
      _.$window = $(window);

      // Start functions
      _.mediaQueryListener();

      // Header
      _.search();
      _.navigation();

      // Content
      _.lazyLoad();

      // Listen for events
      _.eventHandler();

      // Carousels with Slick
      _.flickIt();

      // Map handler for frontpage svg
      _.mapSvgHandler();

      // Map handler for areas
      _.mapHandler();

      // Star ratings
      _.ratings();

      // Various content modifications etc.
      _.content();

      // Add analytics
      _.analytics();

      // Add alert system
      _.alert = new simpleAlert();

      if (_.isIE) {
        svg4everybody({
          nosvg: true,
          polyfill: true
        });
      }
    }
  };
})(Ssl || {}, jQuery);

/* global jQuery:true, _gaq:true */

/**
 * [description]
 * @method
 * @param  {[type]} function (Ssl,         $ [description]
 * @return {[type]}          [description]
 */
;(function (Ssl, $) {
  /**
   * Variuos content modifications etc.
   * @method content
   * @return {[type]} [description]
   */
  Ssl.prototype.analytics = function () {
    var _ = this,
        i = 0,
        ready, listen, push;

    /**
    * Checks if Google Analytics is ready.
    */
    ready = function () {
      if (window._gat && window._gat._getTracker) {
        listen();
      } else if (i < 20) {
        setTimeout(function () {
          ready();
          i++;
        }, 500);
      }
    };

    /**
     * Finds elements to track and adds a listener.
     */
    listen = function () {
      $('*[data-track]').not('.is-tracked').on('click', function () {
        var $this = $(this);
        push($this.attr('data-category'), $this.attr('data-action'), $this.attr('data-label'));
      }).addClass('is-tracked');
    };

    /**
     * Push event to Google Analytics
     * @return boolean
     */
    push = function (category, action, label) {
      var success = false;

      category = category || document.title;

      if (window._gaq) {
        _gaq.push(['_trackEvent', category, action, label]);
        success = true;
      }

      return success;
    };

    if (!_.debug) {
      ready();
    }
  };

})(Ssl || {}, jQuery);

/* global Ssl:true, jQuery:true */

'use strict';

;(function (Ssl, $) {
  /**
   * Variuos content modifications etc.
   * @method content
   * @return {[type]} [description]
   */
  Ssl.prototype.content = function () {
    var _ = this,
        updateText;

    // More button on area page
    if ($('html').hasClass('areapage')) {
      var $item = $($('.activities').find('h3')[2]),
          title = Global.language === 'SV' ? 'Fler aktiviteter' : 'More activities',
          $wrapper;
      $.merge($item, $item.nextAll());
      $item.wrapAll($('<div></div>').addClass('more-content show-more-slideOut'));
      $wrapper = $('.more-content');
      $('<button></button>').attr({ 'type' : 'button' })
        .text(title)
        .addClass('more button turquoise button--center')
        .on('click', function () {
          $wrapper.toggleClass('is-open');
          $(this).hide();
        }).insertAfter($wrapper);
    }

    // Float label pattern for forms
    updateText = function (e) {
      var $input = $(this);
      setTimeout(function () {
        var val = $input.val();
        if (val !== '') {
          $input.parents('.contourField').addClass('is-floating');
        } else {
          $input.parents('.contourField').removeClass('is-floating');
        }
      }, 1);
    };

    // Find form using float label pattern and listen for typing
    $('fieldset input, fieldset textarea').on('keydown change', updateText);

    return this;
  };
})(Ssl || {}, jQuery);

/* global Ssl:true, jQuery:true */

'use strict';

;(function (Ssl, $) {
  /**
   * [event description]
   * @method events
   * @return {[type]} [description]
   */
  Ssl.prototype.eventHandler = function () {
    var _ = this,
        debounceResize;

    debounceResize = _.debounce(function () {
      for (var i = 0, l = _.registerResizeEvents.length; i < l; i++) {
        var f = _.registerResizeEvents[i].split('.');

        if (f.length === 1) {
          _[f[0]]();
        } else if (f.length === 2) {
          _[f[0]][f[1]]();
        }
      }
    }, 50);

    _.$window.on('resize orientationchange load', debounceResize);

    return this;
  };
})(Ssl || {}, jQuery);

/* global Ssl:true, jQuery:true */

'use strict';

;(function (Ssl, $) {
  /**
   * [event description]
   * @method events
   * @return {[type]} [description]
   */
  Ssl.prototype.flickIt = function () {
    var _ = this;

    if ($.isFunction($.fn.flickity)) {
      // This is handled with settings set in the html instead.

      // $('.gallery').flickity({
      //   arrowShape: 'M48.979 40.073L6.149 1.043a3.623 3.623 0 0 0-5.09 0 3.56 3.56 0 0 0 0 5.069l40.234 36.682L1.06 79.476a3.56 3.56 0 0 0 0 5.068 3.623 3.623 0 0 0 5.09 0l42.83-39.029c.747-.748 1.059-1.745 1.017-2.721.042-.976-.27-1.973-1.017-2.721z'
      //   imagesLoaded: true,
      //   cellAlign: 'left',
      //   contain: true,
      //   pageDots: false,
      //   prevNextButtons: true,
      //   watchCSS: true
      // });
    }

    return this;
  };
})(Ssl || {}, jQuery);

/* global Ssl:true, jQuery:true */

'use strict';

;(function (Ssl, $) {
  /**
   * [debounce description]
   * @method debounce
   * @param  {[type]} func      [description]
   * @param  {[type]} wait      [description]
   * @param  {[type]} immediate [description]
   * @return {[type]}           [description]
   */
  Ssl.prototype.debounce = function (func, wait, immediate) {
    var timeout;
    return function () {
      var context = this, args = arguments;
      var later = function() {
        timeout = null;
        if (!immediate) {
          func.apply(context, args);
        }
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) {
        func.apply(context, args);
      }
    };
  };
})(Ssl || {}, jQuery);

/* global Ssl:true, jQuery:true */

'use strict';

;(function (Ssl, $) {
  /**
   * [event description]
   * @method events
   * @return {[type]} [description]
   */
  Ssl.prototype.lazyLoad = function () {
    var _ = this;

    $('[lazyLoad]').each(function (i, container) {
      var $container = $(container),
          $target = $container.find('.lazyLoad-target'),
          $loadMore = $container.find('.lazyLoad-more'),
          path = '/umbraco/surface/' + $container.attr('data-surface'),
          pageNumber = $loadMore.attr('data-page');

      $loadMore.on('click', function () {
        $container.addClass('is-loading');

        $.ajax({
          url : path,
          data : {
            nodeId : Global.currentNodeId,
            pageNumber : pageNumber,
            language : Global.language
          }
        }).done(function (data, status, xhr) {
          // For some reason an empty result === 2
          if (data.length > 2) {
            $(data).appendTo($target);
            pageNumber++;
            $('html, body').animate({ scrollTop: $container.find('.gallery-cell:last-child').position().top }, 500);
          } else {
            $loadMore.addClass('is-faded');
          }

          $container.removeClass('is-loading');
        });
      });
    });

    return this;
  };
})(Ssl || {}, jQuery);

/* global Ssl:true, jQuery:true, Global:true, GMapsApp:true */

'use strict';

;(function (Ssl, $) {
  /**
   * [event description]
   * @method events
   * @return {[type]} [description]
   */
  Ssl.prototype.mapHandler = function () {
    var _ = this,
        GMap,
        $map = $('.map-wrapper'),
        $areas = $('#areas-list'),
        $container = $('<div></div>').addClass('filter-container'),
        initMap, loadMap, checkServiceID, mapSettings, activitytypes, markers;

    // Initilize map with GMaps.js
    // and add filter menu with icons
    initMap = function (settings) {
      var recreationAreas = (settings.url === 'strovomraden' || settings.url === 'recreation-areas');

      settings.markerCallback = {};

      settings.markerCallback.error = function (e) {
        _.alert.add(e, 30);
      };

      // Add hover interactions to markers
      // Passed as a function into GMaps
      if (recreationAreas) {
        settings.markerCallback.hover = function (e) {
          if (e.type === 'mouseover') {
            var $item = $('#areas-list').find('[data-id="' + e.id + '"]');
            var top = 0;
            $item.prevAll().not('.is-hidden').each(function () {
              top += $(this).outerHeight();
            });

            $('#areas-list').animate({ scrollTop: top }, 400);
            $item.addClass('hover');
          } else {
            var $item = $('#areas-list').find('[data-id="' + e.id + '"]');
            $item.removeClass('hover');
          }
        };

      // Add busstop information to markers
      // Passed as a function into GMaps
      } else {
        settings.markerCallback.busStopService = function (e) {
          $.ajax({
            'url' : '/umbraco/surface/mapssurface/getbusstopinfo',
            'method' : 'GET',
            'data' : e.data
          }).done(function (data) {
            e.infoWindow.setContent(data);
          }).fail(function(jqXHR, textStatus) {
            e.infoWindow.setContent('<p>There was an error when fetching the busstop info.</p>');
            // console.log(textStatus);
          });
        };
        settings.markerCallback.standardService = function (e) {
          e.infoWindow.setContent('<h2>' + e.data.title + '</h2>' + e.data.intro);
        };
      }

      // Load GMapsApp found in GMaps.js
      GMap = new GMapsApp(settings);

      // If we are on URL /strovomraden then reset all filters
      if (recreationAreas) {
        GMap.resetFilters();
      }

      var $icons = $('<div></div>').addClass('icons').appendTo($container);
      var $filters = $('<div></div>').addClass('filters').appendTo($container);

      // Count handler used to change the active markers count when filters are activated
      var countHandler = function () {
        GMap.activeFilters.forEach(function (count, id) {
          var $b = $container.find('button[data-id="' + id + '"]');
          $($b[1]).text($($b[1]).attr('data-label') + ' (' + count + ')');
          $b.removeAttr('disabled');
          if (!count) {
            $b.attr({ 'disabled' : 'disabled' });
          }
        });

        // Hide areas in the list for connected hidden markers
        GMap.markers.forEach(function (marker, i) {
          if (marker.show) {
            $areas.find('[data-id="' + marker.id + '"]').removeClass('is-hidden');
          }
        });
      };

      // Add burger menu button for smaller screens
      var $burger = $('<button></button>').text('\u2630')
      .addClass('show-filters')
      .on('click', function () {
        $container.toggleClass('is-open');
      })
      .appendTo($map);

      // Close menu when resizing the browser
      $(window).on('resize', function () {
        $container.removeClass('is-open');
      });


      // Add reset filters text button
      var show = false;
      var $reset = $('<button></button>').attr({
        'type' : 'button',
        'data-id' : 'reset'
      }).addClass('reset')
      .text((recreationAreas) ? 'Återställ' : 'Dölj alla')
      .appendTo($filters)
      .on('click', function () {

        // Recreation area overview
        if (recreationAreas) {
          $container.find('.active').removeClass('active');
          $container.find('button').removeAttr('disabled');
          GMap.resetFilters();
          GMap.showAllMarkers();
          countHandler();

        // Recreation area
        } else {
          if (show) {
            GMap.showAllMarkers();
            $reset.text('Dölj alla');
            show = false;
            $container.find('button').not('[data-count="0"], .reset').addClass('active');
          } else {
            GMap.hideAllMarkers();
            $reset.text('Visa alla');
            show = true;
            $container.find('button').not('[data-count="0"], .reset').removeClass('active');
          }
        }
      });

      // Add reset filters icon button
      var $icon = $('<button></button>')
        .html('<svg><use xlink:href="/assets/images/all.svg#Lager_1"></use></svg>')
        .addClass('reset')
      .on('click', function () {
        $reset.click();
      }).appendTo($icons);

      // Loop through all activity types and create buttons
      $.each(settings.activitytypes, function (i, activity) {
        var title = (recreationAreas) ?
          activity.Name + ' (' + GMap.activeFilters[activity.NodeId] + ')' :
          activity.Name + ' (' + activity.Counts + ')';

        // Create text button
        var $b = $('<button></button>').attr({
          'type' : 'button',
          'data-id' : activity.NodeId,
          'data-label' : activity.Name,
          'data-count' : activity.Counts !== undefined ? activity.Counts : false
        }).text(title)
        .on('click', function () {
          $i.toggleClass('active');
          $b.toggleClass('active');

          // Recreation area overview
          if (recreationAreas) {
            var filters = [];
            $areas.find('li').addClass('is-hidden');
            $filters.find('.active').each(function (i, el) {
              filters.push(parseInt($(el).attr('data-id'), 10));
            });

            GMap.toggleMarkersByFilterIds(filters);
            countHandler();

          // Recreation area
          } else {
            $reset.text('Visa alla');
            show = true;
            GMap.toggleMarkersByFilterId(activity.NodeId);
          }
        });

        // Create icon button
        var $i = $('<button></button>').attr({
          'data-id' : activity.NodeId,
          'data-count' : activity.Counts !== undefined ? activity.Counts : false
        })
        .html('<svg><use xlink:href="' + activity.IconPath + '#Lager_1"></use></svg>')
        .on('click', function () {
          if (!$b.attr('disabled')) {
            $b.click();
          }
        });

        // Disable buttons without a marker count
        if ((recreationAreas && !GMap.activeFilters[parseInt(activity.NodeId, 10)]) ||
            (!recreationAreas && !activity.Counts)) {
          $b.attr({ 'disabled' : 'disabled' });
          $i.attr({ 'disabled' : 'disabled' });
        }

        // Set buttons with a marker count as active
        if (!recreationAreas && activity.Counts) {
          $b.addClass('active');
          $i.addClass('active');
        }

        $b.appendTo($filters);
        $i.appendTo($icons);
      });

      $container.appendTo($map);

      // Toggle a specific service if an ID is set
      checkServiceID(settings.serviceId);
    };

    // Load Google Maps and settings from Umbraco with ajax
    loadMap = function (id) {

      if (!$map.hasClass('is-loaded')) {
        $map.addClass('is-loading');

        // Load the google maps script
        $.getScript('https://www.google.com/jsapi', function () {
          google.load('maps', '3', { other_params: 'v=3&region=SE', callback: function () {

            // Get basic settings from Umbraco
            $.getJSON('/umbraco/Surface/MapsSurface/GetBasicData', {
              'NodeId' : Global.currentNodeId
            }, function (data) {
              mapSettings = data.map;

              // Get activity types from Umbraco
              $.getJSON((mapSettings.UrlName === 'strovomraden' || mapSettings.UrlName === 'recreation-areas') ?
                '/umbraco/Surface/MapInteractivitySurface/GetActivityTypesList' :
                '/umbraco/surface/mapinteractivitysurface/getservicetypesjson', {
                'language' : Global.language,
                'NodeId' : Global.currentNodeId
              }, function (data) {
                if (mapSettings.UrlName === 'strovomraden' || mapSettings.UrlName === 'recreation-areas') {
                  activitytypes = data.activitytypes;
                } else {
                  activitytypes = data.servicetypes.ServiceTypesCollection;
                }
              });

              // Get markers from Umbraco
              $.getJSON((mapSettings.UrlName === 'strovomraden' || mapSettings.UrlName === 'recreation-areas') ?
                '/umbraco/Surface/MapsSurface/GetAreaMarkers' :
                '/umbraco/Surface/MapsSurface/GetServicesByAreaJson', {
                'language' : Global.language,
                'NodeId' : Global.currentNodeId
              }, function (data) {
                if (mapSettings.UrlName === 'strovomraden' || mapSettings.UrlName === 'recreation-areas') {
                  markers = data.markers;
                } else {
                  markers = data.services.ServiceCollection;
                }
              });
            });

            // Count number of completed ajax requests.
            // When all ajax requests are completed, load the map.
            // This is a bit of an ugly hack, but it will do.
            var i = 0;
            $(document).ajaxComplete(function (event, xhr, settings) {
              if (i === 2) {
                // Prevent map from loading again
                $map.addClass('is-loaded');

                initMap({
                  id: (mapSettings.UrlName === 'strovomraden' || mapSettings.UrlName === 'recreation-areas') ? 'google_map' : 'osm_map',
                  position : {
                    lat: parseFloat(mapSettings.Latitude),
                    lng: parseFloat(mapSettings.Longitude)
                  },
                  zoom: mapSettings.Zoom,
                  url: mapSettings.UrlName,
                  activitytypes: activitytypes,
                  markers: markers,
                  serviceId: id
                });
              }
              i++;
            });
          } });
        });
      } else {
        // Toggle a specific service if an ID is set
        checkServiceID(id);
      }
    };

    // Check if a service ID is set.
    // Show correct markers and set filters active
    checkServiceID = function (id) {
      if (id) {
        GMap.hideAllMarkers();
        GMap.toggleMarkersByFilterId(id);

        $container.find('button').removeClass('active');
        $container.find('[data-id="' + id + '"]').addClass('active');
      }
    };

    // Listen for custom event mapLoaded,
    // which is fired when the google map has finished loading.
    $(window).on('mapLoaded', function () {
      $map.removeClass('is-loading');
    });

    // When on URL /strovomraden
    if ($map.hasClass('overview-page') && $areas) {
      loadMap();

      // Show marker infowindow when hovering an area in the list
      $areas.find('li').on('mouseover mouseout', function (e) {
        var id = parseInt($(this).attr('data-id'), 10);
        if (GMap !== undefined) {
          if (e.type === 'mouseover') {
            GMap.markers.forEach(function (marker, i) {
              if (marker.id === id) {
                GMap.markers[i].infowindow.open(GMap.map, GMap.markers[i].marker);
              }
            });
          } else {
            GMap.markers.forEach(function (marker, i) {
              if (marker.id === id) {
                GMap.markers[i].infowindow.close(GMap.map, GMap.markers[i].marker);
              }
            });
          }
        }
      });

    // When on URL /strovomraden/[AREA]
    } else {
      // Hide map
      $('#close-map').on('click', function () {
        $map.removeClass('is-open');
      });

      // Show map
      $('[data-type="open-map"]').on('click', function () {
        $map.addClass('is-open');
        loadMap(parseInt($(this).attr('data-id'), 10));
      });
    }

    return this;
  };

  /**
   * Function that submits the bus stop search form
   * @method submitBusForm
   * @param  {[type]}      button [description]
   * @return {[type]}             [description]
   */
  Ssl.submitBusForm = function (button) {
      var url = 'http://reseplanerare.resrobot.se/bin/query.exe/sn?L=vs_resrobot&';
      if (document.getElementById('resRobotSID')) {
          url += 'SID=' + document.getElementById('resRobotSID').value + '&';
      }
      if (document.getElementById('resrobotS')) {
        url += 'S=' + document.getElementById('resrobotS').value + '&';
      }
      if (document.getElementById('resrobotSALL')) {
        url += 'SALL=' + document.getElementById('resrobotSALL').value + '&';
      }
      if (document.getElementById('resRobotZID')) {
        url += 'ZID=' + document.getElementById('resRobotZID').value + '&';
      }
      url += 'externalCall=yes&queryPageDisplayed=yes&application=PRIVATETRANSPORT&jsEnabled=yes&';
      url += button.name + '=' + button.value;
      window.open(url);
      return false;
  };
})(Ssl || {}, jQuery);

/* global Ssl:true, jQuery:true */

'use strict';

;(function (Ssl, $) {
  /**
   * [event description]
   * @method events
   * @return {[type]} [description]
   */
  Ssl.prototype.mapSvgHandler = function () {
    var _ = this,
        $map = $('#svg-map');

    if ($map[0]) {
      var markers = [
        {
          label: 'Arriesjön',
          x: 166,
          y: 476,
          url: 'arriesjon'
        },
        {
          label: 'Bockeboda',
          x: 415,
          y: 224,
          url: 'bockeboda'
        },
        {
          label: 'Breanäs',
          x: 500,
          y: 90,
          url: 'breanas'
        },
        {
          label: 'Djurholmen',
          x: 156,
          y: 50,
          url: 'djurholmen'
        },
        {
          label: 'Finstorp',
          x: 160,
          y: 214,
          url: 'finstorp'
        },
        {
          label: 'Friseboda',
          x: 466,
          y: 330,
          url: 'friseboda'
        },
        {
          label: 'Frostavallen',
          x: 278,
          y: 238,
          url: 'frostavallen'
        },
        {
          label: 'Fulltofta Naturcentrum',
          x: 309,
          y: 287,
          url: 'fulltofta'
        },
        {
          label: 'Kronoskogen',
          x: 206,
          y: 100,
          url: 'kronoskogen'
        },
        {
          label: 'Järavallen',
          x: 113,
          y: 318,
          url: 'jaravallen'
        },
        {
          label: 'Klåveröd',
          x: 184,
          y: 204,
          url: 'klaverod'
        },
        {
          label: 'Kronoskogen',
          x: 86,
          y: 100,
          url: 'kronoskogen'
        },
        {
          label: 'Möllerödssjö',
          x: 340,
          y: 90,
          url: 'mollerodssjo'
        },
        {
          label: 'Skrylle',
          x: 234,
          y: 388,
          url: 'skrylle'
        },
        {
          label: 'Snogeholm',
          x: 324,
          y: 454,
          url: 'snogeholm'
        },
        {
          label: 'Tjörnedala',
          x: 492,
          y: 436,
          url: 'tjornedala'
        },
        {
          label: 'Vedema',
          x: 315,
          y: 115,
          url: 'vedema'
        },
        {
          label: 'Vedby',
          x: 186,
          y: 146,
          url: 'vedby'
        },
        {
          label: 'Vitemölla',
          x: 462,
          y: 382,
          url: 'vitemolla'
        }

      ];

      var $areas = $('#areas-list'),
          $markers = $('#markers'),
          $labels = $('#labels'),
          paths = window.location.pathname.substr(1).split('/'),
          recreationAreas = (paths[0] === 'strovomraden' || paths[0] === 'recreation-areas');

      $.each(markers, function (i, marker) {
        var size = 60,
            group, img, label, SVGRect, rect;

        size = recreationAreas ? 90 : 60;

        // We only print one specific marker when we're on the area overview page
        if (recreationAreas) {
          if (paths[1] !== marker.url) {
            return;
          }

          // Adjust bigger marker
          size = 90;
          marker.x = marker.x - 10;
          marker.y = marker.y - 20;
        }

        img = document.createElementNS('http://www.w3.org/2000/svg', 'image');
        img.setAttribute('height', size);
        img.setAttribute('width', size);
        img.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', '/assets/images/area-marker.svg');
        img.setAttribute('x', marker.x - 10);
        img.setAttribute('y', marker.y - 10);
        if (!recreationAreas) {
          img.setAttribute('cursor', 'pointer');
        }
        img.setAttribute('data-id', i);
        img.setAttribute('data-url', '/strovomraden/' + marker.url);
        if (!_.isIE) {
          img.classList.add('marker');
        } else {
          img.className += 'marker';
        }

        $markers[0].appendChild(img);

        // We only add labels on the start page
        if (!recreationAreas) {
          group = document.createElementNS('http://www.w3.org/2000/svg', 'g');
          group.setAttribute('x', marker.x);
          group.setAttribute('y', marker.y - 20);
          group.setAttribute('data-id', i);
          group.setAttribute('data-url', '/strovomraden/' + marker.url);
          group.setAttribute('style', 'transform: translate(-20%,0);');
          if (!_.isIE) {
            group.classList.add('label');
          } else {
            group.className += 'label';
          }

          label = document.createElementNS('http://www.w3.org/2000/svg', 'text');
          label.setAttribute('x', marker.x);
          label.setAttribute('y', marker.y - 20);
          label.setAttribute('font-family', 'sans-serif');
          label.setAttribute('font-size', '18');
          label.setAttribute('font-weight', 'normal');
          label.setAttribute('cursor', 'default');
          label.setAttribute('fill', '#222');
          label.appendChild(document.createTextNode(marker.label));

          $labels[0].appendChild(label);

          SVGRect = label.getBBox();

          rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
          rect.setAttribute('x', SVGRect.x - 5);
          rect.setAttribute('y', SVGRect.y - 5);
          rect.setAttribute('width', SVGRect.width + 10);
          rect.setAttribute('height', SVGRect.height + 10);
          rect.setAttribute('fill', 'white');
          rect.setAttribute('stroke-width', 1);
          rect.setAttribute('stroke', '#ddd');
          rect.setAttribute('data-id', i);

          group.appendChild(rect);
          group.appendChild(label);

          $labels[0].appendChild(group);
        }
      });

      $labels = $('#labels');
      // We only add mouseover and click behaviors on the start page
      if (!recreationAreas && !_.msIE) {
        $('#areas-list').on('mouseover mouseout', 'li', function (e) {
          if (e.type === 'mouseover') {
            $labels.find('[data-url="' + $(this).attr('data-url') + '"]')[0].classList.add('hover');
          } else {
            $labels.find('[data-url="' + $(this).attr('data-url') + '"]')[0].classList.remove('hover');
          }
        });

        $markers.on('mouseover mouseout click', '.marker', function (e) {
          var $area = $areas.find('[data-href="' + $(this).attr('data-url') + '"]'),
              top = 0;

          if (e.type === 'mouseover') {
            $map.find('.label[data-id=' + $(this).attr('data-id') + ']').each(function () {
              $(this)[0].classList.add('hover');
            });
            $area.addClass('hover');
            $area.prevAll().not('.is-hidden').each(function () {
              top += $(this).outerHeight();
            });

            $areas.animate({ scrollTop: top }, 400);
          } else if (e.type === 'mouseout') {
            $map.find('.label[data-id=' + $(this).attr('data-id') + ']').each(function () {
              $(this)[0].classList.remove('hover');
            });
            $area.removeClass('hover');
          } else {
            window.location.href = $(this).attr('data-url');
          }
        });
      }
    }

    return this;
  };
})(Ssl || {}, jQuery);

/* global Ssl:true, jQuery:true */

'use strict';

;(function (Ssl, $) {
  /**
   * [mediaQueryListener description]
   * @method mediaQueryListener
   * @return {[type]}           [description]
   */
  Ssl.prototype.mediaQueryListener = function () {
    var _ = this;

    _.registerResizeEvents.push('breakpointTrigger');

    _.beforeElement = window.getComputedStyle ? window.getComputedStyle(document.body, '::before') : false;
    _.currentBreakpoint = '';
    _.lastBreakpoint = '';

    // Listen for media query changes
    _.$window.on('breakpoint-change', function (e, bp) {
      if (_.debug) {
        console.log(bp);
      }
    });

    if(!_.afterElement) {
      return;
    }
  };

  /**
   * [breakPointTrigger description]
   * @method breakPointTrigger
   * @return {[type]}          [description]
   */
  Ssl.prototype.breakpointTrigger = function () {
    var _ = this;

    // Regexp for removing quotes added by various browsers
    _.currentBreakpoint = _.beforeElement.getPropertyValue('content').replace(/^["']|["']$/g, '');

    if (_.currentBreakpoint !== _.lastBreakpoint) {
      _.$window.trigger('breakpoint-change', _.currentBreakpoint);
      _.lastBreakpoint = _.currentBreakpoint;
    }
  };
})(Ssl || {}, jQuery);

/* global Ssl:true, jQuery:true */

'use strict';

;(function (Ssl, $) {
  /**
   * [navigation description]
   * @method navigation
   * @return {[type]}   [description]
   */
  Ssl.prototype.navigation = function () {
      var _ = this,
          $nav = $('#main-nav'),
          $button = $nav.find('button'),
          $ul = $nav.find('ul').first(),
          $ulNew = $nav.find('ul').clone().addClass('extra-menu').appendTo($nav),
          $li = $ul.find('> li'),
          $liNew = $ulNew.find('li'),
          inView, debounce;

      // Add menu item width
      $li.each(function () {
        $(this).attr('data-width', $(this).outerWidth(true));
      });

      /**
       * Adjusts the position of the submenu verticaly
       * @method inView
       * @param  {object} $item Submenu jQuery object
       */
      inView = function ($item) {
        var width = _.$window.width() - ($item.outerWidth() + $item.offset().left);
        if ($item.offset().left < 0) {
          $item.css({
            'margin-left' : Math.abs($item.offset().left)
          });
        } else if (width < 0) {
          $item.css({
            'margin-left' : width
          });
        }
      };

      // Load submenu with ajax
      $li.on('mouseenter mouseleave', function (e) {
        var $this = $(this),
            id = $this.attr('data-id'),
            $submenu, $inner;

        // Check if already processed or currently processing
        if (!$this.hasClass('is-processed') && !$this.hasClass('is-processing')) {
          if (e.type === 'mouseenter' && id !== '0') {
            // Find submenu if already added
            $submenu = $this.find('.submenu');
            $inner = $submenu.find('.submenu-inner');

            // Add submenu if not found
            if (!$submenu.length) {
              $submenu = $('<div></div>').addClass('submenu is-loading').appendTo($this);
              $inner = $('<div></div>').addClass('submenu-inner').appendTo($submenu);
            }

            // Debounce for quick mouseovers
            debounce = setTimeout(function () {
              $this.addClass('is-processing');
              $.ajax({
                'url' : '/umbraco/surface/submenusurface/getsubmenujson',
                'method' : 'GET',
                'data' : {
                  'language' : Global.language,
                  'NodeId' : id
                }
              }).done(function (data) {
                $inner.html(data);
                $this.addClass('is-processed').removeClass('is-processing');
                $submenu.removeClass('is-loading');
                inView($submenu);
              }).fail(function(jqXHR, textStatus) {
                $this.removeClass('is-processing');
                $submenu.remove();
              });
            }, 200);
          } else {
            clearTimeout(debounce);
          }

        // Submenu already processed, adjust accordingly
        } else {
          $submenu = $this.find('.submenu');
          if ($submenu !== 'undefined' && id !== '0') {
            if (e.type === 'mouseenter') {
              // Adjust menu position to be in view
              inView($submenu);
            } else {
              $submenu.css({
                'margin-left' : 0
              });
            }
          }
        }
      });

      // Register function to be run when resizing the browser window
      _.registerResizeEvents.push('navigation.resize');

      // $ulNew.on('mouseenter mouseleave focus blur', function (e) {
      //   $nav.toggleClass('is-open', (e.type === 'mouseenter' || e.type === 'focus'));
      // });

      // Open / Close menu if plus-button is shown
      $button.on('click blur', function (e) {
        if (e.type === 'blur') {
          $nav.removeClass('is-open');
        } else {
          $nav.toggleClass('is-open');
        }
      });

      // Resize event used to calculate and adjust menu size
      _.navigation.resize = function () {
        var navwidth = 48 + (_.currentBreakpoint === 'none' ? 48 : 0),
            availablespace =   $ul.outerWidth(true);

        $li.each(function () {
          navwidth += parseInt($(this).attr('data-width'), 10);
          $(this).toggleClass('is-hidden', (navwidth > availablespace));
        });

        var items = $ul.find('> li:not(.is-hidden)').length;
        $liNew.each(function (i) {
          if (i < items) {
            $(this).addClass('is-hidden');
          } else {
            $(this).removeClass('is-hidden');
          }
        });

        $nav.toggleClass('full', (navwidth < availablespace));
        $button.toggleClass('is-hidden', (navwidth < availablespace));

        return this;
      };

    return this;
  };
})(Ssl || {}, jQuery);

/* global Ssl:true, jQuery:true */

'use strict';

;(function (Ssl, $) {
  /**
   * [ratings description]
   * @method ratings
   * @return {[type]} [description]
   */
  Ssl.prototype.ratings = function () {
    var _ = this,
        $starRating = $('ul.star-rating'),
        nodeTitle = $starRating.parent().attr('data-title'),
        sitePart = $starRating.parent().attr('data-content-type'),
        updateStarRatingBar,
        createStarRatingCookie,
        readStarRatingCookie,
        decodeHtml;

    /**
     * [updateStarRatingBar description]
     * @method updateStarRatingBar
     * @param  {[type]}            id        [description]
     * @param  {[type]}            value     [description]
     * @param  {[type]}            name      [description]
     * @param  {[type]}            textshown [description]
     * @return {[type]}                      [description]
     */
    updateStarRatingBar = function (id, value, name, textshown) {
      // Get current language
      var language = Global.language,
          messageConfirm = '',
          answer, rateBar, starsMsg, spanMsg;

          // No previous voting (using current browser)
      if (readStarRatingCookie(id) === null) {
        // Get the confirm message from Umbraco's Dictionary via a global js variable.
        messageConfirm = GuestRatings.ConfirmMessage.format(name, value);

        // Hand out a confirm dialog
        answer = confirm(decodeHtml(messageConfirm));

        if (answer) {
          // Create parameters

          $.getJSON('/umbraco/Surface/GuestRatingsSurface/UpdateBar', {
            nodeId: id,
            rating: value,
            sitePart: sitePart,
            language: Global.language
          }, function (data) {
            if (data.result == true) {
              rateBar = document.getElementById(id);
              rateBar.style.width = value * 20 + '%';

              createStarRatingCookie(id, value, 100);

              if (textshown) {
                // Temporarily hide the stars message
                starsMsg = document.getElementById('stars-message');
                starsMsg.style.display = 'none';
                spanMsg = document.getElementById('voters-message');

                // Get the thank-you and see-results messages
                // from Umbraco's Dictionary via global js variables
                spanMsg.innerHTML = unescape(GuestRatings.ThankYouMessage +
                  '<br /><a id="see-result" href="javascript:void(0);">' +
                  GuestRatings.SeeResults + '</a>.');
              }
            }
          });
        }
      } else {
        if (textshown) {
          spanMsg = document.getElementById('voters-message');
          // Get the no-no message from Umbraco's Dictionary via a global js variable.
          spanMsg.innerHTML = GuestRatings.OnlyOnceMessage;
        }
      }
    };

    /**
     * [createStarRatingCookie description]
     * @method createStarRatingCookie
     * @param  {[type]}               name  [description]
     * @param  {[type]}               value [description]
     * @param  {[type]}               days  [description]
     * @return {[type]}                     [description]
     */
    createStarRatingCookie = function (name, value, days) {
      if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = '; expires=' + date.toGMTString();
      } else {
        var expires = '';
      }

      document.cookie = name + '=' + value + expires + '; path=/';
    };

    /**
     * [readStarRatingCookie description]
     * @method readStarRatingCookie
     * @param  {[type]}             name [description]
     * @return {[type]}                  [description]
     */
    readStarRatingCookie = function (name) {
      var nameEQ = name + '=';
      var ca = document.cookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];

        while (c.charAt(0) === ' ') {
          c = c.substring(1, c.length);
        }

        if (c.indexOf(nameEQ) == 0) {
          return c.substring(nameEQ.length, c.length);
        }
      }

      return null;
    };

    /**
     * Decode html including strings containing html-coded Swedish characters
     * @method decodeHtml
     * @param  {[type]}   html [description]
     * @return {[type]}        [description]
     */
    decodeHtml = function (html) {
      var txt = document.createElement('textarea');
      txt.innerHTML = html;
      return txt.value;
    };

    // Attach a click event to the "See results/Se resultat" link
    $('body').on('click', '#see-result', function () {
      console.log(1);
        // Get updated rating information
        $.getJSON('/umbraco/Surface/GuestRatingsSurface/GuestRatingsJson', {
            nodeId: Global.currentNodeId,
            sitePart: sitePart,
            language: Global.language
        }, function (data) {
            // Replace the rating bar's percentage value
            $('.current-rating').css({ 'width': data.model.Percentage + '%' });

            // Display and replace the average value message
            $('#stars-message').show().html(data.model.AverageValueMessage);

            // Replace the thank you-message to an updated voters message
            $('#voters-message').text(data.model.NumOfVotersMessage);
        });
    });

    // Attach click events to the stars of the rating bar
    $starRating.on('click', 'a', function (e) {
        e.stopPropagation();
        updateStarRatingBar(Global.currentNodeId, $(this).attr('data-star'), nodeTitle, true);
    });

    // String format function
    String.prototype.format = function () {
      var s = this,
          i = arguments.length;

      while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
      }

      return s;
    };
  };
})(Ssl || {}, jQuery);

/* global Ssl:true, jQuery:true */

'use strict';

;(function (Ssl, $) {
  /**
   * [search description]
   * @method search
   * @return {[type]} [description]
   */
  Ssl.prototype.search = function () {
    var _ = this,
        $wrapper = $('#search-header'),
        $search = $('#search-field'),
        $button = $('#search-button'),
        $list = $('<ul></ul>'),
        $current = $list.find('li').first(),
        total = 0,
        debounce, open, close, goto;

    $search.wrap($('<div></div>').addClass('autocomplete'));
    $list.appendTo($search.parent());

    open = function () {
      $list.addClass('is-visible');
    };

    close = function () {
      window.setTimeout(function () {
        $list.removeClass('is-visible');
      }, 200);
    };

    goto = function ($item) {
      if ($item.attr('data-href') === undefined ) {
        $wrapper.find('form').submit();
      } else {
        window.location.href = $item.attr('data-href');
      }
    };

    debounce = _.debounce(function (val) {
      $.ajax({
        url: '/umbraco/Surface/SearchSurface/CreateAutoSuggestions/',
        data: {
          language : 'SV',
          term : val,
          limit : 10
        },
        type: 'GET',
        dataType: 'json'
      })
      .success(function (result) {
        // Add extra test data right now
        var $ul = $('<ul></ul>'),
            $li;

        total = result.total;

        if (total) {
          for (var i = 0, l = result.result.length; i < l; i++) {
            $li = $('<li></li>').attr({
              'data-href' : result.result[i].Url
            }).html(
              '<span class="title">' + result.result[i].Title + '</span>' +
              '<span class="description">' + result.result[i].PreviewText + '</span>'
            ).appendTo($ul);
          }
          if (total > 10) {
            $li = $('<li></li>').addClass('more-results').attr({
              'data-href' : 'sokresultat.aspx?term=' + val
            }).html(
              '<span class="title">Fler resultat...</span>'
            ).appendTo($ul);
          }
          $list.html($ul.html());
          open();
        } else {
          close();
        }
      })
      .error(function (data) {
        // Do something?
      });
    }, 200);

    $search.on('focus blur', function (e) {
      if (e.type === 'blur' && total) {
        close();
      } else {
        open();
      }
    });

    $search.on('keydown', function (e) {
      // Prevent cursor jumping
      if (e.keyCode === 38 || e.keyCode === 40 || e.keyCode === 27) {
        return false;
      }
    });

    $search.on('keyup', function (e) {
      // Perform search
      var val = $search.val();
      if (val.length > 2) {
        if (e.keyCode !== 13 && e.keyCode !== 38 && e.keyCode !== 40 && e.keyCode !== 27) {
          debounce(val);
        }

        switch (e.keyCode) {
          // Up
          case 38:
            if (!$current.hasClass('is-selected')) {
              $current = $list.find('li').last().addClass('is-selected');
            } else {
              $current.removeClass('is-selected');
              if ($current.prev()[0]) {
                $current = $current.prev().addClass('is-selected');
              } else {
                $current = $list.find('li').last().addClass('is-selected');
              }
            }
          break;

          // Down
          case 40:
            if (!$current.hasClass('is-selected')) {
              $current = $list.find('li').first().addClass('is-selected');
            } else {
              $current.removeClass('is-selected');
              if ($current.next()[0]) {
                $current = $current.next().addClass('is-selected');
              } else {
                $current = $list.find('li').first().addClass('is-selected');
              }
            }
          break;

          // Enter
          case 13:
            goto($list.find('.is-selected'));
          break;

          // Escape
          case 27:
            close();
          break;
        }
      } else {
        close();
      }
    });

    $list.on('click', 'li', function () {
      goto($(this));
    });

    $button.on('click', function (e) {
      e.preventDefault();

      if ($wrapper.hasClass('is-open') && $search.val()) {
        $wrapper.find('form').submit();
      } else {
        $wrapper.toggleClass('is-open');
        if ($wrapper.hasClass('is-open')) {
          window.setTimeout(function () {
            $search.focus();
          }, 300);
        }
      }

      return false;
    });

    if ($('html').hasClass('searchresultspage')) {
      _.$document.on('click', '.pager a[href]', function () {
        $.ajax({
          url: $(this).attr('href'),
          data: { nodeId: Global.currentNodeId, language: Global.language },
          type: 'GET',
          cache: false,
          success: function (result) {
            $('#paged-result').html(result);
          }
        });
        return false;
      });
    }

    return this;
  };
})(Ssl || {}, jQuery);

/* global Ssl:true */

'use strict';

// Just kickstarts the js
;(function () {
  return new Ssl();
})();
