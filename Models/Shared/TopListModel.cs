﻿using System.Collections.Generic;

namespace SkanskaLandskap15.Models.Shared
{
    public class TopListModel
    {
        public string Title { get; set; }
        public string Number { get; set; }
        public int NodeId { get; set; }
        public string Url { get; set; }
        public string AverageValue { get; set; }
        public int Votes { get; set; }
        public string VotesMessage { get; set; }

        public List<TopListModel> TopListItemsCollection { get; set; } 
    }
}