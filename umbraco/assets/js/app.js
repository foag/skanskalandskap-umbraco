/* global jQuery:true */

'use strict';

var Ssl = function () {
  this.init();
};


;(function (Ssl, $) {
  Ssl.prototype = {
    init : function () {
      var _ = this;

      // Debug
      _.debug = true;

      // Variables
      _.registerResizeEvents = [];

      _.$window = $(window);

      // Start functions
      _.mediaQueryListener();

      _.search();
      _.navigation();

      _.eventHandler();
    }
  };
})(Ssl || {}, jQuery);

/* global Ssl:true, jQuery:true */

'use strict';

;(function (Ssl, $) {
  /**
   * [event description]
   * @method events
   * @return {[type]} [description]
   */
  Ssl.prototype.eventHandler = function () {
    var _ = this,
        debounceResize;

    debounceResize = _.debounce(function () {
      for (var i = 0, l = _.registerResizeEvents.length; i < l; i++) {
        var f = _.registerResizeEvents[i].split('.');

        if (f.length === 1) {
          _[f[0]]();
        } else if (f.length === 2) {
          _[f[0]][f[1]]();
        }
      }
    }, 50);

    _.$window.on('resize orientationchange load', debounceResize);

    return this;
  };
})(Ssl || {}, jQuery);

/* global Ssl:true, jQuery:true */

'use strict';

;(function (Ssl, $) {
  /**
   * [debounce description]
   * @method debounce
   * @param  {[type]} func      [description]
   * @param  {[type]} wait      [description]
   * @param  {[type]} immediate [description]
   * @return {[type]}           [description]
   */
  Ssl.prototype.debounce = function (func, wait, immediate) {
    var timeout;
    return function () {
      var context = this, args = arguments;
      var later = function() {
        timeout = null;
        if (!immediate) {
          func.apply(context, args);
        }
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) {
        func.apply(context, args);
      }
    };
  };
})(Ssl || {}, jQuery);

/* global Ssl:true, jQuery:true */

'use strict';

;(function (Ssl, $) {
  /**
   * [mediaQueryListener description]
   * @method mediaQueryListener
   * @return {[type]}           [description]
   */
  Ssl.prototype.mediaQueryListener = function () {
    var _ = this;

    _.registerResizeEvents.push('breakpointTrigger');

    _.beforeElement = window.getComputedStyle ? window.getComputedStyle(document.body, '::before') : false;
    _.currentBreakpoint = '';
    _.lastBreakpoint = '';

    // Listen for media query changes
    _.$window.on('breakpoint-change', function (e, bp) {
      if (_.debug) {
        console.log(bp);
      }
    });

    if(!_.afterElement) {
      return;
    }
  };

  /**
   * [breakPointTrigger description]
   * @method breakPointTrigger
   * @return {[type]}          [description]
   */
  Ssl.prototype.breakpointTrigger = function () {
    var _ = this;

    // Regexp for removing quotes added by various browsers
    _.currentBreakpoint = _.beforeElement.getPropertyValue('content').replace(/^["']|["']$/g, '');

    if (_.currentBreakpoint !== _.lastBreakpoint) {
      _.$window.trigger('breakpoint-change', _.currentBreakpoint);
      _.lastBreakpoint = _.currentBreakpoint;
    }
  };
})(Ssl || {}, jQuery);

/* global Ssl:true, jQuery:true */

'use strict';

;(function (Ssl, $) {
  /**
   * [navigation description]
   * @method navigation
   * @return {[type]}   [description]
   */
  Ssl.prototype.navigation = function () {
      var _ = this,
          $nav = $('#main-nav'),
          $button = $nav.find('button'),
          $ul = $nav.find('ul').first(),
          $ulNew = $nav.find('ul').clone().addClass('extra-menu').appendTo($nav),
          $li = $ul.find('> li'),
          $liNew = $ulNew.find('li');

      $li.each(function () {
        $(this).attr('data-width', $(this).outerWidth(true));
      });

      _.registerResizeEvents.push('navigation.resize');

      // $ulNew.on('mouseenter mouseleave focus blur', function (e) {
      //   $nav.toggleClass('is-open', (e.type === 'mouseenter' || e.type === 'focus'));
      // });

      $button.on('click blur', function (e) {
        if (e.type === 'blur') {
          $nav.removeClass('is-open');
        } else {
          $nav.toggleClass('is-open');
        }
      });

      _.navigation.resize = function () {
        var navwidth = 48 + (_.currentBreakpoint === 'none' ? 48 : 0),
            availablespace =   $ul.outerWidth(true);

        $li.each(function () {
          navwidth += parseInt($(this).attr('data-width'), 10);
          $(this).toggleClass('is-hidden', (navwidth > availablespace));
        });

        var items =    $ul.find('li:not(.is-hidden)').length;
        $liNew.each(function (i) {
          if (i < items) {
            $(this).addClass('is-hidden');
          } else {
            $(this).removeClass('is-hidden');
          }
        });

        $nav.toggleClass('full', (navwidth < availablespace));
        $button.toggleClass('is-hidden', (navwidth < availablespace));

        return this;
      };

    return this;
  };
})(Ssl || {}, jQuery);

/* global Ssl:true, jQuery:true */

'use strict';

;(function (Ssl, $) {
  /**
   * [search description]
   * @method search
   * @return {[type]} [description]
   */
  Ssl.prototype.search = function () {
    var _ = this,
        $wrapper = $('#search-header'),
        $search = $('#search-field'),
        $button = $('#search-button'),
        $list = $('<ul></ul>'),
        $current = $list.find('li').first(),
        total = 0,
        debounce, open, close, goto;

    $search.wrap($('<div></div>').addClass('autocomplete'));
    $list.appendTo($search.parent());

    open = function () {
      $list.addClass('is-visible');
    };

    close = function () {
      window.setTimeout(function () {
        $list.removeClass('is-visible');
      }, 200);
    };

    goto = function ($item) {
      if ($item.attr('data-href') === undefined ) {
        $wrapper.find('form').submit();
      } else {
        window.location.href = $item.attr('data-href');
      }
    };

    debounce = _.debounce(function (val) {
      $.ajax({
        url: 'http://dev.2.skanskalandskap.se/umbraco/Surface/SearchSurface/CreateAutoSuggestions/',
        data: {
          language : 'SV',
          term : val,
          limit : 10
        },
        type: 'GET',
        dataType: 'json'
      })
      .success(function (result) {
        // Add extra test data right now
        var $ul = $('<ul></ul>'),
            $li;

        total = result.total;

        if (total) {
          for (var i = 0, l = result.result.length; i < l; i++) {
            $li = $('<li></li>').attr({
              'data-href' : result.result[i].Url
            }).html(
              '<span class="title">' + result.result[i].Title + '</span>' +
              '<span class="description">' + result.result[i].PreviewText + '</span>'
            ).appendTo($ul);
          }
          if (total > 10) {
            $li = $('<li></li>').addClass('more-results').attr({
              'data-href' : 'sokresultat.aspx?term=' + val
            }).html(
              '<span class="title">Fler resultat...</span>'
            ).appendTo($ul);
          }
          $list.html($ul.html());
          open();
        } else {
          close();
        }
      })
      .error(function (data) {
        // Do something?
      });
    }, 200);

    $search.on('focus blur', function (e) {
      if (e.type === 'blur' && total) {
        close();
      } else {
        open();
      }
    });

    $search.on('keydown', function (e) {
      // Prevent cursor jumping
      if (e.keyCode === 38 || e.keyCode === 40 || e.keyCode === 27) {
        return false;
      }
    });

    $search.on('keyup', function (e) {
      // Perform search
      var val = $search.val();
      if (val.length > 2) {
        if (e.keyCode !== 13 && e.keyCode !== 38 && e.keyCode !== 40 && e.keyCode !== 27) {
          debounce(val);
        }

        switch (e.keyCode) {
          // Up
          case 38:
            if (!$current.hasClass('is-selected')) {
              $current = $list.find('li').last().addClass('is-selected');
            } else {
              $current.removeClass('is-selected');
              if ($current.prev()[0]) {
                $current = $current.prev().addClass('is-selected');
              } else {
                $current = $list.find('li').last().addClass('is-selected');
              }
            }
          break;

          // Down
          case 40:
            if (!$current.hasClass('is-selected')) {
              $current = $list.find('li').first().addClass('is-selected');
            } else {
              $current.removeClass('is-selected');
              if ($current.next()[0]) {
                $current = $current.next().addClass('is-selected');
              } else {
                $current = $list.find('li').first().addClass('is-selected');
              }
            }
          break;

          // Enter
          case 13:
            goto($list.find('.is-selected'));
          break;

          // Escape
          case 27:
            close();
          break;
        }
      } else {
        close();
      }
    });

    $list.on('click', 'li', function () {
      goto($(this));
    });

    $button.on('click', function (e) {
      e.preventDefault();

      if ($wrapper.hasClass('is-open') && $search.val()) {
        $wrapper.find('form').submit();
      } else {
        $wrapper.toggleClass('is-open');
        if ($wrapper.hasClass('is-open')) {
          window.setTimeout(function () {
            $search.focus();
          }, 300);
        }
      }

      return false;
    });

    return this;
  };
})(Ssl || {}, jQuery);

/* global Ssl:true */

'use strict';

// Just kickstarts the js
;(function () {
  return new Ssl();
})();
