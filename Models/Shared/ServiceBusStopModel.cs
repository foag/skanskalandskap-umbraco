﻿
namespace SkanskaLandskap15.Models.Shared
{
    public class ServiceBusStopModel : ServiceModel
    {        
        public string NodeName { get; set; }
        public int AreaNodeId { get; set; }
        public int ServiceTypeNodeId { get; set; }
        public MapMarkerModel MapMarker { get; set; }    
        public FindUsModel FindUs { get; set; }
        public string BusStopTerm { get; set; }
        public string GetHereFromTerm { get; set; }
        public string SearchTerm { get; set; }
        public string Language { get; set; }
        public ServiceTypeModel ServiceTypeObject { get; set; }
    }
}
