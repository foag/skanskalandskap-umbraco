﻿using System.Collections.Generic;

namespace SkanskaLandskap15.Models.Shared
{

    // PARENT CLASS FOR ACTIVITY TYPE AND SERVICE TYPE
    
    public class MapInteractivityTypeModel
    {        
        public int NodeId { get; set; }
        public string Name { get; set; }
        public string IconPath { get; set; }
        public int Counts { get; set; }
    }
}
