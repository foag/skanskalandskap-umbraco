﻿using System.Collections.Generic;


namespace SkanskaLandskap15.Models.Shared
{
    public class MapInteractivityModel
    { 
        public string ContentType { get; set; }
        public string MapType { get; set; }
        public int Counts { get; set; }

        public IEnumerable<ActivityTypeModel> ActivityTypesCollection { get; set; }
        public IEnumerable<ServiceTypeModel> ServiceTypesCollection { get; set; }
       
        public string DisplayAllTerm { get; set; }
        public string SearchTerm { get; set; }
        public string GetHereFromTerm { get; set; }
    }
}