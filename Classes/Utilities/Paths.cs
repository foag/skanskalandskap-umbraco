﻿using System;
using SkanskaLandskap15.Controllers.Masters;


namespace SkanskaLandskap15.Classes.Utilities
{
    public class Paths 
    {
               
        public Paths() {}

        // Returns the url to get a ResRobot xml result from Trafiklab
        public static string GetTrafikLabUrl(String requestType)
        {
            // Initiate return value
            string url = string.Empty;

            // NOTE: Avoid using the "location.name.xml" format since this makes the url encoded twice due to a bug at Trafiklab.
            // Instead add "&format=xml" at the end of the url.
            switch(requestType)
            {
                case "by name":
                    url = "https://api.resrobot.se/v2/location.name?key=" + MasterController.GetTrafikLabApiKey();
                    //url = "https://api.resrobot.se/v2/location.name.xml?key=" + MasterController.GetTrafikLabApiKey();
                   
                    break;
                case "by nearby":
                    url = "https://api.resrobot.se/v2/location.nearbystops?key=" + MasterController.GetTrafikLabApiKey();
                    //url = "https://api.resrobot.se/location.nearbystops.xml?key=" + MasterController.GetTrafikLabApiKey();
                    break;
            }
            return url;
        }

       
        // Returns the domain name due to language used on the development server
        public static string GetDevDomainName(string language)
        {
            // Initiate return value
            string domainName = String.Empty;

            // Get domain name due to language
            switch(language)
            {
                case "SV":
                    domainName = "dev.2.skanskalandskap.se";
                    break;

                 case "EN":
                    domainName = "dev.2.en.skanskalandskap.se";
                    break;
            }
            return domainName;
        }


        // Returns the logo file path due to language.
        public static string GetLogoPath(string language)
        {
            // Initiate return value
            string logoPath = String.Empty;

            // Get path due to language
            switch (language)
            {
                case "SV":
                    logoPath = "/assets/images/logo.svg";
                    break;

                case "EN":
                    logoPath = "/assets/images/logo_en.svg";
                    break;
            }
            return logoPath;
        }


        // Returns an icon file path.
        public static string GetIconPath(string iconType)
        {
            // Initiate return value
            string path = String.Empty;

            // Get path due to icon type
            switch (iconType)
            {  
                case "document":
                    path = "/assets/images/dokument.svg";
                    break;                                   

                case "marker":
                    path = "/assets/images/karta.svg";
                    break;

                case "bus":
                    path = "/assets/images/bus.svg";
                    break;

                case "car":
                    path = "/assets/images/car.svg";
                    break;

                case "utiskane":
                    path = "/assets/images/folder.svg";
                    break;

                case "folder":
                    path = "/assets/images/folder.svg";
                    break;

                case "warning":
                    path = "/assets/images/varning.svg";
                    break;
            }
            return path;
        }


        // Returns file path for map thumbnail (used on the Accommodations landing page).
        public static string GetMapThumbnail(string urlName)
        {
            // Initiate return value
            string path = String.Empty;

            // Construct path
            path = "/assets/images/thumbnailmaps/" + urlName + ".jpg";

            return path;
        }



        public static string GetStandardAreaMarkerPath()
        {
            return "/assets/images/area-marker.png";
        }
    }
}