﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using SkanskaLandskap15.Classes.Utilities;
using Umbraco.Core;
using Umbraco.Web;


namespace dashboardcontrols
{
    public partial class CompleteRatings : System.Web.UI.UserControl
    {
        protected DataTable dtStrovomraden;
        protected DataTable dtRecreationAreas;
        protected DataTable dtGora;
        protected DataTable dtThingsToDo;
        protected DataTable dtBoende;
        protected DataTable dtWhereToStay;

        protected CultureInfo Locale { get; set; }

         // Initiate global database variables        
        protected SqlConnection conn { get; set; }
        String ConnectionString = ConfigurationManager.ConnectionStrings["umbracoDbDSN"].ToString();
        String SqlStatement = String.Empty;


        public CompleteRatings()
        {
            // Initialize the class with some default values          
            //numOfListItems = 0;
        }

        protected void Page_Load(object sender, EventArgs e) { }

        /* To be able to sort multiple gridviews, data sets must be generated within Page_Init */
        protected void Page_Init(object sender, EventArgs e)
        {
           
            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get language content node id:s   
            string nodeIdSwedish = helper.ContentAtXPath("//" + MyConstants.StartPageDocTypeAlias).First().Id.ToString();  // Swedish site
            string nodeIdEnglish = helper.ContentAtXPath("//" + MyConstants.HomePageDocTypeAlias).First().Id.ToString();   // English site

            // Set Swedish Locale to make sure the datatables are sorted correctly with respect to åäö             
            CultureInfo myCultureInfo = new CultureInfo("sv-Se");

            // Get a FileService instance
            var fileService = ApplicationContext.Current.Services.FileService;
            

            /***** Manage Recreational Areas statistics ***/
            
            // Make sure there is a template with the specified alias
            if (fileService.GetTemplate(MyConstants.AreaPageTemplateAlias) != null)
            {
                // Get template's id
                int templateIdAreaPage = fileService.GetTemplate(MyConstants.AreaPageTemplateAlias).Id;

                // Get Strövområden data
                dtStrovomraden = GetDataTable(templateIdAreaPage, nodeIdSwedish);
                dtStrovomraden.Locale = myCultureInfo;

                // Assign datatable to current GridView
                gdvStrovomraden.DataSource = dtStrovomraden;

                // Get Recreation Areas data
                dtRecreationAreas = GetDataTable(templateIdAreaPage, nodeIdEnglish);
                dtRecreationAreas.Locale = myCultureInfo;

                // Assign datatable to current GridView
                gdvRecreationAreas.DataSource = dtRecreationAreas;                
            }
            else
            {
                // Enable the GridView's EmptyDataText property
                gdvStrovomraden.DataSource = null;                
                gdvRecreationAreas.DataSource = null;
            }

            // Bind the results to current GridViews
            gdvStrovomraden.DataBind();
            gdvRecreationAreas.DataBind();



            /***** Manage Things To Do statistics ***/

            if(fileService.GetTemplate(MyConstants.ThingsToDoTemplateAlias) != null)
            {
                // Get template's id
                int templateIdThingsToDo = fileService.GetTemplate(MyConstants.ThingsToDoTemplateAlias).Id;

                // Get Göra data
                dtGora = GetDataTablePublished(templateIdThingsToDo, nodeIdSwedish);
                dtGora.Locale = myCultureInfo;

                // Assign datatable to current GridView
                gdvGora.DataSource = dtGora;               

                // Get Things To Do data
                dtThingsToDo = GetDataTablePublished(templateIdThingsToDo, nodeIdEnglish);
                dtThingsToDo.Locale = myCultureInfo;

                // Assign datatable to current GridView
                gdvThingsToDo.DataSource = dtThingsToDo;                
            }
            else
            {
                // Enable the GridView's EmptyDataText property
                gdvGora.DataSource = null;
                gdvThingsToDo.DataSource = null;
            }

            // Bind the results to current GridViews
            gdvGora.DataBind();
            gdvThingsToDo.DataBind();


            /***** Manage Where To Stay statistics ***/

            if (fileService.GetTemplate(MyConstants.WhereToStayTemplateAlias) != null)
            {
                // Get template's id
                int templateIdWhereToStay = fileService.GetTemplate(MyConstants.WhereToStayTemplateAlias).Id;

                // Get Boende data
                dtBoende = GetDataTable(templateIdWhereToStay, nodeIdSwedish);
                dtBoende.Locale = myCultureInfo;

                // Assign datatable to current GridView
                gdvBoende.DataSource = dtBoende;               

                // Get Where To Stay data
                dtWhereToStay = GetDataTable(templateIdWhereToStay, nodeIdEnglish);
                dtWhereToStay.Locale = myCultureInfo;

                // Assign datatable to current GridView
                gdvWhereToStay.DataSource = dtWhereToStay;               
            }
            else
            {
                // Enable the GridView's EmptyDataText property
                gdvBoende.DataSource = null;
                gdvWhereToStay.DataSource = null;
            }

            // Bind the results to current GridViews
            gdvBoende.DataBind();
            gdvWhereToStay.DataBind();
        }



        private DataTable GetDataTable(int nodeIdTemplate, string nodeIdLanguage)
        {
            // Initiate the return value
            DataTable resultDt = new DataTable();

            try
            {

                // Create a new SQL Connection object
                using (conn = new SqlConnection(ConnectionString))
                {

                    // Create the sql statement
                    string sqlStatement = "SELECT doc.nodeId 'NodeId', LTRIM(RTRIM(doc.text)) 'Name', "
                        + "CAST(star.voteValue/CONVERT(decimal,star.voteNr) As dec(12,1)) 'Average', "
                        + "star.voteValue 'Totals', "
                        + "star.voteNr 'Votes' "
                        + "FROM cmsDocument doc LEFT JOIN xtraStarRating star "
                        + "ON doc.nodeId = star.uniqueId "
                        + "INNER JOIN umbracoNode umnode ON doc.nodeId = umnode.id "
                        + "AND doc.published = 1 "
                        + "AND doc.templateId = " + @nodeIdTemplate
                        + " AND umnode.path Like '%" + @nodeIdLanguage + "%' "
                        + "ORDER BY star.voteValue/CONVERT(decimal,star.voteNr) DESC, star.voteValue DESC, LTRIM(RTRIM(doc.text))";

                    // Open the database connection
                    conn.Open();

                    // Create a new SQL Command
                    using (SqlCommand cmd = new SqlCommand(sqlStatement, conn))
                    {
                        // Parameterize
                        cmd.Parameters.Add("@nodeIdTemplate", SqlDbType.Int);
                        cmd.Parameters["@nodeIdTemplate"].Value = nodeIdTemplate;
                        cmd.Parameters.Add("@nodeIdLanguage", SqlDbType.NVarChar, 150);
                        cmd.Parameters["@nodeIdLanguage"].Value = nodeIdLanguage;

                        resultDt.Load(cmd.ExecuteReader());
                    }

                    // Close the database connection
                    conn.Close();                    
                }          
                  

                int numOfListItems = resultDt.Rows.Count;

                if (numOfListItems > 0)
                {
                    // Add a column to the data table to hold the item's placement in the list
                    DataColumn placement = new DataColumn("Placement", typeof(int));
                    resultDt.Columns.Add(placement);

                    // Add a column to the data table to hold the item's linked name
                    DataColumn linkedName = new DataColumn("LinkedName", typeof(string));
                    resultDt.Columns.Add(linkedName);

                    // Add a column to the data table to hold a hidden version of the the item's name, cleared from the link tag
                    DataColumn hiddenName = new DataColumn("HiddenName", typeof(string));
                    resultDt.Columns.Add(hiddenName);

                    // Add a column to the data table to hold the item's average value or a hyphen character
                    DataColumn averageValue = new DataColumn("AverageValue", typeof(string));
                    resultDt.Columns.Add(averageValue);

                    // Loop through the data table to manipulate some values
                    int counter = 0;
                    foreach (DataRow dr in resultDt.Rows)
                    {
                        // Add the placement value
                        resultDt.Rows[counter]["Placement"] = counter + 1;

                        // Create the item's URL based on the node id
                        string url = umbraco.library.NiceUrl(Convert.ToInt32(resultDt.Rows[counter]["NodeId"]));

                        // Update Name to an html link element
                        resultDt.Rows[counter]["LinkedName"] = "<a href=" + url + " target='_blank'>" + resultDt.Rows[counter]["Name"] + "</a>";

                        // Store the item name in a hidden column for sorting purposes
                        resultDt.Rows[counter]["HiddenName"] = resultDt.Rows[counter]["Name"];

                        // Assign the average value to the display column
                        resultDt.Rows[counter]["AverageValue"] = resultDt.Rows[counter]["Average"].ToString();

                        // If there's no Votes value (item has not been rated)...  
                        if (resultDt.Rows[counter]["Votes"] == DBNull.Value)
                        {
                            resultDt.Rows[counter]["Votes"] = 0;
                            resultDt.Rows[counter]["AverageValue"] = "-";
                        }

                        // Update data table
                        resultDt.AcceptChanges();

                        // Enumerate counter
                        counter += 1;
                    }
                }
            }
            catch(Exception ex)
            {
                // Return a nullified datatable should there be a database error
                resultDt = null;

                // Set an error message
                lblErrorMessage.Text = "Ett fel genererades: " + ex.Message;
            }          
            return resultDt;
        }



        private DataTable GetDataTablePublished(int nodeIdTemplate, string nodeIdLanguage)
        {

            // Initiate the return value
            DataTable finalDt = new DataTable();

            try
            {

                // Initate a data table
                DataTable resultDt = new DataTable();

               // Create a new SQL Connection object
                using (conn = new SqlConnection(ConnectionString))
                {

                    // Create the sql statement
                    string sqlStatement = "SELECT doc.nodeId 'NodeId', LTRIM(RTRIM(doc.text)) 'Name', "
                        + "CAST(star.voteValue/CONVERT(decimal,star.voteNr) As dec(12,1)) 'Average', "
                        + "star.voteValue 'Totals', "
                        + "star.voteNr 'Votes', "
                        + "CAST(doc.published As CHAR) 'Published' "
                        + "FROM cmsDocument doc LEFT JOIN xtraStarRating star "
                        + "ON doc.nodeId = star.uniqueId "
                        + "INNER JOIN umbracoNode umnode ON doc.nodeId = umnode.id "
                        + "AND ((doc.published = 1 AND doc.newest = 0) OR (doc.published = 0 AND doc.newest = 1)) "
                        + "AND doc.templateId = " + @nodeIdTemplate
                        + " AND umnode.path Like '%" + @nodeIdLanguage + "%' "
                        + "ORDER BY CAST(star.voteValue/CONVERT(decimal,star.voteNr) As dec(12,1)) DESC, star.voteValue DESC, doc.updateDate ASC, LTRIM(RTRIM(doc.text))";

                    // Open the database connection
                    conn.Open();

                    // Create a new SQL Command
                    using (SqlCommand cmd = new SqlCommand(sqlStatement, conn))
                    {
                        // Parameterize
                        cmd.Parameters.Add("@nodeIdTemplate", SqlDbType.Int);
                        cmd.Parameters["@nodeIdTemplate"].Value = nodeIdTemplate;
                        cmd.Parameters.Add("@nodeIdLanguage", SqlDbType.NVarChar, 150);
                        cmd.Parameters["@nodeIdLanguage"].Value = nodeIdLanguage;

                        resultDt.Load(cmd.ExecuteReader());
                    }

                    // Close the database connection
                    conn.Close(); 
                }

                int numOfListItems = resultDt.Rows.Count;

                if (numOfListItems > 0)
                {
                    // Add columns to the final data table
                    DataColumn placement = new DataColumn("Placement", typeof(int));
                    finalDt.Columns.Add(placement);

                    DataColumn linkedName = new DataColumn("LinkedName", typeof(string));
                    finalDt.Columns.Add(linkedName);

                    // Add a column to the data table to hold a hidden version of the the item's name, cleared from the link tag
                    DataColumn hiddenName = new DataColumn("HiddenName", typeof(string));
                    finalDt.Columns.Add(hiddenName);

                    DataColumn averageValue = new DataColumn("AverageValue", typeof(string));
                    finalDt.Columns.Add(averageValue);

                    DataColumn votes = new DataColumn("Votes", typeof(string));
                    finalDt.Columns.Add(votes);

                    // Add a column to the data table to hold the item's placement in the list
                    DataColumn publishedValue = new DataColumn("PublishedValue", typeof(string));
                    finalDt.Columns.Add(publishedValue);


                    // Loop through the original data table to sort out duplicates
                    int counterOriginal = 0;
                    int counterFinal = 0;
                    bool duplicates = false;
                    foreach (DataRow dr in resultDt.Rows)
                    {
                        // Always display first item
                        if (counterOriginal > 0)
                        {
                            // Reset the duplicates boolean
                            duplicates = false;
                            int currentId = Convert.ToInt32(resultDt.Rows[counterOriginal]["NodeId"]);

                            // Loop through the item id's that's been processed so far 
                            for (int i = 0; i < counterOriginal; i++)
                            {
                                // If current id already exist, mark it as a duplicate
                                int previousId = Convert.ToInt32(resultDt.Rows[i]["NodeId"]);
                                if (currentId == previousId)
                                {
                                    duplicates = true;
                                    break;
                                }
                            }
                        }

                        // Go on processing current item provided it's not a duplicate
                        if (duplicates == false)
                        {
                            // Add a new row to the final data table
                            finalDt.Rows.Add(1);

                            // Add the placement value
                            finalDt.Rows[counterFinal]["Placement"] = counterFinal + 1;

                            // Assign the published value (1 or 0) to a variable
                            string published = resultDt.Rows[counterOriginal]["Published"].ToString().Trim();

                            // Assign a readable published value; create a linked element for the Name property if published is true. 
                            if (published == "1")
                            {

                                // Create the item's URL based on the node id
                                string url = umbraco.library.NiceUrl(Convert.ToInt32(resultDt.Rows[counterOriginal]["NodeId"]));

                                // Update Name to an html link element
                                finalDt.Rows[counterFinal]["LinkedName"] = "<a href=" + url + " target='_blank'>" + resultDt.Rows[counterOriginal]["Name"] + "</a>";
                                finalDt.Rows[counterFinal]["PublishedValue"] = "Ja";
                            }
                            else
                            {
                                finalDt.Rows[counterFinal]["LinkedName"] = resultDt.Rows[counterOriginal]["Name"];
                                finalDt.Rows[counterFinal]["PublishedValue"] = "Nej";
                            }

                            // Store the item name in a hidden column for sorting purposes
                            finalDt.Rows[counterFinal]["HiddenName"] = resultDt.Rows[counterOriginal]["Name"];

                            // Assign the average value to the display column
                            finalDt.Rows[counterFinal]["AverageValue"] = resultDt.Rows[counterOriginal]["Average"].ToString();

                            // If there's no Votes value (item has not been rated)...  
                            if (resultDt.Rows[counterOriginal]["Votes"] == DBNull.Value)
                            {
                                finalDt.Rows[counterFinal]["Votes"] = 0;
                                finalDt.Rows[counterFinal]["AverageValue"] = "-";
                            }
                            else
                            {
                                finalDt.Rows[counterFinal]["Votes"] = resultDt.Rows[counterOriginal]["Votes"].ToString();
                            }

                            // Update data table
                            finalDt.AcceptChanges();

                            // Enumerate counter
                            counterFinal += 1;
                        }
                        counterOriginal += 1;
                    }
                }
            }
            catch(Exception ex)
            {
                // Return a nullified datatable should there be a database error
                finalDt = null;

                // Set an error message
                lblErrorMessage.Text = "Ett fel genererades: " + ex.Message;
            }         
            return finalDt;
        }



        public string GetSortDirection(string SortExpression)
        {
            if (ViewState[SortExpression] == null)
            {
                ViewState[SortExpression] = "Asc";
            }
            else
                ViewState[SortExpression] = ViewState[SortExpression].ToString() == "Desc" ? "Asc" : "Desc";

            return ViewState[SortExpression].ToString();
        }


        protected void gdvStrovomraden_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
        {
            //Use DataView for sorting DataTable's data 
            DataView view = dtStrovomraden.DefaultView;
            view.Sort = String.Format("{0} {1}", e.SortExpression, GetSortDirection(e.SortExpression));
            gdvStrovomraden.DataSource = view;
            gdvStrovomraden.DataBind();
        }

        protected void gdvRecreationAreas_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
        {
            //Use DataView for sorting DataTable's data 
            DataView view = dtRecreationAreas.DefaultView;
            view.Sort = String.Format("{0} {1}", e.SortExpression, GetSortDirection(e.SortExpression));
            gdvRecreationAreas.DataSource = view;
            gdvRecreationAreas.DataBind();
        }

        protected void gdvGora_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
        {
            //Use DataView for sorting DataTable's data 
            DataView view = dtGora.DefaultView;
            view.Sort = String.Format("{0} {1}", e.SortExpression, GetSortDirection(e.SortExpression));
            gdvGora.DataSource = view;
            gdvGora.DataBind();
        }

        protected void gdvThingsToDo_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
        {
            //Use DataView for sorting DataTable's data 
            DataView view = dtThingsToDo.DefaultView;
            view.Sort = String.Format("{0} {1}", e.SortExpression, GetSortDirection(e.SortExpression));
            gdvThingsToDo.DataSource = view;
            gdvThingsToDo.DataBind();
        }

        protected void gdvBoende_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
        {
            //Use DataView for sorting DataTable's data 
            DataView view = dtBoende.DefaultView;
            view.Sort = String.Format("{0} {1}", e.SortExpression, GetSortDirection(e.SortExpression));
            gdvBoende.DataSource = view;
            gdvBoende.DataBind();
        }

        protected void gdvWhereToStay_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
        {
            //Use DataView for sorting DataTable's data 
            DataView view = dtWhereToStay.DefaultView;
            view.Sort = String.Format("{0} {1}", e.SortExpression, GetSortDirection(e.SortExpression));
            gdvWhereToStay.DataSource = view;
            gdvWhereToStay.DataBind();
        }
    }


    //public static class Extensions
    //{
    //    public static decimal GetMedian(this int[] array)
    //    {
    //        int[] tempArray = array;
    //        int count = tempArray.Length;

    //        Array.Sort(tempArray);

    //        decimal medianValue = 0;

    //        if (count % 2 == 0)
    //        {
    //            // count is even, need to get the middle two elements, add them together, then divide by 2
    //            int middleElement1 = tempArray[(count / 2) - 1];
    //            int middleElement2 = tempArray[(count / 2)];
    //            medianValue = (middleElement1 + middleElement2) / 2;
    //        }
    //        else
    //        {
    //            // count is odd, simply get the middle element.
    //            medianValue = tempArray[(count / 2)];
    //        }

    //        return medianValue;
    //    }
    //}

}