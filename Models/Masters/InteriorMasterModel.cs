﻿using System.Collections.Generic;
using SkanskaLandskap15.Models.Shared;


namespace SkanskaLandskap15.Models.Masters
{
    public class InteriorMasterModel : MasterModel
    {
        public GuestRatingsModel GuestRatingsTerms { get; set; } 
        public IEnumerable<NavigationItemModel> SubNavigation { get; set; }
    }
}