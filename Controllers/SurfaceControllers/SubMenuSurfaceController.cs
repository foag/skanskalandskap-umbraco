﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;




namespace SkanskaLandskap15.Controllers.SurfaceControllers
{
    public class SubMenuSurfaceController : SurfaceController
    {

        // Classes to support dynamically created properties (Duration) for the OutputCache Attribute.
        public static class CacheConfig
        {
            public static int Duration = MasterController.GetOutputCacheDuration("submenu");
        }

        public class MyOutputCacheAttribute : OutputCacheAttribute
        {
            public MyOutputCacheAttribute()
            {
                this.Duration = CacheConfig.Duration;
            }
        }
               
        ///<summary>
        /// Fetches the sub menu for a specific landing page based on its node id. (Typically 
        /// aimed for an ajax request.)</summary>
        /// <param name="contentType">The page's node id.</param>
        /// <param name="language">Current language (SV/EN).</param>       
        /// <returns>A partial populated with a SubMenu object.</returns>  
        [MyOutputCache(VaryByParam = "nodeId;language", Location = OutputCacheLocation.Server)]
        public ActionResult GetSubMenuJson(int nodeId, string language)
        {
            // Initiate the return model
            SubMenuModel model = new SubMenuModel();

            // Make sure language is upper case
            language = language.ToUpper();

            // The culture of an Ajax request thread is neutral and therefore we need to set it 
            // explicitly to be able to get the correct Umbraco context.
            // UmbracoHelperExtensions.EnsureCulture(Thread.CurrentThread, language);
            if (Request.IsAjaxRequest())
            {
                UmbracoHelperExtensions.EnsureCulture(Thread.CurrentThread, language);
            }

            // Get reference to the Umbraco helper             
            UmbracoHelper helper = new UmbracoHelper(UmbracoContext.Current);
            
            // Get content type based on node id
            string contentType = helper.TypedContent(nodeId).DocumentTypeAlias;

            // Get the root node's content type
            MasterController mc = new MasterController();
            string rootDocTypeAlias = mc.GetRootContentType(language);

            
            /* PUFFS */
            
            // Get all content for the puffs section
            model = GetSubMenuPuffs(model, nodeId, contentType, language, helper);

            
            /* LINKS */

            // Get all content for the links section due to content type
            switch (contentType)
            {
                // Get remaining sub pages
                case MyConstants.ThingsToDoLandingPageDocTypeAlias:
                case MyConstants.UsefulInformationLandingPageDocTypeAlias:
                case MyConstants.AboutUsLandingPageDocTypeAlias:
                    model = GetSubMenuLinks(model, nodeId, rootDocTypeAlias, language, helper, model.PuffsCollection);
                    break;

                // Get all sub pages
                default:
                    model = GetSubMenuLinks(model, nodeId, rootDocTypeAlias, language, helper);
                    break;
            }          

            return PartialView("SubMenu", model);
        }



        protected SubMenuModel GetSubMenuPuffs(SubMenuModel model, int nodeId, string contentType, string language, UmbracoHelper helper)
        {

            // Initiate variables
            int maxItems = 0;
            IPublishedContent content;
            string selectedNodes = String.Empty;
            List<SubMenuPuffModel> selectedPuffsCollection = new List<SubMenuPuffModel>();
            bool puffsExists = false;

            // Get content (current landing page)
            content = helper.TypedContent(nodeId);

            // Get puffs due to content type
            switch (contentType)
            {

                // For the following landing pages, get selected sub page puffs 
                case MyConstants.AreasLandingPageDocTypeAlias:
                case MyConstants.ThingsToDoLandingPageDocTypeAlias:
                case MyConstants.AccommodationsLandingPageDocTypeAlias:
                case MyConstants.ForKidsLandingPageDocTypeAlias:
                case MyConstants.UsefulInformationLandingPageDocTypeAlias:
                case MyConstants.AboutUsLandingPageDocTypeAlias:                                   

                    // Get the selected sub page nodes due to content type
                    switch (contentType)
                    {                          
                        // For Things To Do and For Kids landing page - get selected Things To Do and Event puffs
                        case MyConstants.ThingsToDoLandingPageDocTypeAlias:
                        case MyConstants.ForKidsLandingPageDocTypeAlias:
                            selectedNodes = content.GetPropertyValue<string>(MyConstants.tipsForKidsLandingPagePropertyAlias);
                            break;
                        // For any other landing page - get selected sub pages of its own
                        default:
                            selectedNodes = content.GetPropertyValue<string>(MyConstants.TipsSubPagesPropertyAlias);
                            break;
                    }

                    // Check whether at least one sub page was selected
                    if (!string.IsNullOrEmpty(selectedNodes))
                    {
                        // Initiate flag
                        bool accommodationFlag = false;

                        // Accommodation puffs should be treated differently from the rest regarding tags 
                        if (contentType == MyConstants.AccommodationsLandingPageDocTypeAlias)
                        {
                            accommodationFlag = true;
                        }

                        // Get a list of selected sub pages 
                        selectedPuffsCollection = GetSelectedPuffsList(selectedNodes, accommodationFlag, helper, language);
                    }
                    break;


                // For Schools landing page - display news items tagged with "Utomhuspedagogik" 
                case MyConstants.ForSchoolsLandingPageDocTypeAlias:

                    // Set parameters          
                    string tag = MyConstants.ForSchoolsNewsTag;
                    maxItems = MyConstants.PuffsInSubMenu;

                    // Get a News List model containing the specified number of items (max) tagged with 
                    // the specified tag
                    selectedPuffsCollection = GetNewsListByTag(tag, maxItems, helper);
                    break;


                // For News and Events landing page - display closest upcoming events
                case MyConstants.NewsAndEventsLandingPageDocTypeAlias:

                    maxItems = 3;

                    // Get the Events List model, including the default subset of event items
                    selectedPuffsCollection = GetEventsList(maxItems, helper, language);
                    break;
            }

            // Check whether there is at least one sub page node in the collection.
            // (You're able to select unpublished nodes in MNTP. And there might not be any specifically tagged news 
            // or any events at all.)
            if (selectedPuffsCollection.Any())
            {
                // Set flag
                puffsExists = true;
            }

            // Add collection - populated or empty - to the return model
            model.PuffsCollection = selectedPuffsCollection;


            // Published puffs exists
            if (puffsExists)
            {
                // Set boolean for missing title where selected sub pages should not be displayed without a title 
                bool titlesMissing = false;

                // ...so get the puff section's title due to content type
                switch (contentType)
                {
                    case MyConstants.AreasLandingPageDocTypeAlias:
                        model.PuffsTitleTerm = content.GetPropertyValue<string>(MyConstants.titleTipsPropertyAlias);
                        break;

                    case MyConstants.ThingsToDoLandingPageDocTypeAlias:
                    case MyConstants.AccommodationsLandingPageDocTypeAlias:
                    case MyConstants.ForKidsLandingPageDocTypeAlias:
                    case MyConstants.UsefulInformationLandingPageDocTypeAlias:
                    case MyConstants.AboutUsLandingPageDocTypeAlias:
                        model.PuffsTitleTerm = content.GetPropertyValue<string>(MyConstants.titleTipsPropertyAlias).Trim();

                        // Check whether title's missing
                        if (model.PuffsTitleTerm == string.Empty)
                        {
                            titlesMissing = true;
                        }
                        break;
                    
                    case MyConstants.ForSchoolsLandingPageDocTypeAlias:
                        model.PuffsTitleTerm = content.GetPropertyValue<string>(MyConstants.titleTipsPropertyAlias);
                        break;                 

                    case MyConstants.NewsAndEventsLandingPageDocTypeAlias:
                        model.PuffsTitleTerm = content.GetPropertyValue<string>(MyConstants.titleTipsPropertyAlias);
                        break;
                }

                // Empty the collection if title's missing
                if (titlesMissing == true)
                {
                    model.PuffsCollection.Clear();
                }
            }

            // Assign flag to return model
            model.PuffsExists = puffsExists;

            return model;
        }
        



        protected SubMenuModel GetSubMenuLinks(SubMenuModel model, int nodeId, string rootDocTypeAlias, string language, UmbracoHelper helper, List<SubMenuPuffModel> selectedNodes = null)
        {

            // Initiate variables
            IPublishedContent content;
            List<SubMenuLinkModel> listTemp = new List<SubMenuLinkModel>();
            List<SubMenuLinkModel> linksCollection = new List<SubMenuLinkModel>();
            bool linksExists = false;

            // Get content (current landing page)
            content = helper.TypedContent(nodeId);

            // Get links due to content type
            switch (content.DocumentTypeAlias)
            {

                /* ACCOMMODATIONS */

                // Get links to all sub pages, but categorized
                case MyConstants.AccommodationsLandingPageDocTypeAlias:

                    // Get a list of all accommodations tagged with an area
                    List<SubMenuLinkModel> listSubPagesArea = content
                       .Children
                       .Where(c => c.DocumentTypeAlias == MyConstants.AccommodationPageDocTypeAlias)
                       .OrderBy(c => c.Name)
                       .Select(obj => new SubMenuLinkModel()
                       {
                           Title = obj.GetPropertyValue<string>(MyConstants.TitlePropertyAlias),
                           Url = helper.NiceUrl(obj.Id),
                           CategoryId = obj.GetPropertyValue<string>(MyConstants.SubPageTagPropertyAlias)
                       }).ToList();


                    // Get a list of all accommodations of the Boka/Book type 
                    List<SubMenuLinkModel> listSubPagesBook = content
                        .Children
                        .Where(c => c.DocumentTypeAlias == MyConstants.AccommodationPageBookDocTypeAlias)
                        .OrderBy(c => c.Name)
                        .Select(obj => new SubMenuLinkModel()
                        {
                            Title = obj.GetPropertyValue<string>(MyConstants.TitlePropertyAlias),
                            Url = helper.NiceUrl(obj.Id),
                            CategoryId = "book"
                        }).ToList();


                    // Get a list of all accommodations of the Övrigt/Other type 
                    List<SubMenuLinkModel> listSubPagesOther = content
                        .Children
                        .Where(c => c.DocumentTypeAlias == MyConstants.AccommodationPageOtherDocTypeAlias)
                        .OrderBy(c => c.Name)
                        .Select(obj => new SubMenuLinkModel()
                        {
                            Title = obj.GetPropertyValue<string>(MyConstants.TitlePropertyAlias),
                            Url = helper.NiceUrl(obj.Id),
                            CategoryId = "other"
                        }).ToList();

                   
                    // Now, utilize the tags lists to create a categories list 
                    if (listSubPagesArea.Any() || listSubPagesBook.Any() || listSubPagesOther.Any())
                    {
                        model.LinkCategories = CreateAccommodationCategoriesList(listSubPagesArea, listSubPagesBook, listSubPagesOther, helper);                       
                    }
                    
                    // Merge the tags lists
                    listSubPagesBook.AddRange(listSubPagesOther);
                    listSubPagesArea.AddRange(listSubPagesBook);

                    // Assign the merged lists to the links collection
                    linksCollection = listSubPagesArea;

                    break;


                /* ALL SUB PAGES */
                                    
                // Get links to all sub pages 
                case MyConstants.ForKidsLandingPageDocTypeAlias:
                case MyConstants.ForSchoolsLandingPageDocTypeAlias:
                  
                    linksCollection = content
                       .Children
                       .Select(obj => new SubMenuLinkModel()
                       {
                           Title = obj.GetPropertyValue<string>(MyConstants.TitlePropertyAlias),
                           Url = helper.NiceUrl(obj.Id)                           
                       }).ToList();

                    break;


                /* AREAS */

                // Get links to all sub pages but use node name as title
                case MyConstants.AreasLandingPageDocTypeAlias:

                    linksCollection = content
                       .Children
                       .Select(obj => new SubMenuLinkModel()
                       {
                           Title = obj.Name,
                           Url = helper.NiceUrl(obj.Id)
                       }).ToList();

                    break;


                /* REMAINING SUB PAGES */
                
                // For the following content types, get links only to the remaining sub pages 
                case MyConstants.ThingsToDoLandingPageDocTypeAlias:
                case MyConstants.UsefulInformationLandingPageDocTypeAlias:
                case MyConstants.AboutUsLandingPageDocTypeAlias:
                   
                    // Get a temporary list of all sub pages
                    listTemp = content
                       .Children
                       .Select(obj => new SubMenuLinkModel()
                       {
                           NodeId = obj.Id,
                           Title = obj.GetPropertyValue<string>(MyConstants.TitlePropertyAlias),
                           Url = helper.NiceUrl(obj.Id)
                       }).ToList();

                    // Iterate over the temporary list
                    foreach (var item in listTemp)
                    {
                        // Initiate a selected flag
                        bool selected = false;

                        // Check whether current item is in the selected collection
                        foreach (var itemSelected in selectedNodes)
                        {
                            if (item.NodeId == itemSelected.NodeId)
                            {
                                selected = true;
                                break;
                            }
                        }

                        // Item is not in the selected collection 
                        if (selected == false)
                        {   
                            // ...so add it to the links collection
                            linksCollection.Add(item);
                        }
                    }
                    break;


                /* NEWS AND EVENTS */
                
                // For News and Events landing page - get links to all news items
                case MyConstants.NewsAndEventsLandingPageDocTypeAlias:
                    
                    // Get a list of all news sub pages
                    linksCollection = content
                       .Descendants()
                       .Where(c => c.DocumentTypeAlias == MyConstants.NewsPageDocTypeAlias)
                       .OrderByDescending(c => c.GetPropertyValue<DateTime>(MyConstants.PublishingDatePropertyAlias))
                       .Select(obj => new SubMenuLinkModel()
                       {
                           Title = obj.GetPropertyValue<string>(MyConstants.TitlePropertyAlias),
                           Url = helper.NiceUrl(obj.Id)
                       }).ToList();
                    break;
            }

            // Make sure there is at least one published sub page node in the collection.
            if (linksCollection.Any())
            {
                // Set flag
                linksExists = true;
            }

            // Add collection - populated or empty - to the return model
            model.LinksCollection = linksCollection;


            // Published sub pages exists...
            if (linksExists)
            {

                // ...so distribute them in columns
                model = DistributeInColumns(model, linksCollection);

                // ...then get the links section's title
                model.LinksTitleTerm = content.GetPropertyValue<string>(MyConstants.TitleListPropertyAlias);
            }

            // Assign flag to return model
            model.LinksExists = linksExists;

            return model;
        }




        protected SubMenuModel DistributeInColumns(SubMenuModel model, List<SubMenuLinkModel> linksCollection)
        {

            // Distribute the sub menu links in three lists (= columns)                                          

            // There are at most 5 links, so store all of them in the first column list 
            if (linksCollection.Count <= 5)
            {
                List<SubMenuLinkModel> tempList = new List<SubMenuLinkModel>();

                foreach (var link in linksCollection)
                {
                    tempList.Add(link);
                }
                model.LinksCollectionColumn1 = tempList;
                model.LinksCollectionColumn2 = new List<SubMenuLinkModel>();
                model.LinksCollectionColumn3 = new List<SubMenuLinkModel>();
            }

            // There are between 6 and 15 links so distribute them from list 1 and upwards with 5 as topmost  
            if (linksCollection.Count > 5 && linksCollection.Count <= 15)
            {
                List<SubMenuLinkModel> tempList1 = new List<SubMenuLinkModel>();
                List<SubMenuLinkModel> tempList2 = new List<SubMenuLinkModel>();
                List<SubMenuLinkModel> tempList3 = new List<SubMenuLinkModel>();

                int counter = 0;
                foreach (var link in linksCollection)
                {
                    counter += 1;

                    if (counter <= 5)
                    {
                        tempList1.Add(link);
                    }
                    else if (counter > 5 && counter <= 10)
                    {
                        tempList2.Add(link);
                    }
                    else if (counter > 10 && counter <= 15)
                    {
                        tempList3.Add(link);
                    }
                }

                model.LinksCollectionColumn1 = tempList1;
                model.LinksCollectionColumn2 = tempList2;
                model.LinksCollectionColumn3 = tempList3;
            }


            // There are more than 15 links so distribute them as equal as possible
            if (linksCollection.Count > 15)
            {

                List<SubMenuLinkModel> tempList1 = new List<SubMenuLinkModel>();
                List<SubMenuLinkModel> tempList2 = new List<SubMenuLinkModel>();
                List<SubMenuLinkModel> tempList3 = new List<SubMenuLinkModel>();

                int linksPerCol = linksCollection.Count / 3;

                int col1 = linksPerCol;
                int col2 = linksPerCol;
                int col3 = linksPerCol;

                switch (linksCollection.Count % 3)
                {
                    case 0:
                        col2 += col1;
                        col3 += col2;
                        break;
                    case 1:
                        col1 += 1;
                        col2 += (col1 + 1);
                        col3 += (col2 - 1);
                        break;
                    case 2:
                        col1 += 1;
                        col2 += (col1 + 1);
                        col3 += col2;
                        break;
                }

                int counter = 0;
                foreach (var link in linksCollection)
                {
                    counter += 1;

                    if (counter <= col1)
                    {
                        tempList1.Add(link);
                    }

                    else if (counter > col1 && counter <= col2)
                    {
                        tempList2.Add(link);
                    }

                    else if (counter > col2 && counter <= col3)
                    {
                        tempList3.Add(link);
                    }
                }

                model.LinksCollectionColumn1 = tempList1;
                model.LinksCollectionColumn2 = tempList2;
                model.LinksCollectionColumn3 = tempList3;
            }

            return model;
        }




        protected List<SubMenuLinkCategoryModel> CreateAccommodationCategoriesList(List<SubMenuLinkModel> listSubPagesArea, List<SubMenuLinkModel> listSubPagesBook, List<SubMenuLinkModel> listSubPagesOther, UmbracoHelper helper)
        {

            // Initiate the return categories list
            List<SubMenuLinkCategoryModel> listCategories = new List<SubMenuLinkCategoryModel>();

            // Make sure the area tags list has content
            if (listSubPagesArea.Any())
            {
                // Initiate first position flag
                bool addedFirst = false;

                // Iterate over the list
                foreach (var link in listSubPagesArea)
                {
                    // Initiate category object
                    SubMenuLinkCategoryModel objCat = new SubMenuLinkCategoryModel();

                    // Add a first category to list.
                    if (!addedFirst)
                    {
                        // Set flag
                        addedFirst = true;

                        objCat.CategoryId = link.CategoryId;
                        objCat.CategoryName = helper.TypedContent(link.CategoryId).Name;

                        // Add object to list
                        listCategories.Add(objCat);
                    }

                    // List contains at least one item
                    if (addedFirst == true)
                    {
                        // Initiate found flag
                        bool found = false;

                        // Iterate over the categories list
                        foreach (var cat in listCategories)
                        {
                            // Look for a hit                                 
                            if (link.CategoryId == cat.CategoryId)
                            {
                                found = true;
                                break;
                            }
                        }

                        // The category was not already in the list, so add it 
                        if (!found)
                        {
                            objCat.CategoryId = link.CategoryId;
                            objCat.CategoryName = helper.TypedContent(link.CategoryId).Name;

                            // Add object to list
                            listCategories.Add(objCat);
                        }
                    }
                }

                // Area node categories exists
                if (listCategories.Any())
                {
                    //...so sort the categories alphabetically by the area node name 
                    listCategories.Sort((x, y) => string.Compare(x.CategoryName, y.CategoryName));
                }
            }

            // Add the "Boka/Book" category, if used 
            if (listSubPagesBook.Any())
            {
                // Initiate and populate a category object 
                SubMenuLinkCategoryModel objCat = new SubMenuLinkCategoryModel
                {
                    CategoryId = "book",
                    CategoryName = helper.GetDictionaryValue("Reserve")
                };

                // Add category to list
                listCategories.Add(objCat);
            }

            // Add the "Övrigt/Other" category, if used 
            if (listSubPagesOther.Any())
            {
                // Initiate and populate a category object 
                SubMenuLinkCategoryModel objCat = new SubMenuLinkCategoryModel
                {
                    CategoryId = "other",
                    CategoryName = helper.GetDictionaryValue("Other")
                };

                // Add category to list
                listCategories.Add(objCat);
            }

            // Add categories list to return model
            return listCategories;
        }
        

               


        ///<summary>
        /// Fetches a list of selected sub page puffs.  
        /// </summary>       
        /// <param name="subPageNodes">A string of comma-separated node id:s representing the sub pages.</param>
        /// <param name="parentContentType">The content type of target landing page.</param>
        /// <param name="helper">Instance of the Umbraco helper. Optional.</param>
        /// <returns>A list of SubPagePuff objects.</returns>
        protected List<SubMenuPuffModel> GetSelectedPuffsList(string subPageNodes, bool accommodationFlag, UmbracoHelper helper, string language)
        {
            // Initiate the return list
            List<SubMenuPuffModel> puffList = new List<SubMenuPuffModel>();

            // Convert string to list
            var listNodes = subPageNodes.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            // Convert list to a typed collection of nodes
            var collectionNodes = helper.TypedContent(listNodes);

            // Iterate over the nodes 
            foreach (var item in collectionNodes)
            {
                // Make sure current sub page is published (currently it's possible to select 
                // a non-published item in backoffice).
                if (item != null)
                {
                    SubMenuPuffModel puff = GetSubPagePuff(item.Id, accommodationFlag, helper, language);

                    // Add the puff object to the puff list
                    puffList.Add(puff);
                }
            }

            return puffList;
        }





        protected SubMenuPuffModel GetSubPagePuff(int nodeId, bool accommodationFlag, UmbracoHelper helper, string language)
        {

            // Initiate the return model
            SubMenuPuffModel model = new SubMenuPuffModel();

            // Get content based on current node id
            var content = helper.TypedContent(nodeId);

            
            /* TAG */

            // Initiate tag variable
            var tagValue = string.Empty;

            // Get or create tag due to content type
            switch (content.DocumentTypeAlias)
            {
                // Area page has no tag value so let's use the node name
                case MyConstants.AreaPageDocTypeAlias:
                    tagValue = content.Name;
                    break;
                
                // For Event page we use its start and end dates as tag value
                case MyConstants.EventPageDocTypeAlias:

                    // Create a Swedish culture object
                    CultureInfo swedish = new CultureInfo("sv-SE");

                    // Create a DateTimeFormatInfo object to make it possible to get month name as text...
                    DateTimeFormatInfo dtfi = new DateTimeFormatInfo();

                    // ...in Swedish, provided this is current language 
                    if (language == "SV")
                    {
                        dtfi = swedish.DateTimeFormat;
                    }

                    // Initiate an EventItem object
                    EventItemModel modelEvent = new EventItemModel();

                    // Populate the object with event dates in a nice format
                    modelEvent = EventPageController.CreateNiceEventDates(content, modelEvent, dtfi);

                    // Get tag value (in this case - the event dates).
                    tagValue = modelEvent.DateInfo;
                    break;

                default:
                    tagValue = content.GetPropertyValue<string>(MyConstants.SubPageTagPropertyAlias);
                    break;
            }
          
            // Manage tag due to the sub page's parent node's content type
            switch (accommodationFlag)
            {
                // Any kind of accommodation sub page
                case true:

                    // Manage the accommodation page's tag due to content type
                    switch(content.DocumentTypeAlias)
                    {
                        case MyConstants.AccommodationPageDocTypeAlias:

                            // Make sure an area actually was selected (mandatory but you never know...)
                            if (!string.IsNullOrEmpty(tagValue))
                            {
                                // Tag value is an area's node id             
                                Int32 areaId = Convert.ToInt32(tagValue);

                                // Get content based on node id
                                var areaContent = helper.TypedContent(areaId);

                                // Set node's name as tag
                                model.Tag = areaContent.Name;
                            }
                            break;

                        case MyConstants.AccommodationPageBookDocTypeAlias:
                            model.Tag = helper.GetDictionaryValue("Reserve");
                            break;

                        case MyConstants.AccommodationPageOtherDocTypeAlias:
                            model.Tag = helper.GetDictionaryValue("Other");
                            break;
                    }
                    break;

                // Any other content type
                default:                   
                    model.Tag = tagValue;                   
                    break;
            }


            /* IMAGE */
            
            string imagePicker = content.GetPropertyValue<string>(MyConstants.ImagePickerPropertyAlias);

            // Make sure at least one image exist (required but you never know...)
            if (!string.IsNullOrWhiteSpace(imagePicker))
            {
                // Store the main images to a list
                string[] imagesList = imagePicker.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                // Get first image and add to model
                model.Image = helper.Media(imagesList[0]);
            }


            /* TITLE */

            // Use subtitle as title for Area page
            if (content.DocumentTypeAlias == MyConstants.AreaPageDocTypeAlias)
            {
                model.Title = content.GetPropertyValue<string>(MyConstants.SubtitlePropertyAlias).Trim();
            }
            else
            {
                model.Title = content.GetPropertyValue<string>(MyConstants.TitlePropertyAlias).Trim(); 
            }  
          

            // Url           
            model.Url = helper.NiceUrl(content.Id);
            
            // Node id
            model.NodeId = content.Id;
            
            return model;
        }





        ///<summary>
        /// Fetches a subset of news items that was marked with a specific tag.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <param name="numOfItems">The size of the subset.</param>
        /// <param name="helper">Instance of the Umbraco helper.</param>
        /// <returns>A list of SubMenuPuffModel objects.</returns> 
        protected List<SubMenuPuffModel> GetNewsListByTag(string tag, int numOfItems, UmbracoHelper helper)
        {
            // Initiate the return list
            List<SubMenuPuffModel> newsList = new List<SubMenuPuffModel>();

            // Instanciate Json serializer
            JavaScriptSerializer ser = new JavaScriptSerializer();

            // Get id of the News And Events landing page node
            int nodeId = helper.TypedContentAtXPath("//" + MyConstants.NewsAndEventsLandingPageDocTypeAlias).First().Id;

            // Get content based on node id
            var content = helper.TypedContent(nodeId);

            // Get the published news items (just the node id:s) 
            IEnumerable<NewsItemModel> newsIdList = content
                .Descendants()
                .Where(c => c.DocumentTypeAlias == MyConstants.NewsPageDocTypeAlias)
                .OrderByDescending(c => c.GetPropertyValue<DateTime>(MyConstants.PublishingDatePropertyAlias))
                .Select(obj => new NewsItemModel()
                {
                    NodeId = obj.Id
                }).ToList();
                      

            // Iterate over the news item id:s, filter out the tagged items and get properties 
            newsIdList.ForEach(item =>
            {
                // Make sure upper limit is not reached 
                if (newsList.Count() < numOfItems)
                {
                    // Get content based on current node id
                    var itemContent = helper.TypedContent(item.NodeId);

                    // Initiate Tag flag
                    bool tagFlag = false;

                    // Get tags
                    var tagsValue = itemContent.GetPropertyValue<string>(MyConstants.TagNewsPropertyAlias);

                    if (tagsValue != null)
                    {                       

                        // Deserialize tags value
                        List<string> tags = ser.Deserialize<List<string>>(tagsValue);
                                               
                        // Iterate over the selected tags (one or many)
                        foreach (var itemTag in tags)
                        {
                            // Look for the specified tag
                            if (itemTag == tag)
                            {
                                tagFlag = true;
                                break;
                            }
                        }                
                    }

                    // Current news item was marked with the specified tag 
                    if (tagFlag == true)
                    {
                        // Get the news item's content
                        SubMenuPuffModel newsItem = GetNewsItem(itemContent, helper);

                        // Add the news item object to the news list
                        newsList.Add(newsItem);
                    }
                }
            });
                       
            return newsList;
        }



        protected static SubMenuPuffModel GetNewsItem(IPublishedContent content, UmbracoHelper helper)
        {

            // Initiate the return model
            SubMenuPuffModel model = new SubMenuPuffModel();

            // Image 
            model.Image = helper.Media(content.GetPropertyValue(MyConstants.ImagePickerPropertyAlias));

            // Title
            model.Title = content.GetPropertyValue<String>(MyConstants.TitlePropertyAlias);

            // Tag ( = publish date)
            string publishDate = content.GetPropertyValue<string>(MyConstants.PublishingDatePropertyAlias);
            if (string.IsNullOrWhiteSpace(publishDate))
            {
                publishDate = String.Empty;
            }
            else
            {
                // Remove time value
                publishDate = publishDate.Substring(0, 10);
            }

            // Assign publish date to Tag property
            model.Tag = publishDate;

            // Url
            model.Url = helper.NiceUrl(content.Id);

            // Node id
            model.NodeId = content.Id;

            return model;
        }


             
        protected List<SubMenuPuffModel> GetEventsList(int numOfItems, UmbracoHelper helper, string language)
        {
            // Initiate the return list
            List<SubMenuPuffModel> eventsList = new List<SubMenuPuffModel>();                      

            // Create a Swedish culture object
            CultureInfo swedish = new CultureInfo("sv-SE");

            // Create a DateTimeFormatInfo object to make it possible to get month name as text...
            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();

            // ...in Swedish, provided this is current language 
            if (language == "SV")
            {
                dtfi = swedish.DateTimeFormat;
            }

            // Get id of the NewsAndEvents landing page node
            int nodeId = helper.TypedContentAtXPath("//" + MyConstants.NewsAndEventsLandingPageDocTypeAlias).First().Id;

            // Get all content based on the NewsAndEvents node id
            var content = helper.TypedContent(nodeId);

            // Get the specified subset of published and not outdated events (just the node id:s) 
            IEnumerable<EventItemModel> eventsIdList = content
                .Descendants()
                .Where(c => c.DocumentTypeAlias == MyConstants.EventPageDocTypeAlias && c.GetPropertyValue<DateTime>(MyConstants.EventEndPropertyAlias) >= DateTime.Now)
                .OrderBy(c => c.GetPropertyValue<DateTime>(MyConstants.EventStartPropertyAlias))
                .Take(numOfItems)
                .Select(obj => new EventItemModel()
                {
                    NodeId = obj.Id
                }).ToList();
                   

            // Iterate over the event items and get properties
            eventsIdList.ForEach(item =>
            {
                // Get all content based on current node id
                var itemContent = helper.TypedContent(item.NodeId);

                // Get current node's properties
                SubMenuPuffModel eventsItem = GetEvent(itemContent, helper, dtfi);

                // Set the url to be able to make the event item linked
                eventsItem.Url = helper.NiceUrl(item.NodeId);

                // Add event item to the temp list
                eventsList.Add(eventsItem);
            });
            
            return eventsList;
        }



        protected static SubMenuPuffModel GetEvent(IPublishedContent content, UmbracoHelper helper, DateTimeFormatInfo dtfi)
        {

            // Initiate the return model
            SubMenuPuffModel model = new SubMenuPuffModel();

            // Image 
            model.Image = helper.Media(content.GetPropertyValue(MyConstants.ImagePickerPropertyAlias));

            // Tag - construct readable date and time information 
            EventItemModel eiModel = new EventItemModel();
            eiModel = EventPageController.CreateNiceEventDates(content, eiModel, dtfi);
            model.Tag = eiModel.DateInfo;

            // Title
            model.Title = content.GetPropertyValue<String>(MyConstants.TitlePropertyAlias);

            // Url
            model.Url = helper.NiceUrl(content.Id);

            // Node id
            model.NodeId = content.Id;

            return model;
        }
     

    }
}