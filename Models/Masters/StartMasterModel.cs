﻿using SkanskaLandskap15.Models.Shared;

namespace SkanskaLandskap15.Models.Masters
{
    public class StartMasterModel : MasterModel
    {       
        public AreasListModel AreasList { get; set; }
        public string AreasPagePath { get; set; }

        public string AreaMapTitleTerm { get; set; }
        public string FilterAreasLinkTerm { get; set; }
    }
}