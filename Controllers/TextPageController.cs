﻿using System;
using System.Web;
using System.Web.Mvc;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Models;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Web;



namespace SkanskaLandskap15.Controllers
{
    public class TextPageController : InteriorMasterController
    {

        public ActionResult TextPage()
        {
            var model = new TextPageModel();

            // Note: SubTitle and Intro text is managed in the parent view
                     
            // Image (optional) 
            var mediaId = CurrentPage.GetPropertyValue(MyConstants.ImageSinglePropertyAlias);

            // Check whether image was selected
            if(mediaId != null)
            { 
                model.ImageSingle = Umbraco.Media(mediaId);

                // Image caption
                model.ImageCaption = CurrentPage.GetPropertyValue<string>(MyConstants.ImageCaptionPropertyAlias);
                if (string.IsNullOrWhiteSpace(model.ImageCaption))
                {
                    model.ImageCaption = String.Empty;
                }
            }
            
            // Richtext content
            model.Text = CurrentPage.GetPropertyValue<IHtmlString>(MyConstants.TextPropertyAlias);

            // Add url name
            model.UrlName = CurrentPage.UrlName;
         
            // Add properties for a News list on the "Om oss" page
            if (CurrentPage.UrlName == "om-oss")
            {
                // Add a site part term
                model.SitePart = "Stiftelsen";

                // Add an empty NewsItem object 
                model.NewsItem = new NewsItemModel();
            }
            
            return View(model);
        }
    }
}