﻿
namespace SkanskaLandskap15.Models.Shared
{

    public class ActivityTypeModel : MapInteractivityTypeModel
    {
        public string ActivityAreas { get; set; }
        public string UrlName { get; set; } 
    }
}
