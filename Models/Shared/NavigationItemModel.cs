﻿using System.Collections.Generic;


namespace SkanskaLandskap15.Models.Shared
{
    public class NavigationItemModel
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public int NodeId { get; set; }
        public bool Active { get; set; }
        public bool SubPage { get; set; }
        public string Status { get; set; }

        public string LanguageSwitchUrl { get; set; }
        public string LanguageSwitchText { get; set; }

        public IEnumerable<NavigationItemModel> Children { get; set; }
    }
}