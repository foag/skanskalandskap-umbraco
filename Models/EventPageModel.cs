﻿using SkanskaLandskap15.Models.Masters;
using SkanskaLandskap15.Models.Shared;


namespace SkanskaLandskap15.Models
{
    public class EventPageModel : InteriorMasterModel
    {
        public EventItemModel EventItem { get; set; }
        public string PageTitleTerm { get; set; }
    }
}