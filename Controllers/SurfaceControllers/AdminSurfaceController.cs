﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;



namespace SkanskaLandskap15.Controllers.SurfaceControllers
{
    public class AdminSurfaceController : SurfaceController
    {
 

        [HttpGet]
        public JsonResult UpdateServices()
        {

            // Set flag whether to run in test or sharp mode
            bool saveChanges = true;


            // Initiate return value
            string result = string.Empty;

            // Get reference to the Umbraco helper             
            UmbracoHelper helper = new UmbracoHelper(UmbracoContext.Current);

            // Get id of the English recreation areas landing page node
            int nodeId = Umbraco.TypedContentAtXPath("//" + MyConstants.HomePageDocTypeAlias + "/" + MyConstants.AreasLandingPageDocTypeAlias).First().Id;

            // Get content based on landing start page node id
            var content = helper.TypedContent(nodeId);
            
            // Get a list of all services 
            List<ServiceModel> listServices = content
                .Descendants()
                .Where(c => c.DocumentTypeAlias == MyConstants.ServiceDocTypeAlias)
                .Select(obj => new ServiceModel()
                {
                    NodeId = obj.Id,
                    Title = obj.GetPropertyValue<string>(MyConstants.ServiceNamePropertyAlias),
                    ServiceType = obj.GetPropertyValue<string>(MyConstants.ServiceTypePropertyAlias)
                }).ToList();


            try
            {
                // Get list of terms to change
                List<TermsModel> listTerms = GetTerms();

                // Get the Umbraco Content Service
                var cs = Services.ContentService;

                // Iterate over the service items to translate some Swedish terms in English  
                foreach (var item in listServices)
                {

                    // Jump over Hållplats service type
                    if (item.ServiceType != "Hållplats")
                    {
                        // Get the Service node's content node by its id
                        var itemContent = cs.GetById(item.NodeId);

                        // Seek and change
                        bool changed = SeekAndChange(listTerms, item.Title, itemContent, saveChanges);

                        if (changed)
                        {
                            // Save and publish the updated service                      
                            cs.SaveAndPublishWithStatus(itemContent, 0, true);
                        }                  
                    }                   
                }

                result = "Finished without errors";
            }

            catch(Exception exception)
            {
                result = "Error: " + exception;
            }

            // Return model as Json
            return Json(new { result = result }, JsonRequestBehavior.AllowGet); 
        }




        protected bool SeekAndChange(List<TermsModel> listTerms, string title, IContent content, bool saveChanges)
        {
            // Initiate return value
            bool changed = false;

            foreach (var item in listTerms)
            {
                // Strip white spaces from start and end of title
                title = title.Trim();

                // Make title lowercase
                string titleLC = title.ToLower();

                // Check if current Swedish term exists in title
                if(titleLC.IndexOf(item.TermSE.ToLower()) > -1)
                {   
                    // Initiate variable for the found term
                    string targetWord = string.Empty;

                    // Get the Swedish word's start and end position
                    int posFirst = titleLC.IndexOf(item.TermSE.ToLower());
                    int posLast = item.TermSE.Length;

                    // Get the Swedish word
                    targetWord = title.Substring(posFirst, posLast);
                  
                    // Now replace the target word with the English term
                    string newTitle = title.Replace(targetWord, item.TermEN);

                    // Update the service's Title property
                    if (saveChanges)
                    {
                        content.SetValue(MyConstants.ServiceNamePropertyAlias, newTitle);
                        
                        // Assign the changed full title to the title variable
                        title = newTitle;

                        // Set flag
                        changed = true;
                    }
                }               
            }       

            return changed;
        }



        protected List<TermsModel> GetTerms()
        {
            List<TermsModel> listTerms = new List<TermsModel>();

            TermsModel tm1 = new TermsModel();
            tm1.TermSE = "Parkering";
            tm1.TermEN = "Parking";
            listTerms.Add(tm1);

            TermsModel tm2 = new TermsModel();
            tm2.TermSE = "Rastplats";
            tm2.TermEN = "Picnic area";
            listTerms.Add(tm2);

            TermsModel tm3 = new TermsModel();
            tm3.TermSE = "Grillplats";
            tm3.TermEN = "Barbecue area";
            listTerms.Add(tm3);

            TermsModel tm4 = new TermsModel();
            tm4.TermSE = "Stuga";
            tm4.TermEN = "Cabin";
            listTerms.Add(tm4);

            TermsModel tm5 = new TermsModel();
            tm5.TermSE = "Vindskydd";
            tm5.TermEN = "Windshelter";
            listTerms.Add(tm5);

            TermsModel tm6 = new TermsModel();
            tm6.TermSE = "Besökscentrum";
            tm6.TermEN = "Visitor Centre";
            listTerms.Add(tm6);

            TermsModel tm7 = new TermsModel();
            tm7.TermSE = "Grillhus";
            tm7.TermEN = "Barbecue area";
            listTerms.Add(tm7);

            return listTerms;
        }


        protected class TermsModel
        {
            public string TermEN { get; set; }
            public string TermSE { get; set; }
        }

    }
}