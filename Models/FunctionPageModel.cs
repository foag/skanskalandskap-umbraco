﻿using System.Web;
using SkanskaLandskap15.Models.Masters;


namespace SkanskaLandskap15.Models
{
    public class FunctionPageModel : InteriorMasterModel
    {
        public int NodeId { get; set; }
        public string NodeName { get; set; }
        public string IntroInfo { get; set; }
        public IHtmlString Text { get; set; }
    }
}