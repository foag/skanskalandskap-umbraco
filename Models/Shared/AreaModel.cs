﻿
namespace SkanskaLandskap15.Models.Shared
{
    public class AreaModel
    {
     
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Intro { get; set; }
        public string AltIntro { get; set; }
        public string ImagePicker { get; set; }
        public dynamic Image { get; set; }
        public string ImagePath { get; set; }
        public string Url { get; set; }

        public string MapLocation { get; set; }
        public string RT90YCoordinate { get; set; }
        public string RT90XCoordinate { get; set; }
        public MapMarkerModel MapMarkerData { get; set; }
        
        public string AreaActivities { get; set; }        
        public string Status { get; set; }
        public string Name { get; set; }
        public int NodeId { get; set; }
        public string UrlName { get; set; }

        public FindUsModel FindUs { get; set; }
    }
}