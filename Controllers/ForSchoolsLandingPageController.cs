﻿using System.Linq;
using System.Web.Mvc;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Controllers.SurfaceControllers;
using SkanskaLandskap15.Models;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Web;



namespace SkanskaLandskap15.Controllers
{

    public class ForSchoolsLandingPageController : InteriorMasterController
    {

        ///<summary>
        /// Fetches information for the For Schools landing page.
        /// </summary>      
        /// <returns>A ForSchoolsLandingPage object.</returns> 

        public ActionResult ForSchoolsLandingPage()
        {
            // Initiate the return model
            var model = new ForSchoolsLandingPageModel();

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language ("SV" or "EN")
            var currentLanguage = helper.GetDictionaryValue("Language");

            // Assign the content type
            model.ContentType = CurrentPage.DocumentTypeAlias; 
            
            
            /* NEWS LIST */

            // Create an empty News List object
            NewsListModel objNewsList = new NewsListModel();

            // Set parameters          
            string tag = "Utomhuspedagogik";
            int maxItems = 3;

            // Get controller reference
            NewsSurfaceController nsc = new NewsSurfaceController();

            // Get a News List model containing the specified number of items (max) tagged with 
            // the specified tag string.
            objNewsList = nsc.GetNewsByTag(tag, maxItems, helper);

            model.NewsExists = false;
            // Add title provided news items exist
            if (objNewsList.NewsItemsCollection.Count() > 0)
            {
                model.NewsExists = true;
                model.NewsTitleTerm = CurrentPage.GetPropertyValue<string>(MyConstants.TitleTipsPropertyAlias);
            }
         
            // Add the News List object to the return model         
            model.NewsList = objNewsList;

            return View(model);
        }
    }
}