﻿
namespace SkanskaLandskap15.Models.Shared
{
    public class ImageModel
    {
        public dynamic MediaNode { get; set; }
        public string Name { get; set; }
        public string Caption { get; set; }
        public string Photographer { get; set; }
    }
}