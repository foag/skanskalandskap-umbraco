﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Controllers.SurfaceControllers;
using SkanskaLandskap15.Models;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Core.Models;
using Umbraco.Web;



namespace SkanskaLandskap15.Controllers
{
    public class ContactPersonsPageController : InteriorMasterController
    {

        public ActionResult ContactPage()
        {
            // Initiate the return model
            var model = new ContactPageModel();

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language ("SV" or "EN")
            var currentLanguage = helper.GetDictionaryValue("Language");


            /* CONTACT INFO */

            // Title
            model.Title = CurrentPage.GetPropertyValue<String>(MyConstants.TitlePropertyAlias);

            // Title for the contact information section
            model.ContactInfoTitle = CurrentPage.GetPropertyValue<string>(MyConstants.titleContactInfoPropertyAlias).Trim();

            // Get the puffs collection (Nested Content datatype)
            var infoPuffs = CurrentPage.GetPropertyValue<IEnumerable<IPublishedContent>>(MyConstants.contactInfoModulePropertyAlias);

            // Initiate flag
            model.ContactInfoExists = false;

            // Make sure at least one puff exists
            if (infoPuffs != null)
            {
                // Create controller reference
                PuffsSurfaceController psc = new PuffsSurfaceController();

                // Initiate a temp list
                List<PuffModel> tempListInfoPuffs = new List<PuffModel>();

                // Iterate over the puffs
                foreach (var item in infoPuffs)
                {
                    // Instanciate a Puff object
                    var objPuff = new PuffModel();

                    // Get puff properties 
                    objPuff = psc.GetContactInfoProperties(item, objPuff, helper);

                    // Add puff to temp list
                    tempListInfoPuffs.Add(objPuff);
                }

                // Make sure puffs exists
                if (tempListInfoPuffs.Any())
                {
                    model.ContactInfoExists = true;
                }

                // Add temp puffs list to return model
                model.ContactInfoCollection = tempListInfoPuffs;
            }


            /* CONTACT PERSONS */

            // Title for the contact persons section
            model.ContactPersonsTitle = CurrentPage.GetPropertyValue<string>(MyConstants.titleContactPersonsPropertyAlias).Trim();

            // Get a string of comma-separated Contact node id:s
            var strContacts = CurrentPage.GetPropertyValue<string>(MyConstants.ContactPickerPropertyAlias);

            // Initiate a temporary list
            List<ContactModel> tempList = new List<ContactModel>();

            // Initiate flag
            model.ContactsExists = false;

            // Make sure at least one Contact is selected
            if (!string.IsNullOrEmpty(strContacts))
            {
                // Convert string to list
                var listContacts = strContacts.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                // Convert list to typed collection
                var collectionContacts = helper.TypedContent(listContacts);

                foreach (var item in collectionContacts)
                {
                    // Make sure current contact node is published (it's possible to select a
                    // non-published contact in backoffice).
                    if (item != null)
                    {
                        // Get content based on current node id
                        var itemContent = helper.Content(item.Id);

                        // Get a ContactModel object based on current item's content
                        ContactModel modelC = GetContactProperties(itemContent, currentLanguage, helper);

                        // Add object to the temporary list
                        tempList.Add(modelC);
                    }
                }

                // Make sure contacts exists
                if (tempList.Any())
                {
                    model.ContactsExists = true;
                }
            }

            // Add the temporary list to the return model
            model.ContactsCollection = tempList;


            /* Alternate code sample for retrieving Contacts using the nuPickers Xml PrefetchList datatype */

            // Get the picked contacts
            //dynamic pickedItems = CurrentPage.GetPropertyValue(MyConstants.ContactPickerPropertyAlias);

            //  // Get an Umbraco helper
            //var helper = new UmbracoHelper(UmbracoContext.Current);

            //// Initiate a temporary list
            //List<ContactModel> tempList = new List<ContactModel>();

            //// Iterate over the selected items id:s  
            //foreach (var item in pickedItems.PickedKeys)
            //{
            //    // Get content based on current node id
            //    var itemContent = helper.Content(item);

            //    // Get a ContactModel object based on current item's content
            //    ContactModel modelC = GetContactProperties(itemContent, currentLanguage);

            //    // Add object to the temporary list
            //    tempList.Add(modelC);
            //}

            //// Add the temporary list to the return model
            //model.ContactsCollection = tempList;


            // Display title for the contacts list provided one of the two other sections exist.
            //if (string.IsNullOrEmpty(model.Text.ToString()) && string.IsNullOrEmpty(model.UmbracoForm.FormGuid.ToString()))
            //{
            //    model.ContactTitleTerm = String.Empty;
            //}
            //else
            //{
            //    model.ContactTitleTerm = helper.GetDictionaryValue("Contacts");
            //}

            return View(model);
        }



        public ContactModel GetContactProperties(dynamic itemContent, string language, UmbracoHelper helper)
        {
            // Instanciate a Contact object
            ContactModel model = new ContactModel();

            // Portrait picture
            model.ContactPortrait = helper.Media(itemContent.GetPropertyValue(MyConstants.ContactPortraitPropertyAlias));

            // Name
            model.Name = itemContent.GetPropertyValue(MyConstants.ContactNamePropertyAlias).Trim();

            // Email (optional)
            model.Email = itemContent.GetPropertyValue(MyConstants.ContactEmailPropertyAlias);
            if (string.IsNullOrWhiteSpace(model.Email))
            {
                model.Email = String.Empty;
            }

            // Get properties due to current language
            switch (language)
            {

                case "SV":

                    // Function
                    model.Function = itemContent.GetPropertyValue(MyConstants.ContactFunctionPropertyAlias);

                    // Work assignments (optional)
                    model.WorkAssignments = itemContent.GetPropertyValue(MyConstants.ContactworkAssignmentsPropertyAlias);
                    if (string.IsNullOrWhiteSpace(model.WorkAssignments))
                    {
                        model.WorkAssignments = String.Empty;
                    }

                    // Phones (optional)
                    model.Phone1 = itemContent.GetPropertyValue(MyConstants.ContactPhone1PropertyAlias);
                    if (string.IsNullOrWhiteSpace(model.Phone1))
                    {
                        model.Phone1 = String.Empty;
                    }
                    model.Phone2 = itemContent.GetPropertyValue(MyConstants.ContactPhone2PropertyAlias);
                    if (string.IsNullOrWhiteSpace(model.Phone2))
                    {
                        model.Phone2 = String.Empty;
                    }
                    break;

                case "EN":

                    // Function
                    model.Function = itemContent.GetPropertyValue(MyConstants.ContactFunctionENPropertyAlias);

                    // Work assignments (optional)
                    model.WorkAssignments = itemContent.GetPropertyValue(MyConstants.ContactworkAssignmentsENPropertyAlias);
                    if (string.IsNullOrWhiteSpace(model.WorkAssignments))
                    {
                        model.WorkAssignments = String.Empty;
                    }

                    // Phones (optional)
                    model.Phone1 = itemContent.GetPropertyValue(MyConstants.ContactPhoneEN1PropertyAlias);
                    if (string.IsNullOrWhiteSpace(model.Phone1))
                    {
                        model.Phone1 = String.Empty;
                    }
                    model.Phone2 = itemContent.GetPropertyValue(MyConstants.ContactPhoneEN2PropertyAlias);
                    if (string.IsNullOrWhiteSpace(model.Phone2))
                    {
                        model.Phone2 = String.Empty;
                    }
                    break;
            }

            return model;
        }
    }
}