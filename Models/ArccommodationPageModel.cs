﻿using System.Web;
using System.Collections.Generic;
using SkanskaLandskap15.Models.Masters;
using SkanskaLandskap15.Models.Shared;


namespace SkanskaLandskap15.Models
{
    public class AccommodationPageModel : InteriorMasterModel
    {
        public SlideshowModel Slideshow { get; set; }
        public bool SlideshowExists { get; set; }
        
        public IHtmlString Text { get; set; }
        public IHtmlString CabinPrices { get; set; }
        public IHtmlString CabinReserve { get; set; }
        public string CabinPricesTerm { get; set; }
        public string CabinReserveTerm { get; set; }

        public bool FacilitiesExists { get; set; }
        public FacilitiesModel Facilities { get; set; }
        public dynamic CabinPetOptions { get; set; }
        public dynamic CabinBedOptions { get; set; }
        public string CabinCapacitiesTerm { get; set; }

        public string PuffsAndDownloadsTitle { get; set; }
        public bool PuffsExists { get; set; }
        public string TitlePuffs { get; set; }
        public dynamic PuffModule { get; set; }

        public bool DownloadsExists { get; set; }
        public MediaFilesModel MediaFiles { get; set; }
      
        public dynamic AccommodationTag { get; set; }
        public bool AreaTypeTag { get; set; }
        public bool ShowAreaPuff { get; set; }
        public bool ShowExtendedTexts { get; set; }
        public bool ShowGuestRatings { get; set; }
        public SubPagesListModel SubPagesAreas { get; set; }  
        
    }
}