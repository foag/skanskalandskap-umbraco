﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RJP.MultiUrlPicker.Models;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;




namespace SkanskaLandskap15.Controllers.SurfaceControllers
{
    public class PuffsSurfaceController : SurfaceController
    {

        ///<summary>
        /// Fetches sub pages that was selected in backoffice, typically for a landing page's 
        /// Tips section (or Start or Area page).</summary>       
        /// <param name="nodeId">The calling landing page's node id.</param>
        /// <param name="contentType">The calling landing page's content type.</param>
        /// <param name="selectedNodes">Comma-separated list containing the selected nodes.</param>       
        /// <returns>A partial populated with a SubPagesList object. </returns>      
        [ChildActionOnly]
        public ActionResult GetSelectedPuffs(int nodeId, string contentType, string selectedNodes)
        {
            // Initiate the return value
            SubPagesListModel model = new SubPagesListModel();

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language
            string currentLanguage = helper.GetDictionaryValue("Language");         

            // Explicitly set tips flag
            bool tipsExists = false;

            // Initiate a puffs list
            List<SubPagePuffModel> selectedPuffsCollection = new List<SubPagePuffModel>();

            // Check whether at least one sub page was selected
            if (!string.IsNullOrEmpty(selectedNodes))
            {
                // Get a list of selected sub pages 
                selectedPuffsCollection = GetSelectedPuffsList(selectedNodes, currentLanguage, helper);

                // Make sure there is at least one published sub page node in the collection.
                // (You're able to select unpublished nodes in MNTP.)
                if (selectedPuffsCollection.Any())
                {
                    // Set flag
                    tipsExists = true;
                }
            }

            // Add collection - populated or empty - to the return model
            model.PuffsCollection = selectedPuffsCollection;

            // Published sub pages were selected 
            if (tipsExists)
            {
                // Set boolean for small devices where selected sub pages always should be listed in a gallery 
                model.Gallery = true;

                // Set boolean for missing title where selected sub pages should not be displayed without a title 
                bool titlesMissing = false;

                // Get content based on node id
                var content = helper.TypedContent(nodeId);

                // Get the tips section's title due to content type. (For Schools tips title is set in the
                // landing page's controller.)
                switch (contentType)
                {                    
                    case MyConstants.AreaPageDocTypeAlias:
                    case MyConstants.ThingsToDoLandingPageDocTypeAlias:
                    case MyConstants.ForKidsLandingPageDocTypeAlias:
                    case MyConstants.UsefulInformationLandingPageDocTypeAlias:
                    case MyConstants.AboutUsLandingPageDocTypeAlias:

                        model.TitleTerm = content.GetPropertyValue<string>(MyConstants.titleTipsPropertyAlias).Trim();

                        // Check whether title's missing
                        if(model.TitleTerm == string.Empty)
                        {
                            titlesMissing = true;
                        }
                        break;
                }

                // Empty the collection if title's missing
                if(titlesMissing == true)
                {
                    model.PuffsCollection.Clear();
                }
            }

            return PartialView("PuffsSubPages", model);
        }


     

        ///<summary>
        /// Fetches the sub pages that should be listed - possibly alongside a collection of selected subpage puffs.
        /// </summary>       
        /// <param name="nodeId">The calling landing pages' node id.</param>
        /// <param name="contentType">The calling landing page's content type.</param>
        /// <param name="selectedNodes">Comma-separated list containing the selected nodes, if any. Optional.</param>       
        /// <returns>A partial populated with a SubPagesList object.</returns>       
        [ChildActionOnly]
        public ActionResult GetSubPagePuffs(int nodeId, string contentType, string selectedNodes = "")
        {
            // Initiate the return model
            SubPagesListModel model = new SubPagesListModel();

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language
            string currentLanguage = helper.GetDictionaryValue("Language");

            // Get Read More term from Dictionary
            string readMoreTerm = helper.GetDictionaryValue("ReadMore");

            // Initiate a puffs flag
            bool puffsExists = false;

            // Initiate a puffs list
            List<SubPagePuffModel> puffsCollection = new List<SubPagePuffModel>();

            // Due to content type, get the id of the parent node from which the sub pages should be fetched.
            switch (contentType)
            {

                // Provided selected puffs exists, get the remaining sub pages. Otherwise get all 
                // sub pages. In both cases sorted by nodes.
                case MyConstants.ThingsToDoLandingPageDocTypeAlias:               
                case MyConstants.UsefulInformationLandingPageDocTypeAlias:
                case MyConstants.AboutUsLandingPageDocTypeAlias:
                    if(selectedNodes == string.Empty)
                    {
                        puffsCollection = GetAllPuffsList(nodeId, contentType, currentLanguage, helper, readMoreTerm);
                    }
                    else 
                    {
                        // Make sure there are published nodes in the selected nodes list. Otherwise get all sub pages.
                        puffsExists = CheckSelectedSubPages(selectedNodes, helper);

                        if(puffsExists)
                        {
                            puffsCollection = GetRemainingPuffsList(selectedNodes, nodeId, contentType, currentLanguage, helper, readMoreTerm); 
                        }
                        else
                        {
                            puffsCollection = GetAllPuffsList(nodeId, contentType, currentLanguage, helper, readMoreTerm);
                        }                      
                    }
                    break;

                // Get all sub pages, sorted due to content type (by Tag or node)
                case MyConstants.AccommodationsLandingPageDocTypeAlias:
                case MyConstants.ForKidsLandingPageDocTypeAlias:
                case MyConstants.ForSchoolsLandingPageDocTypeAlias:
                    puffsCollection = GetAllPuffsList(nodeId, contentType, currentLanguage, helper, readMoreTerm);
                    break;
            }

            // Get current node's content 
            var content = helper.TypedContent(nodeId);
                        
            // Make sure the puffs collection is not empty
            if (puffsCollection != null)
            {
                // Add collection to return model
                model.PuffsCollection = puffsCollection;

                // Get the tips section's title due to content type. NOTE: The title for accommodations is managed in 
                // AccommodationsLandingPageController.
                switch (contentType)
                {                    
                    case MyConstants.ThingsToDoLandingPageDocTypeAlias:
                    case MyConstants.ForKidsLandingPageDocTypeAlias:
                    case MyConstants.ForSchoolsLandingPageDocTypeAlias:
                    case MyConstants.UsefulInformationLandingPageDocTypeAlias:
                    case MyConstants.AboutUsLandingPageDocTypeAlias:
                        model.TitleTerm = content.GetPropertyValue<string>(MyConstants.TitleListPropertyAlias);
                        break;
                }
            }

            // Return partial populated with the SubPageList model
            return PartialView("PuffsSubPages", model);
        }




        ///<summary>
        /// Method that opens up for landing page controllers to get a list of all sub pages. 
        /// </summary>       
        /// <param name="nodeId">The calling landing pages' node id.</param>
        /// <param name="contentType">The calling landing page's content type.</param>
        /// <param name="helper">Instance of the Umbraco helper.</param>
        /// <param name="language">Current language (SV/EN).</param>
        /// <returns>A list of SubPagePuff objects.</returns>           
        public List<SubPagePuffModel> GetAllSubPagePuffs(int nodeId, string contentType, UmbracoHelper helper, string language)
        {
            // Initiate the return list
            List<SubPagePuffModel> puffsList = new List<SubPagePuffModel>();
                     
            // Get Read More term from Dictionary
            string readMoreTerm = helper.GetDictionaryValue("ReadMore");

            puffsList = GetAllPuffsList(nodeId, contentType, language, helper, readMoreTerm);         

            return puffsList;
        }





        ///<summary>
        /// Fetches all sub pages without exceptions.
        /// </summary>       
        /// <param name="nodeId">The calling landing page's node id.</param>
        /// <param name="contentType">The calling landing page's content type.</param>
        /// <param name="language">Current language (SV/EN).</param>
        /// <param name="helper">Instance of the Umbraco helper.</param>
        /// <param name="readMoreTerm">Link title from Dictionary.</param>
        /// <returns>A list of SubPagePuff objects.</returns>      
        protected List<SubPagePuffModel> GetAllPuffsList(int nodeId, string contentType, string language, UmbracoHelper helper, string readMoreTerm)
        {
            // Initiate the return list
            List<SubPagePuffModel> puffsList = new List<SubPagePuffModel>();

            // Get node content 
            var content = helper.TypedContent(nodeId);

            // Initiate a temp objects list
            List<SubPagePuffModel> listSubPages = null;

            // Get list of node id:s sorted due to landing page's content type
            switch (contentType)
            {
                // Sort by Tag value (applies to accommodations)
                case MyConstants.AccommodationsLandingPageDocTypeAlias:

                    // Set sort order (by node or by category)
                    string sortOrder = "byNode";

                    listSubPages = CreateAccommodationPuffsList(sortOrder, listSubPages, content, helper);
                    break;

                // Get nodes by tree structure order (applies to For Kids and For Schools.)
                default:
                    listSubPages = content
                        .Children
                        .Select(obj => new SubPagePuffModel()
                        {
                            NodeId = obj.Id
                        }).ToList();               
                    break;
            }

            // Iterate over the objects list
            foreach (var item in listSubPages)
            {
                // Get the puff properties based on the current node content  
                SubPagePuffModel puff = GetSubPagePuff(item.NodeId, helper, language, readMoreTerm);

                // Add the puff object to the temp list
                puffsList.Add(puff);
            }

            return puffsList;
        }




        ///<summary>
        /// Fetches the sub pages that remains after a puffs selection was made.
        /// </summary>       
        /// <param name="selectedNodes">Comma-separated list containing the selected nodes.</param>
        /// <param name="nodeId">The calling landing pages' node id.</param>
        /// <param name="contentType">The calling landing page's content type.</param> 
        /// <param name="language">Current language (SV/EN).</param>
        /// <param name="helper">Instance of the Umbraco helper.</param>
        /// <param name="readMoreTerm">Link title from Dictionary.</param>
        /// <returns>A list of SubPagePuff objects.</returns>       
        protected List<SubPagePuffModel> GetRemainingPuffsList(string selectedNodes, int nodeId, string contentType, string language, UmbracoHelper helper, string readMoreTerm)
        {
            // Initiate the return list
            List<SubPagePuffModel> puffsList = new List<SubPagePuffModel>();           

            // Get current node's content 
            var content = helper.TypedContent(nodeId);

            // Convert string of selected nodes to list
            var listSelectedNodes = selectedNodes.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            // Convert list to a typed collection
            var collectionSelectedNodes = helper.TypedContent(listSelectedNodes);

            // Initiate a temp objects list
            List<SubPagePuffModel> listSubPages;

            // Get list of node id:s due to landing page's content type
            switch (contentType)
            {
                // Get nodes by tree structure order (applies to Things To Do, Useful Information and About Us.)
                case MyConstants.ThingsToDoLandingPageDocTypeAlias:
                case MyConstants.UsefulInformationLandingPageDocTypeAlias:
                    listSubPages = content
                       .Children
                       .Where(c => c.DocumentTypeAlias == MyConstants.GeneralSubPageDocTypeAlias)
                       .Select(obj => new SubPagePuffModel()
                       {
                           NodeId = obj.Id
                       }).ToList();
                    break;

                case MyConstants.AboutUsLandingPageDocTypeAlias:
                    listSubPages = content
                       .Children
                       .Where(c => c.DocumentTypeAlias == MyConstants.GeneralSubPageDocTypeAlias 
                           || c.DocumentTypeAlias == MyConstants.ContactPersonsPageDocTypeAlias 
                           || c.DocumentTypeAlias == MyConstants.CoworkersPageDocTypeAlias)
                       .Select(obj => new SubPagePuffModel()
                       {
                           NodeId = obj.Id
                       }).ToList();
                    break;
              
                // There should be no remaining puffs for other content types
                default:
                    listSubPages = null;
                    break;
            }

            // Iterate over the objects list
            foreach (var item in listSubPages)
            {
                // Initiate a selected flag
                bool selected = false;

                // Check whether node is in the selected collection
                foreach (var itemSelected in collectionSelectedNodes)
                {
                    if (item.NodeId == itemSelected.Id)
                    {
                        selected = true;
                        break;
                    }
                }

                // Disregard selected puff 
                if (selected == false)
                {                   
                    // Get the puff properties based on the current node content  
                    SubPagePuffModel puff = GetSubPagePuff(item.NodeId, helper, language, readMoreTerm);

                    // Add the puff object to the temp list
                    puffsList.Add(puff);
                }
            }
          
            return puffsList;
        }



        ///<summary>
        /// Fetches the properties for a Puffs Slideshow module (typically located on a recreation
        /// area page). 
        /// </summary>       
        /// <returns>A PuffsList object.</returns>      
        [ChildActionOnly]
        public ActionResult PuffsSlideshowModule()
        {

            // Initiate the return model
            var model = new PuffsListModel();

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get a populated PuffsList object
            model = GetSlideShowPuffs(CurrentPage.Id, model);

            // Initiate flag
            model.PuffsExists = false;

            // Make sure at least one slide exist
            if (model.PuffsCollection != null && model.PuffsCollection.Count() > 0)
            {
                // Set flag
                model.PuffsExists = true;

                // Get title for the slideshow puff section
                model.MoreInfoTitle = helper.GetDictionaryValue("MoreInfo");
            }

            // Send the Slideshow model to the partial
            return PartialView("PuffsSlideshowModule", model);
        }




        ///<summary>
        /// Fetches global and local puffs that was selected/created for an area page's puffs slideshow.
        /// </summary>       
        /// <param name="nodeId">Indicates the page that wants to display the puffs.</param>
        /// <returns>A PuffsList object.</returns> 
        protected PuffsListModel GetSlideShowPuffs(int nodeId, PuffsListModel model)
        {            
            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current node's content 
            var content = helper.TypedContent(nodeId);

            // Get the puffs collection (Nested Content datatype)
            var puffs = content.GetPropertyValue<IEnumerable<IPublishedContent>>(MyConstants.PuffsSlideshowModulePropertyAlias);

            // Make sure at least one puff exists
            if (puffs != null && puffs.Count() > 0)
            {  
                // Initiate a temp list
                List<PuffModel> tempListPuffs = new List<PuffModel>();
                               
                // Iterate over the puffs
                foreach (var puff in puffs)
                {
                    // Initiate a "show puff" flag
                    bool showFlag = false;

                    // Instanciate a Puff object
                    var objPuff = new PuffModel();                                     

                    // Manage puff due to content type
                    switch (puff.DocumentTypeAlias)
                    {
                        // Global puff
                        case "PuffGlobal":

                            // Get the global puff's node id (MNTP - only a single puff node can be selected)
                            var puffId = puff.GetPropertyValue<string>(MyConstants.GlobalPuffsPropertyAlias);

                            // Make sure there was a puff node selected
                            if (!string.IsNullOrEmpty(puffId))
                            {
                                // Set flag
                                showFlag = true;

                                // Get global puff content 
                                var contentPuff = helper.TypedContent(puffId.ToString());

                                // Get populated puff object
                                objPuff = GetPuff(contentPuff, helper);
                            }
                            break;

                        // Local puff (slideshow type)
                        case "localPuffSlideShow":

                            // Initiate a hide slide flag
                            string hideSlide = string.Empty;

                            // Get value from DTP List Picker 
                            hideSlide = puff.GetPropertyValue<string>(MyConstants.hidePuffSlidePropertyAlias);

                            // Value null = show slide
                            if (hideSlide == null)
                            {
                                // Set flag
                                showFlag = true;

                                // Get populated puff object
                                objPuff = GetPuff(puff, helper);
                            }
                            break;
                    }

                    // Add puff to temp list
                    if(showFlag)
                    {                        
                        tempListPuffs.Add(objPuff);
                    }
                }

                // Add temp puffs list to return model
                model.PuffsCollection = tempListPuffs;
            }

            // Return the PuffsList model
            return model;
        }



        ///<summary>
        /// Checks whether at least one global or local puff exists for a specific page.
        /// </summary>       
        /// <param name="nodeId">Indicates the page.</param>
        /// <returns>A true flag for the existence of puffs.</returns>      
        public bool PuffsExists(int nodeId, UmbracoHelper helper = null)
        {
            // Initiate return value
            bool puffsExists = false;

            // Get an Umbraco helper
            if (helper == null)
            {
                helper = new UmbracoHelper(UmbracoContext.Current);
            }

            // Get current node's content 
            var content = helper.TypedContent(nodeId);

            // Get the puffs collection (Nested Content datatype)
            var puffs = content.GetPropertyValue<IEnumerable<IPublishedContent>>(MyConstants.PuffModulePropertyAlias);

            // Make sure at least one puff exists
            if (puffs != null && puffs.Count() > 0)
            {
                // Set flag
                puffsExists = true;
            }

            return puffsExists;
        }



        ///<summary>
        /// Fetches global and local puffs that was selected/created for a specific page.
        /// </summary>       
        /// <param name="nodeId">Indicates the page that wants to display the puffs.</param>
        /// <returns>A partial populated with a PuffsList object.</returns>      
        [ChildActionOnly]
        public ActionResult GetPuffs(int nodeId)
        {
            // Initiate the return value
            PuffsListModel model = new PuffsListModel();

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current node's content 
            var content = helper.TypedContent(nodeId);

            // Get the puffs collection (Nested Content datatype)
            var puffs = content.GetPropertyValue<IEnumerable<IPublishedContent>>(MyConstants.PuffModulePropertyAlias);

            // Initiate flag
            model.PuffsExists = false;
                    
            // Initiate a temp list
            List<PuffModel> tempListPuffs = new List<PuffModel>();

            // Iterate over the puffs
            foreach (var puff in puffs)
            {
                // Instanciate a Puff object
                var objPuff = new PuffModel();

                // Manage puff due to content type
                switch (puff.DocumentTypeAlias)
                {
                    // Global puff
                    case "PuffGlobal":

                        // Get the global puff's node id (MNTP - only a single puff node can be selected)
                        var puffId = puff.GetPropertyValue<string>(MyConstants.GlobalPuffsPropertyAlias);

                        // Make sure there was a sub page selected
                        if (!string.IsNullOrEmpty(puffId))
                        {
                            // Get global puff content 
                            var contentPuff = helper.TypedContent(puffId.ToString());

                            // Get puff properties
                            objPuff = GetPuff(contentPuff, helper);
                        }
                        break;

                    // Local puff
                    case "PuffLocal":

                        // Get puff properties 
                        objPuff = GetPuff(puff, helper);
                        break;
                }

                // Add puff to temp list
                tempListPuffs.Add(objPuff);                

                // Add temp puffs list to return model
                model.PuffsCollection = tempListPuffs;
            }

            // Set flag
            if(model.PuffsCollection != null && model.PuffsCollection.Count() > 0)
            {
                model.PuffsExists = true;
            }

            // Send the PuffsList model to the partial
            return PartialView("Puffs", model);
        }




        ///<summary>
        /// Fetches selected Accommodation sub pages to make up a collection of accommodations-in-the-area puffs.
        /// </summary>       
        /// <param name="subPageNodes">A string of comma-separated node id:s representing the selected pages.</param>
        /// <param name="helper">Instance of the Umbraco helper. Optional.</param>
        /// <returns>A list of SubPagePuff objects.</returns>    
        public List<SubPagePuffModel> GetSelectedAccommodationSubPages(string subPageNodes, UmbracoHelper helper = null)
        {

            // Initiate the return list
            List<SubPagePuffModel> puffList = new List<SubPagePuffModel>();

            // Get an Umbraco helper (optional), if missing
            if (helper == null)
            {
                helper = new UmbracoHelper(UmbracoContext.Current);
            }

            // Initiate variable
            string parentContentType = string.Empty;

            // Get Read More term from Dictionary
            string readMoreTerm = helper.GetDictionaryValue("ReadMore");

            // Convert string to list
            var listPuffNodes = subPageNodes.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            // Convert list to a typed collection of nodes
            var collectionPuffs = helper.TypedContent(listPuffNodes);

            // Make sure collection has content. Then get the document type of the parent (landing page) node
            if (collectionPuffs.Any())
            {
                parentContentType = collectionPuffs.First().Parent.DocumentTypeAlias;
            }

            // Iterate over the selected nodes 
            foreach (var item in collectionPuffs)
            {
                // Make sure current sub page is published (currently it's possible to select 
                // a non-published item in backoffice).
                if (item != null)
                {
                    // Get all content based on current node id
                    var itemContent = helper.TypedContent(item.Id);

                    SubPagePuffModel puff = GetAccommodationPuff(itemContent, parentContentType, helper, readMoreTerm);

                    // Add the puff object to the temp list
                    puffList.Add(puff);
                }
            }

            return puffList;
        }



        ///<summary>
        /// Fetches one or more area pages for puffs on the general sub page. Or fetches a single area page for a 
        /// puff on an accommodation page. 
        /// </summary>       
        /// <param name="subPageNodes">A string of comma-separated node id:s representing the selected pages.</param>
        /// <param name="helper">Instance of the Umbraco helper. Optional.</param>
        /// <returns>A list of SubPagePuff objects.</returns>    
        public List<SubPagePuffModel> GetAreaPuffs(string subPageNodes, UmbracoHelper helper = null)
        {

            // Initiate the return list
            List<SubPagePuffModel> puffList = new List<SubPagePuffModel>();

            // Get an Umbraco helper (optional), if missing
            if (helper == null)
            {
                helper = new UmbracoHelper(UmbracoContext.Current);
            }

            // Get current language ("SV" or "EN")
            var currentLanguage = helper.GetDictionaryValue("Language");

            // Initiate variable
            string parentContentType = string.Empty;

            // Get terms from Dictionary
            string moreAboutTerm = helper.GetDictionaryValue("MoreAbout");
            string readMoreTerm = helper.GetDictionaryValue("ReadMore");

            // Convert string to list
            var listPuffNodes = subPageNodes.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            // Convert list to a typed collection of nodes
            var collectionPuffs = helper.TypedContent(listPuffNodes);

            // Iterate over the selected nodes 
            foreach (var item in collectionPuffs)
            {
                // Make sure current sub page is published (currently it's possible to select 
                // a non-published item in backoffice).
                if (item != null)
                {
                    SubPagePuffModel puff = GetSubPagePuff(item.Id, helper, currentLanguage, readMoreTerm, moreAboutTerm);

                    // Add the puff object to the temp list
                    puffList.Add(puff);
                }
            }

            return puffList;
        }




        /* HELPER METHODS */


     
        ///<summary>
        /// Fetches a list of sub page puffs.  
        /// </summary>       
        /// <param name="subPageNodes">A string of comma-separated node id:s representing the sub pages.</param>
        /// <param name="language">Current language (SV/EN). Optional.</param>
        /// <param name="helper">Instance of the Umbraco helper. Optional.</param>
        /// <returns>A list of SubPagePuff objects.</returns> 
        protected List<SubPagePuffModel> GetSelectedPuffsList(string subPageNodes, string language = "", UmbracoHelper helper = null)
        {
            // Initiate the return list
            List<SubPagePuffModel> puffList = new List<SubPagePuffModel>();

            // Get current language (optional), if missing
            if (language == string.Empty)
            {
                language = helper.GetDictionaryValue("Language");
            }
            
            // Get an Umbraco helper (optional), if missing
            if (helper == null)
            {
                helper = new UmbracoHelper(UmbracoContext.Current);
            }
                       
            // Get Read more term
            string readMoreTerm = helper.GetDictionaryValue("ReadMore");

            // Convert string to list
            var listNodes = subPageNodes.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            // Convert list to a typed collection of nodes
            var collectionNodes = helper.TypedContent(listNodes);

            // Iterate over the nodes 
            foreach (var item in collectionNodes)
            {
                // Make sure current sub page is published (currently it's possible to select 
                // a non-published item in backoffice).
                if (item != null)
                {                    
                    SubPagePuffModel puff = GetSubPagePuff(item.Id, helper, language, readMoreTerm);

                    // Add the puff object to the temp list
                    puffList.Add(puff);
                }
            }

            return puffList;
        }




        protected SubPagePuffModel GetSubPagePuff(int nodeId, UmbracoHelper helper, string language, string readMoreTerm, string moreAboutTerm = "")
        {

            // Initiate the return model
            SubPagePuffModel model = new SubPagePuffModel();

            // Get content based on current node id
            var content = helper.TypedContent(nodeId);

            // Get the node's parent's document type
            string parentContentType = content.Parent.DocumentTypeAlias;
            

            /* TAG  */

            string tagValue = string.Empty;

            // Manage tag due to the sub page's parent node's content type
            switch (parentContentType)
            {
                case MyConstants.AccommodationsLandingPageDocTypeAlias:

                    // Manage accommodation page's tag due to content type
                    switch(content.DocumentTypeAlias)
                    {
                        case MyConstants.AccommodationPageDocTypeAlias:

                            tagValue = content.GetPropertyValue<string>(MyConstants.SubPageTagPropertyAlias);

                            // Make sure an area actually was selected (mandatory but you never know...)
                            if (!string.IsNullOrEmpty(tagValue))
                            {
                                // Tag value is an area's node id             
                                Int32 areaId = Convert.ToInt32(tagValue);

                                // Get content based on node id
                                var areaContent = helper.TypedContent(areaId);

                                // Set node's name as tag
                                model.Tag = areaContent.Name;

                                // Get map thumbnail
                                model.MapThumbnailPath = Paths.GetMapThumbnail(areaContent.UrlName);
                            }
                            break;

                        case MyConstants.AccommodationPageBookDocTypeAlias:
                            model.Tag = helper.GetDictionaryValue("Reserve");
                            break;

                        case MyConstants.AccommodationPageOtherDocTypeAlias:
                            model.Tag = helper.GetDictionaryValue("Other");
                            break;
                    }
                    break;

                case MyConstants.EventsListDocTypeAlias:

                    // Create a Swedish culture object
                    CultureInfo swedish = new CultureInfo("sv-SE");

                    // Create a DateTimeFormatInfo object to make it possible to get month name as text...
                    DateTimeFormatInfo dtfi = new DateTimeFormatInfo();

                    // ...in Swedish, provided this is current language 
                    if (language == "SV")
                    {
                        dtfi = swedish.DateTimeFormat;
                    }

                    // Initiate an EventItem object
                    EventItemModel modelEvent = new EventItemModel();

                    // Populate the object with event dates in a nice format
                    modelEvent = EventPageController.CreateNiceEventDates(content, modelEvent, dtfi);
                    
                    // Get tag value (in this case - the event dates).
                    model.Tag = modelEvent.DateInfo;

                    break;

                default:

                    // Get tag value.
                    model.Tag = content.GetPropertyValue<string>(MyConstants.SubPageTagPropertyAlias);

                    break;
            }


            /* IMAGE */

            string imagePicker = content.GetPropertyValue<string>(MyConstants.ImagePickerPropertyAlias);

            // Make sure at least one image exist (required but you never know...)
            if (!string.IsNullOrWhiteSpace(imagePicker))
            {
                // Store the main images to a list
                string[] imagesList = imagePicker.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                // Get first image and add to model
                model.Image = helper.Media(imagesList[0]);
                // Get first image's path and add to model
                //Puff.ImagePath = Puff.Image.GetCropUrl("umbracoFile", "Inspirationsbild tumnagel");
            }


            /* TITLE */
                        
            // This is an area page so use the node name instead of title
            if(moreAboutTerm != string.Empty)
            {
                model.Title = moreAboutTerm + " " + content.Name; 
            }
            else
            {
                model.Title = content.GetPropertyValue<string>(MyConstants.TitlePropertyAlias).Trim();
            }


            /* SUB TITLE */

            model.SubTitle = content.GetPropertyValue<string>(MyConstants.SubtitlePropertyAlias);
                            

            /* INTRO */

            // Get intro text explicitly written for puffs
            string intro = content.GetPropertyValue<string>(MyConstants.IntroAltPropertyAlias);

            // If the puff-specific intro is missing, go for the standard intro text (required) 
            if (string.IsNullOrEmpty(intro))
            {                
                //intro = content.GetPropertyValue<string>(MyConstants.IntroPropertyAlias);
                intro = UmbracoHelperExtensions.TruncateAtWord(content.GetPropertyValue<string>(MyConstants.IntroPropertyAlias), 140);
            }

            model.Intro = intro;


            /* GUEST RATINGS AND CABIN CAPACITIES */

            // Explicitly set the GuestRatings object to null
            model.GuestRatings = null;

            // Get guest ratings and cabin capacities for accommodation page
            if (parentContentType == MyConstants.AccommodationsLandingPageDocTypeAlias)
            {             

                // Guest ratings and cabin facilities only exists for cabin pages 
                if (content.DocumentTypeAlias == MyConstants.AccommodationPageDocTypeAlias)
                {
                    GuestRatingsModel objGuestRatings = new GuestRatingsModel();
                    model.GuestRatings = GuestRatingsSurfaceController.GetRatingValues(objGuestRatings, nodeId, language);

                    // Create a temporary list
                    List<FacilityTypeModel> listCabinCapacities = new List<FacilityTypeModel>();

                    // Get main facilities (checkbox list)
                    dynamic pickedFacilities = content.GetPropertyValue(MyConstants.CabinFacilitiesPropertyAlias);

                    // Add main facilities to the Facility Types list
                    listCabinCapacities = GetCabinCapacities(pickedFacilities, listCabinCapacities, helper, language);

                    // Get pet option (radiobutton list)
                    dynamic pickedPetOption = content.GetPropertyValue(MyConstants.CabinPetOptionsPropertyAlias);

                    // Add pet option to the Facility Types list
                    listCabinCapacities = GetCabinCapacities(pickedPetOption, listCabinCapacities, helper, language);

                    // Get bed option (dropdown list)
                    dynamic pickedBedOption = content.GetPropertyValue(MyConstants.CabinBedOptionsPropertyAlias);

                    // Add bed option to the Facility Types list
                    bool bedOption = true;
                    listCabinCapacities = GetCabinCapacities(pickedBedOption, listCabinCapacities, helper, language, bedOption);

                    // Add list to return model
                    model.FacilitiesCollection = listCabinCapacities;
                }
            }


            /* URL */

            model.Url = helper.NiceUrl(content.Id);


            /* PARENT NODE ID */

            model.ParentNodeId = content.Parent.Id;
            model.ReadMoreTerm = readMoreTerm;

            return model;
        }




        public PuffModel GetPuff(IPublishedContent content, UmbracoHelper helper)
        {
            // Initiate the return model
            PuffModel model = new PuffModel();          

            /* IMAGE */

            var imagePicker = content.GetPropertyValue<string>(MyConstants.ImagePickerPropertyAlias);

            // Make sure an image exists (required but you never know...)
            if (!string.IsNullOrWhiteSpace(imagePicker))
            {
                // Assign image to variable
                model.Image = helper.Media(imagePicker);

                // Get image's width and height
                string width = model.Image.GetPropertyValue<string>(MyConstants.MediaFileWidth);
                string height = model.Image.GetPropertyValue<string>(MyConstants.MediaFileHeight);

                // Detect portrait format
                model.PortraitImage = false;
                if(Convert.ToInt32(width) < Convert.ToInt32(height))
                {
                    model.PortraitImage = true;
                }
            }

            /* TITLE */
            model.Title = content.GetPropertyValue<string>(MyConstants.TitlePropertyAlias);
            
            /* INTRO */
            model.Text = content.GetPropertyValue<IHtmlString>(MyConstants.TextPropertyAlias);
            
            /* LINKS */
            var links = content.GetPropertyValue<MultiUrls>(MyConstants.MultiUrlPickerPropertyAlias);

            // Initiate links flag
            model.LinksExists = false;

            // Check whether links exists (optional)
            if (links.Any())
            {
                // Set flag
                model.LinksExists = true;

                // Get links and add to the puff object
                model.LinksCollection = GetLinks(links, helper);
            }

            return model;
        }




        ///<summary>
        /// Checks whether a list of selected nodes actually contains valid and published nodes.
        /// </summary>       
        /// <param name="selectedNodes">Comma-separated list containing selected nodes.</param>
        /// <returns>A boolean.</returns>      
        public bool CheckSelectedSubPages(string selectedNodes, UmbracoHelper helper = null)
        {
            // Initiate the return value
            bool tipsExists = false;

            // Get an Umbraco helper (optional), if missing
            if (helper == null)
            {
                helper = new UmbracoHelper(UmbracoContext.Current);
            }

            // Initiate variable
            string parentContentType = string.Empty;

            // Convert string to list
            var listPuffNodes = selectedNodes.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            // Convert list to a typed collection of nodes
            var collectionPuffs = helper.TypedContent(listPuffNodes);

            // Make sure collection has content. 
            if (collectionPuffs.Any())
            {
                // Iterate over the selected nodes 
                foreach (var item in collectionPuffs)
                {
                    // If current sub page is published, set flag and break out of loop
                    // (Currently it's possible to select a non-published node using MNTP).
                    if (item != null)
                    {
                        tipsExists = true;
                        break;
                    }
                }
            }
            return tipsExists;
        }

         

        protected List<SubPagePuffModel> CreateAccommodationPuffsList(string sortOrder, List<SubPagePuffModel> listSubPages, IPublishedContent content, UmbracoHelper helper)
        {            
 
            // Fill the list due to requested order type
            switch(sortOrder)
            {

                case "byNode":

                    // Get a list of all accommodations by tree structure order
                    listSubPages = content
                       .Children
                       .Select(obj => new SubPagePuffModel()
                       {
                           NodeId = obj.Id
                       }).ToList(); 
                    break;

                case "byCategory":

                    // Get a list of all accommodations tagged with an area
                    List<SubPagePuffModel> listSubPagesArea = content
                       .Children
                       .Where(c => c.DocumentTypeAlias == MyConstants.AccommodationPageDocTypeAlias)
                       .OrderBy(c => c.Name)
                       .Select(obj => new SubPagePuffModel()
                       {
                           NodeId = obj.Id,
                           //ContentType = obj.DocumentTypeAlias,
                           Tag = helper.TypedContent(obj.GetPropertyValue<string>(MyConstants.SubPageTagPropertyAlias)).Name
                       }).ToList();

                    // Sort list by area tag
                    listSubPagesArea.Sort((x, y) => string.Compare(x.Tag, y.Tag));

                    // Get a list of all accommodations of the Boka/Book type 
                    List<SubPagePuffModel> listSubPagesBook = content
                        .Children
                        .Where(c => c.DocumentTypeAlias == MyConstants.AccommodationPageBookDocTypeAlias)
                        .OrderBy(c => c.Name)
                        .Select(obj => new SubPagePuffModel()
                        {
                            NodeId = obj.Id
                            //,
                            //ContentType = obj.DocumentTypeAlias,
                            //Tag = helper.GetDictionaryValue("Reserve")
                        }).ToList();

                    // Get a list of all accommodations of the Övrigt/Other type 
                    List<SubPagePuffModel> listSubPagesOther = content
                        .Children
                        .Where(c => c.DocumentTypeAlias == MyConstants.AccommodationPageOtherDocTypeAlias)
                        .OrderBy(c => c.Name)
                        .Select(obj => new SubPagePuffModel()
                        {
                            NodeId = obj.Id
                            //,
                            //ContentType = obj.DocumentTypeAlias,
                            //Tag = helper.GetDictionaryValue("Other")
                        }).ToList();

                    listSubPagesBook.AddRange(listSubPagesOther);
                    listSubPagesArea.AddRange(listSubPagesBook);

                    listSubPages = listSubPagesArea;

                    break;
            }          

            return listSubPages;
        }



        protected SubPagePuffModel GetAccommodationPuff(IPublishedContent nodeContent, string parentContentType, UmbracoHelper helper, string readMoreTerm)
        {

            // Initiate the return model
            SubPagePuffModel model = new SubPagePuffModel();


            /* TAG  */

            // Set Title as Tag value     
            model.Tag = nodeContent.GetPropertyValue<string>(MyConstants.TitlePropertyAlias);


            /* IMAGE */

            string imagePicker = nodeContent.GetPropertyValue<string>(MyConstants.ImagePickerPropertyAlias);

            // Make sure at least one image exist (required but you never know...)
            if (!string.IsNullOrWhiteSpace(imagePicker))
            {
                // Store the main images to a list
                string[] imagesList = imagePicker.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                // Get first image's path and add to model
                model.Image = helper.Media(imagesList[0]);
                //Puff.ImagePath = Puff.Image.GetCropUrl("umbracoFile", "Inspirationsbild tumnagel");
            }


            /* TITLE */

            // Set Subtitle as Title 
            model.Title = nodeContent.GetPropertyValue<string>(MyConstants.SubtitlePropertyAlias).Trim();


            /* INTRO */

            // Get intro text explicitly written for puffs
            string intro = nodeContent.GetPropertyValue<string>(MyConstants.IntroAltPropertyAlias);

            // If the puff-specific intro is missing, go for the standard intro text (required) 
            if (string.IsNullOrEmpty(intro))
            {
                intro = nodeContent.GetPropertyValue<string>(MyConstants.IntroPropertyAlias);
            }

            model.Intro = intro;


            /* URL PATH AND TITLE */

            model.Url = helper.NiceUrl(nodeContent.Id);
            model.ReadMoreTerm = readMoreTerm;

            return model;
        }





        public PuffModel GetWarningProperties(IPublishedContent content, PuffModel model, UmbracoHelper helper)
        {
            /* ICON */
            model.IconPath = Paths.GetIconPath("warning");

            /* TEXT */
            model.Text = content.GetPropertyValue<IHtmlString>(MyConstants.TextPropertyAlias);
            
            return model;
        }




        public PuffModel GetContactInfoProperties(IPublishedContent content, PuffModel model, UmbracoHelper helper)
        {
            /* TITLE */
            model.Title = content.GetPropertyValue<String>(MyConstants.titlePuffPropertyAlias);

            /* LEFT COLUMN TEXT */
            model.LeftColumnText = content.GetPropertyValue<IHtmlString>(MyConstants.contactPuffLeftColumnPropertyAlias);

            /* RIGHT COLUMN TEXT */
            model.RightColumnText = content.GetPropertyValue<IHtmlString>(MyConstants.contactPuffRightColumnPropertyAlias);

            return model;
        }




        public PuffModel GetCoworkerGroupProperties(IPublishedContent content, PuffModel model, UmbracoHelper helper, string language)
        {
            /* TITLE */
            model.Title = content.GetPropertyValue<String>(MyConstants.titleGroupPropertyAlias);

            /* INTRO TEXT (OPTIONAL) */
            model.Intro = content.GetPropertyValue<string>(MyConstants.introGroupPropertyAlias);

            /* PERSONS */

            // Get a string of comma-separated Persons node id:s
            var strPersons = content.GetPropertyValue<string>(MyConstants.contactPickerPropertyAlias);

            // Initiate a temporary list
            List<ContactModel> tempList = new List<ContactModel>();
           
            // Make sure at least one Person is selected
            if (!string.IsNullOrEmpty(strPersons))
            {
                // Convert string to list
                var listPersons = strPersons.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                // Convert list to typed collection
                var collectionPersons = helper.TypedContent(listPersons);

                // Create reference to controller
                ContactPersonsPageController cppc = new ContactPersonsPageController(); 

                foreach (var item in collectionPersons)
                {
                    // Make sure current persons node is published (it's possible to select a
                    // non-published person in backoffice).
                    if (item != null)
                    {
                        // Get content based on current node id
                        var itemContent = helper.Content(item.Id);

                        // Get a ContactModel object based on current item's content
                        ContactModel modelC = cppc.GetContactProperties(itemContent, language, helper);

                        // Add object to the temporary list
                        tempList.Add(modelC);
                    }
                }               
            }

            // Add the temporary list to the return model
            model.PersonsCollection = tempList; 

            return model;
        }


        

        public List<FacilityTypeModel> GetCabinCapacities(dynamic pickedCollection, List<FacilityTypeModel> tempList, UmbracoHelper helper, string language, bool bedOption = false)
        {

            // Make sure capacities were picked
            if (pickedCollection.PickedKeys != null && pickedCollection.PickedKeys.Length > 0)
            {

                // Iterate over the selected items (one to many)
                foreach (var item in pickedCollection.PickedKeys)
                {
                    // Get current node id               
                    int nodeId = helper.TypedContent(item).Id;

                    // Get the facility type content based on node id
                    var content = helper.TypedContent(nodeId);

                    // Initiate a Facility Type object
                    FacilityTypeModel model = null;

                    // Get properties due to current language
                    switch (language)
                    {
                        // Use node name as facility type name
                        case "SV":
                            model = new FacilityTypeModel
                            {
                                NodeId = content.Id,
                                Name = content.Name,
                                IconPath = helper.Media(content.GetPropertyValue<string>(MyConstants.FacilityTypeIconPropertyAlias)).Url
                            };
                            break;

                        // Use English term property as facility type name
                        case "EN":
                            model = new FacilityTypeModel
                            {
                                NodeId = content.Id,
                                Name = content.GetPropertyValue<string>(MyConstants.TermEnglishPropertyAlias),
                                IconPath = helper.Media(content.GetPropertyValue<string>(MyConstants.FacilityTypeIconPropertyAlias)).Url
                            };
                            break;
                    }

                    // Special treatment for bed options
                    if (bedOption)
                    {
                        // Explicitly set the number of beds property as empty string
                        model.NumOfBeds = string.Empty;

                        // Extract the number of beds from the name property
                        string[] arrName = model.Name.Split(' ');
                        string numOfBeds = arrName[0];

                        // Return the number of beds embedded in a span element
                        model.NumOfBeds = "<span>" + numOfBeds + "</span>"; 
                    }

                    // Add Facility Type object to list
                    tempList.Add(model);
                }
            }
            return tempList;
        }



        public List<LinkModel> GetLinks(MultiUrls links, UmbracoHelper helper = null)
        {
            // Initiate the return list
            var listLinks = new List<LinkModel>();

            // Get an Umbraco helper (optional), if missing
            if (helper == null)
            {
                helper = new UmbracoHelper(UmbracoContext.Current);
            }

            // Iterate over the links 
            foreach (var link in links)
            {
                // Instanciate a Link object
                var objLink = new LinkModel();

                // Get url
                objLink.Url = link.Url;

                // Resolve the url if the link is internal
                if (link.Id != null)
                {
                    objLink.Url = helper.NiceUrl((int)link.Id);
                }

                // Get name
                objLink.Title = link.Name;

                // Get target
                objLink.Target = link.Target;

                // Add link to temp list
                listLinks.Add(objLink);
            }

            return listLinks;
        }
        
    }
}