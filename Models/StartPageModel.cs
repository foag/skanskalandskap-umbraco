﻿using System.Collections.Generic;
using SkanskaLandskap15.Models.Masters;
using SkanskaLandskap15.Models.Shared;


namespace SkanskaLandskap15.Models
{

    // SWEDISH START PAGE MODEL

    public class StartPageModel : StartMasterModel
    {
        public bool TipsExists { get; set; }
        public string TipsTitle { get; set; }
        public string ReadMoreTerm { get; set; } 
        public string MoreTipsTerm { get; set; }
        public string MoreTipsUrl { get; set; }
        public SubPagesListModel SubPages { get; set; }
        public SubPagePuffModel FixedPuff { get; set; }
        public EventItemModel EventItem { get; set; }
        public NewsItemModel NewsItem { get; set; }       
        public dynamic NewsPicker { get; set; }
        public string SelectedNodes { get; set; }
    }
}