﻿using System.Collections.Generic;
using SkanskaLandskap15.Models.Masters;
using SkanskaLandskap15.Models.Shared;


namespace SkanskaLandskap15.Models
{
    public class NewsAndEventsLandingPageModel : InteriorMasterModel
    {
        public EventsListModel EventsList { get; set; }
        public string EventsTitleTerm { get; set; }
        public string MoreEventsTerm { get; set; }

        public NewsListModel NewsList { get; set; }
        public string NewsTitleTerm { get; set; }
        public string MoreNewsTerm { get; set; } 
    }
}