﻿using System;
using System.Web;
using System.Web.Mvc;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Controllers.SurfaceControllers;
using SkanskaLandskap15.Models;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Core.Models;
using Umbraco.Web;



namespace SkanskaLandskap15.Controllers
{
    public class NewsPageController : InteriorMasterController
    {

        public ActionResult NewsPage()
        {
            // Initiate the return model
            var model = new NewsPageModel();

            // Create an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Set news display type
            string requestType = "full";

            // Get the news item's content
            model.NewsItem = GetNewsContent(CurrentPage, helper, requestType);

            model.PageTitleTerm = helper.GetDictionaryValue("NewsPageTitle");

            return View(model);
        }




        /*****************************************************************************************************
           Note: This method is called from NewsAndEventsPageController and NewsSurfaceController.
        ******************************************************************************************************/
        public static NewsItemModel GetNewsContent(IPublishedContent content, UmbracoHelper helper, string requestType)
        {
            
            // Initiate the return model
            NewsItemModel model = new NewsItemModel();

            //Create controller reference
            MediaSurfaceController msc = new MediaSurfaceController();

            // Image 
            model.NewsImage = helper.Media(content.GetPropertyValue(MyConstants.ImagePickerPropertyAlias));

            // Get a populated Media image object
            ImageModel objImage = msc.GetMediaImageProperties(model.NewsImage, helper);

            // Create a nice image caption
            if (objImage.Caption != string.Empty)
            {
                model.ImageCaption = objImage.Caption;

                if (objImage.Photographer != string.Empty)
                {
                    model.ImageCaption += " " + objImage.Photographer;
                }
            }
            else
            {
                if (objImage.Photographer != string.Empty)
                {
                    model.ImageCaption = objImage.Photographer;
                }
            }

            // Title
            model.Title = content.GetPropertyValue<String>(MyConstants.TitlePropertyAlias);

            // Publishing date
            string publishingDate = content.GetPropertyValue<string>(MyConstants.PublishingDatePropertyAlias);
            if (string.IsNullOrWhiteSpace(publishingDate))
            {
                model.PublishingDate = String.Empty;
            }
            else
            {
                // Remove time value
                model.PublishingDate = publishingDate.Substring(0, 10);
            }



            // Get texts due to type of request (full article or puff)
            switch (requestType)
            {
                case "full":

                    // Intro
                    model.Intro = content.GetPropertyValue<String>(MyConstants.IntroPropertyAlias);

                    // Richtext
                    model.Text = content.GetPropertyValue<IHtmlString>(MyConstants.TextPropertyAlias);                    

                    // Get the "Published" header from Dictionary
                    model.PublishedTerm = helper.GetDictionaryValue("Published");
                    break;

                case "puff":

                    // Get alternative intro text (optional)
                    string altIntro = content.GetPropertyValue<string>(MyConstants.IntroAltPropertyAlias);

                    // If present, use alt text
                    if (!string.IsNullOrWhiteSpace(altIntro))
                    {
                        model.Intro = UmbracoHelperExtensions.TruncateAtWord(altIntro, 155);
                    }
                    // Otherwise, use standard intro text (mandatory)
                    else
                    {
                        model.Intro = UmbracoHelperExtensions.TruncateAtWord(content.GetPropertyValue<string>(MyConstants.IntroPropertyAlias), 155);
                    }  
                    
                    // Create Read more link info
                    model.Url = helper.NiceUrl(content.Id);
                    model.UrlTitleTerm = helper.GetDictionaryValue("ReadMore");
                    break;
            }            

            return model;
        }
    }
}