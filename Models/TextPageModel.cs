﻿using System.Web;
using SkanskaLandskap15.Models.Masters;
using SkanskaLandskap15.Models.Shared;


namespace SkanskaLandskap15.Models
{
    public class TextPageModel : InteriorMasterModel
    {
        public int NodeId { get; set; }
        public dynamic ImageSingle { get; set; }
        public string ImageCaption { get; set; }        
        public IHtmlString Text { get; set; }
        public NewsItemModel NewsItem { get; set; }
    }
}