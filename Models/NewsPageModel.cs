﻿using SkanskaLandskap15.Models.Masters;
using SkanskaLandskap15.Models.Shared;


namespace SkanskaLandskap15.Models
{
    public class NewsPageModel : InteriorMasterModel
    {
        public NewsItemModel NewsItem { get; set; }
        public string PageTitleTerm { get; set; }
    }
}