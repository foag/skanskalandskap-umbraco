﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Controllers.SurfaceControllers;
using SkanskaLandskap15.Models;
using Umbraco.Web;




namespace SkanskaLandskap15.Controllers
{
    public class AccommodationPageBookController : InteriorMasterController
    {

        public ActionResult AccommodationPage()
        {
            // Initiate the return model
            var model = new AccommodationPageModel();

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language ("SV" or "EN") 
            string currentLanguage = helper.GetDictionaryValue("Language");


            // UrlName and Content type
            model.UrlName = CurrentPage.UrlName;
            model.ContentType = CurrentPage.DocumentTypeAlias;

            // Set some flags
            model.AreaTypeTag = false;
            model.FacilitiesExists = false;
            model.ShowExtendedTexts = false;
            model.ShowGuestRatings = false;
            model.ShowAreaPuff = false;



            /* SLIDESHOW */

            // Images for slideshow        
            string imagePicker = CurrentPage.GetPropertyValue<string>(MyConstants.ImagePickerPropertyAlias);

            // Initiate flag
            model.SlideshowExists = false;

            // Make sure images exists (required but you never know...)
            if (!string.IsNullOrWhiteSpace(imagePicker))
            {
                // Set flag          
                model.SlideshowExists = true;

                // Create controller reference
                MediaSurfaceController msc = new MediaSurfaceController();

                // Get a populated Slideshow object and add it to the return model
                model.Slideshow = msc.GetSlideshow(imagePicker, helper);
            }




            /* RICHTEXT CONTENT  */

            model.Text = CurrentPage.GetPropertyValue<IHtmlString>(MyConstants.TextPropertyAlias);



            /* PUFFS ARE RETRIEVED FROM THE VIEW */

            // Here we need to check whether puffs exists, though. This is to be able to handle
            // the common section title for puffs and downloads.

            // Create controller reference
            PuffsSurfaceController psc = new PuffsSurfaceController();
            model.PuffsExists = psc.PuffsExists(CurrentPage.Id, helper);



            /* DOWNLOADS  */

            // Pdf:s for the Download section        
            string downloadsPicker = CurrentPage.GetPropertyValue<string>(MyConstants.DownloadsPropertyAlias);

            // Initiate flag
            model.DownloadsExists = false;

            // Check whether downloads exists
            if (!string.IsNullOrWhiteSpace(downloadsPicker))
            {

                // Create controller reference
                MediaSurfaceController msc = new MediaSurfaceController();

                // Get the media files collection and add to return model
                model.MediaFiles = msc.GetDownloadFiles(downloadsPicker, helper, currentLanguage);

                // Make sure at least one of the selected files actually exists in Media 
                if (model.MediaFiles.MediaFilesCollection.Count() > 0)
                {
                    // Set flag          
                    model.DownloadsExists = true;
                }
            }


            // Puffs and/or downloads exists so get the common title
            if (model.PuffsExists || model.DownloadsExists)
            {
                model.PuffsAndDownloadsTitle = helper.GetDictionaryValue("Other");
            } 

            return View(model);
        }  
    

    }
}