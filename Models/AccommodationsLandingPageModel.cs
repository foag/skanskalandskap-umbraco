﻿using SkanskaLandskap15.Models.Masters;
using SkanskaLandskap15.Models.Shared;


namespace SkanskaLandskap15.Models
{

    public class AccommodationsLandingPageModel : InteriorMasterModel
    {        
        public SubPagesListModel SubPages { get; set; }
    }
}