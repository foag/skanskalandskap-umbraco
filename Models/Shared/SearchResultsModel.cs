﻿using System.Collections.Generic;
using Examine;
using PagedList;


namespace SkanskaLandskap15.Models.Shared
{
    public class SearchResultsModel 
    {
        public string SearchTerm { get; set; }        
        public int Number { get; set; }
        public string Message { get; set; }
        public int PageSize { get; set; }
        public string PaginationMessage { get; set; }
        public bool AccessWithoutSearch { get; set; }

        public int NodeId { get; set; }
        public string Language { get; set; }

        public string SearchUrlName { get; set; }
        public string SearchPlaceHolderTerm { get; set; }

        public List<SearchResult> ExamineCollection { get; set; }
        public IPagedList<SearchResultItemModel> SearchResultsCollection { get; set; }
    }
}