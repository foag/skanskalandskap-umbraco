﻿using System;
using System.Web.Mvc;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Models.Masters;
using Umbraco.Web;


namespace SkanskaLandskap15.Controllers.Masters
{
    public class InteractiveMasterController : MasterController
    {

        protected ViewResult View(InteractiveMasterModel model)
        {
            return this.View(null, model);
        }


        protected ViewResult View(string view, InteractiveMasterModel model)
        {

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);
                                  
            model.Subtitle = CurrentPage.GetPropertyValue<string>(MyConstants.SubtitlePropertyAlias);
            if (string.IsNullOrWhiteSpace(model.Subtitle))
            {
                model.Subtitle = String.Empty;
            }

            model.Intro = CurrentPage.GetPropertyValue<string>(MyConstants.IntroPropertyAlias);
            if (string.IsNullOrWhiteSpace(model.Intro))
            {
                model.Intro = String.Empty;
            }


            /*** INTRO TEXT ***/

            //// Get intro text for start and level 2 (= landing) page only
            //if (model.Level <= 2)
            //{
            //    model.Intro = CurrentPage.GetPropertyValue<string>(MyConstants.IntroPropertyAlias).Trim();
            //}  
            

            /*** SUB MENU ***/

            //// Initiate sub menu with an empty list
            //model.SubNavigation = new List<NavigationItemModel>();

            //// No sub menu for area page           
            //if(CurrentPage.DocumentTypeAlias != MyConstants.AreaPageDocTypeAlias)
            //{ 
            //    // Get reference to controller
            //    NavigationSurfaceController nsc = new NavigationSurfaceController();

            //    // Get sub menu
            //    model.SubNavigation = nsc.GetSubMenu(CurrentPage);
            //}


            /*** GLOBAL JAVASCRIPT VARIABLES ***/

            model.ShowAllTerm = helper.GetDictionaryValue("DisplayAll");
            model.HideAllTerm = helper.GetDictionaryValue("HideAll");
            model.ResetTerm = helper.GetDictionaryValue("Reset");
            model.LoadingTerm = helper.GetDictionaryValue("Loading");
            model.BusStopsErrorTerm = helper.GetDictionaryValue("MapErrorBusStops");
            model.GeneralMapErrorTerm = helper.GetDictionaryValue("MapErrorGeneral");
            model.DetailedDirectionsTerm = helper.GetDictionaryValue("DetailedDirections");
            model.HideDetailedDirectionsTerm = helper.GetDictionaryValue("HideDetailedDirections");
            model.CloseFilteringLinkTerm = helper.GetDictionaryValue("CloseFilteringLinkTitle");
            model.ShowAreasLinkTerm = helper.GetDictionaryValue("ShowAreasLinkTitle");

            return base.View(view, model);
        }     
    }
}