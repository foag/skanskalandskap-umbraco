﻿using System;
using System.Text;
using Examine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using umbraco.NodeFactory;
using Umbraco.Core;
using Umbraco.Web;
using UmbracoExamine;
using Umbraco.Core.Logging;



namespace SkanskaLandskap15.Classes.Utilities
{

    ///<summary>
    /// This class is mainly used to make some properties, included through MNTP datatypes, searchable.
    ///</summary>

    public class AppEvents : IApplicationEventHandler
    {
        public void OnApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }

        public void OnApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }

        public void OnApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            //LogHelper.Info(typeof(RegisterEvents), "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!OnApplicationStarted");

            // Get Umbraco helper instance
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language ("SV" or "EN")
            var currentLanguage = helper.GetDictionaryValue("Language");

            // Handle MNTP properties for the Swedish indexer
            ExamineManager.Instance.IndexProviderCollection["SiteContentIndexer"].GatheringNodeData
                += (sender, e) => ExamineEvents.GatheringContentData(sender, e, helper, "SV");

            // Handle MNTP properties for the English indexer
            ExamineManager.Instance.IndexProviderCollection["SiteContentEnglishIndexer"].GatheringNodeData
                += (sender, e) => ExamineEvents.GatheringContentData(sender, e, helper, "EN");
        }
    }


    public static class ExamineEvents
    {
        public static void GatheringContentData(object sender, IndexingNodeDataEventArgs e, UmbracoHelper helper, string language)
        {
            
            // Make sure index type is "content"
            if (e.IndexType == IndexTypes.Content)
            {
                // Get Examine fields
                var fields = e.Fields;

                // Initiate string builders
                var contactsFields1 = new StringBuilder();
                var contactsFields2 = new StringBuilder();
                var globalpuffsFields1 = new StringBuilder();
                var globalpuffsFields2 = new StringBuilder();
                var coworkersFields1 = new StringBuilder();
                var coworkersFields2 = new StringBuilder();
                var coworkersFields3 = new StringBuilder();

                // Initiate varaible 
                string[] nodeIds = null;

                // Iterate over the Examine fields (defined in /config/ExamineIndex.config)
                foreach (var keyValuePair in fields)
                {
                    // Get the field key (e.g. nodeName, Title etc)
                    string propertyAlias = keyValuePair.Key;

                    // Make manipulations for MNTP keys
                    switch (propertyAlias)
                    {

                        // This is the MNTP persons picker
                        case MyConstants.ContactPickerPropertyAlias:

                            // Get the comma-separated node ids
                            nodeIds = keyValuePair.Value.Split(',');

                            // Iterate over the id:s and get the person's name and function
                            foreach (string nodeId in nodeIds)
                            {
                                var node = new Node(Convert.ToInt32(nodeId));

                                // Name                                
                                contactsFields1.AppendLine(node.GetProperty(MyConstants.ContactNamePropertyAlias).ToString());
                                //combinedFields.AppendLine(node.Name);

                                switch (language)
                                {
                                    case "SV":

                                        // Function
                                        contactsFields2.AppendLine(node.GetProperty(MyConstants.ContactFunctionPropertyAlias).ToString());
                                        break;

                                    case "EN":

                                        // Function
                                        contactsFields2.AppendLine(node.GetProperty(MyConstants.ContactFunctionENPropertyAlias).ToString());
                                        break;
                                }
                            }
                            break;

                        // Global puffs - the MNTP picker is contained in a Nested Content datatype. 
                        case MyConstants.PuffsSlideshowModulePropertyAlias:

                            // Get the content of the Nested Content datatype
                            var value1 = e.Fields[propertyAlias];

                            // Deserialize the json content
                            object obj1 = JsonConvert.DeserializeObject(value1);

                            // Get object type
                            var objType1 = obj1.GetType();

                            // The Nested Content object should be of JArray type 
                            if (objType1 == typeof(JArray))
                            {
                                var jArr = obj1 as JArray;

                                // Make sure object exists
                                if (jArr != null)
                                {
                                    // Iterate over the JArray
                                    for (var i = 0; i < jArr.Count; i++)
                                    {
                                        // Get current Nested Content item
                                        var arrayItem = jArr[i];

                                        // Convert to object
                                        var jObj = arrayItem as JObject;

                                        // Make sure object exists
                                        if (jObj != null)
                                        {
                                            // Iterate over the JObject's properties
                                            foreach (var objItem in jObj)
                                            {
                                                // Look for the MNTP datatype
                                                if (objItem.Key == MyConstants.GlobalPuffsPropertyAlias)
                                                {
                                                    // Get the comma-separated node ids
                                                    nodeIds = objItem.Value.ToString().Split(',');

                                                    // Iterate over the id:s and get the puff's title and text
                                                    foreach (string nodeId in nodeIds)
                                                    {
                                                        var node = new Node(Convert.ToInt32(nodeId));

                                                        // Title                                
                                                        globalpuffsFields1.AppendLine(node.GetProperty(MyConstants.TitlePropertyAlias).ToString());

                                                        // Text (richtext, mandatory)
                                                        var text = node.GetProperty(MyConstants.TextPropertyAlias).ToString();
                                                        globalpuffsFields2.AppendLine(text.ToString());                                                        
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            break;

                        // Co-workers - the contact picker is contained in a Nested Content datatype. 
                        case MyConstants.coworkersGroupsModulePropertyAlias:

                            // Get the content of the Nested Content datatype
                            var value2 = e.Fields[propertyAlias];

                            // Deserialize the json content
                            object obj2 = JsonConvert.DeserializeObject(value2);

                            // Get object type
                            var objType2 = obj2.GetType();

                            // The Nested Content object should be of JArray type 
                            if (objType2 == typeof(JArray))
                            {
                                var jArr = obj2 as JArray;

                                // Make sure object exists
                                if (jArr != null)
                                {
                                    // Iterate over the JArray
                                    for (var i = 0; i < jArr.Count; i++)
                                    {
                                        // Get current Nested Content item
                                        var arrayItem = jArr[i];

                                        // Convert to object
                                        var jObj = arrayItem as JObject;

                                        // Make sure object exists
                                        if (jObj != null)
                                        {
                                            // Iterate over the JObject's properties
                                            foreach (var objItem in jObj)
                                            {
                                                // Look for the MNTP datatype
                                                if (objItem.Key == MyConstants.contactPickerPropertyAlias)
                                                {
                                                    // Get the comma-separated node ids
                                                    nodeIds = objItem.Value.ToString().Split(',');

                                                    // Iterate over the id:s and get the person's name, function and work assignments
                                                    foreach (string nodeId in nodeIds)
                                                    {
                                                        var node = new Node(Convert.ToInt32(nodeId));

                                                        // Name                                
                                                        coworkersFields1.AppendLine(node.GetProperty(MyConstants.ContactNamePropertyAlias).ToString());

                                                        switch (language)
                                                        {
                                                            case "SV":

                                                                // Function
                                                                coworkersFields2.AppendLine(node.GetProperty(MyConstants.ContactFunctionPropertyAlias).ToString());

                                                                // Work assignments (optional, textbox multiple)
                                                                var assignments = node.GetProperty(MyConstants.ContactworkAssignmentsPropertyAlias);
                                                                if (assignments != null)
                                                                {
                                                                    coworkersFields3.AppendLine(assignments.ToString());
                                                                }
                                                                break;

                                                            case "EN":

                                                                // Function
                                                                coworkersFields2.AppendLine(node.GetProperty(MyConstants.ContactFunctionENPropertyAlias).ToString());

                                                                // Work assignments (optional, textbox multiple)
                                                                var assignmentsEn = node.GetProperty(MyConstants.ContactworkAssignmentsENPropertyAlias);
                                                                if (assignmentsEn != null)
                                                                {
                                                                    coworkersFields3.AppendLine(assignmentsEn.ToString());
                                                                }
                                                                break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            break;

                       
                        //// An attempt to make a nuPicker datatype searchable. Breaks at the deserialize operation. 
                        //case "CabinFacilities":                         

                        //// Get the json 
                        //var deserialized = serializer.Deserialize<List<TheType>>(keyValuePair.Value);   
                        //var deserialized = JsonConvert.DeserializeObject<List<TheType>>(keyValuePair.Value);

                        //// Iterate over the list
                        //foreach (var item in deserialized)
                        //{
                        //    var node = new Node(Convert.ToInt32(item.key));

                        //    switch (language)
                        //    {
                        //        case "SV":
                        //            combinedFields3.AppendLine(node.Name + " ");
                        //            //combinedFields3.AppendLine(item.label);
                        //            break;
                        //        case "EN":
                        //            combinedFields3.AppendLine(node.GetProperty("TermEnglish").ToString());
                        //            break;
                        //    }
                        //}
                        //break;
                    }
                }


                /* Add the fetched values to new Examine index elements  */
               
                // CONTACTS
                        
                // Make sure first string builder is populated
                if (contactsFields1.Length > 0)
                {
                    e.Fields.Add("_PersonName", contactsFields1.ToString());
                }

                // Make sure second string builder is populated
                if (contactsFields2.Length > 0)
                {
                    // Set value due to language
                    switch (language)
                    {
                        case "SV":
                            e.Fields.Add("_PersonFunction", contactsFields2.ToString());
                            break;
                        case "EN":
                            e.Fields.Add("_PersonFunctionEn", contactsFields2.ToString());
                            break;
                    }
                }
                     
                // GLOBAL PUFFS

                // Make sure first string builder is populated
                if (globalpuffsFields1.Length > 0)
                {
                    e.Fields.Add("_GlobalPuffTitle", globalpuffsFields1.ToString());
                }

                // Make sure second string builder is populated
                if (globalpuffsFields2.Length > 0)
                {
                    e.Fields.Add("_GlobalPuffText", globalpuffsFields2.ToString());
                }
                       

                // COWORKERS

                // Make sure first string builder is populated
                if (coworkersFields1.Length > 0)
                {
                    e.Fields.Add("_PersonName", coworkersFields1.ToString());
                }

                // Make sure second string builder is populated
                if (coworkersFields2.Length > 0)
                {
                    // Set value due to language
                    switch (language)
                    {
                        case "SV":
                            e.Fields.Add("_PersonFunction", coworkersFields2.ToString());
                            break;
                        case "EN":
                            e.Fields.Add("_PersonFunctionEn", coworkersFields2.ToString());
                            break;
                    }
                }

                // Make sure third string builder is populated
                if (coworkersFields3.Length > 0)
                {
                    // Set value due to language
                    switch (language)
                    {
                        case "SV":
                            e.Fields.Add("_PersonWorkAssignments", coworkersFields3.ToString());
                            break;
                        case "EN":
                            e.Fields.Add("_PersonWorkAssignmentsEn", coworkersFields3.ToString());
                            break;
                    }
                }
            }
        }
    }

    //public class TheType
    //{
    //    public string key { get; set; }
    //    public string label { get; set; }
    //}
}