/*! modernizr 3.2.0 (Custom Build) | MIT *
 * http://modernizr.com/download/?-touchevents !*/
!function(e,n,t){function o(e,n){return typeof e===n}function s(){var e,n,t,s,a,i,r;for(var l in f)if(f.hasOwnProperty(l)){if(e=[],n=f[l],n.name&&(e.push(n.name.toLowerCase()),n.options&&n.options.aliases&&n.options.aliases.length))for(t=0;t<n.options.aliases.length;t++)e.push(n.options.aliases[t].toLowerCase());for(s=o(n.fn,"function")?n.fn():n.fn,a=0;a<e.length;a++)i=e[a],r=i.split("."),1===r.length?Modernizr[r[0]]=s:(!Modernizr[r[0]]||Modernizr[r[0]]instanceof Boolean||(Modernizr[r[0]]=new Boolean(Modernizr[r[0]])),Modernizr[r[0]][r[1]]=s),d.push((s?"":"no-")+r.join("-"))}}function a(e){var n=u.className,t=Modernizr._config.classPrefix||"";if(p&&(n=n.baseVal),Modernizr._config.enableJSClass){var o=new RegExp("(^|\\s)"+t+"no-js(\\s|$)");n=n.replace(o,"$1"+t+"js$2")}Modernizr._config.enableClasses&&(n+=" "+t+e.join(" "+t),p?u.className.baseVal=n:u.className=n)}function i(){return"function"!=typeof n.createElement?n.createElement(arguments[0]):p?n.createElementNS.call(n,"http://www.w3.org/2000/svg",arguments[0]):n.createElement.apply(n,arguments)}function r(){var e=n.body;return e||(e=i(p?"svg":"body"),e.fake=!0),e}function l(e,t,o,s){var a,l,f,c,d="modernizr",p=i("div"),h=r();if(parseInt(o,10))for(;o--;)f=i("div"),f.id=s?s[o]:d+(o+1),p.appendChild(f);return a=i("style"),a.type="text/css",a.id="s"+d,(h.fake?h:p).appendChild(a),h.appendChild(p),a.styleSheet?a.styleSheet.cssText=e:a.appendChild(n.createTextNode(e)),p.id=d,h.fake&&(h.style.background="",h.style.overflow="hidden",c=u.style.overflow,u.style.overflow="hidden",u.appendChild(h)),l=t(p,e),h.fake?(h.parentNode.removeChild(h),u.style.overflow=c,u.offsetHeight):p.parentNode.removeChild(p),!!l}var f=[],c={_version:"3.2.0",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(e,n){var t=this;setTimeout(function(){n(t[e])},0)},addTest:function(e,n,t){f.push({name:e,fn:n,options:t})},addAsyncTest:function(e){f.push({name:null,fn:e})}},Modernizr=function(){};Modernizr.prototype=c,Modernizr=new Modernizr;var d=[],u=n.documentElement,p="svg"===u.nodeName.toLowerCase(),h=c._config.usePrefixes?" -webkit- -moz- -o- -ms- ".split(" "):[];c._prefixes=h;var m=c.testStyles=l;Modernizr.addTest("touchevents",function(){var t;if("ontouchstart"in e||e.DocumentTouch&&n instanceof DocumentTouch)t=!0;else{var o=["@media (",h.join("touch-enabled),("),"heartz",")","{#modernizr{top:9px;position:absolute}}"].join("");m(o,function(e){t=9===e.offsetTop})}return t}),s(),a(d),delete c.addTest,delete c.addAsyncTest;for(var v=0;v<Modernizr._q.length;v++)Modernizr._q[v]();e.Modernizr=Modernizr}(window,document);

/* global jQuery:true */

'use strict';

var Ssl = function () {
  this.init();
};


;(function (Ssl, $) {
  Ssl.prototype = {
    init : function () {
      var _ = this;

      // Debug
      _.debug = true;

      // Variables
      _.registerResizeEvents = [];

      _.$window = $(window);

      // Start functions
      _.mediaQueryListener();

      _.search();
      _.navigation();

      _.eventHandler();
    }
  };
})(Ssl || {}, jQuery);

/* global Ssl:true, jQuery:true */

'use strict';

;(function (Ssl, $) {
  /**
   * [event description]
   * @method events
   * @return {[type]} [description]
   */
  Ssl.prototype.eventHandler = function () {
    var _ = this,
        debounceResize;

    debounceResize = _.debounce(function () {
      for (var i = 0, l = _.registerResizeEvents.length; i < l; i++) {
        var f = _.registerResizeEvents[i].split('.');

        if (f.length === 1) {
          _[f[0]]();
        } else if (f.length === 2) {
          _[f[0]][f[1]]();
        }
      }
    }, 50);

    _.$window.on('resize orientationchange load', debounceResize);

    return this;
  };
})(Ssl || {}, jQuery);

/* global Ssl:true, jQuery:true */

'use strict';

;(function (Ssl, $) {
  /**
   * [debounce description]
   * @method debounce
   * @param  {[type]} func      [description]
   * @param  {[type]} wait      [description]
   * @param  {[type]} immediate [description]
   * @return {[type]}           [description]
   */
  Ssl.prototype.debounce = function (func, wait, immediate) {
    var timeout;
    return function () {
      var context = this, args = arguments;
      var later = function() {
        timeout = null;
        if (!immediate) {
          func.apply(context, args);
        }
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) {
        func.apply(context, args);
      }
    };
  };
})(Ssl || {}, jQuery);

/* global Ssl:true, jQuery:true */

'use strict';

;(function (Ssl, $) {
  /**
   * [mediaQueryListener description]
   * @method mediaQueryListener
   * @return {[type]}           [description]
   */
  Ssl.prototype.mediaQueryListener = function () {
    var _ = this;

    _.registerResizeEvents.push('breakpointTrigger');

    _.beforeElement = window.getComputedStyle ? window.getComputedStyle(document.body, '::before') : false;
    _.currentBreakpoint = '';
    _.lastBreakpoint = '';

    // Listen for media query changes
    _.$window.on('breakpoint-change', function (e, bp) {
      if (_.debug) {
        console.log(bp);
      }
    });

    if(!_.afterElement) {
      return;
    }
  };

  /**
   * [breakPointTrigger description]
   * @method breakPointTrigger
   * @return {[type]}          [description]
   */
  Ssl.prototype.breakpointTrigger = function () {
    var _ = this;

    // Regexp for removing quotes added by various browsers
    _.currentBreakpoint = _.beforeElement.getPropertyValue('content').replace(/^["']|["']$/g, '');

    if (_.currentBreakpoint !== _.lastBreakpoint) {
      _.$window.trigger('breakpoint-change', _.currentBreakpoint);
      _.lastBreakpoint = _.currentBreakpoint;
    }
  };
})(Ssl || {}, jQuery);

/* global Ssl:true, jQuery:true */

'use strict';

;(function (Ssl, $) {
  /**
   * [navigation description]
   * @method navigation
   * @return {[type]}   [description]
   */
  Ssl.prototype.navigation = function () {
      var _ = this,
          $nav = $('#main-nav'),
          $button = $nav.find('button'),
          $ul = $nav.find('ul').first(),
          $ulNew = $nav.find('ul').clone().addClass('extra-menu').appendTo($nav),
          $li = $ul.find('> li'),
          $liNew = $ulNew.find('li');

      $li.each(function () {
        $(this).attr('data-width', $(this).outerWidth(true));
      });

      _.registerResizeEvents.push('navigation.resize');

      // $ulNew.on('mouseenter mouseleave focus blur', function (e) {
      //   $nav.toggleClass('is-open', (e.type === 'mouseenter' || e.type === 'focus'));
      // });

      $button.on('click blur', function (e) {
        if (e.type === 'blur') {
          $nav.removeClass('is-open');
        } else {
          $nav.toggleClass('is-open');
        }
      });

      _.navigation.resize = function () {
        var navwidth = 48 + (_.currentBreakpoint === 'none' ? 48 : 0),
            availablespace =   $ul.outerWidth(true);

        $li.each(function () {
          navwidth += parseInt($(this).attr('data-width'), 10);
          $(this).toggleClass('is-hidden', (navwidth > availablespace));
        });

        var items =    $ul.find('li:not(.is-hidden)').length;
        $liNew.each(function (i) {
          if (i < items) {
            $(this).addClass('is-hidden');
          } else {
            $(this).removeClass('is-hidden');
          }
        });

        $nav.toggleClass('full', (navwidth < availablespace));
        $button.toggleClass('is-hidden', (navwidth < availablespace));

        return this;
      };

    return this;
  };
})(Ssl || {}, jQuery);

/* global Ssl:true, jQuery:true */

'use strict';

;(function (Ssl, $) {
  /**
   * [search description]
   * @method search
   * @return {[type]} [description]
   */
  Ssl.prototype.search = function () {
    var _ = this,
        $wrapper = $('#search-header'),
        $search = $('#search-field'),
        $button = $('#search-button'),
        $list = $('<ul></ul>'),
        $current = $list.find('li').first(),
        total = 0,
        debounce, open, close, goto;

    $search.wrap($('<div></div>').addClass('autocomplete'));
    $list.appendTo($search.parent());

    open = function () {
      $list.addClass('is-visible');
    };

    close = function () {
      window.setTimeout(function () {
        $list.removeClass('is-visible');
      }, 200);
    };

    goto = function ($item) {
      if ($item.attr('data-href') === undefined ) {
        $wrapper.find('form').submit();
      } else {
        window.location.href = $item.attr('data-href');
      }
    };

    debounce = _.debounce(function (val) {
      $.ajax({
        url: 'http://dev.2.skanskalandskap.se/umbraco/Surface/SearchSurface/CreateAutoSuggestions/',
        data: {
          language : 'SV',
          term : val,
          limit : 10
        },
        type: 'GET',
        dataType: 'json'
      })
      .success(function (result) {
        // Add extra test data right now
        var $ul = $('<ul></ul>'),
            $li;

        total = result.total;

        if (total) {
          for (var i = 0, l = result.result.length; i < l; i++) {
            $li = $('<li></li>').attr({
              'data-href' : result.result[i].Url
            }).html(
              '<span class="title">' + result.result[i].Title + '</span>' +
              '<span class="description">' + result.result[i].PreviewText + '</span>'
            ).appendTo($ul);
          }
          if (total > 10) {
            $li = $('<li></li>').addClass('more-results').attr({
              'data-href' : 'sokresultat.aspx?term=' + val
            }).html(
              '<span class="title">Fler resultat...</span>'
            ).appendTo($ul);
          }
          $list.html($ul.html());
          open();
        } else {
          close();
        }
      })
      .error(function (data) {
        // Do something?
      });
    }, 200);

    $search.on('focus blur', function (e) {
      if (e.type === 'blur' && total) {
        close();
      } else {
        open();
      }
    });

    $search.on('keydown', function (e) {
      // Prevent cursor jumping
      if (e.keyCode === 38 || e.keyCode === 40 || e.keyCode === 27) {
        return false;
      }
    });

    $search.on('keyup', function (e) {
      // Perform search
      var val = $search.val();
      if (val.length > 2) {
        if (e.keyCode !== 13 && e.keyCode !== 38 && e.keyCode !== 40 && e.keyCode !== 27) {
          debounce(val);
        }

        switch (e.keyCode) {
          // Up
          case 38:
            if (!$current.hasClass('is-selected')) {
              $current = $list.find('li').last().addClass('is-selected');
            } else {
              $current.removeClass('is-selected');
              if ($current.prev()[0]) {
                $current = $current.prev().addClass('is-selected');
              } else {
                $current = $list.find('li').last().addClass('is-selected');
              }
            }
          break;

          // Down
          case 40:
            if (!$current.hasClass('is-selected')) {
              $current = $list.find('li').first().addClass('is-selected');
            } else {
              $current.removeClass('is-selected');
              if ($current.next()[0]) {
                $current = $current.next().addClass('is-selected');
              } else {
                $current = $list.find('li').first().addClass('is-selected');
              }
            }
          break;

          // Enter
          case 13:
            goto($list.find('.is-selected'));
          break;

          // Escape
          case 27:
            close();
          break;
        }
      } else {
        close();
      }
    });

    $list.on('click', 'li', function () {
      goto($(this));
    });

    $button.on('click', function (e) {
      e.preventDefault();

      if ($wrapper.hasClass('is-open') && $search.val()) {
        $wrapper.find('form').submit();
      } else {
        $wrapper.toggleClass('is-open');
        if ($wrapper.hasClass('is-open')) {
          window.setTimeout(function () {
            $search.focus();
          }, 300);
        }
      }

      return false;
    });

    return this;
  };
})(Ssl || {}, jQuery);

/* global Ssl:true */

'use strict';

// Just kickstarts the js
;(function () {
  return new Ssl();
})();
