﻿
namespace SkanskaLandskap15.Models.Shared
{
    public class MapBasicDataModel
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int Zoom { get; set; }
        public string UrlName { get; set; }
        public bool AddMarkers { get; set; }
    }
}