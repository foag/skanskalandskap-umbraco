﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Controllers.SurfaceControllers;
using SkanskaLandskap15.Models;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Core.Models;
using Umbraco.Web;



namespace SkanskaLandskap15.Controllers
{
    public class CoworkersPageController : InteriorMasterController
    {

        public ActionResult CoworkersPage()
        {
            // Initiate the return model
            var model = new CoworkersPageModel();

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language ("SV" or "EN")
            var currentLanguage = helper.GetDictionaryValue("Language");


            /* PAGE PROPERTIES */

            // Title
            model.Title = CurrentPage.GetPropertyValue<String>(MyConstants.TitlePropertyAlias);

            // Sub title
            model.Subtitle = CurrentPage.GetPropertyValue<String>(MyConstants.subTitlePropertyAlias);


            /* CO-WORKER GROUPS */

            // Get the groups collection (Nested Content datatype)
            var groups = CurrentPage.GetPropertyValue<IEnumerable<IPublishedContent>>(MyConstants.coworkersGroupsModulePropertyAlias);

            // Initiate flag
            model.CoworkerGroupsExists = false;

            // Make sure at least one group exists
            if (groups != null)
            {
                // Create controller reference
                PuffsSurfaceController psc = new PuffsSurfaceController();

                // Initiate a temp list
                List<PuffModel> tempListGroupPuffs = new List<PuffModel>();

                // Iterate over the groups
                foreach (var item in groups)
                {
                    // Instanciate a Puff object
                    var objPuff = new PuffModel();

                    // Get group properties 
                    objPuff = psc.GetCoworkerGroupProperties(item, objPuff, helper, currentLanguage);

                    // Add group to temp list
                    tempListGroupPuffs.Add(objPuff);
                }

                // Make sure groups exists
                if (tempListGroupPuffs.Any())
                {
                    model.CoworkerGroupsExists = true;
                }

                // Add temp list to return model
                model.CoworkerGroupsCollection = tempListGroupPuffs;
            }           

            return View(model);
        }

    }
}