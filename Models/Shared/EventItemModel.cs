﻿using System.Collections.Generic;
using System.Web;


namespace SkanskaLandskap15.Models.Shared
{
    public class EventItemModel
    {
        public int NodeId { get; set; }
        public string Url { get; set; }
        public string UrlTitleTerm { get; set; }
        public string ContentType { get; set; }
        public string Title { get; set; }
        public string Intro { get; set; }
        public IHtmlString Text { get; set; }
        public dynamic EventImage { get; set; }
        public string ImageCaption { get; set; }
        public string Tags { get; set; }
               
        public string DateInfo { get; set; }
        public string TimeInfo { get; set; }
        public string LocationName { get; set; }
        public string LocationUrl { get; set; }        
        public string OutdatedInfo { get; set; }
            
        public List<EventItemModel> UpcomingEventsCollection { get; set; }
        public string NoEventsMessage { get; set; }
        public string GridClass { get; set; }

        public string DateTerm { get; set; }
        public string TimeTerm { get; set; }
        public string LocationTerm { get; set; }
        public string TitleTerm { get; set; }
        public string SeeMoreEventsTerm { get; set; }
        public string MoreEventsTerm { get; set; }
        public string EntireCalendarTerm { get; set; }
        
        public string EventsLandingPageUrl { get; set; }
        public string SitePart { get; set; }
    }
}
