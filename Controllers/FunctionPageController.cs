﻿using System;
using System.Web;
using System.Web.Mvc;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Models;
using Umbraco.Web;



namespace SkanskaLandskap15.Controllers
{
    public class FunctionPageController : InteriorMasterController
    {

        public ActionResult FunctionPage()
        {
            var model = new FunctionPageModel();
                      
            // Intro
            model.IntroInfo = CurrentPage.GetPropertyValue<String>(MyConstants.introInfoPropertyAlias);
            if (string.IsNullOrWhiteSpace(model.IntroInfo))
            {
                model.IntroInfo = String.Empty;
            }

            // Richtext content
            model.Text = CurrentPage.GetPropertyValue<IHtmlString>(MyConstants.textPropertyAlias);             
            
            return View(model);
        }
    }
}