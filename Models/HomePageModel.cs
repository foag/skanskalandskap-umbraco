﻿using SkanskaLandskap15.Models.Masters;
using SkanskaLandskap15.Models.Shared;
using System.Collections.Generic;


namespace SkanskaLandskap15.Models
{

    // ENGLISH START PAGE MODEL

    public class HomePageModel : StartMasterModel
    {
        public bool TipsExists { get; set; }
        public string TipsTitle { get; set; }
        public string ReadMoreTerm { get; set; }
        public string MoreTipsTerm { get; set; }
        public string MoreTipsUrl { get; set; }
        public bool FixedPuffsExists { get; set; }
        public List<PuffModel> FixedPuffsCollection { get; set; }
        public SubPagesListModel SubPages { get; set; }
        public string SelectedNodes { get; set; }
        public PuffsListModel PuffsList { get; set; }
    }
}