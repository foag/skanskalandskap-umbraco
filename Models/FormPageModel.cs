﻿using System.Web;
using SkanskaLandskap15.Models.Masters;
using SkanskaLandskap15.Models.Shared;


namespace SkanskaLandskap15.Models
{
    public class FormPageModel : InteriorMasterModel
    {
        public IHtmlString Text { get; set; }
        public FormModel UmbracoForm { get; set; }
    }
}