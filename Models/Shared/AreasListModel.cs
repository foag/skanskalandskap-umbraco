﻿using System.Collections.Generic;

namespace SkanskaLandskap15.Models.Shared
{
    public class AreasListModel
    {
        public List<AreaModel> AreasCollection { get; set; }
    }
}