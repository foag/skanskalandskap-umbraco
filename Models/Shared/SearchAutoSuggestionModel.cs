﻿
namespace SkanskaLandskap15.Models.Shared
{
    public class SearchAutoSuggestionModel 
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public string PreviewText { get; set; }       
    }
}