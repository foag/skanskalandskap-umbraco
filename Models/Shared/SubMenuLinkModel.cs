﻿
namespace SkanskaLandskap15.Models.Shared
{
    public class SubMenuLinkModel
    {
        public int NodeId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string CategoryId { get; set; }
    }
}