var map;
var zoom;
var latitude;
var longitude;
var mapActive = false;

function initialize() {

    if(mapActive){       

        var latlng;

        latlng = new google.maps.LatLng(latitude, longitude);

        var myOptions = {
            zoom: zoom,
            center: latlng,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map"), myOptions);

        addMarker(latlng);

    }   
}

function addMarker(location) {

   marker = new google.maps.Marker({
            position: location,
            draggable: false,
            map: map
        });     
}


window.onload = function () {
    initialize();
}