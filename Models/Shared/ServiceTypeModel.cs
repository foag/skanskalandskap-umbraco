﻿
namespace SkanskaLandskap15.Models.Shared
{
    public class ServiceTypeModel : MapInteractivityTypeModel
    {        
        public string UrlName { get; set; }
        public string NodeName { get; set; }
        public string Title { get; set; }
        public dynamic MapIcon { get; set; }
        public string ServiceType { get; set; }
    }
}
