﻿using System.Web;
using SkanskaLandskap15.Models.Masters;
using SkanskaLandskap15.Models.Shared;


namespace SkanskaLandskap15.Models
{
    public class GeneralSubPageModel : InteriorMasterModel
    {       
        public SlideshowModel Slideshow { get; set; }
        public bool SlideshowExists { get; set; }

        public IHtmlString Text { get; set; }

        public FormModel UmbracoForm { get; set; }
        public string TitleForm { get; set; }
        public IHtmlString IntroForm { get; set; }

        public string PuffsAndDownloadsTitle { get; set; }
        public bool PuffsExists { get; set; }
        public string TitlePuffs { get; set; }
        public dynamic PuffModule { get; set; }
               
        public bool DownloadsExists { get; set; }
        public MediaFilesModel MediaFiles { get; set; }
        
        public bool AreasTipsExists { get; set; }
        public string AreasTipsTitle { get; set; }
        public SubPagesListModel SubPagesAreas { get; set; }         

        public string SubPageTag { get; set; }
        public SubPagesListModel SubPages { get; set; }

        public bool AccommodationTipsExists { get; set; }
        public string AccommodationsTipsTitle { get; set; }
        public SubPagesListModel SubPagesAccommodations { get; set; }

        public string MoreAboutTerm { get; set; }
    }
}