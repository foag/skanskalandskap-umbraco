﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace SkanskaLandskap15.Classes.Utilities
{

    public static class StringExtensions
    {
        public static bool DetectIsJson(this string input)
        {
            input = input.Trim();
            if (input.StartsWith("{") && input.EndsWith("}"))
                return true;
            if (input.StartsWith("["))
                return input.EndsWith("]");
            else
                return false;
        }
    }
}