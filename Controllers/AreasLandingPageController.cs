﻿using System.Web.Mvc;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Controllers.Masters;
using SkanskaLandskap15.Controllers.SurfaceControllers;
using SkanskaLandskap15.Models;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Web;



namespace SkanskaLandskap15.Controllers
{
    public class AreasLandingPageController : InteractiveMasterController
    {

        public ActionResult AreasLandingPage()
        {
            // Initiate the return model
            var model = new AreasLandingPageModel();

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Get current language ("SV" or "EN")
            var currentLanguage = helper.GetDictionaryValue("Language");
            
            // Assign the content type
            model.ContentType = CurrentPage.DocumentTypeAlias;

            // Set the map element's id            
            model.MapType = "google_map"; 

            // Add an empty InteractivePanel object to the return model
            model.MapInteractivity = new MapInteractivityModel();

            // Create a controller reference
            AreasSurfaceController asc = new AreasSurfaceController();

            // Get a list of all areas 
            model.AreasList = asc.GetAreasList(currentLanguage, false, CurrentPage.DocumentTypeAlias);

            // Get some Dictionary terms for the interactive map (in small devices)
            model.MapLinkTerm = helper.GetDictionaryValue("MapLinkTitle");
            model.ListLinkTerm = helper.GetDictionaryValue("ListLinkTitle");
            model.FilteringLinkTerm = helper.GetDictionaryValue("FilteringLinkTitle");
            model.ShowAreasLinkTerm = helper.GetDictionaryValue("ShowAreasLinkTitle");

            // Get title and intro for the activities filtering dialog (in small devices)
            model.FilteringTitleTerm = helper.GetDictionaryValue("MobileFilteringActivitiesTitle");
            model.FilteringIntroTerm = helper.GetDictionaryValue("MobileFilteringActivitiesIntro");
                                           
            return View(model);
        }
    }
}