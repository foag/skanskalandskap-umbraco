﻿using System;
using System.Web.Mvc;
using SkanskaLandskap15.Classes.Utilities;
using SkanskaLandskap15.Models.Masters;
using SkanskaLandskap15.Models.Shared;
using Umbraco.Web;


namespace SkanskaLandskap15.Controllers.Masters
{
    public class InteriorMasterController : MasterController
    {

        protected ViewResult View(InteriorMasterModel model)
        {
            return this.View(null, model);
        }


        protected ViewResult View(string view, InteriorMasterModel model)
        {

            // Get an Umbraco helper
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Title is always mandatory, subtitle and intro might not be 
            model.Title = CurrentPage.GetPropertyValue<string>(MyConstants.TitlePropertyAlias);

            model.Subtitle = CurrentPage.GetPropertyValue<string>(MyConstants.SubtitlePropertyAlias);
            if (string.IsNullOrWhiteSpace(model.Subtitle))
            {
                model.Subtitle = String.Empty;
            }

            model.Intro = CurrentPage.GetPropertyValue<string>(MyConstants.IntroPropertyAlias);
            if (string.IsNullOrWhiteSpace(model.Intro))
            {
                model.Intro = String.Empty;
            }


            /*** GUEST RATINGS ***/

            // Guest rating terms - for accommodation page only
            if (CurrentPage.DocumentTypeAlias == MyConstants.AccommodationPageDocTypeAlias)
            {
                // Create a new GuestRatings object
                GuestRatingsModel grModel = new GuestRatingsModel();

                // Get some Dictionary items aimed for the javascript part of the Guest Ratings function
                grModel.ConfirmMessage = helper.GetDictionaryValue("ConfirmMessage");
                grModel.ThankYouMessage = helper.GetDictionaryValue("ThankYouMessage");
                grModel.OnlyOnceMessage = helper.GetDictionaryValue("OnlyOnceMessage");
                grModel.SeeResults = helper.GetDictionaryValue("SeeResults");

                // Add the GuestRatings object to the return model
                model.GuestRatingsTerms = grModel;
            }


            /*** SUB MENU ***/

            //// Get reference to controller
            //NavigationSurfaceController nsc = new NavigationSurfaceController();

            //// Get sub menu
            //model.SubNavigation = nsc.GetSubMenu(CurrentPage);

            return base.View(view, model);
        }     
    }
}