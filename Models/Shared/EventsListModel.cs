﻿using System;
using System.Collections.Generic;


namespace SkanskaLandskap15.Models.Shared
{
    public class EventsListModel
    {
        public List<EventItemModel> EventItemsCollection { get; set; }
        public Int32 PageNumber { get; set; }
        public Int32 RecordsPerPage { get; set; }
        public bool LoadMore { get; set; }
        public string LoadMoreInfo { get; set; }
        public string NoEventsMessage { get; set; }
    }
}
