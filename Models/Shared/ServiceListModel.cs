﻿using System.Collections.Generic;

namespace SkanskaLandskap15.Models.Shared
{
    public class ServiceListModel
    {
        public bool TrafiklabAvailable { get; set; }
        public List<ServiceModel> ServiceCollection { get; set; }  
    }
}
